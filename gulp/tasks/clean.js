/**
 * Gulp-task. Clean build directory.
 *
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   copyright © 2016-2017 delphinpro
 * @license     licensed under the MIT license
 */

const fs   = require('fs');
const path = require('path');
const del  = require('del');

const conf = require('../../gulp.config.js');

function scanDir(dir) {
  let result = [];
  let nodes  = fs.readdirSync(path.resolve(dir));

  nodes.forEach(node => {
    let name = path.join(dir, node);

    result.push(name);

    if (fs.statSync(name).isDirectory()) {
      result = [...result, ...scanDir(name)];
    }
  });

  return result;
}

function normalizePathExcludes(excludes, root) {
  return typeof excludes === 'object'
      ? excludes.map(item => path.join(root, item))
      : [];
}

String.prototype.startOf = function(substring) {
  if (typeof substring !== 'string' || substring === '') return false;

  return (this.indexOf(substring) === 0);
};

String.prototype.shortPath = function() {
  let re = new RegExp('\\' + path.sep, 'g');
  return this.replace(conf.root.build, '').replace(re, '/');
};

const LINE_WIDTH = 70;

const LINE_H     = '─'; // 2500
const LINE_V     = '│'; // 2502
const LINE_TR    = '┐'; // 2510
const LINE_BR    = '┘'; // 2518
const LINE_TL    = '┌'; // 250C
const LINE_BL    = '└'; // 2514
const LINE_RIGHT = '┤'; // 2524
const LINE_LEFT  = '├'; // 251C
// const LINE_TOP    = '┬'; // 252C
// const LINE_BOTTOM = '┴'; // 2534
// const LINE_CROSS  = '┼'; // 253C

/*
 const D_LINE_H      = '═'; // 2550
 const D_LINE_V      = '║'; // 2551
 const D_LINE_CROSS  = '┼'; //
 const D_LINE_TR     = '┐'; //
 const D_LINE_BR     = '┘'; //
 const D_LINE_TL     = '╔'; // 2554
 const D_LINE_BL     = '└'; //
 const D_LINE_RIGHT  = '┤'; //
 const D_LINE_LEFT   = '├'; //
 const D_LINE_TOP    = '┬'; //
 const D_LINE_BOTTOM = '┴'; //
 */

/*
 const LINE_RIGHT_DOUBLE = '╡'; // 2561
 const DOUBLE_LINE_H            = '╢'; // 2562
 const DOUBLE_LINE_V            =
 const DOUBLE_LINE_RIGTH_SINGLE = '╢'; // 2562
 ╖2556
 ╕2555
 ╣2563
 ╗2557
 ╝255D
 ╜255C
 ╛255B
 ╞255E
 ╟255F
 ╚255A

 ╩2569
 ╦2566
 ╠2560

 ╬256C
 ╧2567
 ╨2568
 ╤2564
 ╥2565
 ╙2559
 ╘2558
 ╒2552
 ╓2553
 ╫256B
 ╪256A
 */

function printList(caption, list, isPath = false) {
  console.log(LINE_TL.padEnd(LINE_WIDTH, LINE_H) + LINE_TR);
  console.info(`${LINE_V} ${caption}`.padEnd(LINE_WIDTH) + LINE_V);
  console.log(LINE_LEFT.padEnd(LINE_WIDTH, LINE_H) + LINE_RIGHT);
  list.forEach(item => console.info(`${LINE_V} ${isPath ? item.shortPath() : item}`.padEnd(LINE_WIDTH) + LINE_V));
  console.log(LINE_BL.padEnd(LINE_WIDTH, LINE_H) + LINE_BR);
}

module.exports = function(options, args) {
  return function(done) {

    if (!fs.existsSync(options.root.build)) {
      console.log('Clean task: Nothing to delete.');
      done();
    }

    let originalExcludes = normalizePathExcludes(options.cleaning.exclude, options.root.build);
    printList('Original excludes:', originalExcludes, true);

    let fsObjects = scanDir(options.root.build);
    // printList('File system objects:', fsObjects, true);

    for (let i = 0, len = fsObjects.length; i < len; i++) { }

    let tmp = fsObjects.filter(item => {
      // if (12 > index || index > 15) return false;
      // console.log('-----------');
      // console.log(`item > (${index})`, item);
      let keep = true;
      for (let i = 0, len = originalExcludes.length; i < len; i++) {
        let origin = originalExcludes[i];
        // console.log('');

        if (item.startOf(origin)) {
          keep = false;
        }

        let lastIndex = 0;
        while (lastIndex !== -1 && origin !== options.root.build) {
          let test = (item === origin);
          // console.log('test >', test, origin);
          if (test) keep = false;
          lastIndex = origin.lastIndexOf(path.sep);
          origin    = origin.substring(0, lastIndex);
        }
      }

      // console.log(keep ? 'DELETE' : 'EXCLUDE');

      return keep;
    });

    let parent  = '';
    let targets = tmp.filter(item => {
      if (!parent) {
        parent = item;
      }

      // console.log(
      //     'parent >',
      //     (item !== parent && item.startOf(parent)) ? '[+]' : '[-]',
      //     parent.replace('public_html', '').replace(/\\/g, '/'),
      //     '<=>',
      //     item.replace('public_html', '').replace(/\\/g, '/'),
      // );

      if (item !== parent && item.startOf(parent)) {
        return false;
      } else {
        parent = item;
      }

      return true;
    });

    printList('Targets:', targets, true);

    if (!(typeof args === 'object' && args.preview)) {
      // noinspection JSUnresolvedFunction
      del(targets).then(paths => {
        console.warn(`Removed: ${paths.length} item${paths.length !== 1 ? 's' : ''}`);
        done();
      });
    }
  };
};
