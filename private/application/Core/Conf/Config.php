<?php
/**
 * @author      delphinpro <delphinpro@gmail.com>
 * @author      Kirill Popov https://github.com/Wangoviridans
 * @link        https://github.com/Wangoviridans/php-config-class
 * @license     licensed under the MIT license
 */

namespace WofhTools\Core\Conf;

/**
 * Class Config
 * @package WofhTools\Core\Conf
 */
class Config extends Nested
{
    const CONFIG_FILE = 'application.json';
    const ENV_FILE = 'env.json';
    const ENV_DEFAULT = 'production';

    /** @var Config */
    private static $instance;

    /** @var array */
    protected $container;

    /** @var string */
    private $filename;

    /** @var string */
    private $configDirectory;

    /** @var string */
    private $env;

    /**
     * Config constructor.
     *
     * @param string $configDirectory
     */
    private function __construct($configDirectory)
    {
        $this->configDirectory = $configDirectory;
    }

    /**
     * @param string $configDirectory
     */
    public static function load($configDirectory)
    {
        self::$instance = new self($configDirectory);
        self::$instance->readEnvironment();
        self::$instance->readConfig();
    }

    /**
     *
     */
    private function readEnvironment()
    {
        $envFile = $this->configDirectory . DIRECTORY_SEPARATOR . self::ENV_FILE;
        $this->env = self::ENV_DEFAULT;

        if (is_file($envFile) && is_readable($envFile)) {
            $jsonString = file_get_contents($envFile);
            $jsonObject = json_decode($jsonString, true);

            if (json_last_error() == JSON_ERROR_NONE) {
                if (array_key_exists('env', $jsonObject)) {
                    $configFile = $this->configDirectory . DIRECTORY_SEPARATOR
                        . $jsonObject['env'] . DIRECTORY_SEPARATOR
                        . self::CONFIG_FILE;

                    if (is_file($configFile) && is_readable($configFile)) {
                        $this->env = $jsonObject['env'];
                    }
                }
            }
        }

        if ($this->env === self::ENV_DEFAULT) {
            $configFile = $this->configDirectory . DIRECTORY_SEPARATOR
                . $this->env . DIRECTORY_SEPARATOR
                . self::CONFIG_FILE;

            if (!is_file($configFile) || !is_readable($configFile)) {
                self::fatalError('Config file not exists');
            }
        }
    }

    /**
     * @param string $message
     */
    private static function fatalError($message)
    {
        header('Content-Type: text/plain');
        echo "FATAL ERROR \n\n";
        echo $message . "\n\n";
        debug_print_backtrace();
        exit(1);
    }

    /**
     *
     */
    private function readConfig()
    {
        $this->filename = $this->configDirectory . DIRECTORY_SEPARATOR
            . $this->env . DIRECTORY_SEPARATOR
            . self::CONFIG_FILE;

        $container = json_decode(file_get_contents($this->filename), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            self::fatalError("Can't read config: " . json_last_error_msg());
        }

        $this->container = $container;

    }

    /**
     * @return Config
     */
    public static function Inst()
    {
        if (!is_object(self::$instance)) {
            self::fatalError(
                'No instance of ' . __CLASS__ . "\n" .
                'Call method ' . __CLASS__ . '::load()'
            );
        }

        return self::$instance;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options)
    {
        foreach ($options as $option => $value) {
            $this->set($option, $value);
        }

        return $this;
    }

    /**
     * @param string $option
     * @param mixed  $value
     *
     * @return $this
     */
    public function set($option, $value)
    {
        self::setNestedOption($this->container, $option, $value);

        return $this;
    }

    /**
     * @param array $options
     *
     * @return array
     */
    public function getOptions(array $options)
    {
        $result = array();
        foreach ($options as $option => $default) {
            if (is_numeric($option)) {
                $option = $default;
                $default = null;
            }
            $result[] = $this->get($option, $default);
        }

        return $result;
    }

    /**
     * @param string     $option
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function get($option, $default = null)
    {
        return self::getNestedOption($this->container, $option, $default);
    }

    /**
     * @param array $options
     */
    public function unsetOptions(array $options)
    {
        foreach ($options as $option) {
            $this->remove($option);
        }
    }

    /**
     * @param $option
     */
    public function remove($option)
    {
        self::removeNestedOption($this->container, $option);
    }

    /**
     * @param $option
     *
     * @return bool
     */
    public function has($option)
    {
        return self::hasNestedOption($this->container, $option);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->container;
    }

    public function save()
    {
        file_put_contents($this->filename, json_encode($this->container));
    }

    public function getStaticDomain($scheme = '//')
    {
        if (!in_array($scheme, array('//', 'http://', 'https://'))) {
            $scheme = '//';
        }

        return $scheme . $this->get('app.staticName') . '.' . $this->get('app.domain');
    }

    public function getStaticDir($root = '')
    {
        $path = $root . str_replace('/', DIRECTORY_SEPARATOR, $this->get('app.staticPath'));
        if (is_dir($root)) {
            $path = realpath($path);
        }

        return $path;
    }

    public function showDebug()
    {
        if (PHP_SAPI == 'cli') {
            return false;
        }

        return $this->get('app.debug')
            && $this->get('app.allowed_ip') == $_SERVER['REMOTE_ADDR'];
    }
}
