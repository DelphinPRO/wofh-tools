<?php
/**
 * @since             22.11.2014 20:59
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

if (PHP_SAPI != 'cli') {
	exit;
}

require_once __DIR__ . '/app/vendor/JSMin.php';

$jsAppFiles = glob(__DIR__ . '/public_html/js/app/*.js');

$content   = array();
$content[] = file_get_contents(__DIR__ . '/public_html/js/app/main.js');

foreach ($jsAppFiles as $file) {
	if (basename($file) == 'main.js') {
		continue;
	}

	$content[] = file_get_contents($file);
}

$all         = join(";\n\n", $content);
$allMinified = JSMin::minify($all);

file_put_contents(__DIR__ . '/public_html/js/app.js', $allMinified);

//////////////////
$content    = array();
$jsAppFiles = array(
	"/public_html/js/lib/bootstrap/dropdown.js",
	"/public_html/js/lib/bootstrap/tab.js",
	"/public_html/js/lib/bootstrap/tooltip.js",
	"/public_html/js/lib/bootstrap/collapse.js",
);
foreach ($jsAppFiles as $file) {
	$content[] = file_get_contents(__DIR__ . $file);
}
$all         = join(";\n\n", $content);
$allMinified = JSMin::minify($all);

file_put_contents(__DIR__ . '/public_html/js/bootstrap.js', $allMinified);
//\\\\\\\\\\\\\\\\\\

echo 'OK';