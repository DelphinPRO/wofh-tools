/**
 * Configuration
 *
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   copyright © 2017 delphinpro
 * @license     licensed under the MIT license
 */

'use strict';
const path = require('path');

/*==
 *== Main settings
 *== ===================================================================== ==*/

const _serverPort = 3000;
const _useProxy = true;
const _localDomain = 'wofh-tools.project';
const _browsers = ['chrome'];
const _reloadDebounce = 300;
const root = {
  src      : 'source',
  build    : 'public_html',
  dist     : 'dist',
  staticDir: 'static',
};

let config = {
  root,
  sprite: {},
};

/*==
 *== Task lists
 *== ===================================================================== ==*/

config.watchableTasks = [
//  'webpack',
  'scss',
];

config.defaultTasks = [
//  ['clean'],
  ['sprite'],
//  ['webpack:vendor'],
  [
//    'webpack',
    'scss',
  ],
];

config.shortListTasks = [[/*'webpack',*/ 'scss']];

/*==
 *== Sass build settings
 *== ===================================================================== ==*/

config.scss = {
  src       : 'sass',
  build     : path.join(root.staticDir, 'css'),
  sass      : {outputStyle: 'nested'},
  extensions: ['scss'],
  resolver  : {
    source     : '/' + root.staticDir + '/images/',
    replacement: '../images/',
  },
};

/*==
 *== Autoprefixer settings
 *== ===================================================================== ==*/

config.autoprefixer = {
  browsers: ['last 2 versions', 'not ie < 11'],
};

/*==
 *== Main javascript build settings
 *== ===================================================================== ==*/

config.javascript = {
  processor: 'webpack',
};

/*==
 *== Javascript processing of webpack
 *== ===================================================================== ==*/

config.webpack = {
  src       : 'js',
  build     : path.join(root.staticDir, 'js'),
  entryExt  : 'js',
  extensions: ['js', 'scss', 'css', 'vue'],
};

/*==
 *== Sprite generation settings
 *== ===================================================================== ==*/

config.sprite.svg = {
  src : 'source/svg/*.svg',
  dest: '../svg-sprite.svg',
};

/*==
 *== Cleaning settings
 *== Директории без оконечных слешей, слеш прямой
 *== Одиночные файлы
 *== ===================================================================== ==*/

config.cleaning = {
  exclude: [],
};

/*==
 *== BrowserSync settings
 *== http://www.browsersync.io/docs/options/
 *== ===================================================================== ==*/

config.browserSync = {
  watchOptions   : {ignoreInitial: true},
  // files          : ['' + root.build + '**/*.*', '!' + root.build + '**/*.map'],
  browser        : _browsers,
  notify         : true,
  startPath      : '/',
  proxy          : _localDomain,
  port           : _serverPort,
  reloadDebounce : _reloadDebounce,
  reloadOnRestart: true,
  ghostMode      : {
    clicks: true,
    forms : true,
    scroll: true,
  },
};

config.bs = {
  instance: 'delphinpro',
};

if (!_useProxy) {
  config.browserSync.proxy = null;
  config.browserSync.server = {
    baseDir  : root.build,
    directory: true,
  };
}

module.exports = config;
