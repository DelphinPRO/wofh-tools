<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Bad browser</title>
	<style type="text/css">
		*{margin:0;padding:0;}
		body,html{height:100%;width:100%;}
		body{text-align:center;max-width:700px;margin:0 auto;color:#363636;background: #fcfcfc;
			font:62.5% "Segoe UI", Tahoma, Arial, sans-serif;}
		.body-wrapper{width:100%;height:100%;text-align:center;}
		.block{display:inline-block;vertical-align:middle;position:relative;padding:25px;
			border:1px solid #dedede;box-shadow:0 0 10px -5px #000;border-radius:10px;background:#ffffff;}
		.helper{display:inline-block;height:100%;width:0;vertical-align:middle;}
		img{border:0}
		a{color:#39a3e7;}
		a:hover{color:#b32317;}
		h1 {font:2.4em Arial,Helvetica,sans-serif;margin-bottom:1.05em;}
		p{margin:0.5em 0 0;font-size:1.5em;}
		.bb{margin:1.5em 0 0;}
		.bb a{display:inline-block;margin:0 20px;text-align:center;}
		.bb img{margin:auto;display:block;}
	</style>
</head>
<body>
<div class="body-wrapper">
	<div class="block">
		<? if (isset( $_GET['noscript'] )) { ?>
		<h1>Отключен JavaScript.</h1>

		<p>В вашем браузере отключен javascript.</p>
		<p>Функционирование нашего сайта в этом случае невозможно.</p>
		<p>Рекомендуем вам включить javascript в браузере.</p>
		<? } else { ?>
		<h1>Несовместимое ПО.</h1>

		<p>Вы используете устаревший браузер.</p>
		<p>Функционирование нашего сайта в нем невозможно.</p>
		<p>Рекомендуем вам обновить версию или скачать более современный браузер.</p>
		<p class="bb">
			<a href="http://www.mozilla.org/ru/firefox/new/" target="_blank" rel="nofollow"><img src="/design/img/bb/firefox.png" alt="FireFox">FireFox</a>
			<a href="http://www.google.com/chrome/" target="_blank" rel="nofollow"><img src="/design/img/bb/chrome.png" alt="Chrome">Chrome</a>
			<a href="http://www.opera.com/" target="_blank" rel="nofollow"><img src="/design/img/bb/opera.png" alt="Opera">Opera</a>
			<a href="http://windows.microsoft.com/en-us/internet-explorer/ie-10-worldwide-languages" target="_blank" rel="nofollow"><img src="/design/img/bb/ie_10.png" alt="Internet Explorer 10">Internet Explorer 10</a>
		</p>
		<? } ?>
	</div>
	<div class="helper"></div>
</div>
</body>
</html>


