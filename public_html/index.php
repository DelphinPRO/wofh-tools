<?php

/**
 * Application entry point.
 *
 * @since              11.12.‎12 ‏‎10:40
 * @package            DFramefork
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

if (version_compare(PHP_VERSION, "5.3.0") < 0) {
	header('Content-Type: text/html; charset=utf-8');
	die( "<h1>Need version PHP 5.3.0 or higher :(</h1><p>Your version " . PHP_VERSION . "</p>" );
}

define( 'DIR_PUBLIC', dirname(__FILE__) );
define( 'DIR_ROOT', realpath(DIR_PUBLIC . '/../') );
define( 'DIR_APP', DIR_ROOT . DIRECTORY_SEPARATOR . 'app' );
define( 'DIR_PRIVATE', DIR_ROOT . DIRECTORY_SEPARATOR . 'private' );

$_PROFILER_TIME_START = microtime(true);
$_ERROR_STACK         = array();
$_ERROR_BUFFERING     = true;

$bootstrapFile = realpath(DIR_APP . '/df/bootstrap/bootstrap.php');

if (!is_file($bootstrapFile)) {
	die( '<title>Bootstrap file not found</title>
	Fatal error: Bootstrap file not found.<br/>
	Check whether the installation.' );
}

if (isset($_GET['w'],$_GET['f'])) {
	include __DIR__ . DIRECTORY_SEPARATOR . 'flag.php';
	exit;
}

include DIR_ROOT . '/vendor/autoload.php';
include DIR_ROOT . '/private/libraries/AltoRouter.php';
include $bootstrapFile;
