<?php
/**
 * @since              07.01.14 18:36
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

use df\helper\FS;
use WofhTools\Core\Conf\Config;

defined('DIR_ROOT') or define('DIR_ROOT', realpath('../'));
defined('DIR_APP') or define('DIR_APP', DIR_ROOT . DIRECTORY_SEPARATOR . 'app');
defined('DIR_PRIVATE') or define('DIR_PRIVATE', DIR_ROOT . DIRECTORY_SEPARATOR . 'private');
require_once DIR_ROOT . "/app/vendor/class.upload.php";
require_once DIR_ROOT . "/app/df/helper/FS.php";
require_once DIR_PRIVATE . "/application/Core/Conf/Nested.php";
require_once DIR_PRIVATE . "/application/Core/Conf/Config.php";

Config::load(DIR_PRIVATE . DIRECTORY_SEPARATOR . 'config');
$config = Config::Inst();

//FS::path();
function get_remote_file($url, $referrer)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($ch, CURLOPT_FAILONERROR, true);
  curl_setopt($ch, CURLOPT_COOKIEFILE, '');
  curl_setopt($ch, CURLOPT_REFERER, $referrer);
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $data = curl_exec($ch);
  curl_close($ch);

  return $data;
}

function error($message = '')
{
  if (isset($_GET['debug']) && $_GET['debug'] == 1) {
    echo $message;
    exit;
  }
  header("Content-Type: image/png");
  echo file_get_contents(FS::path('/public_html/img/no-flag-30.png'));
  exit;
}

$sign = isset($_GET['w']) && preg_match('/(ru|en|int)[0-9]+[st]*/', $_GET['w']) ? $_GET['w'] : '__';
$flag = isset($_GET['f']) && preg_match('/[a-z0-9]+/', $_GET['f']) ? $_GET['f'] : 'flag';
$size = isset($_GET['s']) && abs((int)$_GET['s']) <= 300 ? abs((int)$_GET['s']) : '30';

if (!preg_match('`^(ru|en|int)(\d+)(s|t*)$`Usi', $sign, $m)) {
  error('Invalid sign');
}

//$relFlagsDirectory = "/images/worlds/$sign/flags";
$staticDir = $config->getStaticDir(DIR_ROOT . DIRECTORY_SEPARATOR);
$_relFlagsDirectory = "/uploads/worlds/$sign/flags";
$absFlagsDirectory = $staticDir . str_replace('/', DIRECTORY_SEPARATOR, $_relFlagsDirectory);
//die($absFlagsDirectory);

$chunks = explode('/', trim($_relFlagsDirectory, '/'));
try {
  $subDir = '';
  foreach ($chunks as $chunk) {
    $subDir .= DIRECTORY_SEPARATOR . $chunk;
    if (!is_dir($staticDir . $subDir)) {
      if (!mkdir($staticDir . $subDir, 0777)) {
        throw new Exception();
      }
      chmod($staticDir . $subDir, 0777);
    }
  }
} catch (\Exception $e) {
  error($e->getMessage());
}

if (file_exists($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_' . $size . '.gif')) {
  header("Content-Type: image/gif");
  echo file_get_contents($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_' . $size . '.gif');
  exit;
}

if (file_exists($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_300.gif') && ($size != 300)) {
  $handle = new upload($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_300.gif');
  $handle->image_resize = true;
  $handle->image_ratio_y = true;
  $handle->image_x = $size;
  $handle->file_new_name_body = $flag . "_" . $size;
  $handle->file_auto_rename = false;
  $handle->dir_auto_create = true;
  $handle->no_upload_check = true;
  $handle->process($absFlagsDirectory);
  if ($handle->processed) {
    header("Content-Type: image/gif");
    chmod($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_' . $size . '.gif', 0777);
    echo file_get_contents($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_' . $size . '.gif');
    exit;
  }
}

$country = $m[1];
$worldNum = (int)$m[2];
$flagUrl = "/gen/flag/$flag.gif";

switch ($country) {
  case 'ru':
    $serverBaseURL = "https://ru{$worldNum}{$m[3]}.waysofhistory.com";
    break;
  case 'en':
    $serverBaseURL = "https://en{$worldNum}.waysofhistory.com";
    break;
  case 'int':
    $serverBaseURL = "https://int{$worldNum}.waysofhistory.com";
    break;
  default:
    error();
}

$bin = get_remote_file("{$serverBaseURL}{$flagUrl}", "{$serverBaseURL}/");

if ($bin !== false) {
  file_put_contents($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_300.gif', $bin);
  chmod($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_300.gif', 0777);

  if ($size != 300) {
    $handle = new upload($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_300.gif');
    $handle->image_resize = true;
    $handle->image_ratio_y = true;
    $handle->image_x = $size;
    $handle->file_new_name_body = $flag . "_" . $size;
    $handle->file_auto_rename = false;
    $handle->dir_auto_create = true;
    $handle->no_upload_check = true;
    $handle->process($absFlagsDirectory);
    if ($handle->processed) {
      header("Content-Type: image/gif");
      chmod($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_' . $size . '.gif', 0777);
      echo file_get_contents($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_' . $size . '.gif');
      exit;
    }
  } else {
    header("Content-Type: image/gif");
    echo file_get_contents($absFlagsDirectory . DIRECTORY_SEPARATOR . $flag . '_300.gif');
    exit;
  }
}

error('No errors, show default flag');
