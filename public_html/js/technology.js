/**
 * wofh-tools.project.
 *
 * @since        17.11.2014 19:14
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

var Tree = (function () {
    var ITEM_WIDTH = 215 + 48;
	var DATA;
	var optPrefix = 'scTree',
		commonLineColor = 'rgba(209, 209, 209, 0.1)',
		takeLineColor = 'rgba(0, 128, 0, 0.9)',
		needLineColor = 'rgba(18, 104, 168, 0.9)',
		groups = [],
		canvas,
		context,
		$canvas
		;

	function _drawLine(fromId, toId) {
		try {
			var margin = 20,
				h1 = DATA[fromId]['obj'].height() + margin * 2 + 10,
				h2 = DATA[toId]['obj'].height() + margin * 2 + 10,
				begin = {x: DATA[fromId]['pos'].left + ITEM_WIDTH, y: DATA[fromId]['pos'].top + h1 / 2},
				end = {x: DATA[toId]['pos'].left + 0, y: DATA[toId]['pos'].top + h2 / 2};

			context.moveTo(begin.x, begin.y);
			context.bezierCurveTo(
				begin.x + 50, begin.y + 10,
				end.x - 50, end.y - 10,
				end.x, end.y);
		} catch ($e) {
			console.error($e);
			//console.log(fromId, '->', toId);
			//console.log(DATA[fromId]);
			//console.log(DATA[toId]);
		}
	}

	return {

		currentId      : -1,
		items          : null,
		requiredScience: 0,

		init: function ($_tree, $_canvas) {
			$canvas = $_canvas;
			this.tree = $_tree;
			canvas = document.getElementById($_canvas.get(0).id);
			canvas.width = $_canvas.width();
			canvas.height = $_canvas.height();
			context = canvas.getContext('2d');

			this.items = $('.sci-item');
		},

		setData: function (data) {
			DATA = data;
			for (var id in DATA) if (DATA.hasOwnProperty(id)) {
				var obj = $('#id' + id);
				DATA[id]['obj'] = obj;
				DATA[id]['pos'] = obj.position();
				var takes = DATA[id]['takes'];
				if (takes.length == 0) {
					obj.addClass('ending');
				}
			}
		},

		updatePosition: function () {
			for (var id in DATA) if (DATA.hasOwnProperty(id)) {
				DATA[id]['pos'] = DATA[id]['obj'].position();
			}
		},

		drawLines: function () {
			canvas.width = $canvas.width();
			canvas.height = $canvas.height();
//				return;

			context.lineWidth = 1;
			context.strokeStyle = commonLineColor;
			context.beginPath();

			for (var id in DATA) if (DATA.hasOwnProperty(id)) {
				var takes = DATA[id]['takes'];
				for (var i in takes) {
					if (takes.hasOwnProperty(i)) {
						_drawLine(id, takes[i]);
					}
				}
			}
			context.stroke();
			context.closePath();
		},

		selectItem: function (id) {
			this.items.removeClass('current need take');
			this.drawLines();

			if (id == -1) return;

			this.selectTakes(id);
			var sum = this.selectNeeds(id) + '';
			this.requiredScience = sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			DATA[id]['obj'].addClass('current');
			this.currentId = id;
			location.hash = 'sci' + id;

			this.drawTakeLines(id);
			this.drawNeedsLines(id);
		},

		selectTakes: function (currentId) {
			for (var i in DATA[currentId]['takes']) {
				if (DATA[currentId]['takes'].hasOwnProperty(i)) {
					var id = DATA[currentId]['takes'][i];
					DATA[id]['obj'].addClass('take');
				}
			}
		},

		selectNeeds: function (currentId) {
			if (currentId === undefined) return 0;

			var needs = DATA[currentId]['nAll'],
				sum = 0;

			for (var i in needs) {
				if (needs.hasOwnProperty(i)) {
					var id = needs[i];
					DATA[id]['obj'].addClass('need');
					if (!DATA[id]['learn']) {
						sum += DATA[id]['cost'];
					}
				}
			}
			return sum + DATA[currentId]['cost'];
			//return sum + (!DATA[currentId]['learn'] ? DATA[currentId]['cost'] : 0);
		},

		drawTakeLines: function (currentId) {
			if (currentId === undefined) return;

			context.lineWidth = 2;
			context.lineJoin = 'round';
			context.strokeStyle = takeLineColor;
			context.beginPath();
			var takes = DATA[currentId].takes;
			for (var x in takes) {
				if (takes.hasOwnProperty(x)) {
					_drawLine(currentId, takes[x]);
				}
			}
			context.stroke();
			context.closePath();
		},

		drawNeedsLines: function (currentId) {
			if (currentId === undefined) return;

			context.lineWidth = 2;
			context.lineJoin = 'round';
			context.strokeStyle = needLineColor;
			context.beginPath();

			var all = DATA[currentId].nAll;
//				console.log(SC_LIST[currentId]);
//				console.log(all);
//				console.trace();
			all.push(currentId);
			for (var y in all) {
				if (all.hasOwnProperty(y)) {
					var sc = DATA[all[y]],
						needs = sc.need;
					for (var z in needs) {
						if (needs.hasOwnProperty(z)) {
							if (!DATA[needs[z]]['learn'])
								_drawLine(needs[z], all[y]);
						}
					}
				}
			}
			all.pop();

			context.stroke();
			context.closePath();
		},

		moveTo: function (id) {
			if (DATA[id] == undefined) return;
//		this.tree.scrollLeft(SC_LIST[id]['pos'].left - (Math.floor(this.tree.width() / 2) - Math.floor(ITEM_WIDTH / 2)));
			var scroll = DATA[id]['pos'].left - (Math.floor(this.tree.width() / 2) - Math.floor(ITEM_WIDTH / 2));
			this.tree.animate({scrollLeft: scroll}, 400);
		},

		searchTechnology: function (word) {
			for (var id in DATA) if (DATA.hasOwnProperty(id)) {
				var science = DATA[id];
				var name = science.title.toLowerCase();
				if (name.indexOf(word) === 0) {
					this.selectItem(id);
					this.moveTo(id);
					break;
				}
			}
		},
		setOption       : function (jqObj, opt, callback) {
			var self = this;
			if (wt.storage.getBool('scTree-' + opt)) {
				jqObj.prop('checked', 'checked');
			} else {
				jqObj.removeAttr('checked');
			}
			jqObj.change(function () {
				if ($(this).is(':checked')) {
					self.tree.addClass(opt);
					wt.storage.setBool('scTree-' + opt, true);
				} else {
					self.tree.removeClass(opt);
					wt.storage.setBool('scTree-' + opt, false);
				}
				callback();
			}).change();
		}

	}
}());


$(function () {

	var $tree = $('#tree'),
		searchSciInterval;

	Tree.init($tree, $('#canvas'));

	$('body').mousewheel(function (e) {
		return !(e.shiftKey || e.ctrlKey);
	});

	$tree.mousewheel(function (event, delta) {
		if (!event.shiftKey) {
			var scroll = $tree.scrollLeft();
			$tree.scrollLeft(scroll - (delta * 100));
			return false;
		} else {
			var scroll = $tree.scrollTop();
			$tree.scrollTop(scroll - (delta * 100));
			return false;
		}
		return true;
	});

	$tree.on('click', '.sci-item', function (e) {
		e.stopPropagation();
		if (e.target.tagName == 'INPUT') return;
		Tree.selectItem($(this).data('id'));
		$('#tree-science-cost').val(Tree.requiredScience);
	});

	$('#tree-science-search').on('input', function () {
		clearTimeout(searchSciInterval);
		var input = $(this);
		searchSciInterval = setTimeout(function () {
			var word = input.val().toLowerCase();
			if (word != '') {
				Tree.searchTechnology(word);
			} else {
				//Tree.clear();
			}
			$('#sum').val(Tree.requiredScience);
		}, 500);
	});

	Tree.setOption($('#to-builds'), 'show-builds', function () {
		Tree.updatePosition();
		Tree.selectItem(Tree.currentId);
	});

	Tree.setOption($('#to-dep'), 'show-deposits', function () {
		Tree.updatePosition();
		Tree.selectItem(Tree.currentId);
	});

	$.getJSON('/technology/list.json', function (response) {
		//console.log(response);
		Tree.setData(response);
		Tree.drawLines();

		if (location.hash) {
			var hash = location.hash.replace(/#sci/, '');
			Tree.selectItem(hash);
			$('#tree-science-cost').val(Tree.requiredScience);
			Tree.moveTo(hash);
		}

	});

	var moved = false,
		startXMove = 0,
		startXScroll = 0,
		startYMove = 0,
		startYScroll = 0;

	Tree.tree.mousedown(function (e) {
		moved = true;
		startXMove = wt.mouse.x(e);
		startXScroll = Tree.tree.scrollLeft();
		startYMove = wt.mouse.y(e);
		startYScroll = Tree.tree.scrollTop();
	});
	Tree.tree.mousemove(function (e) {
		if (moved) {
			var currentXMove = wt.mouse.x(e);
			var deltaX = startXMove - currentXMove;
			Tree.tree.scrollLeft((startXScroll + deltaX));
			var currentYMove = wt.mouse.y(e);
			var deltaY = startYMove - currentYMove;
			Tree.tree.scrollTop((startYScroll + deltaY));
		}
	});
	$(document).mouseup(function () {
		moved = false;
	});

});