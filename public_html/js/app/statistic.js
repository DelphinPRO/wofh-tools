/**
 * Created with PhpStorm.
 *
 * @since        02.03.2016 20:53
 * @author       delphinpro delphinpro@yandex.ru
 * @copyright    copyright (C) 2016 delphinpro
 * @license      Licensed under the MIT license
 */

$(function () {
    "use strict";

    var $compareList = $('#compare-list');
    var $compareListAccounts = $('#compare-accounts');
    var $compareButton = $('#compare');

    $('#compare-clear').on('click', function (e) {
        e.preventDefault();
        var $t = $(this);
        $.ajax({
            url     : wtData.stat.clearCompareLink,
            type    : 'get',
            dataType: 'html',
            success : function (r) {
                console.log(r);
                $compareList.hide();
                $compareButton.attr('href', '#');
                $compareListAccounts.html('');
                location.href = wtData.stat.compareLink;
            },
            error   : function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
    });
    $('.js-add-to-compare').on('click', function (e) {
        e.preventDefault();
        var $t = $(this);
        var title = $t.data('title');
        $.ajax({
            url     : wtData.stat.addCompareLink,
            type    : 'get',
            dataType: 'json',
            data    : {
                id  : $t.data('id'),
                sign: $t.data('sign')
            },
            success : function (r) {
                // ids array
                console.log(r);
                var i, item, len = r.length, html = '', ids = [];
                if (len > 0) {
                    $compareList.fadeIn();
                }
                for (i = 0; i < len; i++) {
                    item = r[i];
                    ids.push(item['accountId']);
                    html += '<div style="padding: 2px 0;">' +
                        '<img src="' + item['countryFlag'] + '" alt="" title="' + item['countryTitle'] + '"> ' +
                        '<span>' + item['accountName'] + '</span>' +
                        '</div>';
                }
                $compareButton.attr('href', wtData.stat.compareLink + '?ids[]=' + ids.join('&ids[]='));
                $compareListAccounts.html(html);
            },
            error   : function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
    });
});
