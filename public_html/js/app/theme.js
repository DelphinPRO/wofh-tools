/**
 * wofh-tools.
 *
 * @since        14.10.2014 2:44
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

wt.theme = (function () {
	var swTheme;
	return {
		doInit  : function () {
			swTheme = $('#swTheme');
			var wtTheme = wt.cookie.get('wtTheme', 'dark');
			if (wtTheme == 'light') {
				swTheme.removeAttr('checked');
			} else {
				swTheme.prop('checked', 'checked');
				Highcharts.setOptions(Highcharts.darkTheme);
			}
			swTheme.on('change', this.doSwitch);
			Highcharts.setOptions({
				credits: {
					enabled: true,
					href   : 'http://wofh-tools.ru/stat/',
					text   : 'wofh-tools.ru/stat/'
				}
			});
		},
		doSwitch: function () {
			wt.cookie.set('wtTheme', swTheme.is(':checked') ? 'dark' : 'light', 2592000, '/');
			window.location.reload();
		}
	}
}());
