/**
 * Wofh Tools.
 *
 * @since        14.10.2014 1:45
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

wt.mouse = (function() {
	return {
		x: function (e) {
			e = e || window.event;
			if (e.pageX) {
				return e.pageX;
			}
			else if (e.clientX) {
				return e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
			}
			else {
				return 0;
			}
		},

		y: function (e) {
			e = e || window.event;
			if (e.pageY) {
				return e.pageY;
			}
			else if (e.clientY) {
				return e.clientY + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
			}
			else {
				return 0;
			}
		}
	}
}());