/**
 * wofh-tools.
 *
 * @since        14.10.2014 2:52
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

wt.cookie = (function () {
	return {
		set : function (name, value, expires, path, domain, secure) {
			expires instanceof Date ? expires = expires.toUTCString()
				: typeof(expires) == 'number' && (expires = (new Date(+(new Date) + expires * 1e3)).toUTCString());
			var r = [name + "=" + encodeURIComponent(value)], s, i;
			for (i in s = {expires: expires, path: path, domain: domain}) {
				if (s.hasOwnProperty(i))
					s[i] && r.push(i + "=" + s[i]);
			}
			return secure && r.push("secure"), document.cookie = r.join(";"), true;
		},

		get: function (name, defaultValue) {
			var cookie = " " + document.cookie,
				search = " " + name + "=",
				value = defaultValue || null,
				offset = 0,
				end = 0;
			if (cookie.length > 0) {
				offset = cookie.indexOf(search);
				if (offset != -1) {
					offset += search.length;
					end = cookie.indexOf(";", offset);
					if (end == -1) end = cookie.length;
					value = decodeURIComponent(cookie.substring(offset, end));
				}
			}
			return value;
		}
	}
}());
