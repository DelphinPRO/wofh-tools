/**
 * wofh-tools.
 *
 * @since        14.10.2014 2:35
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

wt.charts = (function () {
	return {
		lines        : function (id, params) {
			$(id).highcharts({
				title   : {text: params.title, x: -20},
				subtitle: {text: params.subtitle, x: -20},
				xAxis   : {
					categories: params.categories,
					labels    : {
						rotation: -45,
						align   : 'right'
					}
				},
				yAxis   : {
					title    : {text: params.label},
					plotLines: [
						{value: 0, width: 1, color: '#eee'}
					]
				},
				tooltip : {
					useHTML     : true,
					headerFormat: '<span class="pull-left">{point.key}</span><table>',
					pointFormat : '<tr style="color:{series.color}"><td>{series.name}:</td><td>{point.y}</td></tr>',
					footerFormat: '</table>',
					crosshairs  : true,
					shared      : true,
					valueSuffix : params.suffix,
					positioner  : function (labelWidth, labelHeight, point) {
						if (point.plotX > labelWidth) {
							return {x: point.plotX - labelWidth, y: 420 - labelHeight};
						} else {
							return {x: point.plotX + 100, y: 420 - labelHeight};
						}
					}
				},
				legend  : {
					backgroundColor: 'RGB(59,61,68)',
					//floating: false,
					layout       : 'horizontal',
					//align        : 'top',
					verticalAlign: 'bottom',
					borderWidth  : 0
					//maxHeight: 130,
					//y: -70
				},
				series  : params.series
			});
		},
		pie          : function (id, params) {
			$(id).highcharts({
				credits    : {enabled: false},
				chart      : {plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false},
				title      : {text: params.title},
				subtitle   : {text: params.subtitle},
				tooltip    : {pointFormat: '<b>{point.y}</b> {series.name}'},
				plotOptions: {
					pie: {
						dataLabels: {
							enabled: true,
							color  : '#999',
							format : '<b>{point.name}</b>: {point.percentage:.1f} %'
						}
					}
				},
				series     : params.series
			});
		},
		splineAndArea: function (id, params) {
			$(id).highcharts({
				title   : {text: params.title},
				subtitle: {text: params.subtitle},
				xAxis   : [
					{
						categories: params.categories,
						labels    : {
							rotation: -45, align: 'right', formatter: function () {
								var date = new Date(this.value);
								var day = date.getDay();
								this.value = date.getDate() + ' ' + wt.helpers.getMonth(date.getMonth());
								if (day == 0) {
									this.value = '<span style="color:#DF5353;">' + this.value + '</span>';
								}
								return this.value;
							}
						}
					}
				],
				yAxis   : [
					{
						title: {text: params.text1, style: {color: params.primaryColor}}
					},
					{
						title   : {text: params.text2, style: {color: params.secondColor}},
						opposite: true
					}
				],
				tooltip : {
					formatter : function () {
						var s = this.x;
						$.each(this.points, function (i, point) {
							var val = point.y.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
							s += '<br><span style="color:' + point.series.color + '">' + point.series.name + ': ' +
							'<b>' + (point.y > 0 && i == 0 ? '+' + val : val) +
							(params['sfx'] ? params['sfx'] : '') + '</b>' + '</span>';
						});
						return s;
					},
					positioner: function (labelWidth, labelHeight, point) {
						if (point.plotX > labelWidth) {
							return {x: point.plotX - labelWidth + 50, y: 320 - labelHeight};
						} else {
							return {x: point.plotX + 100, y: 320 - labelHeight};
						}
					},
					crosshairs: true,
					shared    : true
				},
				legend  : {
					layout       : 'vertical',
					align        : 'left',
					x            : 100,
					y            : 60,
					verticalAlign: 'top',
					floating     : true
				},
				series  : [
					{
						name : params.series[1].text,
						color: params.secondColor,
						type : 'column',
						yAxis: 1,
						data : params.series[1].data
					},
					{
						name : params.series[0].text,
						color: params.primaryColor,
						type : 'areaspline',
						data : params.series[0].data
					}
				]
			});
		},
		bar          : function (chartBars) {
			if (chartBars.length == 0) return;
			setTimeout(function () {
				chartBars.each(function (i, e) {
					var w = $(e).data('width');
					$(e).animate({'width': w + '%'}, 1000);
				});
			}, 300);
		},
		lazy         : function () {
			var self = this;
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				if (wt.data.stat.charts[e.target.hash]) {
					var cId = wt.data.stat.charts[e.target.hash]['cId'];
					if ($(cId).is(':empty')) {
						self.splineAndArea(cId, wt.data.stat.charts[e.target.hash]);
					} else {
						$(cId).highcharts().reflow();
					}
				}
			});
			$('a[href="#tab1"]').tab('show');
		}
	}
}());