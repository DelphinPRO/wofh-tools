/**
 * wofh-tools.ru
 *
 * @since        04.11.2014 10:40
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

wt.bookmarks = (function () {


	var $bm = $('#user-bookmarks'),
		$btn = $('.user-bookmark');

	function getBm() {
		return wt.storage.exists('user-bookmarks')
			? JSON.parse(wt.storage.get('user-bookmarks'))
			: {};
	}

	function getIcon(type) {
		var icons = {
			'account': '/img/icons/account.png',
			'town'   : '/img/icons/town.png',
			'country': '/img/icons/country.png'
		};
		return (icons[type])
			? '<img src="' + icons[type] + '" alt="">'
			: '';
	}

	function createBookmark(data) {
		return $('<div id="' + data.id + '" class="user-bookmark-item">' +
		'<div style="float:right;cursor:pointer;" onclick="wt.bookmarks.remove(\'' + data.id + '\')">&times;</div>' +
		'<a href="' + data.uri + '" style="overflow:hidden;margin-right:30px;">' +
		getIcon(data.section) +
		data.title +
		'</a>' +
		'</div>');
	}

	function print() {
		var bm = getBm();
		var cId = $btn.data('id');
		for (var id in bm) {
			var b = createBookmark({
				'id'     : id,
				'uri'    : bm[id][0],
				'title'  : bm[id][1],
				'section': bm[id][2]
			});
			$bm.append(b);
			if (id == cId) {
				$btn
					.removeClass('icon-star-empty btn-default')
					.addClass('icon-star btn-success');
				$('#' + id).addClass('selected');
			}
		}
		var cleanAll = $('<div/>', {
			text: 'delete all',
			css : {
				float    : 'right',
				cursor   : 'pointer',
				textAlign: 'right',
				fontSize : '11px'
			}
		});
		cleanAll.on('click', function () {
			if (confirm('Все закладки будут удалены! Вы уверены?')) {
				wt.storage.set('user-bookmarks', JSON.stringify({}));
				$userBms.html('');
				$btn
					.addClass('icon-star-empty btn-default')
					.removeClass('icon-star btn-success');
			}
		});
		$bm.after(cleanAll);
	}

	return {
		init: function () {
			var self = this;
			print();
			$btn.on('click', function () {
				var o = $(this),
					data = {
						id     : o.data('id'),
						uri    : o.data('uri'),
						title  : o.data('title'),
						section: o.data('section')
					};
				self.toggle(data);
			});
		},

		toggle: function (data) {
			var bm = getBm();

			if (bm[data.id] === undefined) {
				bm[data.id] = [data.uri, data.title, data.section];
				$bm.append(createBookmark(data));
			} else {
				delete bm[data.id];
				$bm.find('#' + data.id).remove();
			}
			wt.storage.set('user-bookmarks', JSON.stringify(bm));
			$btn.toggleClass('icon-star icon-star-empty btn-default btn-success');
		},

		remove: function(id) {
			var bm = getBm();

			if (bm[id] === undefined) return;
			delete bm[id];
			$bm.find('#' + id).slideUp(function(){
				$(this).remove();
			});

			wt.storage.set('user-bookmarks', JSON.stringify(bm));
		}
	}
}());
