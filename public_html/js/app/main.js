/**
 * Wofh Tools.
 *
 * @since        14.10.2014 1:43
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

var wtData = wtData || {
		route    : '',
		bookmarks: {
			'hash': ''
		},
		stat     : {
			world: {
				sign: ''
			},
            compareLink: ''
		}
	};

function getScrollBarWidth() {
	var div = document.createElement('div');
	div.style.overflowY = 'scroll';
	div.style.width = '100px';
	div.style.height = '50px';
	div.style.visibility = 'hidden';
	document.body.appendChild(div);
	var scrollWidth = div.offsetWidth - div.clientWidth;
	document.body.removeChild(div);

	if ($(window).height() == $(document).height()) {
		scrollWidth = 0;
	}
	return scrollWidth;
}

var wt = {
	data          : wtData,
	scrollBarWidth: getScrollBarWidth(),
	helpers       : {
		getMonth: function (month, lang, form) {
			lang = lang || 'RU';
			form = 0;
			var months = {
				'RU': [
					['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
				]
			};
			return months[lang][form][month];
		}
	},
	actions       : {
		init: function () {
			$('.action-show-login').on('click', function () {
				wt.windows.init('#wnd-login');
				wt.windows.show('#wnd-login');
				return false;
			});
		}
	},

	storage: {
		set: function (name, value) {
			localStorage.setItem(name, value);
		},

		get: function (name) {
			return localStorage.getItem(name);
		},

		del: function (name) {
			localStorage.removeItem(name);
		},

		exists: function (name) {
			return localStorage.getItem(name) !== null;
		},

		setBool: function (key, data) {
			localStorage.setItem(key, data ? 1 : 0);
		},

		getBool: function (key) {
			return localStorage.getItem(key) == 1;
		}
	},

	math : {
		format: function(n) {
			return parseInt(n).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		}
	},

	log: function () {
		console.log(arguments);
	}
};

$(function () {

	wt.theme.doInit();
	wt.actions.init();
	wt.bookmarks.init();

	//input number fields
	$('input.num').keypress(function (e) {
		var c = e.which;
		return (!(((c < 48) || (c > 57)) && (c > 31)));
	});

	if (wt.data.stat.charts) {
		for (var i in wt.data.stat.charts) {
			if (wt.data.stat.charts[i]['lazy']) {
				var index = wt.data.stat.charts[i]['lazyIndex'];
				wt.data.stat.charts[index] = wt.data.stat.charts[i];
				continue;
			}
			switch (wt.data.stat.charts[i]['type']) {
				case 'pie':
					wt.charts.pie(wt.data.stat.charts[i]['cId'], wt.data.stat.charts[i]);
					break;
				case 'lines':
					wt.charts.lines(wt.data.stat.charts[i]['cId'], wt.data.stat.charts[i]);
					break;
//				case 'bar':
//					wt.charts.bar(wt.data.stat.charts[i]['cId'], wt.data.stat.charts[i]);
//					break;
				case 'splineAndArea':
					wt.charts.splineAndArea(wt.data.stat.charts[i]['cId'], wt.data.stat.charts[i]);
					break;
			}
		}
		wt.charts.bar($('.chart-bar'));
		wt.charts.lazy();
	}


	$('.wofh-api-worlds-list').on('change', '.world-option', function(){
		var cb = $(this);
		var cb_p = cb.closest('.checkbox-switcher');
		var wId = parseInt(cb.data('world-id'));
		var option = cb.data('option');
		var checked = $(this).is(':checked');
		cb_p.addClass('__process');
		console.log(checked);

		$.ajax({
			url     : "/wofh/option-switch/",
			type    : "POST",
			dataType: "json",
			data:  {
				worldId : wId,
				option: option
			}
		}).done(function (response) {
			cb_p.removeClass('__process');
			console.log('Response', response);
			if (response.error) {
				alert(response.message);
				cb.attr('checked', !checked);
			} else {
				//cb1.attr('checked', true);
			}
		}).fail(function (xhr, status, error) {
			cb_p.removeClass('__process');
			console.error('Error: ' + error);
		});

		return false;
	});

	$('.dropdown-select-selected').on('click', function(e){
		$(this).closest('.dropdown-select').toggleClass('open');
		e.stopPropagation();
	});

	$('body').on('click', function(){
		$('.dropdown-select').removeClass('open');
	});
});
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
});
