/**
 * wofh-tools.project.
 *
 * @since        01.11.2014 18:05
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

wt.windows = (function () {
	var windows = [];
	return {
		_stack        : [],
		_topMostZIndex: 9000,
		_mask         : null,
		init          : function (id) {
			var $e = this._getCachedElement(id),
				self = this;

			$e.overlay({
				oneInstance : false,
				fixed       : true,
				onBeforeLoad: function () {
					$('html').addClass('isModal');
					$('body').css({marginRight: wt.scrollBarWidth, overflow: 'hidden'});
					$('.navbar').css({paddingRight: wt.scrollBarWidth});
				},
				onLoad      : function () {
					//self.setPosition($e);
				},
				onClose     : function () {
					$('html').removeClass('isModal');
					$('body').css({marginRight: 0, overflow: 'auto'});
					$('.navbar').css({paddingRight: 0});
				}
			});
			var $o = $e.data('overlay');
			$e.find('.a-cancel').click(function () {
				$o.close();
			});

		},
		show          : function (id) {
			var $e = this._getCachedElement(id),
				self = this;

			var $o = $e.data('overlay');
			if ($o) {
				this.register($o);
				if (!$o.isOpened()) {
					$o.load();
				}
			} else {
				console.error('error');
			}
		},
		register      : function ($o) {
			var $e = $o.getOverlay();
			this._stack.push($e);
			this._showMask();
			var index = this._stack.length - 1;
			var self = this;
			$o.onClose = function () {
				self._stack.splice(index, 1);
				if (self._stack.length == 0) {
					self._closeMask();
				} else {
					self._reSetZIndexes();
				}
			};
			this._reSetZIndexes();
		},
		setPosition   : function ($e) {
			var topOffset = 50;
			if (typeof $e == 'string') $e = $($e);

			var offset = $(document).scrollTop();
			var top = offset + topOffset + "px";
			if (offset + topOffset + $e.outerHeight() >= $(document).height()) {
				top = $(document).height() - $e.outerHeight() - topOffset;
			}
			$e.css({'position': 'absolute'}).animate({top: top});
		},
		showLoader    : function ($e) {
			$e.html('<div class="overlay-loader">Загрузка...</div>');
		},
		hide          : function ($e) {
			if (typeof $e == 'string') $e = $($e);
			var $o = $e.data('overlay');
			if ($o) {
				$o.close();
			} else {
				$e.find('a.close').trigger('click');
			}
		},

		_getCachedElement  : function (id) {
			if (typeof(windows[id]) == "undefined") {
				windows[id] = $(id);
				console.log(windows[id]);
				var save = windows[id].html();

				windows[id].addClass('overlay');
				windows[id].html('<div class="close">&times;</div>' +
				'<div class="panel panel-default">' +
				'<div class="panel-heading"><h3 class="panel-title">Заголовок</h3></div>' +
				'<div class="panel-body"></div>' +
					//'<div class="panel-footer text-center">' +
					//'<button class="btn btn-success a-ok">OK</button>' +
					//'</div>' +
				'</div>');
				windows[id].find('.panel-body').html(save);

				windows[id].appendTo('body');
			}
			return windows[id];
		},
		_reSetZIndexes     : function () {
			var index = this._stack.length - 1;
			this._stack[index].css({zIndex: this._topMostZIndex});
			this._mask.css({zIndex: this._topMostZIndex - 1});
			for (var i = index - 1, z = this._topMostZIndex - 2; i >= 0; i--, z--) {
				this._stack[i].css({zIndex: z});
			}
		},
		_closeTopMostWindow: function () {
			var $e = this._stack[this._stack.length - 1];
			var $o = $e.data('overlay');
			if ($o) {
				$o.close();
			} else {
				$e.find('a.close').trigger('click');
			}
		},
		_showMask          : function () {
			if (!this._mask) {
				var self = this;
				this._mask = $('<div/>', {
					'class': 'overlay-mask',
					css    : {
						backgroundColor: '#000',
						opacity        : 0.3,
						position       : 'fixed',
						left           : 0,
						top            : 0,
						width          : '100%',
						height         : '100%',
						display        : 'none'
					}
				});
				this._mask.appendTo('body');
				this._mask.on('click', function () {
					self._closeTopMostWindow();
				});
			}
			this._mask.fadeIn();
		},
		_closeMask         : function () {
			if (this._mask) {
				this._mask.hide();
			}
		}
	}
}());
