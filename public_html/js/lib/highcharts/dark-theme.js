/**
 * wofh-tools.project.
 *
 * @since        03.11.2014 17:04
 * @author       DelphinPRO delphinpro@yandex.ru
 * @copyright    Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license      Licensed under the MIT license
 */

"use strict";

Highcharts.darkTheme = {
	colors  : ["#DDDF0D", "#7798BF", "#55BF3B", "#DF5353", "#aaeeee", "#ff0066", "#eeaaee",
		"#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	chart   : {
		backgroundColor    : {
			//linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
			linearGradient: { x1: 0, y1: 0, x2: 0, y2: 0 },
			stops         : [
				//[0, 'RGB(68,71,79)'],
				//[1, 'RGB(55,58,64)']
				[0, 'RGB(71,73,82)'],
				[1, 'RGB(71,73,82)']
			]
		},
		borderWidth        : 1,
		borderRadius       : 0,
		borderColor        : 'RGB(81,85,98)',
		//borderColor        : '#515562',
		plotBackgroundColor: null,
		plotShadow         : false,
		plotBorderWidth    : 0
	},
	title   : {
		style: {
			color: '#ccc',
			font : '16px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
		}
	},
	subtitle: {
		style: {
			color: '#DDD',
			font : '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
		}
	},
	xAxis   : {
		gridLineWidth: 0,
		lineColor    : '#999',
		tickColor    : '#999',
		labels       : {
			style: {
				color     : '#999',
				fontWeight: 'bold'
			}
		},
		title        : {
			style: {
				color: '#AAA',
				font : 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
			}
		}
	},
	yAxis   : {
		alternateGridColor: null,
		minorTickInterval : null,
		gridLineColor     : 'rgba(255, 255, 255, .1)',
		minorGridLineColor: 'rgba(255,255,255,0.07)',
		lineWidth         : 0,
		tickWidth         : 0,
		labels            : {
			style: {
				color     : '#999',
				fontWeight: 'bold'
			}
		},
		title             : {
			style: {
				color: '#AAA',
				font : 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
			}
		}
	},
	legend  : {
		itemStyle      : {color: '#CCC'},
		itemHoverStyle : {color: '#FFF'},
		itemHiddenStyle: {color: '#333'},
		backgroundColor: '#474952'
		//backgroundColor: {
		//	linearGradient: {x1: 0, y1: 0, x2: 0, y2: 0},
		//	stops         : [
		//		[0, 'rgba(96, 96, 96, 0)'],
		//		[1, 'rgba(16, 16, 16, 0)']
		//		//[0, 'RGB(59,61,68)'],
		//		//[1, 'RGB(59,61,68)']
		//	]
		//}
	},
	labels  : {
		style: {color: '#CCC'}
	},
	tooltip : {
		backgroundColor: {
			linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
			stops         : [
				//[0, 'rgba(96, 96, 96, .8)'],
				//[1, 'rgba(16, 16, 16, .8)']
				[0, 'RGBA(59,61,68,0.8)'],
				[1, 'RGBA(59,61,68,0.8)']
			]
		},
		borderWidth    : 0,
		style          : {color: '#eee'}
	},


	plotOptions: {
		series     : {shadow: true},
		line       : {
			dataLabels: {color: '#CCC'},
			marker    : {lineColor: '#333'}
		},
		spline     : {
			marker: {lineColor: '#333'}
		},
		scatter    : {
			marker: {lineColor: '#333'}
		},
		candlestick: {lineColor: 'white'}
	},

	toolbar: {
		itemStyle: {color: '#CCC'}
	},

	navigation   : {
		buttonOptions: {
			symbolStroke     : '#DDDDDD',
			hoverSymbolStroke: '#FFFFFF',
			theme            : {
				fill  : {
					linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
					stops         : [
						[0.4, '#606060'],
						[0.6, '#333333']
					]
				},
				stroke: '#000000'
			}
		}
	},

	// scroll charts
	rangeSelector: {
		buttonTheme: {
			fill  : {
				linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
				stops         : [
					[0.4, '#888'],
					[0.6, '#555']
				]
			},
			stroke: '#000000',
			style : {
				color     : '#CCC',
				fontWeight: 'bold'
			},
			states: {
				hover : {
					fill  : {
						linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
						stops         : [
							[0.4, '#BBB'],
							[0.6, '#888']
						]
					},
					stroke: '#000000',
					style : {color: 'white'}
				},
				select: {
					fill  : {
						linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
						stops         : [
							[0.1, '#000'],
							[0.3, '#333']
						]
					},
					stroke: '#000000',
					style : {color: 'yellow'}
				}
			}
		},
		inputStyle : {
			backgroundColor: '#333',
			color          : 'silver'
		},
		labelStyle : {color: 'silver'}
	},

	navigator: {
		handles     : {
			backgroundColor: '#666',
			borderColor    : '#AAA'
		},
		outlineColor: '#CCC',
		maskFill    : 'rgba(16, 16, 16, 0.5)',
		series      : {
			color    : '#7798BF',
			lineColor: '#A6C7ED'
		}
	},

	scrollbar                 : {
		barBackgroundColor   : {
			linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
			stops         : [
				[0.4, '#888'],
				[0.6, '#555']
			]
		},
		barBorderColor       : '#CCC',
		buttonArrowColor     : '#CCC',
		buttonBackgroundColor: {
			linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
			stops         : [
				[0.4, '#888'],
				[0.6, '#555']
			]
		},
		buttonBorderColor    : '#CCC',
		rifleColor           : '#FFF',
		trackBackgroundColor : {
			linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
			stops         : [
				[0, '#000'],
				[1, '#333']
			]
		},
		trackBorderColor     : '#666'
	},

	// special colors for some of the demo examples
	legendBackgroundColor     : 'rgba(48, 48, 48, 0.8)',
	legendBackgroundColorSolid: 'rgb(70, 70, 70)',
	dataLabelsColor           : '#444',
	textColor                 : '#E0E0E0',
	maskColor                 : 'rgba(255,255,255,0.3)'
};
