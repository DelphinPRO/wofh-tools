http_path = "/"
css_dir = "public/css"
sass_dir = "public/sass"
images_dir = "public/img"
javascripts_dir = "public/js"
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed
relative_assets = true
line_comments = false
# sass_options = {:sourcemap => 'auto'}
