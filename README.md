# Wofh-Tools.ru

Repository of wofh-tools.ru

## Requirements

* PHP 5.3.3 or greater
* MySQL 5.0 or greater, and PDO library must be enabled
* NodeJS (for development only)

## How to install

1. Create an empty database on your server
2. Rename `private/config/application-EXAMPLE.json` into `application.json` and fill section 'db'
3. Run commands:

```
composer install
npm install
bower install
gulp build
php dolphin migration:migrate
```

Profit! :)
