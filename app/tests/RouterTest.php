<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 17.08.14
 * Time: 14:53
 */
namespace tests;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class RouterTest extends \PHPUnit_Extensions_SeleniumTestCase {

	protected $coverageScriptUrl = 'http://wofh-tools.project/phpunit_coverage.php';

	protected function setUp()
	{
//		$this->setupSpecificBrowser(array(
//			'browserName' => '*firefox',
//			'host'    => 'wofh-tools.project',
//		));
		$this->setBrowser('*firefox');
		$this->setBrowserUrl('http://wofh-tools.project');
	}

	public function testHomepage()
	{
		$this->open('/');
		$this->assertEquals('Wofh Tools', $this->getTitle());
	}

	public function testWarReportsPage()
	{
		$this->open('/log/');
		$this->assertEquals('Логовница :: Wofh Tools', $this->getTitle());
	}

	public function testWarReportPage()
	{
		$this->open('/log/36ef3d52a2a2/');
		$this->assertEquals('Сирко напал на 70volodya :: Wofh Tools', $this->getTitle());
	}

	public function testScienceReportsPage()
	{
		$this->open('/sci/');
		$this->assertEquals('Науковница :: Wofh Tools', $this->getTitle());
	}

	public function testScienceDistributionPage()
	{
		$this->open('/science-distribution/');
//		$this->assertEquals('Таблица научного распределения :: Wofh Tools', $this->getTitle());
		$this->assertTitle('Таблица научного распределения :: Wofh Tools');
	}

}
