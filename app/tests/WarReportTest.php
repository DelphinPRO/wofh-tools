<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 13.08.14
 * Time: 1:42
 */
namespace tests;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class WarReportTest extends \PHPUnit_Extensions_SeleniumTestCase
{

	protected $coverageScriptUrl = 'http://wofh-tools.project/phpunit_coverage.php';

	protected function setUp()
	{
//		$this->setupSpecificBrowser(array(
//			'browserName' => '*firefox',
//			'host'    => 'wofh-tools.project',
//		));
		$this->setBrowser('*firefox');
		$this->setBrowserUrl('http://wofh-tools.project');
	}

	public function testLoginFormExists()
	{
		$this->open('http://wofh-tools.project/log/');
//		$this->url('/log/');
		$this->assertEquals('Логовница :: Wofh Tools', $this->getTitle());
//		$this->assertEquals('Логовница :: Wofh Tools', $this->title());
//		$this->assertElementPresent("id=login-form");
//		$this->assertElementPresent("dom=document.forms['login-form'].identity");
//		$this->assertElementPresent("dom=document.forms['login-form'].password");
//		$this->assertElementPresent("xpath=//form[@id='login-form']/input[@type='submit']");
	}

//	public function testSci()
//	{
//		$this->url('/sci/');
//		$this->assertEquals('Науковница :: Wofh Tools', $this->title());
//		$this->assertElementPresent("id=login-form");
//		$this->assertElementPresent("dom=document.forms['login-form'].identity");
//		$this->assertElementPresent("dom=document.forms['login-form'].password");
//		$this->assertElementPresent("xpath=//form[@id='login-form']/input[@type='submit']");
//	}

}
