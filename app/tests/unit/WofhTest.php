<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 18.08.14
 * Time: 2:25
 */
namespace tests\unit;

use df\model\Wofh;

define( 'DIR_ROOT', realpath(__DIR__ . '/../../..') );

require_once 'PHPUnit/Autoload.php';
require_once DIR_ROOT . '/app/df/AutoLoader.php';
\df\AutoLoader::$APPLICATION_DIR = DIR_ROOT . DIRECTORY_SEPARATOR . 'app';
spl_autoload_register(array('\df\AutoLoader', 'coreAutoLoad'));

//require_once 'PHPUnit/Framework/TestCase.php';

class WofhTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @dataProvider _signToId
	 */
	public function testSignToId($sign, $id)
	{
		$this->assertEquals($id, Wofh::signToId($sign));
	}

	public function _signToId()
	{
		return array(
			array('Ru21', 1021),
			array('ru21', 1021),
			array('RU21', 1021),

			array('En3', 2003),
			array('en3', 2003),
			array('EN3', 2003),

			array('De1', 3001),
			array('de1', 3001),
			array('DE1', 3001),
		);
	}

	/**
	 * @dataProvider _idToSign
	 */
	public function testIdToSign($sign, $id)
	{
		$this->assertEquals($sign, Wofh::idToSign($id));
	}

	public function _idToSign()
	{
		return array(
			array('Ru21', 1021),
			array('En3', 2003),
			array('De1', 3001),
		);
	}

	/**
	 * @dataProvider _domainToId
	 */
	public function testDomainToId($domain, $id)
	{
		$this->assertEquals($id, Wofh::domainToId($domain));
	}

	public function _domainToId()
	{
		return array(
			array('http://w21.wofh.ru', 1021),
			array('w21.wofh.ru', 1021),
			array('http://en3.waysofhistory.com', 2003),
			array('en3.waysofhistory.com', 2003),
			array('http://w1.wofh.de', 3001),
			array('w1.wofh.de', 3001),
		);
	}

	/**
	 * @dataProvider _idToDomain
	 */
	public function testIdToDomain($domain, $id)
	{
		$this->assertEquals($domain, Wofh::idToDomain($id));
	}

	public function _idToDomain()
	{
		return array(
			array('http://w21.wofh.ru', 1021),
			array('http://en3.waysofhistory.com', 2003),
			array('http://w1.wofh.de', 3001),
		);
	}

	/**
	 * @dataProvider _getLinkStatusWorlds
	 */
	public function testGetLinkStatusWorlds($country, $link)
	{
		$this->assertEquals($link, Wofh::getLinkStatusWorlds($country));
	}

	public function _getLinkStatusWorlds()
	{
		return array(
			array(Wofh::RU, "http://wofh.ru/aj_statistics"),
			array(Wofh::EN, "http://en.waysofhistory.com/aj_statistics"),
			array(Wofh::DE, "http://wofh.de/aj_statistics"),
		);
	}

	/**
	 * @dataProvider _getLinkStatistic
	 */
	public function testGetLinkStatistic($worldId, $link)
	{
		$this->assertEquals($link, Wofh::getLinkStatistic($worldId));
	}

	public function _getLinkStatistic()
	{
		return array(
			array(1021, "http://w21.wofh.ru/aj_statistics"),
			array(2003, "http://en3.waysofhistory.com/aj_statistics"),
			array(3001, "http://w1.wofh.de/aj_statistics"),
		);
	}

	/**
	 * @dataProvider _getLinkConstData
	 */
	public function testGetLinkConstData($worldId, $link)
	{
		$this->assertEquals($link, Wofh::getLinkConstData($worldId));
	}

	public function _getLinkConstData()
	{
		return array(
			array(1021, "http://w21.wofh.ru/js/gen/const.js"),
			array(2003, "http://en3.waysofhistory.com/js/gen/const.js"),
			array(3001, "http://w1.wofh.de/js/gen/const.js"),
		);
	}

	/**
	 * @dataProvider _getLinkConst1Data
	 */
	public function testGetLinkConst1Data($worldId, $link)
	{
		$this->assertEquals($link, Wofh::getLinkConst1Data($worldId));
	}

	public function _getLinkConst1Data()
	{
		return array(
			array(1021, "http://w21.wofh.ru/js/gen/const1.js"),
			array(2003, "http://en3.waysofhistory.com/js/gen/const1.js"),
			array(3001, "http://w1.wofh.de/js/gen/const1.js"),
		);
	}
}
