<?php
/**
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   copyright © 2013—2018 delphinpro
 * @license     licensed under the MIT license
 */

use df\helper\FS;

$pkg = FS::loadJSON(FS::path('package.json'));
define('WT_VERSION', $pkg['version']);
