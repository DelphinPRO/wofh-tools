<?php
/**
 * @since           02.07.14 21:21
 * @package         DFramework
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df;

class Session
{
    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * @param $name
     *
     * @return null
     */
    public function get($name)
    {
        return (isset($_SESSION[$name])) ? $_SESSION[$name] : null;
    }

    /**
     * @param $name
     */
    public function del($name)
    {
        unset($_SESSION[$name]);
    }

    public function __toString()
    {
        return print_r($_SESSION, true);
    }
}
