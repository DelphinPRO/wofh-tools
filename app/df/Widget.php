<?php
/**
 * @since              26.05.13 18:42
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

abstract class Widget extends Model
{
    protected $_widgetName;
    protected $_dfLayout;
//	protected $_dfDefinition;
    protected $_widgetData;

    abstract public function callWidget(Controller $call);

    public function __construct($name)
    {
        $this->_widgetName = $name;
        $this->_widgetData = array();
//		unset($definition['name'], $definition['layout']);
//		$this->_dfData       = $definition;
    }

//	final public function selectLayout($layout)
//	{
//		$this->_dfLayout = $layout;
//	}

    final public function get($name)
    {
        return array_key_exists($name, $this->_widgetData)
            ? $this->_widgetData[$name]
            : null;
    }

    final public function set($name, $value)
    {
        $this->_widgetData[$name] = $value;
    }

    final public function getData()
    {
        return $this->_widgetData;
    }

    final public function getName()
    {
        return $this->_widgetName;
    }

    final public function getLayout()
    {
        return $this->_dfLayout;
    }

    final public function setLayout($layout)
    {
        $this->_dfLayout = $layout;
    }
}
