<?php
/**
 * @since              06.09.13 21:56
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

class User
{

    public
        $id,
        $login,
        $email,
        $password,
        $register,
        $lastVisit,
        $sex,
        $status,
        $group,
        $rights,
        $uid,
        $profile;

    private $isAuth;

    /** @var Cookie */
    private $cookie;

    private static $instance;

    //=======================================================
    //   Public methods
    //=======================================================

    public static function getCurrent()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function hash($string)
    {
        return md5($string);
    }

    public function auth($email, $hash, $remember = true)
    {
        $db = Registry::getDb();
        $db->query("SELECT * FROM users WHERE email = :email AND `password` = :pass LIMIT 1",
            array('email' => $email, 'pass' => $hash));

        if ($userData = $db->fetch()) {
            $this->id = (int)$userData['id'];
            $this->login = $userData['login'];
            $this->email = $userData['email'];
            $this->password = $userData['password'];
            $this->register = strtotime($userData['register']);
            $this->lastVisit = strtotime($userData['lastVisit']);
            $this->sex = (int)$userData['sex'];
            $this->status = (bool)$userData['status'];
            $this->group = (int)$userData['group'];
            $this->rights = (int)$userData['rights'];
            $this->uid = $this->_generateUid($this->email, $this->password);

            $this->isAuth = true;
            $session = new Session(); // TODO $this->session
            $session->set('user', array('id' => $this->id, 'uid' => $this->uid));
            if ($remember) {
                $this->cookie->set('id', $this->id);
                $this->cookie->set('uid', $this->uid);
            }

            $db->query("
				UPDATE users
				SET lastVisit = :lastVisit, uid = :uid
				WHERE id = :id LIMIT 1",
                array('lastVisit' => date(STD_DATETIME), 'uid' => $this->uid, 'id' => $this->id));

            // TODO
//			if ($this->register == 0) {
//				$userData->id       = (int)$userData->id;
//				$userData->register = time();
//				$this->db->simpleQuery("UPDATE users SET register = '" . date('Y-m-d H:s:i', $userData->register) . "' WHERE id = {$userData->id}");
//			}

            return true;
        } else {
            unset($_SESSION['user']);
            $this->cookie->del('id');
            $this->cookie->del('uid');
        }
        return false;
    }

    public function logout()
    {
        if ($this->id <= 0) {
            return;
        }

        Registry::getDb()
            ->query("UPDATE users SET uid = NULL WHERE id = :id LIMIT 1", array('id' => $this->id));

        unset($_SESSION['user']);
        $this->cookie->del('id');
        $this->cookie->del('uid');
    }

    /**
     * @param $email
     * @param $passwordHash
     *
     * @return mixed
     * @deprecated
     */
    public static function checkAuth($email, $passwordHash)
    {
        $db = Db::Inst();
        $stmt = $db->prepare("SELECT * FROM users WHERE email = :email AND password = :pass LIMIT 1");
        $stmt->execute(array('email' => $email, 'pass' => $passwordHash));
        $userData = $stmt->fetchObject();
        if ($userData !== false && is_object($userData)) {
            $userData->uid = md5($userData->email . $userData->password);
            unset($userData->password);
        }
        return $userData;
    }

    public function isAuth()
    {
        return $this->isAuth;
    }

    public function isAdmin()
    {
        return $this->id == 1;
    }

    //=======================================================
    //   Private methods
    //=======================================================

    public function __construct()
    {
        $this->id = 0;
        $this->isAuth = false;

        $this->cookie = Registry::getInstance('df\Cookie');

        $uid = isset($_SESSION['user']['uid']) ? $_SESSION['user']['uid']
            : (isset($_COOKIE['uid']) ? $_COOKIE['uid'] : '');
        $id = isset($_SESSION['user']['id']) ? (int)$_SESSION['user']['id']
            : (isset($_COOKIE['id']) ? (int)$_COOKIE['id'] : 0);

        if (empty($uid)) {
            return;
        }
        if (empty($id)) {
            return;
        }

        $db = Registry::getDb();
        $db->query("SELECT * FROM users WHERE id = :id AND uid = :uid LIMIT 1",
            array('id' => $id, 'uid' => $uid));

        if ($userData = $db->fetch()) {
            $this->id = (int)$userData['id'];
            $this->login = $userData['login'];
            $this->email = $userData['email'];
            $this->password = $userData['password'];
            $this->register = strtotime($userData['register']);
            $this->lastVisit = strtotime($userData['lastVisit']);
            $this->sex = (int)$userData['sex'];
            $this->status = (bool)$userData['status'];
            $this->group = (int)$userData['group'];
            $this->rights = (int)$userData['rights'];
            $this->uid = $userData['uid'];

            $this->isAuth = true;
            $_SESSION['user']['id'] = $this->id;
            $_SESSION['user']['uid'] = $this->uid;
            if (isset($_COOKIE['id']) && isset($_COOKIE['uid'])) {
                $this->cookie->set('id', $this->id);
                $this->cookie->set('uid', $this->uid);
            }

            // TODO
//			$db->query("
//				UPDATE users
//				SET lastVisit = :lastVisit, uid = :uid
//				WHERE id = :id LIMIT 1",
//				array('lastVisit' => date(STD_DATETIME), 'uid' => $this->uid, 'id' => $this->id));
        }
    }

    private function _generateUid($email, $password)
    {
        do {
            $uid = md5($email . $password . time());
            $invalid = Registry::getDb()
                ->query("SELECT uid FROM users WHERE uid = :uid", array('uid' => $uid))
                ->fetchObject();
        } while ($invalid->uid);
        return $uid;
    }

}
