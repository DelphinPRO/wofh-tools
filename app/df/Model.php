<?php

/**
 * Базовая модель
 *
 * @since              07.01.13 18:52
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

/**
 * Class Model
 *
 * @property Db $db
 * @package df
 */
abstract class Model extends Object
{

    public function __get($name)
    {
        if ($name == 'db') {
            return Registry::getDb();
        }

        return parent::__get($name);
    }
}
