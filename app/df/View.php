<?php

/**
 * Базовое представление
 *
 * @since              07.01.13 22:18
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use df\helper\FS;

abstract class View extends Object
{

//	private static $_jsDir = '/js/';

//	/** @var string Имя шаблона */
//	protected $__layout;

//	/** @var string */
//	protected $__format;

//	/** @var int */
//	protected $__stack;

    /** @var array Переменные для шаблона */
    protected $_dfVars;

//	/** @var array */
//	protected $__widgets;

//	/** @var array */
//	protected $__scripts;

//	private $_notFoundTemplates = array();

    //=======================================================
    //   Public methods
    //=======================================================

//	public function __get($name)
//	{
//		if (isset( $this->_dfVars[$name] )) {
//			return $this->_dfVars[$name];
//		}

    // try load model
//		$result = parent::__get($name);
//		if (is_object($result)) return $result;

//		if ($this->__stack > 0) {
//			for ($i = $this->__stack - 1; $i >= 0; $i--) {
//				if (isset( $this->_dfVars[$i][$name] )) {
//					return $this->_dfVars[$i][$name];
//				}
//			}
//		}
//
//		if ($this->config->app->debug) {
//			$bt = debug_backtrace();
//			trigger_error('Undefined variable: View::$' . $name . ' (' . str_replace(DIR_ROOT, '', $bt[0]['file']) . ':' . $bt[0]['line'] . ')');
//		}

//		return null;
//	}

    public function set($name, $value)
    {
        $this->_dfVars[$name] = $value;
    }

    public function __construct()
    {
        $this->_dfVars = array();
//		$this->__layout       = is_null($layout) ? $this->router->route->action : $layout;
//		$this->__component    = $this->router->route->component;
//		$this->__name         = $this->router->route->controller;
//		$this->__format       = $this->router->route->format;
//		if ($this->__format == 'html') $this->__format = 'phtml';

//		$this->__stack   = 0;
//		$this->__widgets = array();
//		$this->__scripts = array();

//		$this->_breadcrumbs = array();

//		$this->user = User::getCurrent();
    }

//	public function addScript($source)
//	{
//		$this->__scripts[] = trim($source, '/');
//	}

//	public function addJavascript($package, $script)
//	{
//		$this->__scripts[$package][] = trim($script, '/');
//	}

//	public function printJavascript()
//	{
//		if ($this->config->app->debug) {
//			return $this->_printJavascriptAll();
//		} else {
//			return $this->_printJavascriptOne();
//		}
//	}

//	private function _printJavascriptAll()
//	{
//		$html = '';
//		foreach ($this->__scripts as $package => $scripts) {
//			foreach ($scripts as $script) {
//				$html .= '<script src="' . self::$_jsDir . $script . '.js' . '"></script>' . "\n";
//			}
//		}
//		return $html;
//	}

//	private function _printJavascriptOne()
//	{
//		return $this->_printJavascriptAll();
//	}

//	public function printScripts()
//	{
//		$html    = '';
//		$noCache = '?' . substr(md5(WT_VERSION), 0, 8);
//		foreach ($this->__scripts as $script) {
//			if ($this->config->app->debug) {
//				if (!file_exists(_dir('/js' . $script . '.js'))
//					|| filemtime(_dir('/js/~source' . $script . '.src.js')) > filemtime(_dir('/js' . $script . '.js'))
//				) {
//					$full = file_get_contents(_dir('/js/~source' . $script . '.src.js'));
//					$minify = JSMin::minify($full);
//					$minify = $full;
//					if (preg_match('~/\*\*?\!.+\*/~Usi', $full, $m)) {
//						$minify = $m[0] . "\n" . $minify;
//					}
//					file_put_contents(_dir('/js' . $script . '.min.js'), $minify);
//				}
//				$html .= '<script src="/_js/' . $script . '.src.js"></script>' . "\n\t";
//			} else {
//				$html .= '<script src="/js/' . $script . '.js' . $noCache . '"></script>' . "\n\t";
//			}
//		}
//		return $html;
//	}

//	public function addWidget($instance, $position)
//	{
//		if ($instance instanceof Widget) {
//			$this->__widgets[$position][] = $instance;
//		} else {
//			throw new ErrorApp( get_class($instance) . ' is not a widget.' );
//		}
//	}

//	public function getWidgets($position)
//	{
//		return isset( $this->__widgets[$position] ) ? $this->__widgets[$position] : array();
//	}

//	public function configure($name, $value)
//	{
//		$this->$name = $value;
//	}

//	public function render($templateName)
//	{
//		if ($this->router->route->format == 'json') {
//			$data = $this->_dfVars[$this->__stack];
//			unset( $data['title'], $data['wtData'] );
//			return json_encode_utf8($data);
//		}
//
//		$template = DIR_APP
//			. DIRECTORY_SEPARATOR . 'templates'
//			. DIRECTORY_SEPARATOR . $this->config->app->template
//			. DIRECTORY_SEPARATOR . $templateName;
//
//		if (file_exists($template . '.phtml')) {
//			return $this->_render($template);
//		}
//
//		$this->_notFoundTemplates[] = $template;
//
//		$template = FS::path('/app/df/views/' . $templateName);
//		pre($template, 0);
//		pre($this);

//		return $this->_render($template);
//	}

    public function is($property)
    {
        return isset($this->_dfVars[$property]);
    }

    public function isEmpty($property)
    {
        return !isset($this->_dfVars[$property])
        || empty($this->_dfVars[$property]);
    }

    public function e($html)
    {
        return htmlspecialchars($html);
    }

//	/**
//	 * @param      $options
//	 * @param null $selected
//	 * @param null $name
//	 * @param null $attributes
//	 *
//	 * @return string
//	 * @deprecated
//	 */
//	public static function select($options, $selected = null, $name = null, $attributes = null)
//	{
//		$name       = is_null($name) ? '' : ' name="' . htmlspecialchars($name) . '"';
//		$attrString = '';
//
//		if (is_array($attributes)) {
//			foreach ($attributes as $attrName => $attrValue) {
//				$attrString .= ' ' . htmlspecialchars($attrName) . '="' . htmlspecialchars($attrValue) . '"';
//			}
//		}
//
//		$html = '<select' . $name . $attrString . '>';
//		foreach ($options as $optionKey => $optionValue) {
//			$html .= '<option value="' . htmlspecialchars($optionKey) . '">' . htmlspecialchars($optionValue) . '</option>';
//		}
//		$html .= '</select>';
//		return $html;
//	}

    public function getCountTransactions()
    {
        return Registry::getDb()->getCountTransactions();
    }

    public function getTransactions()
    {
        return Registry::getDb()->getTransactions();
    }

    public function getErrorStack()
    {
        global $_ERROR_STACK;
        return $_ERROR_STACK;
    }

    public function getTemplate($template)
    {
        $tmpl = FS::path('/app/df/extensions/templates/'
            . $this->config->get('app.template') . '/'
            . $template);
        return $this->_render($tmpl);
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    final protected function _render($template)
    {
        global $_ERROR_BUFFERING;

//		echo '<code style="border: 1px solid red;line-height:2;">' . str_replace(DIR_ROOT, '', $template) . '.phtml</code>' . "\n";
        $format = '.phtml';
//		$_ERROR_BUFFERING = false;
        ob_start();
        extract($this->_dfVars);
        include $template . $format;
        $content = ob_get_clean();

//		$_ERROR_BUFFERING = true;
        return $content;
    }

    //=======================================================
    //   Private methods
    //=======================================================

//	private function _render($template)
//	{
//		$this->__format = $this->__format == 'html' ? 'phtml' : $this->__format;
//		pre($template, 0);
//		$format = 'phtml';
//		if (!file_exists($template . '.' . $format)) {
//			$this->_notFoundTemplates[] = $template;
//			TODO: admin
//			$tm = FS::path('/app/templates/' . $this->config->app->template . '/' . $this->__layout);
//			pre($tm . '.' . $format, 0);
//			if (!file_exists($tm . '.' . $format)) {
//				$this->_notFoundTemplates[] = $tm . '.' . $format;
//				foreach ($this->_notFoundTemplates as &$dd) {
//					$dd = str_ireplace(DIR_ROOT, '', $dd);
//				}
//				pre($this->_notFoundTemplates);
//				$nf = implode ('<br>File not found: ', $this->_notFoundTemplates);
//				return $this->config->app->debug
//					? "<pre>File not found: " . str_replace(DIR_ROOT, '', $nf)// . '.' . $format
//					. '<br>throw in        ' . str_replace(DIR_ROOT, '', __FILE__) . ' (' . __LINE__ . ') '
//					. '<br>' . Dumper::printDebugBacktrace(debug_backtrace(false)) . '</pre>'
//					: '<pre><b>System error</b><br>1002. File not found.</pre>';
//			}
//			$template = $tm;
//		}
//
//		$_ERROR_BUFFERING = false;
//		ob_start();
//		include $template . '.' . $format;
//		$content                     = ob_get_clean();
//		$_ERROR_BUFFERING = true;
//		return $content;
//	}
}
