-- wofh-tools
-- @since           09.11.2014 18:52
-- @package
-- @author			DelphinPRO delphinpro@yandex.ru
-- @copyright		Copyright (C) 2014 DelphinPRO. All rights reserved.
-- @license         Licensed under the MIT license

SELECT
  acc.accountId,
  acc.accountName,
  acc.accountSex,
  acc.countryId,
  acc.role,
  acc.active,
  acc.extra,
  accData.pop,
  accData.towns,
  accData.science,
  accData.production,
  accData.war,
  country.countryTitle,
  country.countryFlag
FROM z_ruXX_accounts acc
  LEFT JOIN z_ruXX_accounts_stat accData
    ON acc.accountId = accData.accountId
  LEFT JOIN z_ruXX_countries country
    ON acc.countryId = country.countryId
WHERE accData.stateDate = :lastDate
      AND acc.active = 1