-- wofh-tools
-- @since           09.11.2014 20:16
-- @package
-- @author			DelphinPRO delphinpro@yandex.ru
-- @copyright		Copyright (C) 2014 DelphinPRO. All rights reserved.
-- @license         Licensed under the MIT license

SELECT
  country.countryId,
  country.countryTitle,
  country.countryFlag,
  country.extra,
  countryData.pop,
  countryData.accounts,
  countryData.towns,
  countryData.science,
  countryData.production,
  countryData.war

FROM z_ruXX_countries country
  LEFT JOIN z_ruXX_countries_stat countryData
    ON country.countryId = countryData.countryId

WHERE countryData.stateDate = :lastDate
      AND country.active = 1