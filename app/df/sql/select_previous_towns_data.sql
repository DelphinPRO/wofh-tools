SELECT
  d.townId,
  d.accountId AS realAccountId,
  d.pop,
  d.wonderId,
  d.wonderLevel,

  t.townTitle,
  t.accountId,
  t.lost,
  t.destroyed,
  t.extra
FROM z_ruXX_towns_stat AS d
  LEFT JOIN z_ruXX_towns AS t
    ON d.townId = t.townId
WHERE d.stateDate = :lastDate
      AND t.destroyed = 0
