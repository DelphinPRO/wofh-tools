SELECT

  account.*,
  data.*,

  country.countryTitle,
  country.countryFlag

FROM z_ruXX_accounts account
  JOIN z_ruXX_accounts_stat data ON data.accountId = account.accountId
  LEFT JOIN z_ruXX_countries country ON country.countryId = account.countryId

WHERE account.accountId = :accountId
      AND data.stateDate = :stateDate
