<?php
/**
 * @since           12.07.14 19:03
 * @package         DFramework
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df\helper;

class AdjacencyList
{
    private static $tmpData;

    public static function renderUnorderedList($rgData, \Closure $callback)
    {

        $html = '<ul>';
        foreach ($rgData as $item) {
            $children = $item['children'];
            $item['children'] = !empty($children);
            $html .= '<li>';
            ob_start();
            $callback($item);
            $html .= ob_get_clean();
            if (!empty($children)) {
                $html .= self::renderUnorderedList($children, $callback);
            }
            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }

    public static function buildTree($rgData, $start = 0, $idKey = 'id', $idParentKey = 'parent_id')
    {
        self::$tmpData = self::assignKeys($rgData);
        $result = self::buildTreeRecursive($start, $idKey, $idParentKey);
        return $result;
    }

    private static function buildTreeRecursive($start, $idKey, $idParentKey)
    {
        $rgResult = array();

        foreach (self::$tmpData as $item) {
            if ($item[$idParentKey] == $start) {
                $item['children'] = self::buildTreeRecursive($item[$idKey], $idKey, $idParentKey);
                $rgResult[] = $item;
            }
        }

        return empty($rgResult) ? null : $rgResult;
    }

    private static function assignKeys($rgData, $key = 'id')
    {
        $tmp = array();
        foreach ($rgData as $item) {
            $tmp[$item[$key]] = $item;
        }
        return $tmp;
    }
}