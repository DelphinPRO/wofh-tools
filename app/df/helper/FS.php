<?php
/**
 * @since             09.06.14 15:53
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\helper;

class FS
{

    private static $curl;

    public static function loadJSON($filename, $assoc = true, $default = null)
    {
        if (is_file($filename)) {
            $sContent = file_get_contents($filename);
            $rgData = json_decode($sContent, $assoc);
            $JSONLastError = json_last_error();
            if ($JSONLastError == JSON_ERROR_NONE) {
                return $rgData;
            }
            trigger_error('FS::loadJSON(): ' . JSON::getErrorMessage($JSONLastError), E_USER_WARNING);
            return $default;
        }
        $filename = str_replace(array(DIR_ROOT, '\\'), array('', '/'), $filename);
        trigger_error('FS::loadJSON(): File not found [ ' . $filename . ' ]', E_USER_WARNING);
        return $default;
    }

    public static function path($path, $absolute = true)
    {
//		$path = self::_preparePath($path);
        $path = str_replace('/', DIRECTORY_SEPARATOR, ltrim($path, '/'));
        if ($absolute) {
            $path = DIR_ROOT . DIRECTORY_SEPARATOR . $path;
        }
        return $path;
    }

    public static function saveFile($filename, $content, $perms = 0777)
    {
        $status = file_put_contents($filename, $content) !== false;
        if ($status) {
            chmod($filename, $perms);
        }
        return $status;
    }

    public static function readFile($filename)
    {
        return file_get_contents($filename);
    }

    public static function readURL($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        $data = curl_exec($ch);
        self::$curl = array(
            'errno' => curl_errno($ch),
            'error' => curl_error($ch),
        );
        curl_close($ch);
        return $data;
    }

    public static function mkdir($dir, $perms = 0777, $recursive = true)
    {
        if (is_dir($dir)) {
            return true;
        }

        if ($recursive) {
            $chunks = explode(DIRECTORY_SEPARATOR, $dir);
            $path = array_shift($chunks);
            $chunks = array_filter($chunks);

            foreach ($chunks as $chunk) {
                $path .= DIRECTORY_SEPARATOR . $chunk;

                if (!is_dir($path)) {
                    $status = mkdir($path, $perms);

                    if ($status === false) {
                        throw new \Exception('mkdir error');
                    }

                    return chmod($dir, $perms);
                }
            }
        } else {
            $status = mkdir($dir, $perms, false);
            if ($status) {
                throw new \Exception('mkdir error');
            }
            return chmod($dir, $perms);
        }

        return false;
    }

    public static function getCurlError()
    {
        return self::$curl['error'];
    }

    public static function getCurlErrorNo()
    {
        return self::$curl['errno'];
    }
}
