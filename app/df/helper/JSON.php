<?php
/**
 * @since             09.06.14 21:39
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\helper;

class JSON
{

    public static $TAB = "\t";
    public static $PRETTY = true;

    private static $instance;

    public static function encode($in/*, $indent = 0, \Closure $_escape = null*/)
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        $string = self::$instance->_formatJSON($in, 0, null);
        self::$PRETTY = true;
        return $string;
    }

    public static function getErrorMessage($errorCode)
    {
        switch ($errorCode) {
            case JSON_ERROR_NONE:
                return 'No error has occurred';
            case JSON_ERROR_DEPTH:
                return 'The maximum stack depth has been exceeded';
            case JSON_ERROR_STATE_MISMATCH:
                return 'Invalid or malformed JSON';
            case JSON_ERROR_CTRL_CHAR:
                return 'Control character error, possibly incorrectly encoded';
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, Invalid JSON';
            case JSON_ERROR_UTF8:
                return 'Malformed UTF-8 characters, possibly incorrectly encoded';
            default:
                return 'Unknown error';
        }
    }

    private function _formatJSON($in, $indent = 0, \Closure $_escape = null)
    {
        if (__CLASS__ && isset($this)) {
            $_myself = array($this, __FUNCTION__);
        } elseif (__CLASS__) {
            $_myself = array('self', __FUNCTION__);
        } else {
            $_myself = __FUNCTION__;
        }

        if (is_null($_escape)) {
            $_escape = function ($str) {
                return str_replace(
                    array('\\', '"', "\n", "\r", "\b", "\f", "\t", '/', '\\\\u'),
                    array('\\\\', '\\"', "\\n", "\\r", "\\b", "\\f", "\\t", '\\/', '\\u'),
                    $str);
            };
        }

        $out = '';

        $idx = 0;
        $isArray = true;
        foreach ($in as $k1 => $v1) {
            if (!is_numeric($k1) || ((int)$k1 != $idx)) {
                $isArray = false;
                break;
            }
            $idx++;
        }

        $maxLen = 0;
        foreach ($in as $key => $value) {
            $len = strlen((string)$key);
            if ($len > $maxLen) {
                $maxLen = $len;
            }
        }

        foreach ($in as $key => $value) {
            if (self::$PRETTY) {
                $out .= str_repeat(self::$TAB, $indent + 1);
            }
            if (!$isArray) {
                $len = strlen((string)$key);
                $out .= "\"" . $_escape((string)$key) . "\"";
                if (self::$PRETTY) {
                    $out .= str_repeat(" ", $maxLen - $len) . ": ";
                } else {
                    $out .= ":";
                }
            }

            if (is_object($value) || is_array($value)) {
//				$out .= "\n";
                $out .= call_user_func($_myself, $value, $indent + 1, $_escape);
            } elseif (is_bool($value)) {
                $out .= $value ? 'true' : 'false';
            } elseif (is_null($value)) {
                $out .= 'null';
            } elseif (is_string($value)) {
                $out .= "\"" . $_escape($value) . "\"";
            } else {
                $out .= $value;
            }

            if (self::$PRETTY) {
                $out .= ",\n";
            } else {
                $out .= ",";
            }
        }

        if (!empty($out)) {
            if (self::$PRETTY) {
                $out = substr($out, 0, -2);
            } else {
                $out = substr($out, 0, -1);
            }
        }

//		$out = str_repeat("\t", $indent) . "{\n" . $out;
        if (self::$PRETTY) {
            if ($isArray) {
                $out = "[\n" . $out;
                $out .= "\n" . str_repeat(self::$TAB, $indent) . "]";
            } else {
                $out = "{\n" . $out;
                $out .= "\n" . str_repeat(self::$TAB, $indent) . "}";
            }
        } else {
            if ($isArray) {
                $out = "[" . $out . "]";
            } else {
                $out = "{" . $out . "}";
            }
        }

        return $out;
    }

    public static function decode($json, $assoc = true, $depth = 512)
    {
        return \json_decode($json, $assoc, $depth);
    }
}