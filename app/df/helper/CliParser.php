<?php
/**
 * @since              09.12.13 23:01
 * @author             EuGen
 * @link                http://forum.php.su/topic.php?forum=35&topic=793&postid=1361196221
 */

namespace df\helper;

class CliParser
{

    const PARSER_DELIMITER_COMMAND = ' ';
    const PARSER_DELIMITER_VALUE = '=';
    const PARSER_MARKER_SHORTKEY = '-';
    const PARSER_MARKER_LONGKEY = '--';

    protected $_rgSynonyms = null;
    protected $_sData = '';

    public function __construct($sData = null)
    {
        $this->_sData = isset($sData) ? (string)$sData : join(' ', $_SERVER['argv']);
    }

    /**
     * @param array $rgSynonyms - an array that contains synonyms.
     *     Each row of $rgSynonyms contains an array in format [synonym]=>array(key0,key1,...)
     */
    public function setSynonyms($rgSynonyms)
    {
        $this->_rgSynonyms = $rgSynonyms;
    }

    /**
     * @param string $sData - optional string to parse
     * @param bool $bAllowSynonyms - including synonyms flag, true by default
     *
     * @return mixed
     */
    public function parseString($sData = null, $bAllowSynonyms = true)
    {
        $sData = isset($sData) ? (string)$sData : $this->_sData;
        if (!$sData) {
            return null;
        }
        $rgResult = array();
        $rgCLI = preg_split('/[' . preg_quote(self::PARSER_DELIMITER_COMMAND) . ']+/', $sData, -1, PREG_SPLIT_NO_EMPTY);
        foreach ($rgCLI as $sCLI) {
            $rgValue = preg_split('/[' . preg_quote(self::PARSER_DELIMITER_VALUE) . ']+/', $sCLI, -1,
                PREG_SPLIT_NO_EMPTY);
            if (preg_match('/^' . preg_quote(self::PARSER_MARKER_LONGKEY) . '(.+)$/', $rgValue[0], $rgMatches)) {
                $rgResult[$rgMatches[1]] = isset($rgValue[1]) ? $rgValue[1] : true;
            } elseif (preg_match('/^' . preg_quote(self::PARSER_MARKER_SHORTKEY) . '(.+)$/', $rgValue[0], $rgMatches)) {
                foreach (str_split($rgMatches[1]) as $sKey) {
                    $rgResult[$sKey] = isset($rgValue[1]) ? $rgValue[1] : true;
                }
            }
        }
        if ($bAllowSynonyms && isset($this->_rgSynonyms) && is_array($this->_rgSynonyms)) {
            array_walk($this->_rgSynonyms, function ($rgSynonym, $sKey) use (&$rgResult) {
                foreach ($rgSynonym as $sSynonym) {
                    if (array_key_exists($sSynonym, $rgResult)) {
                        $rgResult[$sKey] = $rgResult[$sSynonym];
                        array_walk($rgSynonym, function ($sOldKey) use (&$rgResult) {
                            unset($rgResult[$sOldKey]);
                        });
                        break;
                    }
                }
            });
        }
        return $rgResult;
    }
}
