<?php

/**
 * @since             31.01.14 08:05
 * @package           DFramework
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df;

use df\exception\CliException;
use df\exception\DFException;
use df\helper\CliParser;
use df\helper\FS;
use df\Console;

define('CLI_MODE', true);

/**
 * Command line interface
 * Class CliApp
 *
 * @package df
 */
class CliApp extends Model
{
    public static $sendEmail;

    private static $_instance;
    private static $_isRunning = false;

    /** @var CliParser */
    private $Parser;

    /** @var array */
    private $args;

    /**
     * @return CliApp
     */
    public static function Inst()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    private function __construct()
    {
        CliApp::$sendEmail = false;
    }

    public function run()
    {
        if (self::$_isRunning) {
            return;
        }
        self::$_isRunning = true;

        if (PHP_SAPI != 'cli') {
            return;
        }

        $this->_start();
        $hasErrors = false;

        try {

            if (empty($this->args) || !isset($this->args['cmd'])) {
                $this->_help();
            }

            $commands = explode('/', $this->args['cmd'], 2);
            $action = strtolower(isset($commands[1]) ? $commands[1] : $commands[0]);
            $controller = ucfirst(strtolower($commands[0]));
            $className = "\\df\\cli\\{$controller}Controller";

            if (file_exists(FS::path('/app/cli/' . $controller . 'Controller.php'))) {
                require_once FS::path('/app/cli/' . $controller . 'Controller.php');
            }

            if (!class_exists($className)) {
                throw new CliException("Invalid command: Controller not exists ($controller)");
            }

            $controller = new $className($this->args);

            if (!method_exists($controller, $action)) {
                throw new CliException("Invalid command: Method not exists ($action)");
            }

            $controller->$action();
        } catch (CliException $e) {
            CliApp::$sendEmail = true;
            $hasErrors = true;
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }
            Console::log("\n-----------------------------------------------");
            Console::log($e->getMessage());
            Console::log($e->getFile() . ":" . $e->getLine());
            Console::log($e->getTraceAsString());
        } catch (\PDOException $e) {
            CliApp::$sendEmail = true;
            $hasErrors = true;
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }
            Console::log("\n-----------------------------------------------");
            Console::log($e->getMessage());
            Console::log($e->getFile() . ":" . $e->getLine());
            Console::log($e->getTraceAsString());
        } catch (DFException $e) {
            CliApp::$sendEmail = true;
            $hasErrors = true;
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }
            Console::log("\n-----------------------------------------------");
            Console::log($e);
        }
        $sendEmail = isset($this->args['cmd']) && $this->args['cmd'] == 'error' && $hasErrors;
        $sendEmail = isset($this->args['cmd']) && $this->args['cmd'] == 'success' && !$hasErrors ? true : $sendEmail;

        $this->_halt($sendEmail, $hasErrors);
        exit;
    }

    private function _start()
    {
        $this->Parser = new CliParser();
        $this->Parser->setSynonyms(array(
            'Output' => array('o', 'output'),
            'Email' => array('e', 'email'),
        ));

        $this->args = $this->Parser->parseString();

        if (isset($this->args['output']) || empty($this->args)) {
//			CliApp::$stdOut = true;
        }

        Console::line();
        Console::log($this->config->get('app.site_name') . ' v.' . WT_VERSION);
        Console::log(date('d-m-Y H:i:s') . ' / ' . date_default_timezone_get());
        Console::line();
    }

    private function _halt($sendEmail = true, $hasErrors = false)
    {
        $totalTime = get_execution_time();
        if ($totalTime > 60) {
            $min = floor($totalTime / 60);
            $sec = round($totalTime % 60, 0);
            $totalTime = $min . ' m ' . $sec;
        }
        Console::line();
        Console::log("total time   : " . $totalTime . " s.");
        Console::log("memory usage : "
            . preg_replace('~(\d(?=(?:\d{3})+(?!\d)))~s', "\\1 ", memory_get_peak_usage(true))
            . " bytes of " . ini_get('memory_limit'));
        Console::log("transactions : " . $this->db->getCountTransactions());
        Console::doubleLine();
        Console::log('');

        if ($sendEmail) {
            $mailSubject = 'wofh-tools.ru: [' . ($hasErrors ? 'OK' : 'ERROR') . '] cron/' . $this->args['cmd'];
            $headers = 'From: cron@wofh-tools.ru' . "\r\n"
                . 'X-Mailer: Wofh-Tools.ru' . "\r\n"
                . 'Content-type: text/plain; charset=utf-8';

            if (CliApp::$sendEmail) {
                mail($this->config->get('cli.admin_email'), $mailSubject, join('', Console::$buffer), $headers);
            }
        }
    }

    private function _help()
    {
        if (empty($this->args)) {
//			CliApp::$stdOut = true;
        }

//		TODO: HELP
        Console::line();
        Console::log('TODO: HELP');
        Console::log('Output on             : --output, -o');
        Console::log('Send email            : --email, -e');
        Console::log('Select world          : --world=ru21');
        Console::log('Load statistic        : --cmd=stat/load');
        Console::log('Update statistic      : --cmd=stat/update');
        Console::log('Limit update          : --limit=15');
        Console::log('Send email            : --email=[error|success]');
        Console::log('For dev (paths)       : --dev');
        Console::log('Install               : --cmd=install/install');
        $this->_halt(false);
        exit;
    }

}
