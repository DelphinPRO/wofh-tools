<?php
/**
 * @since             09.07.14 14:45
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\system\controllers;

use df\Controller;

class Error404Controller extends Controller
{
    public function display()
    {
        $this->document->setPageTitle('Страница не найдена');
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }
}
