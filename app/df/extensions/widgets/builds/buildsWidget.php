<?php
/**
 * @since           23.11.2014 15:28
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright        Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df\extensions\widgets\builds;

use df\Controller;
use df\extensions\models\GameData;
use df\Widget;

class buildsWidget extends Widget
{
    //=======================================================
    //   Public methods
    //=======================================================

    public function callWidget(Controller $call)
    {
        $world = $call->getCurrentWorld();
        $game = new GameData();
        $builds = $game->getBuilds($world);

        $this->set('builds', $builds);
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    //=======================================================
    //   Private methods
    //=======================================================

}