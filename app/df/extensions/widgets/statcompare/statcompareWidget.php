<?php
/**
 * @since       02.03.2016 20:35
 * @author      delphinpro delphinpro@yandex.ru
 * @copyright   copyright (C) 2016 delphinpro
 * @license     Licensed under the MIT license
 */

namespace df\extensions\widgets\statcompare;

use df\Controller;
use df\extensions\models\Statistic;
use df\extensions\models\Wofh;
use df\Widget;

/**
 * Class statcompareWidget
 */
class statcompareWidget extends Widget
{

    public function callWidget(Controller $call)
    {
        $sign = $call->get('_sign');
        $world = $call->get('_world');
        $stateDate = $call->get('_stateDate');

        $rating = $this->request->get->offsetGet('rating', '');
        $params = array('sign' => $world->lowerSign);
        if ($rating) {
            $params['rating'] = $rating;
        }

        $wtData = $this->document->get('wtData');
        $wtData['stat']['compareLink'] = $this->router->link('compare', $params);
        $wtData['stat']['addCompareLink'] = $this->router->link('addCompare', array('sign' => $world->lowerSign));
        $wtData['stat']['clearCompareLink'] = $this->router->link('clearCompare', array('sign' => $world->lowerSign));
        $this->document->set('wtData', $wtData);

        $this->set('compareLink', $this->router->link('compare', $params));
        $this->set('addCompareLink', $this->router->link('addCompare', array('sign' => $world->lowerSign)));
        $this->set('clearCompareLink', $this->router->link('clearCompare', array('sign' => $world->lowerSign)));

        if (!empty($_SESSION['wt.stat.compare'][$sign]['accounts'])) {
            $ids = $_SESSION['wt.stat.compare'][$sign]['accounts'];
            $statistic = new Statistic();
            $data = $statistic->getAccountsToCompare($world, $stateDate, $ids);
            $this->set('compare', $data);
        } else {
            $this->set('compare', null);
        }

        //$this->set('selectedWorld', $world);
        //$this->set('selectedWorldId', $sign ? $world->id : 0);
        //$this->set('selected', $this->router->route->action);
    }
}