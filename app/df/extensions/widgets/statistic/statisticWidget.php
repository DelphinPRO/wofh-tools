<?php
/**
 * @since              11.12.13 23:54
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\widgets\statistic;

use df\Controller;
use df\extensions\models\Wofh;
use df\Widget;

/**
 * Class statisticWidget
 */
class statisticWidget extends Widget
{

    public function callWidget(Controller $call)
    {
//		$w = ;
//		pre($w);
        $this->set('worlds', Wofh::getWorlds(array('WORKING', 'STATISTIC')));
        $this->set('isArchive', $call->get('_isArchive'));
        $sign = $call->get('_sign');
        $world = $call->get('_world');

        $this->set('selectedWorld', $world);
        $this->set('selectedWorldId', $sign ? $world->id : 0);
        $this->set('selected', $this->router->route->action);
//		if ($this->_dfLayout == 'calendar') {
//			$sign  = isset( $_GET['sign'] ) ? strtolower($_GET['sign']) : '';
//			$world = Wofh::getWorldBySign($sign);
//			$stateTime = !is_null($world->updateStat) ? date('d.m.Y H:i', $world->updateStat) : date('d.m.Y H:i');
//			$this->set('stateTime', $stateTime);
//			$this->set('prevLink', '');
//		}
//		pre($this);
    }
}