<?php
/**
 * @since             18.01.14 04:45
 * @package           Wofh-Tools
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */
namespace df\extensions\models;

use df\Model;

/**
 * @property Wofh wofh
 */
class ScienceReport0 extends Model
{
    const SC_STATUS_NONE = 0;
    const SC_STATUS_STARTED = 1;
    const SC_STATUS_LEARN = 2;

    public
        $id,
        $worldId,
        $generateTime,
        $accountId,
        $accountName,
        $countryId,
        $countryName,
        $countryFlag,
        $version,
        $scCurrent,
        $scKnown,
        $scStarted,
        $scNext,
        $validKey,
        $verified;

    private $scList;


    public function getReport($key)
    {
        return $this->_createReport($key);
    }

    private function _createReport($key)
    {

        if (!$this->scList) {
            $this->scList = $this->db->query("
			SELECT *
			FROM cd_science
			WHERE worldId = :worldId
			ORDER BY id
		", array('worldId' => $logData['worldId']))->fetchByField('id');
        }

        $learnCount = 0;

        $logData['worldId'] = (int)$logData['worldId'];
        $logData['playerId'] = (int)$logData['playerId'];
        $logData['scStarted'] = json_decode($logData['scStarted'], true);
        $logData['scCurrent'] = json_decode($logData['scCurrent'], true);
        $logData['scKnownOrig'] = json_decode($logData['scKnown'], true);
        $logData['scKnown'] = json_decode($logData['scKnown'], true);
        $logData['scNext'] = json_decode($logData['scNext'], true);
        $logData['investedTotal'] = (int)$logData['investedTotal'];
        $logData['markupTotal'] = (int)$logData['markupTotal'];
        $logData['markupNext'] = (int)$logData['markupNext'];
        $logData['countTotal'] = (int)$logData['countTotal'];
        $logData['countryId'] = !is_null($logData['countryId']) ? (int)$logData['countryId'] : null;
        $logData['verified'] = (bool)(int)$logData['verified'];

        foreach ($logData['scKnown'] as $id => $status) {
            $invested = 0;
            $learnAvail = false;
            $learnStatus = self::SC_STATUS_NONE;
            $rowCssClass = 'sc-none';
            if ($status > 0) {
                if ($status == 2) {
                    $learnStatus = self::SC_STATUS_LEARN;
                    $rowCssClass = 'learn';
                    $invested = (int)$this->scList[$id]['cost'];
                }
                if ($status == 1) {
                    $learnAvail = true;
                    if (isset($logData['scStarted'][$id])) {
                        $learnStatus = self::SC_STATUS_STARTED;
                        $learnAvail = false;
                        $rowCssClass = 'sc-started';
                        $invested = $logData['scStarted'][$id];
                    }
                    if (isset($logData['scCurrent']['id']) && ($logData['scCurrent']['id'] == $id)) {
                        $learnStatus = self::SC_STATUS_STARTED;
                        $learnAvail = false;
                        $rowCssClass = 'sc-current';
                        $invested = $logData['scCurrent']['progress'];
                    }
                }
                if (!$learnAvail) {
                    $learnCount++;
                }
            }
            $logData['scKnown'][$id] = array(
                'id' => $id,
                'orig' => $status,
                'learnStatus' => $learnStatus,
                'learnAvail' => $learnAvail,
                'rowCssClass' => $rowCssClass,
                'title' => $this->scList[$id]['title'],
                'cost' => (int)$this->scList[$id]['cost'],
                'invested' => $invested,
            );
        }

        usort($logData['scKnown'], function ($a, $b) {
            if ($a['cost'] == $b['cost']) {
                return 0;
            }
            return ($a['cost'] > $b['cost']);
        });

        return $logData;
    }

    public function getValidId()
    {
        $i = 0;
        do {
            $id = substr(strtolower(md5(uniqid())), 0, 12);
            if ($i++ > 100) {
                die('System error: Invalid ID.');
            }
        } while ((int)$this->db->simpleQuery("SELECT COUNT(*) FROM wt_reports_science WHERE id = '{$id}'")->fetchColumn());
        return $id;
    }

    public function saveReport($id, $key, $sData)
    {

        $this->db->setPrefix('ruXX', $world->lowerSign);
        $this->db->query("
			SELECT c.countryId, c.countryTitle, c.countryFlag
			FROM z_ruXX_accounts acc
			LEFT JOIN z_ruXX_countries c
				ON c.countryId = acc.countryId
			WHERE acc.accountId = :accountId",
            array(
                'accountId' => (int)$rgJSON['account'][0],
            ));
        $c = $this->db->fetch();

        $scList = $this->db->query("SELECT id, cost FROM cd_science WHERE worldId = :worldId",
            array('worldId' => $worldId))->fetchByField('id');

        $scStarted = isset($rgJSON['science']['started']) ? $rgJSON['science']['started']
            : (isset($rgJSON['started']) ? $rgJSON['started'] : null);

        $scCurrent = isset($rgJSON['science']['current'])
            ? $rgJSON['science']['current']
            : (isset($rgJSON['current'][0]) ? $rgJSON['current'][0] : null);

        $scKnown = isset($rgJSON['science']['known']) ? $rgJSON['science']['known'] : $rgJSON['known'];

        $investedTotal = 0;
        $countTotal = 0;
        foreach ($scKnown as $scId => $scStatus) {
            if ($scStatus == 2) {
                $investedTotal += $scList[$scId]['cost'];
                $countTotal++;
            }
        }

        if (is_array($scStarted)) {
            foreach ($scStarted as $invested) {
                $investedTotal += (int)$invested;
            }
        }

        $investedTotal += isset($scCurrent['progress']) ? (int)$scCurrent['progress'] : 0;

        $k0 = 0.0;
        $k1 = 0.0;
        $k2 = 80.0;
        $k3 = 1.299999952316284;
        $markupTotal = 0;
        for ($i = 1; $i < $countTotal; $i++) {
            $m = round($k1 + $k2 * pow($i, $k3));
            $d = pow(10, strlen((string)$m) - 2);
            $markup = floor($m / $d) * $d;
            $markupTotal += (int)$markup;
        }
        $m = round($k1 + $k2 * pow($countTotal, $k3));
        $d = pow(10, strlen((string)$m) - 2);
        $markupNext = (int)floor($m / $d) * $d;

        $scNext = isset($rgJSON['science']['next'])
            ? json_encode($rgJSON['science']['next'])
            : (isset($rgJSON['next']) ? $rgJSON['next'] : null);

        $reportData = array(
            'id' => $id,
            'worldId' => (int)$worldId,
            'generateTime' => date(STD_DATETIME, $rgJSON['generatetime']),
            'accountId' => (int)$rgJSON['account'][0],
            'accountName' => $rgJSON['account'][1],
            'countryId' => isset($c['countryId']) ? (int)$c['countryId'] : null,
            'countryName' => isset($c['countryName']) ? $c['countryName'] : null,
            'countryFlag' => isset($c['countryFlag']) ? $c['countryFlag'] : null,
            'version' => $rgJSON['version'],
            'scCurrent' => json_encode($scCurrent),
            'scKnown' => json_encode($scKnown),
            'scStarted' => json_encode($scStarted),
            'scNext' => $scNext,
            'investedTotal' => $investedTotal,
            'markupTotal' => $markupTotal,
            'markupNext' => $markupNext,
            'countTotal' => $countTotal,
            'validKey' => $key,
            'verified' => $verified
        );

        dump($rgJSON, 0);
        dump($reportData);

        $this->db->prepare("
			INSERT INTO wt_reports_science
			SET
				id            = :id,
				worldId       = :worldId,
				generateTime  = :generateTime,
				accountId     = :accountId,
				accountName   = :accountName,
				countryId     = :countryId,
				countryName   = :countryName,
				countryFlag   = :countryFlag,
				version       = :version,
				scCurrent     = :scCurrent,
				scKnown       = :scKnown,
				scStarted     = :scStarted,
				scNext        = :scNext,
				investedTotal = :investedTotal,
				markupTotal   = :markupTotal,
				markupNext    = :markupNext,
				countTotal    = :countTotal,
				validKey      = :validKey,
				verified      = :verified
			");
        $this->db->execute($reportData);

        if (!$verified) {
//			$this->addMessage('Ваш отчет не прошел проверку подлинности.', App::ERROR);
        }
        return true;
    }
}
