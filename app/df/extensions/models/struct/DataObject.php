<?php
/**
 * @since             11.11.2014 21:15
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\struct;

/**
 * Class DataObject
 *
 * @package df\extensions\models\struct
 * @ p roperty $date
 */
class DataObject
{
    protected $_date;

    public function __get($name)
    {
        $property = '_' . $name;
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        trigger_error('Undefined variable: ' . $property, E_USER_NOTICE);
        return null;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->_date = $date;
    }

}
