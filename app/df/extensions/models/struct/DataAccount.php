<?php
/**
 * @since             15.11.2014 14:47
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\struct;

use df\extensions\models\World;
use df\Model;

class DataAccount extends Model
{
    public $id;
    public $name;
    public $race;
    public $sex;
    public $role;
    public $active;
    public $extra;
    public $stateDate;
    public $pop;
    public $towns;
    public $science;
    public $production;
    public $war;
    public $deltaPop;
    public $deltaTowns;
    public $deltaScience;
    public $deltaProduction;
    public $deltaWar;

    public $deltaPopWeek;
    public $deltaAccountsWeek;
    public $deltaTownsWeek;
    public $deltaScienceWeek;
    public $deltaProductionWeek;
    public $deltaWarWeek;

    public $deltaPopMonth;
    public $deltaAccountsMonth;
    public $deltaTownsMonth;
    public $deltaScienceMonth;
    public $deltaProductionMonth;
    public $deltaWarMonth;

    private $_roleNames = array();
    private $_roleMnemonics = array();

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct()
    {
        $this->id = (int)$this->accountId;
        $this->name = $this->accountName;
        $this->race = (int)$this->accountRace;
        $this->sex = (int)$this->accountSex;
        $this->role = (int)$this->role;
        $this->active = (bool)(int)$this->active;
        $this->extra = json_decode($this->extra, true);
        $this->pop = (int)$this->pop;
        $this->towns = (int)$this->towns;
        $this->science = (float)$this->science;
        $this->production = (float)$this->production;
        $this->war = (float)$this->war;
        $this->deltaPop = (int)$this->deltaPop;
        $this->deltaTowns = (int)$this->deltaTowns;
        $this->deltaScience = (float)$this->deltaScience;
        $this->deltaProduction = (float)$this->deltaProduction;
        $this->deltaWar = (float)$this->deltaWar;

        unset($this->accountId, $this->accountName, $this->accountRace, $this->accountSex);

        if ($this->role & 1) {
            $this->_roleNames[] = 'Глава';
            $this->_roleMnemonics[] = '<img src="/img/icons/country.png" height="20" alt="Глава" title="Глава">';
        }
        if ($this->role & 2) {
            $this->_roleNames[] = 'Мудрец';
            $this->_roleMnemonics[] = '<img src="/img/icons/science.png" height="20" alt="Мудрец" title="Мудрец">';
        }
        if ($this->role & 4) {
            $this->_roleNames[] = 'Воевода';
            $this->_roleMnemonics[] = '<img src="/img/icons/war1.png" height="20" alt="Воевода" title="Воевода">';
        }
        if ($this->role & 8) {
            $this->_roleNames[] = 'Казначей';
            $this->_roleMnemonics[] = '<img src="/img/icons/money.png" height="20" alt="Казначей" title="Казначей">';
        }
        if ($this->role & 16) {
            $this->_roleNames[] = 'Помощник воеводы';
            $this->_roleMnemonics[] = '<img src="/img/icons/war.png" height="20" alt="Помощник воеводы" title="Помощник воеводы">';
        }
        if ($this->role & 32) {
            $this->_roleNames[] = 'Второй пом. воеводы';
            $this->_roleMnemonics[] = '<img src="/img/icons/war.png" height="20" alt="Второй пом. воеводы" title="Второй пом. воеводы">';
        }
        if ($this->role & 64) {
            $this->_roleNames[] = '?(64)';
            $this->_roleMnemonics[] = '<img src="/img/icons/unknown.png" height="20" alt="?(64)" title="?(64)">';
        }
        if ($this->role & 128) {
            $this->_roleNames[] = '?(128)';
            $this->_roleMnemonics[] = '<img src="/img/icons/unknown.png" height="20" alt="?(128)" title="?(128)">';
        }

    }

    public function getLink(World $world)
    {
        return '<a class="acc acc-' . $this->race . '-' . $this->sex . '"' .
        ' href="' . $this->router->link('statAccount', array(
            'sign' => $world->lowerSign,
            'accountId' => $this->id,
        )) . '">' . $this->name . '</a>';
    }

    public function getLinkExternal(World $world)
    {
        return $world->working
            ? '<a href="' . $world->domain . '/account?id=' . $this->id . '"' .
            ' class="wofh-link icon-link-ext" target="_blank"></a>'
            : '';
    }

    public function getRole()
    {
        return join(', ', $this->_roleNames);
    }

    public function getRoleAsMnemonic()
    {
        return join(' ', $this->_roleMnemonics);
    }

    public function getRace()
    {
        $races = array('Индеец', 'Европеец', 'Азиат', 'Африканец');
        return $races[$this->race];
    }

    public function getAge()
    {
        $d = new \DateTime('@' . $this->extra['create']);
        $i = $d->diff(new \DateTime($this->stateDate));
        return $i->days . pluralForm($i->days, array(' день', ' дня', ' дней'));
    }

    public function loadWeekData($stateDate)
    {
        $data = $this->db->query("
				SELECT d.pop, d.towns, d.science, d.production, d.war
				FROM z_ruXX_accounts_stat d
				WHERE d.countryId = '{$this->id}'
					AND (d.stateDate = '{$stateDate}' - INTERVAL 1 WEEK)
				")->fetch();

        $this->deltaPopWeek = $this->pop - (int)$data['pop'];
        $this->deltaTownsWeek = $this->towns - (int)$data['towns'];
        $this->deltaScienceWeek = $this->science - (int)$data['science'];
        $this->deltaProductionWeek = $this->production - (int)$data['production'];
        $this->deltaWarWeek = $this->war - (int)$data['war'];
    }

    public function delta($number, $null = '—')
    {
        return '<span class="' . getStatusClass($number) . '">' . getSignedNumber($number, $null) . '</span>';
    }

    public function format($number)
    {
        return numFormat($number);
    }

    public function loadMonthData($stateDate)
    {
        $data = $this->db->query("
				SELECT d.pop, d.towns, d.science, d.production, d.war
				FROM z_ruXX_accounts_stat d
				WHERE d.countryId = '{$this->id}'
					AND (d.stateDate = '{$stateDate}' - INTERVAL 1 MONTH)
				")->fetch();

        $this->deltaPopMonth = $this->pop - (int)$data['pop'];
        $this->deltaTownsMonth = $this->towns - (int)$data['towns'];
        $this->deltaScienceMonth = $this->science - (int)$data['science'];
        $this->deltaProductionMonth = $this->production - (int)$data['production'];
        $this->deltaWarMonth = $this->war - (int)$data['war'];
    }
    //=======================================================
    //   Protected methods
    //=======================================================

    //=======================================================
    //   Private methods
    //=======================================================

}
