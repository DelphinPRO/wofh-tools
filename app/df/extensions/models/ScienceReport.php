<?php
/**
 * @since             26.11.2014 04:09
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models;

use df\exception\SciReportException;
use df\Model;
use df\Registry;

class ScienceReport extends Model
{

    const ID_LENGTH = 12;

    const SC_STATUS_NONE = 0;
    const SC_STATUS_STARTED = 1;
    const SC_STATUS_LEARN = 2;

    public
        $id,
        $worldId,
        $generateTime,
        $accountId,
        $accountName,
        $accountSex,
        $accountRace,
        $countryId,
        $countryName,
        $countryFlag,
        $version,
        $science,
        $scCurrent,
        $scKnown,
        $scStarted,
        $scNext,
        $validKey,
        $verified,
        $clientIP,
        $clientIpInt;

    public $exportString = '';

    private $errorCode;

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct()
    {
        $this->worldId = (int)$this->worldId;
        $this->generateTime = strtotime($this->generateTime);
        $this->accountId = (int)$this->accountId;
        $this->accountSex = (int)$this->accountSex;
        $this->accountRace = (int)$this->accountRace;
        $this->countryId = (int)$this->countryId;
        $this->scCurrent = json_decode($this->scCurrent, true);
        $this->scKnown = json_decode($this->scKnown, true);
        $this->scStarted = json_decode($this->scStarted, true);
        $this->scNext = (int)$this->scNext;
        $this->verified = (bool)(int)$this->verified;
        $this->clientIpInt = (int)$this->clientIpInt;

        $this->science = array();
    }

    public static function getLogPerDay()
    {
        $db = Registry::getDb();
        $db->query("SELECT COUNT(*) AS countLogs FROM wt_reports_science WHERE generateTime >= :now - INTERVAL 1 DAY",
            array('now' => date('Y-m-d H:i:s')));
        return $db->fetchObject()->countLogs;
    }

    public static function getLogToday()
    {
        $db = Registry::getDb();
        $db->query("SELECT COUNT(*) AS countLogs FROM wt_reports_science WHERE generateTime >= :now",
            array('now' => date('Y-m-d')));
        return $db->fetchObject()->countLogs;
    }

    public static function getLogList()
    {
        $db = Registry::getDb();
        $where = '';
        $db->query("SELECT *,
				clientIP AS clientIpInt,
				INET_NTOA(clientIP) AS clientIP
				FROM wt_reports_science $where ORDER BY generateTime DESC LIMIT 0, 20");
        return $db->fetchAll();
    }

    public static function getCountryAccounts($countryId, $worldId)
    {
        try {
            $world = Wofh::getWorld($worldId);
            $db = Registry::getDb();
            $db->setPrefix('ruXX', $world->lowerSign);
            $db->query("
				SELECT COUNT(*) AS countAccounts
				FROM z_ruXX_accounts
				WHERE countryId = :countryId",
                array('countryId' => $countryId));
            $count = (int)$db->fetchObject()->countAccounts;
            return $count ?: 1;
        } catch (\PDOException $e) {
            return 1;
        }
    }

    public static function getRelativeReports()
    {
        $accountId = isset($_SESSION['wt.science.account.id']) ? (int)$_SESSION['wt.science.account.id'] : 0;
        $worldId = isset($_SESSION['wt.science.world.id']) ? (int)$_SESSION['wt.science.world.id'] : 0;
        $clientIp = isset($_SESSION['wt.science.client.ip']) ? $_SESSION['wt.science.client.ip'] : '';
        $world = Wofh::getWorld($worldId);

        if (!$accountId || !$worldId || !$clientIp || !$world) {
            return array();
        }

        $db = Registry::getDb();
        $db->setPrefix('ruXX', $world->lowerSign);
        $db->query("
			SELECT *,
				clientIP AS clientIpInt,
				INET_NTOA(clientIP) AS clientIP
			FROM wt_reports_science
			WHERE worldId = :worldId
			AND accountId = :accountId
			AND clientIP = INET_ATON(:clientIp)
			",
            array(
                'accountId' => $accountId,
                'worldId' => $worldId,
                'clientIp' => $clientIp,
            )
        );
        $reports = array();
        while ($r = $db->fetchObject(__NAMESPACE__ . '\\ScienceReport')) {
            $reports[] = $r;
        }
        return $reports;

    }

    /**
     * @param string $id
     *
     * @throws SciReportException
     * @return ScienceReport
     */
    public static function Load($id)
    {
        $db = Registry::getDb();
        $db->query(
            "SELECT *,
				clientIP AS clientIpInt,
				INET_NTOA(clientIP) AS clientIP
			FROM wt_reports_science WHERE id = :id LIMIT 1",
            array('id' => $id)
        );

        /** @var ScienceReport $report */
        $report = $db->fetchObject(__NAMESPACE__ . '\\ScienceReport');

        if (!$report) {
            throw new SciReportException("Report [$id] not found", 1);
        }

        $report->_prepare();
        return $report;
    }

    /**
     * @param $sData
     *
     * @return ScienceReport
     * @throws SciReportException
     */
    public static function Create($sData)
    {
        $rgData = json_decode(urldecode($sData), true);
        if (!isset($rgData['version'])) {
            $rgData['version'] = '1.3';
        }
        if (!isset($rgData['generatetime'])) {
            $rgData['generatetime'] = time();
        }
        if ((json_last_error() != JSON_ERROR_NONE)
            || !isset($rgData['account'])
            || !isset($rgData['domain'])
//			|| !isset( $rgData['generatetime'] )
            || !isset($rgData['science'])
            || !isset($rgData['version'])
        ) {
            throw new SciReportException("Invalid data");
        }

        $report = new self($rgData);

        $worldId = Wofh::domainToId($rgData['domain']);

        $report->id = $report->getValidId();
        $report->worldId = $worldId;
        $report->generateTime = date(STD_DATETIME, $rgData['generatetime']);
        $report->accountId = $rgData['account'][0];
        $report->accountName = $rgData['account'][1];
        $report->accountSex = $rgData['account'][2];
        $report->accountRace = $rgData['account'][3];
        $report->countryId = isset($rgData['country']['id']) ? $rgData['country']['id'] : null;
        $report->countryName = isset($rgData['country']['name']) ? $rgData['country']['name'] : null;
        $report->countryFlag = isset($rgData['country']['flag']) ? $rgData['country']['flag'] : null;
        $report->version = $rgData['version'];
        $report->scCurrent = $rgData['science']['current'];
        $report->scKnown = $rgData['science']['known'];
        $report->scStarted = $rgData['science']['started'];
        $report->scNext = $rgData['science']['next'];
        $report->validKey = '';
        $report->verified = true;
        $report->clientIP = $_SERVER['REMOTE_ADDR'];

        $report->_prepare();

        return $report;
    }

    private function _prepare()
    {
        $world = Wofh::getWorld($this->worldId);
        $game = new GameData();
        $science = $game->getScience($world);
//		dmp($science);

        if (!isset($science[$this->scNext])) {
            $this->scNext = null;
        }

        foreach ($this->scKnown as $id => $status) {
            $progress = 0;
            $cssClass = '';

            if ($status == self::SC_STATUS_LEARN) {
                $progress = $science[$id]['cost'];
                $cssClass = 'sc-complete';
            }
            if ($status == self::SC_STATUS_STARTED) {
                if ($id == $this->scCurrent['id']) {
                    $progress = isset($this->scCurrent['progress']) ? $this->scCurrent['progress'] : 0;
                    $cssClass = 'sc-current';
                } else {
                    $progress = isset($this->scStarted[$id]) ? $this->scStarted[$id] : 0;
                    $cssClass = $progress ? 'sc-started' : 'sc-take';
                }
            }

            $this->science[$id] = array(
                'id' => $id,
                'status' => $status,
                'title' => $science[$id]['title'],
                'cost' => $science[$id]['cost'],
                'progress' => $progress,
                'current' => $id == $this->scCurrent['id'],
                'rowCssClass' => $cssClass,
                'bonuses' => $science[$id]['bonuses'],
            );
        }

        usort($this->science, function ($a, $b) {
            if ($a['cost'] == $b['cost']) {
                return 0;
            }
            return ($a['cost'] < $b['cost']) ? 1 : -1;
        });

        $this->exportString = rawurlencode(json_encode(array(
            'account' => array(
                $this->accountId,
                $this->accountName,
                $this->accountSex,
                $this->accountRace,
            ),
            'domain' => str_replace('https://', '', $world->domain),
            'generatetime' => $this->generateTime,
            'science' => array(
                'current' => $this->scCurrent,
                'known' => $this->scKnown,
                'next' => $this->scNext,
                'started' => $this->scStarted,
            ),
        )));
    }

    public function save()
    {
        $this->db->prepare("
			INSERT INTO wt_reports_science
			SET
				id            = :id,
				worldId       = :worldId,
				generateTime  = :generateTime,
				accountId     = :accountId,
				accountName   = :accountName,
				accountSex    = :accountSex,
				accountRace   = :accountRace,
				countryId     = :countryId,
				countryName   = :countryName,
				countryFlag   = :countryFlag,
				version       = :version,
				scCurrent     = :scCurrent,
				scKnown       = :scKnown,
				scStarted     = :scStarted,
				scNext        = :scNext,
				validKey      = :validKey,
				verified      = :verified,
				clientIP      = INET_ATON(:clientIP)
			");
        $this->db->execute(array(
            'id' => $this->id,
            'worldId' => $this->worldId,
            'generateTime' => $this->generateTime,
            'accountId' => $this->accountId,
            'accountName' => $this->accountName,
            'accountSex' => $this->accountSex,
            'accountRace' => $this->accountRace,
            'countryId' => $this->countryId,
            'countryName' => $this->countryName,
            'countryFlag' => $this->countryFlag,
            'version' => $this->version,
            'scCurrent' => json_encode($this->scCurrent),
            'scKnown' => json_encode($this->scKnown),
            'scStarted' => json_encode($this->scStarted),
            'scNext' => $this->scNext,
            'validKey' => $this->validKey,
            'verified' => $this->verified,
            'clientIP' => $this->clientIP
        ));
    }

    public function lastError()
    {
        return $this->errorCode;
    }

    public function getErrorMessage()
    {
        switch ($this->errorCode) {
            case 0:
                return 'No errors';
                break;

            case 1:
                return 'Invalid format input data';
                break;

            case 2:
                return 'Unsupported version';
                break;

            case 3:
                return 'Report not found';
                break;

            default:
                return 'Unknown error';
        }
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function getValidId()
    {
        $i = 0;
        do {
            if ($i++ > 100) {
                throw new SciReportException('System error: Invalid ID.');
            }

            $id = substr(strtolower(md5(uniqid())), 0, self::ID_LENGTH);
            $query = "SELECT COUNT(*) FROM wt_reports_science WHERE id = '{$id}'";
            $count = (int)$this->db->simpleQuery($query)->fetchColumn();
        } while ($count);
        return $id;
    }

    //=======================================================
    //   Private methods
    //=======================================================

}
