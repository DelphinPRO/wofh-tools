<?php
/**
 * @since               10.01.14 20:35
 * @package             Wofh Tools
 * @author              DelphinPRO delphinpro@yandex.ru
 * @copyright           Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license             Licensed under the MIT license
 */

namespace df\extensions\models;

use df\Console;
use df\helper\FS;
use df\Model;

/**
 * Class World
 *
 * @package df\model
 */
class World extends Model
{
    public
        $id,
        $num,
        $sign,
        $lowerSign,
        $title,
        $fullTitle,
        $started,
        $canReg,
        $working,
        $needCode,
        $statistic,
        $loadStat,
        $updateStat,
        $updateConst,
        $desc,
        $isUpdate,
        $country,
        $domain,
        $age,
        $startedTimestamp;

    private static $_countAccounts;

    public function __construct($data = null)
    {
        self::$_countAccounts = array();

        if ($data) {
            $this->_initialization($data);
        }

        $this->id = (int)$this->id;
        $this->num = $this->id % 1000;
        $this->canReg = (bool)(int)$this->canReg;
        $this->title = (string)$this->title;
        $this->working = (bool)(int)$this->working;
        $this->needCode = (bool)(int)$this->needCode;
        $this->statistic = (bool)(int)$this->statistic;
        $this->loadStat = strtotime($this->loadStat);
        $this->updateStat = strtotime($this->updateStat);
        $this->updateConst = strtotime($this->updateConst);
        $this->desc = (string)$this->desc;
        $this->lowerSign = strtolower($this->sign);
        $this->sign = ucfirst($this->lowerSign);
        $this->country = preg_replace('/(\d+[st]{0,1})/', '', $this->lowerSign);
        $this->fullTitle = $this->sign . '&nbsp;&laquo;' . (string)$this->title . '&raquo;';

        if (!is_null($this->isUpdate)) {
            $isUpdate = strtotime($this->isUpdate);
            if (time() - $isUpdate < 60 * 5) {
                $this->isUpdate = true;
            } else {
                $this->isUpdate = false;
            }
        } else {
            $this->isUpdate = false;
        }

        if (!$this->started) {
            $this->age = 0;
            $this->startedTimestamp = false;
        } else {
            $this->startedTimestamp = strtotime($this->started);
            $this->age = floor((time() - $this->startedTimestamp) / (60 * 60 * 24));
            if (!$this->working) {
//				$this->db->setPrefix('ruXX', $this->lowerSign);
                try {
//					$this->db->query("SELECT UNIX_TIMESTAMP(MAX(stateDate)) AS stateDate FROM z_ruXX_accounts_stat");
//					$max       = $this->db->fetchObject()->stateDate;
//					$this->age = floor(( $max - $this->startedTimestamp ) / ( 60 * 60 * 24 ));
                } catch (\PDOException $e) {

                }
            }
        }
    }

    public function getRealLastUpdate()
    {
        try {
            $this->db->query(q("
				SELECT MAX(stateDate) AS lastUpdate
				FROM z_ruXX_accounts_stat
			", $this->lowerSign), array());

            $lastUpdate = strtotime($this->db->fetchObject()->lastUpdate);
            if (date(STD_DATE, $lastUpdate) == date(STD_DATE, $this->updateStat)) {
                return $this->updateStat;
            } else {
                $file = FS::path('/tmp/' . $this->lowerSign . '/stat-' . $this->lowerSign . '-' . date(STD_DATE,
                        $lastUpdate) . '.json');
                if (file_exists($file)) {
                    $json = FS::loadJSON($file);
                    if (json_last_error() == JSON_ERROR_NONE) {
                        return $json['time'];
                    }
                }
            }

            return $lastUpdate;
        } catch (\PDOException $e) {
            return false;
        }
    }

    private function _initialization($data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    public function beginUpdate()
    {
        $this->isUpdate = time();
        $this->save();
    }

    public function endUpdate($lastUpdateStat = null)
    {
        if ($lastUpdateStat) {
            $this->updateStat = $lastUpdateStat;
        }
        $this->isUpdate = false;
        $this->save();
    }

    public function save()
    {
        try {
            $this->db->query("
			INSERT INTO wt_worlds
			SET
				id          = :id,
				title       = :title,
				sign        = :sign,
				started     = :started,
				canReg      = :canReg,
				working     = :working,
				needCode    = :needCode,
				statistic   = :statistic,
				loadStat    = :loadStat,
				updateStat  = :updateStat,
				updateConst = :updateConst,
				`desc`      = :description,
				isUpdate    = :isUpdate
			ON DUPLICATE KEY UPDATE
				title       = :title,
				sign        = :sign,
				started     = :started,
				canReg      = :canReg,
				working     = :working,
				needCode    = :needCode,
				statistic   = :statistic,
				loadStat    = :loadStat,
				updateStat  = :updateStat,
				updateConst = :updateConst,
				`desc`      = :description,
				isUpdate    = :isUpdate
		", array(
                'id' => $this->id,
                'title' => $this->title,
                'sign' => $this->sign,
                'started' => $this->started,
                'canReg' => $this->canReg,
                'working' => $this->working,
                'needCode' => $this->needCode,
                'statistic' => $this->statistic,
                'loadStat' => $this->loadStat ? date(STD_DATETIME, $this->loadStat) : null,
                'updateStat' => $this->updateStat ? date(STD_DATETIME, $this->updateStat) : null,
                'updateConst' => $this->updateConst ? date(STD_DATETIME, $this->updateConst) : null,
                'description' => $this->desc,
                'isUpdate' => $this->isUpdate ? date(STD_DATETIME, $this->isUpdate) : null,
            ));
        } catch (\PDOException $e) {
            $this->lastError = $e->getMessage();

            return false;
        }

        return true;
    }

    public function getLastError()
    {
        return $this->lastError;
    }

    public function getFlag($flag, $size = 30)
    {
        if (is_null($flag)) {
            return '';
        } // todo: default flag

        $sign = $this->lowerSign;
        $path = $this->config->getStaticDir(DIR_ROOT . DIRECTORY_SEPARATOR) . "/uploads/worlds/$sign/flags/{$flag}_{$size}.gif";
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);
        if (file_exists($path)) {
            return $this->config->getStaticDomain() . "/uploads/worlds/$sign/flags/{$flag}_{$size}.gif";
        } else {
            return "/flag.php?w=$sign&f=$flag&s=$size";
        }
    }

    /**
     * @param        $type
     * @param int $id
     * @param string $title
     * @param string $view
     * @param string $flag
     *
     * @return string
     * @deprecated
     */
    public function buildLink($type, $id = 0, $title = '', $view = '', $flag = '')
    {
        $html = '';
        $btn = ($view == 'button') ? ' class="btn"' : '';

        switch ($type) {

            case 'world':
                $html .= '<a' . $btn . ' href="/stat/' . $this->lowerSign . '/">';
                $html .= $this->sign . ' — ' . $this->title;
                $html .= '</a>';
                if ($this->working) {
                    $html .= ' <a' . $btn . ' href="' . $this->domain . '" target="_blank"><i class="icon-link-ext"></i></a>';
                }
                break;

            case 'account':
                $html .= '<a href="/stat/' . $this->lowerSign . '/accounts/' . $id . '/">' . $title . '</a>';
                if ($this->working) {
                    $html .= ' <a href="' . $this->domain . '/account?id=' . $id . '" target="_blank"><i class="icon-link-ext"></i></a>';
                }
                break;

            case 'country' :
                $html .= '<a' . $btn . ' href="/stat/' . $this->lowerSign . '/countries/' . $id . '/">';
                $html .= '<img src="' . $this->getFlag($flag) . '" alt="" style="height:16px;margin-top:-2px;" /> ';
                $html .= $title . '</a>';
                if ($this->working) {
                    $html .= ' <a' . $btn . ' href="http://' . $this->domain . '/countryinfo?id=' . $id . '" target="_blank"><i class="icon-link-ext"></i></a>';
                }

                break;

            default:
                return 'Unknown link type';
        }

        if ($view == 'button') {
            $html = '<div class="btn-group">' . $html . '</div>';
        }

        return $html;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function countAccounts()
    {
        if (!isset(self::$_countAccounts[$this->id]) or is_null(self::$_countAccounts[$this->id])) {
            try {
                $queryCountAccounts = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts", $this->lowerSign);
                self::$_countAccounts[$this->id] = $this->db->query($queryCountAccounts)->fetchObject()->total;
            } catch (\PDOException $e) {
                if ($e->getCode() != '42S02') {
                    throw $e;
                }
                self::$_countAccounts[$this->id] = 0;
            }
        }

        return self::$_countAccounts[$this->id];
    }

    public function getLinkAccount($id)
    {
        return '' . $this->domain . '/account?id=' . $id;
    }

    public function getLinkCountry($id)
    {
        return '' . $this->domain . '/countryinfo?id=' . $id;
    }

}
