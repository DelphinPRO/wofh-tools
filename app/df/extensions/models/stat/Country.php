<?php
/**
 * @since             15.11.2014 18:44
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\stat;

use df\extensions\models\Wofh;
use df\Registry;

class Country extends StatObject
{
    public $id;
    public $name;
    public $flag;
    public $active;
    public $extra;
    public $date;
    public $timestamp;

    public $pop;
    public $accounts;
    public $towns;
    public $science;
    public $production;
    public $war;

    public $deltaPop;
    public $deltaAccounts;
    public $deltaTowns;
    public $deltaScience;
    public $deltaProduction;
    public $deltaWar;

    public $deltaPopWeek;
    public $deltaAccountsWeek;
    public $deltaTownsWeek;
    public $deltaScienceWeek;
    public $deltaProductionWeek;
    public $deltaWarWeek;

    public $deltaPopMonth;
    public $deltaAccountsMonth;
    public $deltaTownsMonth;
    public $deltaScienceMonth;
    public $deltaProductionMonth;
    public $deltaWarMonth;

    protected $cache = array();

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct($id, $timestamp)
    {
        $this->timestamp = $timestamp;
        $this->date = date(STD_DATE, $timestamp);
        $this->world = Registry::get('world');

        $this->db->setPrefix('ruXX', $this->world->lowerSign);
        $data = $this->db->query("
				SELECT *
				FROM z_ruXX_countries s
					LEFT JOIN z_ruXX_countries_stat d
						ON s.countryId = d.countryId
				WHERE s.countryId = '$id'
					AND d.stateDate = '{$this->date}'
			")->fetch();
//		pre($this->db->getLastQuery(), 0);
//		pre($data);

        if (empty($data)) {
            $data = $this->db->query("
					SELECT *
					FROM z_ruXX_countries s
						LEFT JOIN z_ruXX_countries_stat d
							ON s.countryId = d.countryId
					WHERE s.countryId = '$id'
					ORDER BY d.stateDate DESC
					LIMIT 1
				")->fetch();

            if (empty($data)) {
                return;
            }
        }

        $this->id = (int)$data['countryId'];
        $this->name = $data['countryTitle'];
        $this->flag = $data['countryFlag'];
        $this->active = (bool)(int)$data['active'];
        $this->extra = json_decode($data['extra'], true);
        $this->date = $data['stateDate'];
        $this->timestamp = strtotime($data['stateDate']);

        $this->pop = (int)$data['pop'];
        $this->accounts = (int)$data['accounts'];
        $this->towns = (int)$data['towns'];
        $this->science = (float)$data['science'];
        $this->production = (float)$data['production'];
        $this->war = (float)$data['war'];

        $this->deltaPop = (int)$data['deltaPop'];
        $this->deltaAccounts = (int)$data['deltaAccounts'];
        $this->deltaTowns = (int)$data['deltaTowns'];
        $this->deltaScience = (float)$data['deltaScience'];
        $this->deltaProduction = (float)$data['deltaProduction'];
        $this->deltaWar = (float)$data['deltaWar'];

//		dmp($data, 0);
//		dmp((array)$this);

    }

    public function getAccountsList()
    {
        $this->db->query("
					SELECT s.accountId, s.accountName, s.accountRace, s.accountSex, d.role, s.extra,
						d.pop, d.towns, d.deltaPop, d.deltaTowns, d.stateDate
					FROM z_ruXX_accounts s
						JOIN z_ruXX_accounts_stat d
							ON d.accountId = s.accountId
					WHERE s.countryId = '{$this->id}'
						AND s.active = 1
						AND d.stateDate = '{$this->date}'
					ORDER BY d.pop DESC
				");
        $accounts = $this->db->fetchObjects('\df\extensions\models\struct\DataAccount');
        return $accounts;
    }

    public function getEvents()
    {
        $EVENT_TOWN_RENAME = Wofh::EVENT_TOWN_RENAME;
        $EVENT_TOWN_CREATE = Wofh::EVENT_TOWN_CREATE;
        $EVENT_TOWN_LOST = Wofh::EVENT_TOWN_LOST;

        $this->db->query("
				SELECT
					e.stateDate as `date`,
					e.eventId as id,
					e.townId,
					e.accountId,
					e.countryId,
					e.countryIdFrom,
					e.role,
					e.extra
				FROM z_ruXX_events e
				WHERE (e.countryId = :countryId OR e.countryIdFrom = :countryId)
				AND e.stateDate <= :stateDate
				AND e.stateDate > :stateDate - INTERVAL 1 WEEK
				AND e.eventId != '$EVENT_TOWN_RENAME'
				--	AND e.eventId != '$EVENT_TOWN_CREATE'
				--	AND e.eventId != '$EVENT_TOWN_LOST'
				ORDER BY e.stateDate DESC, e.eventId ASC
				-- LIMIT 20
			", array(
            'countryId' => $this->id,
            'stateDate' => $this->date
        ));
        $events = $this->db->fetchObjects('\df\extensions\models\stat\Event');
        $events0 = array();
        foreach ($events as $event) {
            $events0[$event->date][$event->id][] = $event;
        }
        return $events0;
    }

    public function getDiplomacy()
    {
        $this->db->query("
				SELECT
					c.countryTitle AS countryName,
					c.countryFlag,
					c.active,
--					dip.stateDate AS `date`,
					dip.id2,
					dip.status
				FROM z_ruXX_countries_diplomacy dip
					LEFT JOIN z_ruXX_countries c
						ON c.countryId = dip.id2
				WHERE (dip.id1 = :countryId)
				AND dip.stateDate = :stateDate
				AND c.active = 1
			", array(
            'countryId' => $this->id,
            'stateDate' => $this->date
        ));
        $dip = array(1 => array(), 2 => array());
        while ($row = $this->db->fetch()) {
            $row['countryId'] = (int)$row['id2'];
            $row['status'] = (int)$row['status'];
            $row['active'] = (bool)(int)$row['active'];
            unset($row['id2']);
            $dip[(int)$row['status']][] = $row;
        }
        return $dip;
    }

    public function loadExtendedData()
    {
        $this->loadWeekData();
        $this->loadMonthData();
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function getChartData()
    {
        if (!$this->cache) {
            $endDate = $this->active ? $this->date : date(STD_DATE, $this->extra['delete']);

            $data = $this->db->query("
				SELECT d.stateDate, d.pop, d.science, d.production, d.war,
					d.deltaPop, d.deltaScience, d.deltaProduction, d.deltaWar
				FROM z_ruXX_countries s
					JOIN z_ruXX_countries_stat d
						ON d.countryId = s.countryId
				WHERE s.countryId = '{$this->id}'
					AND (d.stateDate > '$endDate' - INTERVAL 3 WEEK)
					AND (d.stateDate <= '$endDate')
				ORDER BY d.stateDate")->fetchAll();

            $range = array();
            $td = array();
            foreach ($data as $item) {
                list($d, $m) = explode(' ', date('d m', strtotime($item['stateDate'])));
                $key = '2014-' . $m . '-' . $d;
                $td['pop'][$key] = (int)$item['pop'];
                $td['popDay'][$key] = (int)$item['deltaPop'];
                $td['science'][$key] = (float)$item['science'];
                $td['scienceDay'][$key] = (float)$item['deltaScience'];
                $td['production'][$key] = (float)$item['production'];
                $td['productionDay'][$key] = (float)$item['deltaProduction'];
                $td['war'][$key] = (float)$item['war'];
                $td['warDay'][$key] = (float)$item['deltaWar'];
                $range[] = $key;
            }
            $this->cache = array(
                'range' => $range,
                'chart' => $td,
                'title' => array(
                    "Рейтинг населения страны «" . $this->name . "»",
                    "Научный рейтинг страны «" . $this->name . "»",
                    "Производственный рейтинг страны «" . $this->name . "»",
                    "Военный рейтинг страны «" . $this->name . "»",
                ),
            );
        }
        return $this->cache;
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function loadWeekData()
    {
        $data = $this->db->query("
				SELECT d.pop, d.accounts, d.towns, d.science, d.production, d.war
				FROM z_ruXX_countries_stat d
				WHERE d.countryId = '{$this->id}'
					AND (d.stateDate = '{$this->date}' - INTERVAL 1 WEEK)
				")->fetch();

        $this->deltaPopWeek = $this->pop - (int)$data['pop'];
        $this->deltaAccountsWeek = $this->accounts - (int)$data['accounts'];
        $this->deltaTownsWeek = $this->towns - (int)$data['towns'];
        $this->deltaScienceWeek = $this->science - (int)$data['science'];
        $this->deltaProductionWeek = $this->production - (int)$data['production'];
        $this->deltaWarWeek = $this->war - (int)$data['war'];
    }

    private function loadMonthData()
    {
        $data = $this->db->query("
				SELECT d.pop, d.accounts, d.towns, d.science, d.production, d.war
				FROM z_ruXX_countries_stat d
				WHERE d.countryId = '{$this->id}'
					AND (d.stateDate = '{$this->date}' - INTERVAL 1 MONTH)
				")->fetch();

        $this->deltaPopMonth = $this->pop - (int)$data['pop'];
        $this->deltaAccountsMonth = $this->accounts - (int)$data['accounts'];
        $this->deltaTownsMonth = $this->towns - (int)$data['towns'];
        $this->deltaScienceMonth = $this->science - (int)$data['science'];
        $this->deltaProductionMonth = $this->production - (int)$data['production'];
        $this->deltaWarMonth = $this->war - (int)$data['war'];
    }

}
