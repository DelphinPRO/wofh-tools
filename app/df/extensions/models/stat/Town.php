<?php
/**
 * @since             16.11.2014 01:13
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\stat;

use df\Registry;

class Town extends StatObject
{

    public $id;
    public $name;
    public $accountId;
    public $lost;
    public $destroyed;
    public $extra;

    public $date;

    public $pop;
    public $delta;
    public $wonderId;
    public $wonderLevel;

    //=======================================================
    //   Public methods
    //=======================================================

    public static function getClass()
    {
        return __NAMESPACE__ . '\Town';
    }

    public function __construct()
    {
        $this->world = Registry::get('world');

        if (isset($this->townId, $this->townTitle)) {
            $this->id = (int)$this->townId;
            $this->name = $this->townTitle;
            $this->accountId = (int)$this->accountId;
            $this->lost = (bool)(int)$this->lost;
            $this->destroyed = (bool)(int)$this->destroyed;
            $this->extra = json_decode($this->extra, true);

            $this->date = $this->stateDate;

            $this->pop = (int)$this->pop;
            $this->delta = (int)$this->delta;
            $this->wonderId = (int)$this->wonderId;
            $this->wonderLevel = (int)$this->wonderLevel;

            unset($this->townId, $this->townTitle, $this->stateDate);
        }

    }

    public function getWonder()
    {
        if (!$this->wonderId) {
            return '';
        }
        return $this->wonderId . ' (' . $this->wonderLevel . ')';
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function getChartData()
    {
        // TODO: Implement getChartData() method.
    }

    //=======================================================
    //   Private methods
    //=======================================================

}
