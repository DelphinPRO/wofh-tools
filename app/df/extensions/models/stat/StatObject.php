<?php
/**
 * @since             15.11.2014 18:45
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\stat;

use df\extensions\models\World;
use df\Model;

/**
 * Class StatObject
 *
 * @package df\extensions\models
 */
abstract class StatObject extends Model
{

    /** @var  World */
    protected $world;
    //=======================================================
    //   Public methods
    //=======================================================

    public function format($number)
    {
        return number_format($number, 0, '.', ' ');
    }

    public function getDate($date, $format = 'd M Y')
    {
        return empty($this->extra[$date]) ? '—' : date($format, $this->extra[$date]);
    }

    public function getDelta($number)
    {
        return '<span class="' . getStatusClass($number) . '">' . getSignedNumber($number, '—') . '</span>';
    }

    public function getChartPop()
    {
        $data = $this->getChartData();
        $e = array(
            'cId' => '#rating1',
            'type' => 'splineAndArea',
            'lazy' => true,
            'lazyIndex' => '#tab1',
            'title' => $data['title'][0],
            'subtitle' => $this->world->sign . ' «' . $this->world->title . '», ' . date('d.m.Y H:i', $this->timestamp),
            'primaryColor' => '#428bca',
            'secondColor' => '#B1D5FB',
            'text1' => 'Население, чел.',
            'text2' => 'Динамика, чел.',
            'sfx' => ' чел.',
            'categories' => array_values($data['range']),
            'series' => array(
                array('text' => 'Население', 'data' => array_values($data['chart']['pop'])),
                array('text' => 'Динамика', 'data' => array_values($data['chart']['popDay'])),
            )
        );
        return $e;
    }

    public function getChartScience()
    {
        $data = $this->getChartData();
        $e = array(
            'cId' => '#rating2',
            'type' => 'splineAndArea',
            'lazy' => true,
            'lazyIndex' => '#tab2',
            'title' => $data['title'][1],
            'subtitle' => $this->world->sign . ' «' . $this->world->title . '», ' . date('d.m.Y H:i', $this->timestamp),
            'primaryColor' => '#8BBC21',
            'secondColor' => '#D5EEA0',
            'text1' => 'Научный рейтинг',
            'text2' => 'Динамика рейтинга',
            'categories' => array_values($data['range']),
            'series' => array(
                array('text' => 'Научный рейтинг', 'data' => array_values($data['chart']['science'])),
                array('text' => 'Динамика', 'data' => array_values($data['chart']['scienceDay'])),
            )
        );
        return $e;
    }

    public function getChartProduction()
    {
        $data = $this->getChartData();
        $e = array(
            'cId' => '#rating3',
            'type' => 'splineAndArea',
            'lazy' => true,
            'lazyIndex' => '#tab3',
            'title' => $data['title'][2],
            'subtitle' => $this->world->sign . ' «' . $this->world->title . '», ' . date('d.m.Y H:i', $this->timestamp),
            'primaryColor' => '#F28F43',
            'secondColor' => '#F8CDAB',
            'text1' => 'Производственный рейтинг',
            'text2' => 'Динамика рейтинга',
            'categories' => array_values($data['range']),
            'series' => array(
                array('text' => 'Производственный рейтинг', 'data' => array_values($data['chart']['production'])),
                array('text' => 'Динамика', 'data' => array_values($data['chart']['productionDay'])),
            )
        );
        return $e;
    }

    public function getChartWar()
    {
        $data = $this->getChartData();
        $e = array(
            'cId' => '#rating4',
            'type' => 'splineAndArea',
            'lazy' => true,
            'lazyIndex' => '#tab4',
            'title' => $data['title'][3],
            'subtitle' => $this->world->sign . ' «' . $this->world->title . '», ' . date('d.m.Y H:i', $this->timestamp),
            'primaryColor' => '#C42525',
            'secondColor' => '#FAAEAE',
            'text1' => 'Военный рейтинг',
            'text2' => 'Динамика рейтинга',
            'categories' => array_values($data['range']),
            'series' => array(
                array('text' => 'Военный рейтинг', 'data' => array_values($data['chart']['war'])),
                array('text' => 'Динамика', 'data' => array_values($data['chart']['warDay'])),
            )
        );
        return $e;
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    abstract protected function getChartData();

    //=======================================================
    //   Private methods
    //=======================================================

}
