<?php
/**
 * @since             15.11.2014 23:45
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\stat;

use df\Registry;

class Account extends StatObject
{
    public $id;
    public $name;
    public $race;
    public $sex;
    public $countryId;
    public $countryName;
    public $countryFlag;
    public $role;
    public $active;
    public $extra;
    public $date;
    public $timestamp;

    public $pop;
    public $towns;
    public $science;
    public $production;
    public $war;

    public $deltaPop;
    public $deltaTowns;
    public $deltaScience;
    public $deltaProduction;
    public $deltaWar;

    public $deltaPopWeek;
    public $deltaTownsWeek;
    public $deltaScienceWeek;
    public $deltaProductionWeek;
    public $deltaWarWeek;

    public $deltaPopMonth;
    public $deltaTownsMonth;
    public $deltaScienceMonth;
    public $deltaProductionMonth;
    public $deltaWarMonth;

    protected $cache = array();

    private $_roleNames = array();
    private $_roleMnemonics = array();

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct($id, $timestamp)
    {
        $this->timestamp = $timestamp;
        $this->date = date(STD_DATE, $timestamp);
        $this->world = Registry::get('world');

        $this->db->setPrefix('ruXX', $this->world->lowerSign);

        $this->db->query(loadSql('get_account_data'), array(
            'accountId' => $id,
            'stateDate' => date(STD_DATE, $timestamp),
        ));
        $data = $this->db->fetch();

        if (empty($data)) {
            $date = $this->db->query("SELECT MAX(stateDate) as stateDate FROM z_ruXX_accounts_stat WHERE accountId = '{$id}'")->fetchObject()->stateDate;
            $this->db->query(loadSql('get_account_data'), array(
                'accountId' => $id,
                'stateDate' => $date,
            ));
            $data = $this->db->fetch();

            if (empty($data)) {
                return;
            }
        }

        $this->id = (int)$data['accountId'];
        $this->name = $data['accountName'];
        $this->race = (int)$data['accountRace'];
        $this->sex = (int)$data['accountSex'];
        $this->countryId = (int)$data['countryId'];
        $this->role = (int)$data['role'];
        $this->active = (bool)(int)$data['active'];
        $this->extra = json_decode($data['extra'], true);
        $this->date = $data['stateDate'];
        $this->timestamp = strtotime($data['stateDate']);

        $this->pop = (int)$data['pop'];
        $this->towns = (int)$data['towns'];
        $this->science = (float)$data['science'];
        $this->production = (float)$data['production'];
        $this->war = (float)$data['war'];

        $this->deltaPop = (int)$data['deltaPop'];
        $this->deltaTowns = (int)$data['deltaTowns'];
        $this->deltaScience = (float)$data['deltaScience'];
        $this->deltaProduction = (float)$data['deltaProduction'];
        $this->deltaWar = (float)$data['deltaWar'];

        $this->deltaPopWeek = (int)$data['deltaPopWeek'];
        $this->deltaTownsWeek = (int)$data['deltaTownsWeek'];
        $this->deltaScienceWeek = (float)$data['deltaScienceWeek'];
        $this->deltaProductionWeek = (float)$data['deltaProductionWeek'];
        $this->deltaWarWeek = (float)$data['deltaWarWeek'];

        $this->deltaPopMonth = (int)$data['deltaPopMonth'];
        $this->deltaTownsMonth = (int)$data['deltaTownsMonth'];
        $this->deltaScienceMonth = (float)$data['deltaScienceMonth'];
        $this->deltaProductionMonth = (float)$data['deltaProductionMonth'];
        $this->deltaWarMonth = (float)$data['deltaWarMonth'];

        $this->countryName = (string)$data['countryTitle'];
        $this->countryFlag = (string)$data['countryFlag'];

        $this->checkRoles();
    }

    public function getRace()
    {
        $races = array('Индеец', 'Европеец', 'Азиат', 'Африканец');
        return $races[$this->race];
    }

    public function getAge()
    {
        $d = new \DateTime('@' . $this->extra['create']);
        $i = $d->diff(new \DateTime($this->date));
        return $i->days . pluralForm($i->days, array(' день', ' дня', ' дней'));
    }

    public function getRole()
    {
        return join(', ', $this->_roleNames);
    }

    public function getRoleAsMnemonic()
    {
        return join(' ', $this->_roleMnemonics);
    }

    public function getTowns()
    {
        if (!$this->cache['towns']) {
            $ids = $this->db->query("SELECT townId FROM z_ruXX_towns WHERE accountId = '{$this->id}'")->fetchByField('townId');
            $ids = join(',', array_keys($ids));
            $this->db->query("
				SELECT t.*,
					d.pop,
					d.wonderId,
					d.wonderLevel,
					d.delta
				FROM z_ruXX_towns t
					LEFT JOIN z_ruXX_towns_stat d
						ON d.townId = t.townId
				WHERE d.stateDate = '{$this->date}'
				AND d.townId IN ($ids)
			");
            $this->cache['towns'] = $this->db->fetchObjects(Town::getClass());
        }
        return $this->cache['towns'];
    }

    public function getEvents()
    {
        $this->db->query("
				SELECT
					e.stateDate AS `date`,
					e.eventId AS id,
					e.townId,
					e.accountId,
					e.countryId,
					e.countryIdFrom,
					e.role,
					e.extra
				FROM z_ruXX_events e
				WHERE (e.accountId = :accountId)
				AND e.stateDate <= :stateDate
--				AND e.stateDate > :stateDate - INTERVAL 1 MONTH
				ORDER BY e.stateDate DESC, e.eventId ASC
				-- LIMIT 20
			", array(
            'accountId' => $this->id,
            'stateDate' => $this->date
        ));
        $events = $this->db->fetchObjects('\df\extensions\models\stat\Event');
        $events0 = array();
        foreach ($events as $event) {
            $events0[$event->date][$event->id][] = $event;
        }
        return $events0;
    }


    //=======================================================
    //   Protected methods
    //=======================================================

    protected function getChartData()
    {
        if (!$this->cache['chart']) {

            $data = $this->db->query("
				SELECT
					d.stateDate, d.pop, d.science, d.production, d.war,
					d.deltaPop, d.deltaScience, d.deltaProduction, d.deltaWar
				FROM z_ruXX_accounts s
					JOIN z_ruXX_accounts_stat d
						ON d.accountId = s.accountId
				WHERE s.accountId = '{$this->id}'
					AND (d.stateDate >= '{$this->date}' - INTERVAL 3 WEEK)
					AND (d.stateDate <= '{$this->date}')
				ORDER BY d.stateDate
			")->fetchAll();

            $range = array();
            $td = array();

            foreach ($data as $item) {
                list($d, $m, $y) = explode(' ', date('d m Y', strtotime($item['stateDate'])));
                $key = $y . '-' . $m . '-' . $d;
                $td['pop'][] = (int)$item['pop'];
                $td['popDay'][] = (int)$item['deltaPop'];
                $td['science'][] = (float)$item['science'];
                $td['scienceDay'][] = (float)$item['deltaScience'];
                $td['production'][] = (float)$item['production'];
                $td['productionDay'][] = (float)$item['deltaProduction'];
                $td['war'][] = (float)$item['war'];
                $td['warDay'][] = (float)$item['deltaWar'];
                $range[] = $key;
            }

            $this->cache['chart'] = array(
                'range' => $range,
                'chart' => $td,
                'title' => array(
                    "Рейтинг населения игрока «" . $this->name . "»",
                    "Нучный рейтинг игрока «" . $this->name . "»",
                    "Производственный рейтинг игрока «" . $this->name . "»",
                    "Военный рейтинг игрока «" . $this->name . "»",
                ),
            );
        }
        return $this->cache['chart'];
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function checkRoles()
    {
        if ($this->role & 1) {
            $this->_roleNames[] = 'Глава';
            $this->_roleMnemonics[] = '<img src="/img/icons/country.png" height="20" alt="Глава" title="Глава">';
        }
        if ($this->role & 2) {
            $this->_roleNames[] = 'Мудрец';
            $this->_roleMnemonics[] = '<img src="/img/icons/science.png" height="20" alt="Мудрец" title="Мудрец">';
        }
        if ($this->role & 4) {
            $this->_roleNames[] = 'Воевода';
            $this->_roleMnemonics[] = '<img src="/img/icons/war1.png" height="20" alt="Воевода" title="Воевода">';
        }
        if ($this->role & 8) {
            $this->_roleNames[] = 'Казначей';
            $this->_roleMnemonics[] = '<img src="/img/icons/money.png" height="20" alt="Казначей" title="Казначей">';
        }
        if ($this->role & 16) {
            $this->_roleNames[] = 'Помощник воеводы';
            $this->_roleMnemonics[] = '<img src="/img/icons/war.png" height="20" alt="Помощник воеводы" title="Помощник воеводы">';
        }
        if ($this->role & 32) {
            $this->_roleNames[] = 'Второй пом. воеводы';
            $this->_roleMnemonics[] = '<img src="/img/icons/war.png" height="20" alt="Второй пом. воеводы" title="Второй пом. воеводы">';
        }
        if ($this->role & 64) {
            $this->_roleNames[] = '?(64)';
            $this->_roleMnemonics[] = '<img src="/img/icons/unknown.png" height="20" alt="?(64)" title="?(64)">';
        }
        if ($this->role & 128) {
            $this->_roleNames[] = '?(128)';
            $this->_roleMnemonics[] = '<img src="/img/icons/unknown.png" height="20" alt="?(128)" title="?(128)">';
        }
    }

}
