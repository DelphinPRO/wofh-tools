<?php
/**
 * @since             15.11.2014 21:05
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models\stat;

use df\extensions\models\Wofh;
use df\extensions\models\World;
use df\Registry;

class Event extends StatObject
{
    const CONTEXT_VIEW_DEFAULT = 0;
    const CONTEXT_VIEW_COUNTRY = 1;
    const CONTEXT_VIEW_ACCOUNT = 2;
    const CONTEXT_VIEW_TOWN = 3;

    const SEX_FEMALE = 0;
    const SEX_MALE = 1;

    public static $CONTEXT;

    private static $templates;

    public $date;
    public $timestamp;
    public $id;
    public $townId;
    public $accountId;
    public $countryId;
    public $countryIdFrom;
    public $role;
    public $extra;
    public $icon;

    private $_key;

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct()
    {
        $this->timestamp = strtotime($this->date);
        $this->id = (int)$this->id;
        $this->townId = (int)$this->townId;
        $this->accountId = (int)$this->accountId;
        $this->countryId = (int)$this->countryId;
        $this->countryIdFrom = (int)$this->countryIdFrom;
        $this->role = (int)$this->role;
        $this->extra = json_decode($this->extra, true);
        $this->icon = getIconEvent($this->id);
        $this->world = Registry::get('world');

        $this->loadTemplates();
    }

    public function getIcon()
    {
        $data = array(
            20 => 'event-account-create',
            25 => 'event-account-rename',
            24 => 'event-account-delete',
            21 => 'event-account-country-in',
            22 => 'event-account-country-out',
            23 => 'event-account-country-change',
            1 => 'event-town-create',
            2 => 'event-town-rename',
            3 => 'event-town-lost',
            4 => 'event-town-destroy',
            40 => 'event-country-create',
            41 => 'event-country-flag',
            42 => 'event-country-rename',
            43 => 'event-country-destroy',
            61 => 'event-wonder-create',
            60 => 'event-wonder-destroy',
            62 => 'event-wonder-activate',
        );
        if (isset($data[$this->id])) {
            return '/img/icons/' . $data[$this->id] . '.png';
        } else {
            return '/img/icons/unknown.png';
        }
    }

    public function getText()
    {
        $this->_key = $this->id;

        if (self::$CONTEXT == self::CONTEXT_VIEW_COUNTRY) {
            if ($this->id == Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE) {
                if (Registry::get('country')->id == $this->countryId) {
                    $this->_key = 'EVENT_ACCOUNT_COUNTRY_CHANGE_IN';
                }
                if (Registry::get('country')->id == $this->countryIdFrom) {
                    $this->_key = 'EVENT_ACCOUNT_COUNTRY_CHANGE_OUT';
                }
            }
        }

        return $this->_parse(array(
            'account' => $this->getAccountLink(),
            'accountFrom' => isset($this->extra['accountFrom']) ? $this->extra['accountFrom'] : '',
            'town' => $this->getTownLink(),
            'townFrom' => isset($this->extra['townNameFrom']) ? $this->extra['townNameFrom'] : '',
            'wonder' => isset($this->extra['wonderName']) ? $this->extra['wonderName'] : '',
            'country' => $this->getCountryLink(),
            'countryFrom' => $this->getCountryLinkFrom(),
            'flag' => $this->getFlag('countryFlag'),
            'flagFrom' => $this->getFlag('countryFlagFrom'),
            'role' => createRoleText($this->role),
        ));
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function getFlag($flag)
    {
        if (!isset($this->extra[$flag])) {
            return '';
        }
        return '<img src="' . $this->world->getFlag($this->extra[$flag]) . '">';
    }

    protected function getCountryLink()
    {
        $link = $this->router->link('statCountry', array(
            'sign' => $this->world->lowerSign,
            'countryId' => $this->countryId,
        ));
        return '<a href="' . $link . '">' . $this->extra['countryName'] . '</a>';
    }

    protected function getCountryLinkFrom()
    {
        if (!isset($this->extra['countryNameFrom'])) {
            return '';
        }
        $link = $this->router->link('statCountry', array(
            'sign' => $this->world->lowerSign,
            'countryId' => $this->countryIdFrom,
        ));
        return '<a href="' . $link . '">' . $this->extra['countryNameFrom'] . '</a>';
    }

    protected function getAccountLink()
    {
        $link = $this->router->link('statAccount', array(
            'sign' => $this->world->lowerSign,
            'accountId' => $this->accountId,
        ));
        return '<a href="' . $link . '">' . $this->extra['accountName'] . '</a>';
    }

    protected function getTownLink()
    {
        $link = $this->router->link('statTown', array(
            'sign' => $this->world->lowerSign,
            'townId' => $this->townId,
        ));
        return '<a href="' . $link . '">' . $this->extra['townName'] . '</a>';
    }

    protected function getChartData()
    {
        // Not use.
    }


    //=======================================================
    //   Private methods
    //=======================================================

    private function _parse($params)
    {
        $status = array(
            'positive' => '<span class="text-success text-event">',
            'negative' => '<span class="text-danger text-event">',
            'neutral' => '<span class="text-event">',
        );
        $params = array_merge($params, $status);
        $sex = isset($this->extra['accountSex']) ? $this->extra['accountSex'] : 1;
        $tmpl = isset(self::$templates[self::$CONTEXT][$this->_key][$sex])
            ? self::$templates[self::$CONTEXT][$this->_key][$sex]
            : self::$templates[self::CONTEXT_VIEW_DEFAULT][$this->id][$sex];
        foreach ($params as $key => $value) {
            $tmpl = str_replace('{%' . $key . '%}', $value, $tmpl);
        }
        return $tmpl . '</span>';
    }

    private function loadTemplates()
    {
        if (self::$templates) {
            return;
        }
        if (is_null(self::$CONTEXT)) {
            self::$CONTEXT = 0;
        }
        self::$templates = array(
            self::CONTEXT_VIEW_DEFAULT => array(
                Wofh::EVENT_TOWN_CREATE => array(
                    self::SEX_FEMALE => '{%positive%}{%account%} основала город {%town%}',
                    self::SEX_MALE => '{%positive%}{%account%} основал город {%town%}',
                ),
                Wofh::EVENT_TOWN_DESTROY => array(
                    '{%negative%}Разрушен город {%town%} (бывшая владелица {%account%})',
                    '{%negative%}Разрушен город {%town%} (бывший владелец {%account%})',
                ),
                Wofh::EVENT_TOWN_RENAME => array(
                    '{%neutral%}{%account%} переименовала город {%townFrom%} в {%town%}',
                    '{%neutral%}{%account%} переименовал город {%townFrom%} в {%town%}',
                ),
                Wofh::EVENT_TOWN_LOST => array(
                    '{%negative%}{%account%} потеряла город {%town%}',
                    '{%negative%}{%account%} потерял город {%town%}',
                ),
                Wofh::EVENT_WONDER_DESTROY => array(
                    '{%negative%}Чудо <span style="font-size:1.2em">{%wonder%}</span> разрушено в {%town%}, владелица {%account%}',
                    '{%negative%}Чудо <span style="font-size:1.2em">{%wonder%}</span> разрушено в {%town%}, владелец {%account%}',
                ),
                Wofh::EVENT_WONDER_CREATE => array(
                    '{%neutral%}<span style="font-size:1.2em">{%wonder%}</span> строится в {%town%}, владелица - {%account%}',
                    '{%neutral%}<span style="font-size:1.2em">{%wonder%}</span> строится в {%town%}, владелец - {%account%}',
                ),
                Wofh::EVENT_WONDER_ACTIVATE => array(
                    '{%positive%}Чудо <span style="font-size:1.2em">{%wonder%}</span> активировала {%account%} в городе {%town%}',
                    '{%positive%}Чудо <span style="font-size:1.2em">{%wonder%}</span> активировал {%account%} в городе {%town%}',
                ),
                Wofh::EVENT_ACCOUNT_CREATE => array(
                    '{%positive%}Новый игрок приходит в мир: {%account%}',
                    '{%positive%}Новый игрок приходит в мир: {%account%}',
                ),
                Wofh::EVENT_ACCOUNT_RENAME => array(
                    '{%neutral%}{%accountFrom%} взяла новое имя - {%account%}',
                    '{%neutral%}{%accountFrom%} взял новое имя - {%account%}',
                ),
                Wofh::EVENT_ACCOUNT_DELETE => array(
                    '{%negative%}{%account%} удалила аккаунт',
                    '{%negative%}{%account%} удалил аккаунт',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_IN => array(
                    '{%neutral%}{%account%} вступила в страну {%flag%} {%country%}',
                    '{%neutral%}{%account%} вступил в страну {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_OUT => array(
                    '{%neutral%}{%account%} покинула страну {%flag%} {%country%}',
                    '{%neutral%}{%account%} покинул страну {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE => array(
                    '{%neutral%}{%account%} перешла из страны {%flagFrom%} {%countryFrom%} в {%flag%} {%country%}',
                    '{%neutral%}{%account%} перешел из страны {%flagFrom%} {%countryFrom%} в {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_ROLE_IN => array(
                    '{%neutral%}{%account%} назначена на должность {%role%} в стране {%flag%} {%country%}',
                    '{%neutral%}{%account%} назначен на должность {%role%} в стране {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_ROLE_OUT => array(
                    '{%neutral%}{%account%} уволена с должности {%role%} в стране {%flag%} {%country%}',
                    '{%neutral%}{%account%} уволен с должности {%role%} в стране {%flag%} {%country%}',
                ),
                Wofh::EVENT_COUNTRY_CREATE => array(
                    '{%positive%}Основана страна {%flag%} {%country%}',
                    '{%positive%}Основана страна {%flag%} {%country%}',
                ),
                Wofh::EVENT_COUNTRY_FLAG => array(
                    '{%neutral%}Страна {%country%} сменила государственный флаг {%flagFrom%} -> {%flag%}',
                    '{%neutral%}Страна {%country%} сменила государственный флаг {%flagFrom%} -> {%flag%}',
                ),
                Wofh::EVENT_COUNTRY_RENAME => array(
                    '{%neutral%}Страна {%countryFrom%} теперь называется {%flag%} {%country%}',
                    '{%neutral%}Страна {%countryFrom%} теперь называется {%flag%} {%country%}',
                ),
                Wofh::EVENT_COUNTRY_DESTROY => array(
                    '{%negative%}Страна {%flag%} {%country%} больше не существует',
                    '{%negative%}Страна {%flag%} {%country%} больше не существует',
                ),
            ),

            self::CONTEXT_VIEW_COUNTRY => array(
                Wofh::EVENT_ACCOUNT_CREATE => array(
                    '{%positive%}Новый игрок приходит в мир: {%account%}',
                    '{%positive%}Новый игрок приходит в мир: {%account%}',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_IN => array(
                    '{%positive%}{%account%} вступила в страну',
                    '{%positive%}{%account%} вступил в страну',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_OUT => array(
                    '{%negative%}{%account%} покинула страну',
                    '{%negative%}{%account%} покинул страну',
                ),
                'EVENT_ACCOUNT_COUNTRY_CHANGE_IN' => array(
                    '{%positive%}{%account%} пришла из страны {%flagFrom%} {%countryFrom%}',
                    '{%positive%}{%account%} пришел из страны {%flagFrom%} {%countryFrom%}',
                ),
                'EVENT_ACCOUNT_COUNTRY_CHANGE_OUT' => array(
                    '{%negative%}{%account%} ушла в страну {%flag%} {%country%}',
                    '{%negative%}{%account%} ушел в страну {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_ROLE_IN => array(
                    '{%neutral%}{%account%} назначена на должность {%role%}',
                    '{%neutral%}{%account%} назначен на должность {%role%}',
                ),
                Wofh::EVENT_ACCOUNT_ROLE_OUT => array(
                    '{%neutral%}{%account%} уволена с должности {%role%}',
                    '{%neutral%}{%account%} уволен с должности {%role%}',
                ),
                Wofh::EVENT_COUNTRY_CREATE => array(
                    '{%positive%}Основана страна {%flag%} {%country%}',
                    '{%positive%}Основана страна {%flag%} {%country%}',
                ),
                Wofh::EVENT_COUNTRY_FLAG => array(
                    '{%neutral%}Страна {%country%} сменила государственный флаг {%flagFrom%} -> {%flag%}',
                    '{%neutral%}Страна {%country%} сменила государственный флаг {%flagFrom%} -> {%flag%}',
                ),
                Wofh::EVENT_COUNTRY_RENAME => array(
                    '{%neutral%}Страна {%countryFrom%} теперь называется {%flag%} {%country%}',
                    '{%neutral%}Страна {%countryFrom%} теперь называется {%flag%} {%country%}',
                ),
                Wofh::EVENT_COUNTRY_DESTROY => array(
                    '{%negative%}Страна {%flag%} {%country%} больше не существует',
                    '{%negative%}Страна {%flag%} {%country%} больше не существует',
                ),
            ),

            Event::CONTEXT_VIEW_ACCOUNT => array(
                Wofh::EVENT_TOWN_CREATE => array(
                    self::SEX_FEMALE => '{%positive%}Основан город {%town%}',
                    self::SEX_MALE => '{%positive%}Основан город {%town%}',
                ),
                Wofh::EVENT_TOWN_RENAME => array(
                    '{%neutral%}Город {%townFrom%} переименован в {%town%}',
                    '{%neutral%}Город {%townFrom%} переименован в {%town%}',
                ),
                Wofh::EVENT_TOWN_LOST => array(
                    '{%negative%}Потерян город {%town%}',
                    '{%negative%}Потерян город {%town%}',
                ),
                Wofh::EVENT_WONDER_DESTROY => array(
                    '{%negative%}Чудо <span style="font-size:1.2em">{%wonder%}</span> разрушено в {%town%}',
                    '{%negative%}Чудо <span style="font-size:1.2em">{%wonder%}</span> разрушено в {%town%}',
                ),
                Wofh::EVENT_WONDER_CREATE => array(
                    '{%neutral%}<span style="font-size:1.2em">{%wonder%}</span> строится в городе {%town%}',
                    '{%neutral%}<span style="font-size:1.2em">{%wonder%}</span> строится в городе {%town%}',
                ),
                Wofh::EVENT_WONDER_ACTIVATE => array(
                    '{%positive%}Чудо <span style="font-size:1.2em">{%wonder%}</span> активировано в городе {%town%}',
                    '{%positive%}Чудо <span style="font-size:1.2em">{%wonder%}</span> активировано в городе {%town%}',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_IN => array(
                    '{%neutral%}Принята в страну {%flag%} {%country%}',
                    '{%neutral%}Принят в страну {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_OUT => array(
                    '{%neutral%}{%account%} покинула страну {%flag%} {%country%}',
                    '{%neutral%}{%account%} покинул страну {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE => array(
                    '{%neutral%}Перешла из страны {%flagFrom%} {%countryFrom%} в {%flag%} {%country%}',
                    '{%neutral%}Перешел из страны {%flagFrom%} {%countryFrom%} в {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_ROLE_IN => array(
                    '{%neutral%}назначена на должность {%role%} в стране {%flag%} {%country%}',
                    '{%neutral%}назначен на должность {%role%} в стране {%flag%} {%country%}',
                ),
                Wofh::EVENT_ACCOUNT_ROLE_OUT => array(
                    '{%neutral%}уволена с должности {%role%} в стране {%flag%} {%country%}',
                    '{%neutral%}уволен с должности {%role%} в стране {%flag%} {%country%}',
                ),
            ),
        );
    }

}
