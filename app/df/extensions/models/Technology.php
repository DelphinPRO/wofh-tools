<?php
/**
 * @since             22.01.14 08:23
 * @package           Wofh-Tools
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models;

use df\helper\FS;
use df\Model;

/**
 * Class Tech
 *
 * @property Wofh $wofh
 */
class Technology extends Model
{
    private $_scList;
    private $_nAll;

    public function getScienceTree(World $world)
    {
        $cacheDir = FS::path('/tmp/cache/' . $world->lowerSign);
        $cacheFile = FS::path('/tmp/cache/' . $world->lowerSign . '/science.tree.json');
//		unlink($cacheFile);
//		unlink($cacheDir . '/science.list.json');
//		rmdir($cacheDir);
//		exit;
        if (!file_exists($cacheFile)) {

            $this->loadScienceList($world);

            if (empty($this->_scList)) {
                return false;
            }

            $this->_scList[37]['place'] += 1;
            $this->_scList[52]['place'] += 1;
            $this->_scList[55]['place'] += 6;
            $this->_scList[89]['place'] += 1;
            $this->_scList[105]['place'] += 5;
            $this->_scList[123]['place'] += 2;
            $this->_scList[130]['place'] += 5;
            $this->_scList[140]['place'] += 2;
            $this->_scList[175]['place'] += 7;
            $this->_scList[182]['place'] += 5;

            $result = array();
            foreach ($this->_scList as $id => &$item) {
                $result[$item['place']][$id] = $item;
            }
            ksort($result);

            foreach ($result as &$places) {
                uasort($places, function ($a, $b) {
                    if ($a['cost'] == $b['cost']) {
                        return 0;
                    }
                    return $a['cost'] > $b['cost'];
                });
            }

            FS::mkdir($cacheDir, 0666);
            FS::saveFile($cacheFile, json_encode($result), 0666);

            return $result;
        }
        $result = FS::loadJSON($cacheFile);
        return $result;
    }

    public function getJSONList(World $world)
    {
        $cacheDir = FS::path('/tmp/cache/' . $world->lowerSign);
        $cacheFile = FS::path('/tmp/cache/' . $world->lowerSign . '/science.list.json');

        if (!file_exists($cacheFile)) {

            $this->loadScienceList($world);

            $result = json_encode($this->_scList);

            if (empty($this->_scList)) {
                return $result;
            }

            FS::mkdir($cacheDir, 0666);
            FS::saveFile($cacheFile, $result, 0666);

            return $result;
        }

        $result = FS::readFile($cacheFile);
        return $result;
    }

    private function loadScienceList(World $world)
    {
        $this->_scList = $this->db
            ->query("
				SELECT *
				FROM cd_science
				WHERE worldId = :worldId
				ORDER BY id
				", array('worldId' => $world->id))
            ->fetchByField('id');

        foreach ($this->_scList as &$science0) {
            $science0['id'] = (int)$science0['id'];
            $science0['cost'] = (int)$science0['cost'];
            $science0['need'] = json_decode($science0['need'], true);
            $science0['takes'] = json_decode($science0['takes'], true);
            $science0['builds'] = json_decode($science0['builds'], true);
            $science0['units'] = json_decode($science0['units'], true);
            $science0['bonuses'] = json_decode($science0['bonuses'], true);
            $science0['deposit'] = array();
            $science0['nAll'] = array();
            unset($science0['worldId']);
        }

        $deps = $this->db
            ->query("
				SELECT id, title, science
				FROM cd_deposit
				WHERE worldId = :worldId
				ORDER BY id
				", array('worldId' => $world->id))
            ->fetchAll();
        foreach ($deps as $dep) {
            if (isset($this->_scList[$dep['science']])) {
                $this->_scList[$dep['science']]['deposit'][] = $dep['title'];
            }
        }

        foreach ($this->_scList as $id => $science) {
            $this->_nAll = array();
            $this->_scList[$id]['nAll'] = array_values(array_unique($this->_getAllNeeds($id)));
            $this->_scList[$id]['fCost'] = $this->_getFCost($this->_scList[$id]['cost']);

            if (empty($science['need'])) {
                $this->_scList[$science['id']]['place'] = 0;
                $this->_getLevel($science, 1);
            }
            if (!empty($science['deposit'])) {
                $this->_scList[$science['id']]['deposit'] = array_unique($science['deposit']);
                $this->_getLevel($science, 1);
            }
        }
    }

    private function _getFCost($cost)
    {
        if ($cost >= 10000000) {
            return ($cost / 1000000) . ' M';
        } elseif ($cost >= 10000) {
            return ($cost / 1000) . ' K';
        } else {
            return $cost;
        }
    }

    private function _getAllNeeds($scienceId)
    {
        foreach ($this->_scList[$scienceId]['need'] as $needId) {
            if ($needId == -1) {
                continue;
            }

            if (!in_array($needId, $this->_nAll)) {
                $this->_nAll[] = $needId;
                $this->_getAllNeeds($needId);
            }
        }

        return $this->_nAll;
    }

    private function _getLevel($science, $level)
    {
        foreach ($science['takes'] as $takeId) {
            if ($takeId > -1) {
                if (!isset($this->_scList[$takeId]['place']) || $this->_scList[$takeId]['place'] < $level) {
                    $this->_scList[$takeId]['place'] = $level;
                }
                $this->_getLevel($this->_scList[$takeId], $level + 1);
            }
        }
    }
}
