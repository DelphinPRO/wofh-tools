<?php
/**
 * @since               10.01.14 20:23
 * @package             Wofh-Tools
 * @author              DelphinPRO delphinpro@yandex.ru
 * @copyright           Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license             Licensed under the MIT license
 */

namespace df\extensions\models;

use df\helper\FS;
use df\Model;
use df\Registry;

/**
 * Class Wofh
 *
 * @package df\model
 */
abstract class Wofh extends Model
{
    const EVENT_TOWN_CREATE = 1;
    const EVENT_TOWN_RENAME = 2;
    const EVENT_TOWN_LOST = 3;
    const EVENT_TOWN_DESTROY = 4;

    const EVENT_ACCOUNT_CREATE = 20;
    const EVENT_ACCOUNT_COUNTRY_IN = 21;
    const EVENT_ACCOUNT_COUNTRY_OUT = 22;
    const EVENT_ACCOUNT_COUNTRY_CHANGE = 23;
    const EVENT_ACCOUNT_DELETE = 24;
    const EVENT_ACCOUNT_RENAME = 25;
    const EVENT_ACCOUNT_ROLE_IN = 26;
    const EVENT_ACCOUNT_ROLE_OUT = 27;

    const EVENT_COUNTRY_CREATE = 40;
    const EVENT_COUNTRY_FLAG = 41;
    const EVENT_COUNTRY_RENAME = 42;
    const EVENT_COUNTRY_DESTROY = 43;
    const EVENT_COUNTRY_PEACE = 44;
    const EVENT_COUNTRY_WAR = 45;

    const EVENT_WONDER_DESTROY = 60;
    const EVENT_WONDER_CREATE = 61;
    const EVENT_WONDER_ACTIVATE = 62;

    const RU = 1;
    const EN = 2;
    const INT = 4;
    const RU_S = 5;
    const RU_T = 6;

    private static $instance;

    private static $countries = array(
        'ru%' => self::RU,
        'en%' => self::EN,
        'int%' => self::INT,
        'ru%s' => self::RU_S,
        'ru%t' => self::RU_T,
    );
    private static $regExp = 'ru|en|int';
    private static $domains = array(
        1 => "https://ru%%X.waysofhistory.com",
        2 => "https://en%%X.waysofhistory.com",
        4 => "https://int%%X.waysofhistory.com",
        5 => "https://ru%%Xs.waysofhistory.com",
        6 => "https://ru%%Xt.waysofhistory.com",
    );
    private static $rootDomains = array(
        1 => "https://waysofhistory.com",
        2 => "https://waysofhistory.com",
        4 => "https://waysofhistory.com",
        5 => "https://waysofhistory.com",
        6 => "https://waysofhistory.com",
    );
    private static $linkToListOfWorlds = '/aj_statistics';
    private static $linkToStatistic = '/aj_statistics';
    private static $linkToConstData = '/gen/js/ru/const.js';
    private static $linkToConst1Data = '/js/gen/const1.js';

    private static $_worldsList = null;

    ///////////////////////////////////////////////////////////////////////////
    //
    //  HELPERS
    //
    ///////////////////////////////////////////////////////////////////////////

    public static function signToId($sign)
    {
        $pattern = '`^((' . self::$regExp . ')(\d+)(s|t*))$`Usi';
        if (preg_match($pattern, $sign, $m)) {
            $countryNum = self::$countries[
              str_replace($m[3], '%', strtolower($m[1]))
            ];
            $worldNum = (int)$m[3];
            return $countryNum * 1000 + $worldNum;
        }
        return false;
    }

    public static function idToSign($id)
    {
        $a = array_flip(self::$countries);
        return str_replace('%', ($id % 1000), ucfirst($a[(int)floor($id / 1000)]));
    }

    public static function domainToId($domain)
    {
        $domain = preg_replace('~^https://(.*)$~Usi', '\\1', $domain);
        foreach (self::$domains as $countryNum => $d) {
            $d = preg_replace('~^https://(.*)$~Usi', '\\1', $d);
            $pattern = str_replace('%%X', '(\d+)', $d);
            $pattern = '~^' . str_replace('.', '\.', $pattern) . '$~Usi';
            if (preg_match($pattern, $domain, $m)) {
                $id = $countryNum * 1000 + $m[1];
                return $id;
            }
        }
        return null;
    }

    public static function idToDomain($id)
    {
        return str_replace(
            '%%X',
            ($id % 1000),
            self::$domains[(int)floor($id / 1000)]
        );
    }

    public static function getLinkStatusWorlds($countryId)
    {
        $lang = array(1 => "ru", "en", "en", "en", "ru", "ru");

        $param = $lang[$countryId];

        return 'https://ru.waysofhistory.com' . self::$linkToListOfWorlds . '?lang=' . $param;
    }

    public static function getLinkStatistic($worldId)
    {
        return self::idToDomain($worldId) . self::$linkToStatistic;
    }

    public static function getLinkConstData($worldId)
    {
        if ($worldId >= 3000 && $worldId < 4000) {
            return self::idToDomain($worldId) . '/js/gen/const.js';
        } else {
            return self::idToDomain($worldId) . self::$linkToConstData;
        }
    }

    public static function getLinkConst1Data($worldId)
    {
        return self::idToDomain($worldId) . self::$linkToConst1Data;
    }

    public static function getGameCountries()
    {
        return self::$countries;
    }

    public static function getGameUrl($country)
    {
        return isset(self::$rootDomains[$country]) ? self::$rootDomains[$country] : 'unknown';
    }

    public static function check()
    {
        $gameCountries = Wofh::getGameCountries();

        foreach ($gameCountries as $sign => $gameCountry) {
            $statusLink = Wofh::getLinkStatusWorlds($gameCountry);
            $data = FS::readURL($statusLink);
            if (FS::getCurlErrorNo() != 0) {
                continue;
            }

            $json = json_decode($data, true);
            if (json_last_error() != JSON_ERROR_NONE) {
                continue;
            }

            foreach ($json['worlds'] as $domain => $status) {
                $name = $domain;
                if (isset($status['name'])) {
                    $name = $status['name'];
                    if (strpos($status['name'], '-') !== false) {
                        list ($a, $name) = explode('-', $status['name']);
                    }
                }
                $name = trim($name);
                $id = Wofh::domainToId($domain);
                if ($id) {
                    $world = Wofh::getWorld($id);
                    $world->title = ($world->title && ($world->title == $domain)) ? $name : $world->title;
                    $world->canReg = (bool)(int)$status['canreg'];
                    $world->working = (bool)(int)$status['working'];
                    $world->needCode = false;//(bool)(int)$status['needcode'];
                    $world->save();
                }
                unset($world);
            }
        }
    }

    /**
     * @param array $options
     *
     * @return World[]
     */
    public static function getWorlds(array $options = array())
    {
        $opts = array(
            '!WORKING' => 'working = 0',
            'WORKING' => 'working = 1',
            'STATISTIC' => 'statistic = 1',
        );
        $sWhere = '';
        $rgWhere = array();
        $worlds = array();
        foreach ($options as $option) {
            if (array_key_exists($option, $opts)) {
                $rgWhere[] = $opts[$option];
            }
        }
        if (!empty($rgWhere)) {
            $sWhere = ' AND ' . join(' AND ', $rgWhere);
        }

        $db = Registry::getDb();
        $db->query("
			SELECT *
			FROM wt_worlds
			WHERE id <> 2001
			AND id <> 2002
			AND id >= 1015
			$sWhere
			ORDER BY id DESC");
        while ($row = $db->fetchObject(__NAMESPACE__ . '\World')) {
            $row->domain = self::idToDomain($row->id);
            $worlds[$row->id] = $row;
            self::$_worldsList[$row->id] = $row;
        }

        usort($worlds, function ($a, $b) {
            $cid1 = floor($a->id / 1000);
            $cid2 = floor($b->id / 1000);

            if ($cid1 == $cid2) {
                if ($a->id == $b->id) {
                    return 0;
                }
                return $a->id < $b->id ? 1 : -1;
            }

            return $cid1 > $cid2 ? 1 : -1;

        });

        return $worlds;
    }

    /**
     * @param int $id
     *
     * @return World | null
     */
    public static function getWorld($id)
    {
        if (!isset(self::$_worldsList[$id])) {

            $db = Registry::getDb();
            $db->query("SELECT * FROM wt_worlds WHERE id = :id", array('id' => $id));

            if (!$db->rowCount()) {

                self::$_worldsList[$id] = new World(array(
                    'id' => $id,
                    'sign' => Wofh::idToSign($id),
                    'title' => Wofh::idToSign($id),
                    'started' => date(STD_DATETIME),
                    'domain' => self::idToDomain($id),
                ));

            } else {

                while ($row = $db->fetchObject(__NAMESPACE__ . '\World')) {
                    $row->domain = self::idToDomain($row->id);
                    self::$_worldsList[$row->id] = $row;
                }

            }

        }

        return isset(self::$_worldsList[$id]) ? self::$_worldsList[$id] : null;
    }

    /**
     * @param $sign
     *
     * @return World | null
     */
    public static function getWorldBySign($sign)
    {
        if (!$sign) {
            return null;
        }
        $id = self::signToId($sign);
        if (!$id) {
            return null;
        }
        return self::getWorld($id);
    }
}
