<?php

/**
 * @since             25.01.2016 14:07
 * @package           Wofh Tools
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models;

require_once __DIR__ . '/jtokenizer.php';

class JsDataExtractor
{

    const J_TYPE_OBJECT = 'OBJECT';
    const J_TYPE_ARRAY = 'ARRAY';

    private $jsSource;
    private $tokens;

    public function importData($jsSource, array $identifiers)
    {
        $identifiersLen = count($identifiers);
        $identifiers = join('', $identifiers);
        $this->tokens = array();
        $this->jsSource = $jsSource;
        $tokens = j_token_get_all($this->jsSource, false);

        //$tokens = array_filter($tokens, function ($token) {
        //    return ( $token[0] != J_COMMENT ) && ( $token[0] != J_LINE_TERMINATOR );
        //});

        $search = array();
        $off = true;
        $depth = 0;
        foreach ($tokens as $token) {
            if (($token[0] == J_COMMENT) || ($token[0] == J_LINE_TERMINATOR)) {
                continue;
            }
            $search[] = $token[1];
            if (count($search) > $identifiersLen) {
                array_shift($search);
            }
            if (!$off) {
                $this->tokens[] = $token;
                if ($token[0] == '[' || $token[0] == '{') {
                    $depth++;
                }
                if ($token[0] == ']' || $token[0] == '}') {
                    $depth--;
                }
                if ($depth == 0) {
                    break;
                }
            }
            if (join('', $search) == $identifiers) {
                $off = false;
            }
        }

        //$this->echoTokens();
    }

    public function getData()
    {
        $depth = 0;
        $result = array();
        $cursor = null;
        $identifier = null;
        $ignoreComma = false;
        $key = null;

        foreach ($this->tokens as $k => $token) {
            list($tokenType, $tokenValue, $t2, $t3) = $token;
            if ($ignoreComma && $tokenType == ',') {
                $ignoreComma = false;
                continue;
            }
            $ignoreComma = false;
            switch ($tokenType) {
                case ':':
                    $result[$depth]['data'][$identifier] = null;

                    $cursor = array(
                        'depth' => $depth,
                        'key' => $identifier,
                    );
                    continue 2;
                case ',':
                    if ($result[$depth]['type'] == self::J_TYPE_OBJECT) {
                        $result[$depth]['data'][$cursor['key']] = $identifier;
                    }
                    if ($result[$depth]['type'] == self::J_TYPE_ARRAY) {
                        $result[$depth]['data'][] = $identifier;
                    }
                    continue 2;
                case '[':
                    $result[($depth + 1)] = array(
                        'type' => self::J_TYPE_ARRAY,
                        'pointer' => is_null($cursor) ? $cursor['depth'] : $depth,
                        'key' => ($result[$depth]['type'] == self::J_TYPE_ARRAY) ? null : $cursor['key'],
                        'data' => array(),
                    );
                    $depth++;
                    break;
                case '{':
                    $result[($depth + 1)] = array(
                        'type' => self::J_TYPE_OBJECT,
                        'pointer' => $depth !== 0 ? $depth : null,
                        'key' => ($result[$depth]['type'] == self::J_TYPE_ARRAY) ? null : $cursor['key'],
                        'data' => array(),
                    );
                    $depth++;
                    break;
                case ']':
                    if (!is_null($identifier)) {
                        $result[$depth]['data'][] = $identifier;
                    }
                    $identifier = null;
                    $prevCursor = array(
                        'depth' => $result[$depth]['pointer'],
                        'key' => $result[$depth]['key'],
                    );
                    if (!is_null($prevCursor['depth'])) {
                        if (is_null($prevCursor['key'])) {
                            $result[$prevCursor['depth']]['data'][] = $result[$depth]['data'];
                        } else {
                            $result[$prevCursor['depth']]['data'][$prevCursor['key']] = $result[$depth]['data'];
                        }
                        unset($result[$depth]);
                    }
                    $depth--;
                    $ignoreComma = true;
                    break;
                case '}':
                    if (!is_null($identifier) && $cursor) {
                        $result[$depth]['data'][$cursor['key']] = $identifier;
                    }
                    $identifier = null;
                    $prevCursor = array(
                        'depth' => $result[$depth]['pointer'],
                        'key' => $result[$depth]['key'],
                    );
                    if (!is_null($prevCursor['depth']) /*&& $prevCursor['depth']*/) {
                        if (is_null($prevCursor['key'])) {
                            $result[$prevCursor['depth']]['data'][] = $result[$depth]['data'];
                        } else {
                            $result[$prevCursor['depth']]['data'][$prevCursor['key']] = $result[$depth]['data'];
                        }
                        unset($result[$depth]);
                    }
                    $depth--;
                    $ignoreComma = true;
                    break;
                case J_IDENTIFIER:
                case '-':
                case J_STRING_LITERAL:
                case J_NUMERIC_LITERAL:
                    $tokenValue = trim($tokenValue, '\'"');
                    if ($identifier == '-' && $tokenType == J_NUMERIC_LITERAL) {
                        $identifier .= $tokenValue;
                    } else {
                        $identifier = $tokenValue;
                    }
                    break;
            }
        }

        return $result[1]['data'];
    }

    public function echoTokens()
    {
        echo '<style>
.wt-debug-tokens-list {max-height:400px;overflow:auto;margin-bottom: 20px;padding: 5px;border:1px solid #777;}
.wt-debug-tokens-list > div {display: inline-block;vertical-align: top;font-family: monospace;border: 1px solid #999;padding:3px 5px;}
.wt-debug-tokens-list > div > span:first-child {color:#999;}
</style>';
        echo '<h1 style="font-size: 20px;">' . count($this->tokens) . '</h1>';
        echo "<div class='wt-debug-tokens-list'>";
        foreach ($this->tokens as $token) {
            echo "<div><span>{$token[0]}</span> <span>{$token[1]}</span></div> ";
        }
        echo "</div> ";
    }
}
