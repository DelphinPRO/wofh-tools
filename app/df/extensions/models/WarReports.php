<?php
/**
 * @since             31.07.14 20:33
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models;

use df\Model;

/**
 * @property Wofh wofh
 */
class WarReports extends Model
{
    /**
     * @param $key
     *
     * @return bool|WarReport
     */
    public function getReport($key)
    {
        $this->db->query("SELECT * FROM wt_reports_war WHERE id = '$key'");
        if (!$report = $this->db->fetchObject(__NAMESPACE__ . '\WarReport')) {
            return false;
        }
        $report->report = new ReportData($report->report);

        return $report;
    }

    public function getTotal($worldId = 0)
    {
        $where = '';
        if ($worldId > 0) {
            $where .= " worldId = " . (int)$worldId;
        }
        if ($where) {
            $where = 'WHERE ' . $where;
        }
        $this->db->query("SELECT COUNT(*) FROM wt_reports_war $where");
        $count = (int)$this->db->fetchColumn();

        return $count;
    }

    public function getList($worldId, $page = 1)
    {
        $where = '';
        if ($worldId > 0) {
            $where .= " worldId = " . (int)$worldId;
        }
        if ($where) {
            $where = 'WHERE ' . $where;
        }
        $limitCount = 20;
        $limitStart = ($page - 1) * $limitCount;
        $logs = array();
        $this->db->query("SELECT * FROM wt_reports_war $where
				ORDER BY generateTime DESC LIMIT $limitStart, $limitCount");
        $latestLogs = $this->db->fetchAll();

        foreach ($latestLogs as $log) {
            $log['report'] = json_decode($log['report'], true);

            $logs[] = array(
                'id' => $log['id'],
                'url' => '/log/' . $log['id'] . '/',
                'title1' => $log['report']['aggressor']['title'],
                'title2' => $log['report']['defending']['title'],
                'time1' => date('d.m.Y - H:i', strtotime($log['reportTime'])),
                'time2' => date('d.m.Y - H:i', strtotime($log['generateTime'])),
                'worldTitle' => $log['worldSign'] . ' — ' . $log['worldTitle'],
                'spy' => $log['spy'],
                'verified' => $log['verified'],
                'id1' => $log['aggressorId'],
                'id2' => $log['defendingId'],
                'owner' => $log['owner'],
            );
        }

        return $logs;
    }

    public function getCountPerDay()
    {
        try {
            $this->db->query("SELECT COUNT(*) AS countReports FROM wt_reports_war WHERE generateTime >= :now - INTERVAL 1 DAY",
                array('now' => date('Y-m-d H:i:s')));

            return $this->db->fetchObject()->countReports;
        } catch (\PDOException $e) {
            if ($e->getCode() == '42S02') {
                return 0;
            }
            throw $e;
        }
    }

    public function getCountToday()
    {
        try {
            $this->db->query("SELECT COUNT(*) AS countReports FROM wt_reports_war WHERE generateTime >= :now",
                array('now' => date('Y-m-d')));

            return $this->db->fetchObject()->countReports;
        } catch (\PDOException $e) {
            if ($e->getCode() == '42S02') {
                return 0;
            }
            throw $e;
        }
    }

    public function getWorldsAvailable($selectedWorldId)
    {
        $this->db->query("SELECT DISTINCT worldId, worldSign FROM wt_reports_war ORDER BY worldId");
        $result = array();
        while ($row = $this->db->fetch()) {
            if ($row['worldId'] == $selectedWorldId) {
                $row['active'] = ' active';
            } else {
                $row['active'] = '';
            }
            $result[] = $row;
        }

        return $result;
    }

    public function transfer()
    {
        $counter = 0;
        $reports = $this->db->query("SELECT * FROM wt_armylog")->fetchAll();
//		$exists = $this->db->query("SELECT id FROM wt_reports_army")->fetchAll();

        foreach ($reports as $log) {
            $report = new WarReport();
            $counter++;
//			if ($counter < 3) continue;
//			if ($counter > 100) break;
//			echo $counter, ', ';

            $data = json_decode($log['log_data'], true);
            $jsonError = json_last_error();
            if ($jsonError != JSON_ERROR_NONE) {
//				echo JSON::getErrorMessage(json_last_error()). ' ';
                continue;
            }
            if (is_string($data['data2']['account1'])) {
                $data['data2']['account1'] = json_decode($data['data2']['account1'], true);
                $jsonError = json_last_error();
                if ($jsonError != JSON_ERROR_NONE) {
//					echo JSON::getErrorMessage(json_last_error()). ' ';
                    continue;
                }
            }
            if (is_string($data['data2']['account2'])) {
                $data['data2']['account2'] = json_decode($data['data2']['account2'], true);
                $jsonError = json_last_error();
                if ($jsonError != JSON_ERROR_NONE) {
//					echo JSON::getErrorMessage(json_last_error()). ' ';
                    continue;
                }
            }
            $worldId = $this->wofh->domain2Id($data['data2']['domain']);
//			pre($worldId);

            if ('w1.test.wofh.ru' == $data['data2']['domain']) {
                continue;
            } // TODO: w2.test.wofh.ru
            if ('w2.test.wofh.ru' == $data['data2']['domain']) {
                continue;
            } // TODO: w2.test.wofh.ru

            if ('w1.wofh.de' == $data['data2']['domain']) {
//				dmp($log, '$log', 0); dmp($data, '$data', 0); dmp((array)$report);
            }
            unset($log['log_data']);

            $id = $log['id'];
            $sign = $this->wofh->id2sign($worldId);
            $options = json_decode($log['options'], true);
            $verified = (bool)(int)$log['verified'];

            $res = $report->create($id, $sign, $data, $verified, $options);

            if ($res !== true) {
                echo '<div>', $counter, '</div>';
                echo '<table style="width:100%;table-layout:fixed;"><tr><td style="width:50%;vertical-align:top;">';
                $report->report = (array)$report->report;
                dmp((array)$report, 'report', 0);
                echo '</td><td style="width:50%;vertical-align:top;">';
                dmp((array)$res, 'остаток', 0);
                echo '</td></tr></table>';
                exit;
            }

            $report->save();

//			$report['report'] = json_decode($report['report'], true);
            unset($report);
        }

//		die ( 'END - Обработано ' . $counter );
        return $counter . ' Reports transfer.';
    }
}
