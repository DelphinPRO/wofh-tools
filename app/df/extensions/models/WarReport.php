<?php
/**
 * @since               18.01.14 03:18
 * @package             Wofh-Tools
 * @author              DelphinPRO delphinpro@yandex.ru
 * @copyright           Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license             Licensed under the MIT license
 */

namespace df\extensions\models;

use df\exception\DFException;
use df\helper\FS;
use df\helper\JSON;
use df\Model;

/**
 * Class WarReport
 *
 * @package df\component\tools
 * @property Wofh $wofh
 */
class WarReport extends Model
{
    public $id = '';
    public $worldId;
    public $worldSign = '';
    public $worldTitle = '';
    public $generateTime;
    public $reportTime;
    public $domain = '';
    public $owner;
    public $spy;
    public $aggressorId;
    public $aggressorTownId;
    public $aggressorCountryId;
    public $defendingId;
    public $defendingTownId;
    public $defendingCountryId;
    public $private;
    public $validKey = '';
    public $verified;

    /** @var ReportData */
    public $report;

    public $timeString;

    public function __construct()
    {
        $this->worldId = (int)$this->worldId;
        $this->generateTime = $this->generateTime ? strtotime($this->generateTime) : null;
        $this->reportTime = $this->reportTime ? strtotime($this->reportTime) : null;
        $this->owner = (int)$this->owner;
        $this->spy = (bool)(int)$this->spy;
        $this->aggressorId = (int)$this->aggressorId;
        $this->aggressorTownId = (int)$this->aggressorTownId;
        $this->aggressorCountryId = (int)$this->aggressorCountryId;
        $this->defendingId = (int)$this->defendingId;
        $this->defendingTownId = (int)$this->defendingTownId;
        $this->defendingCountryId = (int)$this->defendingCountryId;
        $this->private = (bool)(int)$this->private;
        $this->verified = (bool)$this->verified;
        $this->report = json_decode($this->report, true);
        $this->timeString = $this->getTimeString();

        $this->report['aggressor']['army'] = $this->_loadTactic($this->worldId, $this->report['aggressor']['army']);
        $this->report['defending']['army'] = $this->_loadTactic($this->worldId, $this->report['defending']['army']);
        $this->report['aggressor']['survive'] = $this->_loadTactic($this->worldId,
            $this->report['aggressor']['survive']);
        $this->report['defending']['survive'] = $this->_loadTactic($this->worldId,
            $this->report['defending']['survive']);
        $this->report['aggressor']['losses'] = $this->_loadTactic($this->worldId, $this->report['aggressor']['losses']);
        $this->report['defending']['losses'] = $this->_loadTactic($this->worldId, $this->report['defending']['losses']);
        if ($this->report['spy']) {
            if ($this->report['spy']['armyOwn']) {
                $this->report['spy']['armyOwn'] = $this->_loadTactic($this->worldId, $this->report['spy']['armyOwn']);
            }
            if ($this->report['spy']['armyTotal']) {
                $this->report['spy']['armyTotal'] = $this->_loadTactic($this->worldId,
                    $this->report['spy']['armyTotal']);
            }
        }
    }

    public function create($id, $sign, $data, $options = array())
    {
//		pre($sign .': ' . $id, 0);
//		dmp($data, '$data', 0);

        $world = Wofh::getWorldBySign($sign);

        $aggressor = null;
        $defending = null;

        try {
            $this->db->setPrefix('ruXX', $world->lowerSign);
            $this->db->prepare("
				SELECT c.countryId, c.countryTitle, c.countryFlag
				FROM z_ruXX_accounts p
				JOIN z_ruXX_countries c ON c.countryId = p.countryId
				WHERE p.accountId = :accountId");
            $aggressor = $this->db
                ->execute(array('accountId' => $data['account1'][0]))
                ->fetch();
            $defending = $this->db
                ->execute(array('accountId' => $data['account2'][0]))
                ->fetch();
        } catch (\PDOException $e) {
//			echo $id, '<br>';
//			echo $sign, '<br>';
//			dmp($data, '$data', 0);
//			DFException::handlerException($e); // TODO
//			exit;
        }
//		pre($aggressor, 0);
//		pre($defending, 0);

        FS::saveFile(FS::path('/tmp/warlogs/' . $id . '.tmp.json'), json_encode($data));
        $this->id = $id;
        $this->worldId = $world->id;
        $this->worldSign = $sign;
        $this->worldTitle = $world->title;
        $this->generateTime = time();
        $this->reportTime = $data['time'];
        $this->domain = $data['domain'];
        $this->owner = $data['owner'];
        $this->spy = isset($data['spy']);
        $this->aggressorId = $data['account1'][0];
        $this->aggressorTownId = $data['town1'][0];
        $this->aggressorCountryId = $aggressor ? (int)$aggressor['countryId'] : 0;
        $this->defendingId = $data['account2'][0];
        $this->defendingTownId = $data['town2'][0];
        $this->defendingCountryId = $defending ? (int)$defending['countryId'] : 0;
        $this->private = true;
        $this->validKey = $data['key'];
        $this->verified = true;
        $this->report = new ReportData();
        $this->timeString = $this->getTimeString();

        $this->report->aggressor = null;
        $this->report->defending = null;
        $this->report->spy = isset($data['spy']);
        $this->report->grab = isset($data['data']['grabbed']) ? wt_decodeAZ($data['data']['grabbed']) : null;
        $this->report->full = isset($data['data']['fullload']);
        $this->report->result = $data['data']['result'];
        $this->report->kill = isset($data['data']['killed']) ? $data['data']['killed'] : null;
        $this->report->destroy = isset($data['data']['destroy']) ? $data['data']['destroy'] : null;
        $this->report->fort = isset($data['data']['defstruct'])
            ? array(
                'id' => $data['data']['defstruct'][0],
                'level' => $data['data']['defstruct'][1]
            ) : null;
        $this->report->aDamage = isset($data['data']['aaddamage']) ? $data['data']['aaddamage'] : null;
        $this->report->defDamage = isset($data['data']['defdamage']) ? $data['data']['defdamage'] : null;
        $this->report->armyLand = isset($data['data']['armyland']) ? wt_decode26($data['data']['armyland']) : null;
        $this->report->surviveLand = isset($data['data']['surviveland']) ? wt_decode26($data['data']['surviveland']) : null;
        $this->report->deathAir = isset($data['data']['deathair']) ? wt_decode26($data['data']['deathair']) : null;
        $this->report->wonderStop = isset($data['data']['wonderstop']) ? $data['data']['wonderstop'] : null;
//		$this->report->options     = $log['options'];

        $this->report->aggressor = array(
            'title' => $data['account1'][1],
            'town' => $data['town1'][1],
            'country' => $aggressor ? $aggressor['countryTitle'] : null,
            'flag' => $aggressor ? $aggressor['countryFlag'] : null,
            'sex' => $data['account1'][2],
            'race' => $data['account1'][3],
            'bonus' => isset($data['data']['bonus1']) ? $data['data']['bonus1'] : null,
            'army' => isset($data['data']['army1']) ? wt_decode26($data['data']['army1']) : null,
            'survive' => isset($data['data']['survived1']) ? wt_decode26($data['data']['survived1']) : null,
            'losses' => null, //todo
            'tactic' => isset($data['data']['tactics1']) ? $data['data']['tactics1'] : null,
            'wonder' => isset($data['data']['wonder1']) ? $data['data']['wonder1'] : null,
        );
        if ($this->report->aggressor['army'] != $this->report->aggressor['survive']) {
            $losses = array();
            foreach ($this->report->aggressor['army'] as $k => $count) {
                if (isset($this->report->aggressor['survive'][$k])) {
                    $loss = $count - $this->report->aggressor['survive'][$k];
                    if ($loss > 0) {
                        $losses[$k] = $loss;
                    }
                } else {
                    $losses[$k] = $count;
                }
            }
            $this->report->aggressor['losses'] = $losses;
        }

        $this->report->defending = array(
            'title' => $data['account2'][1],
            'town' => $data['town2'][1],
            'country' => $defending ? $defending['countryTitle'] : null,
            'flag' => $defending ? $defending['countryFlag'] : null,
            'sex' => $data['account2'][2],
            'race' => $data['account2'][3],
            'bonus' => isset($data['data']['bonus2']) ? $data['data']['bonus2'] : null,
            'army' => isset($data['data']['army2']) ? wt_decode26($data['data']['army2']) : null,
            'survive' => isset($data['data']['survived2']) ? wt_decode26($data['data']['survived2']) : null,
            'losses' => null,
            'tactic' => isset($data['data']['tactics2']) ? $data['data']['tactics2'] : null,
            'wonder' => isset($data['data']['wonder2']) ? $data['data']['wonder2'] : null,
        );
        if ($this->report->defending['army'] != $this->report->defending['survive']) {
            $losses = array();
            foreach ($this->report->defending['army'] as $k => $count) {
                if (isset($this->report->defending['survive'][$k])) {
                    $loss = $count - $this->report->defending['survive'][$k];
                    if ($loss > 0) {
                        $losses[$k] = $loss;
                    }
                } else {
                    $losses[$k] = $count;
                }
            }
            $this->report->defending['losses'] = $losses;
        }

        if ($this->report->spy) { // Отчет разведки
            $streams = null;

            if (isset($data['data']['data']['streamlist'])
                && count($data['data']['data']['streamlist'])
            ) {
                $streams = array('in' => array(), 'out' => array());
                foreach ($data['data']['data']['streamlist'] as $stream) {
                    $key = $stream['in'] ? 'in' : 'out';
                    $streams[$key][] = array(
                        'res' => $stream['res'],
                        'count' => $stream['count'],
                        'townId' => $stream['town'],
                        'town' => isset ($data['data2']['townnames'][$stream['town']])
                            ? $data['data2']['townnames'][$stream['town']]
                            : '[Нет данных]',
                        // TODO: страна, игрок
                    );
                }
            }
            $this->report->spy = array(
                'streams' => $streams,
                'res' => isset($data['data']['data']['res']) ? wt_decodeAZ($data['data']['data']['res']) : null,
                'armyOwn' => isset($data['data']['data']['armyown']) ? wt_decode26($data['data']['data']['armyown']) : null,
                'armyTotal' => isset($data['data']['data']['armythere']) ? wt_decode26($data['data']['data']['armythere']) : null,
                'level' => isset($data['data']['data']['level']) ? $data['data']['data']['level'] : null,
                'fort' => isset($data['data']['data']['defstruct'])
                    ? array(
                        'id' => $data['data']['data']['defstruct'][0],
                        'level' => $data['data']['data']['defstruct'][1]
                    ) : null,
            );
        }

//		if('w1.wofh.de' == $data['data2']['domain']) {
//			dmp($data, '$data', 0);
//			dmp((array)$this->report, 0);
//			dmp((array)$this);
//		}

        unset(
            $data['spy'], $data['key'],
            $data['data']['account1'], $data['data']['account2'],
            $data['data']['army1'], $data['data']['army2'],
            $data['data']['bonus1'], $data['data']['bonus2'],
            $data['data']['defstruct'], $data['data']['fullload'], $data['data']['grabbed'],
            $data['data']['race1'], $data['data']['race2'], $data['data']['result'],
            $data['data']['survive1'], $data['data']['survive2'],
            $data['data']['tactics1'], $data['data']['tactics2'],
            $data['data']['wonder1'], $data['data']['wonder2'],
            $data['data']['killed'],

            $data['data']['destroy'], $data['data']['aaddamage'],
            $data['data']['armyland'], $data['data']['surviveland'],
            $data['data']['defdamage'], $data['data']['deathair'],
            $data['data']['wonderstop'],

            $data['data']['data']['armyown'], $data['data']['data']['armythere'],
            $data['data']['data']['defstruct'], $data['data']['data']['level'],
            $data['data']['data']['res'], $data['data']['data']['streamlist'],

            $data['account1'][0], $data['account1'][1],
            $data['account1'][2], $data['account1'][3],
            $data['account2'][0], $data['account2'][1],
            $data['account2'][2], $data['account2'][3],
            $data['domain'], $data['owner'],
            $data['generatetime'], $data['time'],
            $data['town1'][0], $data['town1'][1],
            $data['town2'][0], $data['town2'][1],
            $data['townnames']
        );

        if (empty($data['data']['data'])) {
            unset($data['data']['data']);
        }
        if (empty($data['data'])) {
            unset($data['data']);
        }
        if (empty($data['data2']['account1'])) {
            unset($data['data2']['account1']);
        }
        if (empty($data['data2']['account2'])) {
            unset($data['data2']['account2']);
        }
        if (empty($data['data2']['town1'])) {
            unset($data['data2']['town1']);
        }
        if (empty($data['data2']['town2'])) {
            unset($data['data2']['town2']);
        }
        if (empty($data['data2'])) {
            unset($data['data2']);
        }

//		pre($this, 0);
//		pre($data);

        return empty($data) ? true : $data;
    }

    public function save()
    {
        JSON::$PRETTY = false;
        $data = array(
            'id' => $this->id,
            'worldId' => $this->worldId,
            'worldSign' => $this->worldSign,
            'worldTitle' => $this->worldTitle,
            'generateTime' => date(STD_DATETIME, $this->generateTime),
            'reportTime' => date(STD_DATETIME, $this->reportTime),
            'domain' => $this->domain,
            'ownerId' => $this->owner,
            'spy' => (int)$this->spy,
            'aggressorId' => $this->aggressorId,
            'aggressorTownId' => $this->aggressorTownId,
            'aggressorCountryId' => $this->aggressorCountryId,
            'defendingId' => $this->defendingId,
            'defendingTownId' => $this->defendingTownId,
            'defendingCountryId' => $this->defendingCountryId,
            'private' => (int)$this->private,
            'validKey' => $this->validKey,
            'verified' => (int)$this->verified,
            'report' => JSON::encode($this->report),
        );
        $this->db->prepare("INSERT INTO wt_reports_war VALUES (
				:id, :worldId, :worldSign, :worldTitle, :generateTime, :reportTime,
				:domain, :ownerId, :spy,
				:aggressorId, :aggressorTownId, :aggressorCountryId,
				:defendingId, :defendingTownId, :defendingCountryId,
				:private, :validKey, :verified, :report)
				ON DUPLICATE KEY UPDATE
					report = :report
			");
        $this->db->execute($data);
    }

    public function getMyReports()
    {
        $this->db->query("
			SELECT *
			FROM wt_reports_war
			WHERE worldId = :worldId
			AND `owner` = :ownerId
			ORDER BY reportTime DESC",
            array(
                'worldId' => $this->worldId,
                'ownerId' => $this->owner
            )
        );
        $data = $this->db->fetchAll();
        return $data;
    }

//	public function _saveReport($id, $worldSign, $post, $verified)
//	{
//		$worldId = $this->wofh->sign2id($worldSign);
//
//		$sign = strtolower($worldSign);
//
//		try {
//			$this->db->prepare("
//				SELECT c.countryId, c.countryTitle, c.countryFlag
//				FROM z_{$sign}_accounts p
//				JOIN z_{$sign}_countries c ON c.countryId = p.countryId
//				WHERE p.accountId = :accountId");
//			$aggressor = $this->db
//				->execute(array('accountId' => $post['data2']['account1'][0]))
//				->fetchObject();
//			$defending = $this->db
//				->execute(array('accountId' => $post['data2']['account2'][0]))
//				->fetchObject();
//		}
//		catch (\PDOException $e) {
//			return;
//		}
//
//		$report = array(
//			'id'                 => $id,
//			'worldId'            => $worldId,
//			'worldSign'          => $worldSign,
//			'worldTitle'         => $this->wofh->getWorldBySign($worldSign)->title,
//			'generateTime'       => date(STD_DATETIME, $post['data2']['generatetime']),
//			'reportTime'         => date(STD_DATETIME, $post['data2']['reporttime']),
//			'domain'             => $post['data2']['domain'],
//			'ownerId'            => $post['data2']['owner'],
//			'spy'                => isset( $post['spy'] ) ? 1 : 0,
//			'aggressorId'        => $post['data2']['account1'][0],
//			'aggressorTownId'    => $post['data2']['town1'][0],
//			'aggressorCountryId' => $aggressor ? (int)$aggressor->countryId : null,
//			'defendingId'        => $post['data2']['account2'][0],
//			'defendingTownId'    => $post['data2']['town2'][0],
//			'defendingCountryId' => $defending ? (int)$defending->countryId : null,
//			'private'            => !empty ( $log['options'] ),
//			'validKey'           => $post['key'],
//			'verified'           => $verified,
//		);
//
//		$report['report'] = array(
//			'aggressor'   => null,
//			'defending'   => null,
//			'spy'         => null,
//			'grab'        => isset( $post['data']['grabbed'] ) ? wt_decodeAZ($post['data']['grabbed']) : null,
//			'full'        => (int)isset( $post['data']['fulload'] ),
//			'result'      => $post['data']['result'],
//			'kill'        => isset( $post['data']['killed'] ) ? $post['data']['killed'] : null,
//			'destroy'     => isset( $post['data']['destroy'] ) ? $post['data']['destroy'] : null,
//			'fort'        => isset( $post['data']['defstruct'] )
//					? array('id'    => $post['data']['defstruct'][0],
//					        'level' => $post['data']['defstruct'][1]) : null,
//			'aDamage'     => isset( $post['data']['aaddamage'] ) ? $post['data']['aaddamage'] : null,
//			'defDamage'   => isset( $post['data']['defdamage'] ) ? $post['data']['defdamage'] : null,
//			'armyLand'    => isset( $post['data']['armyland'] ) ? wt_decode26($post['data']['armyland']) : null,
//			'surviveLand' => isset( $post['data']['surviveland'] ) ? wt_decode26($post['data']['surviveland']) : null,
//			'deathAir'    => isset( $post['data']['deathair'] ) ? wt_decode26($post['data']['deathair']) : null,
//			'wonderStop'  => isset( $post['data']['wonderstop'] ) ? $post['data']['wonderstop'] : null,
//			'options'     => $log['options'],
//		);
//
//		$report['report']['aggressor'] = array(
//			'title'   => $post['data2']['account1'][1],
//			'town'    => $post['data2']['town1'][1],
//			'country' => $aggressor ? $aggressor->countryTitle : null,
//			'flag'    => $aggressor ? $aggressor->countryFlag : null,
//			'sex'     => $post['data2']['account1'][2],
//			'race'    => $post['data2']['account1'][3],
//			'bonus'   => isset( $post['data']['bonus1'] ) ? $post['data']['bonus1'] : null,
//			'army'    => isset( $post['data']['army1'] ) ? wt_decode26($post['data']['army1']) : null,
//			'survive' => isset( $post['data']['survive1'] ) ? wt_decode26($post['data']['survive1']) : null,
//			'losses'  => null, //todo
//			'tactic'  => isset( $post['data']['tactics1'] ) ? $post['data']['tactics1'] : null,
//			'wonder'  => isset( $post['data']['wonder1'] ) ? $post['data']['wonder1'] : null,
//		);
//		$report['report']['defending'] = array(
//			'title'   => $post['data2']['account2'][1],
//			'town'    => $post['data2']['town2'][1],
//			'country' => $defending ? $defending->countryTitle : null,
//			'flag'    => $defending ? $defending->countryFlag : null,
//			'sex'     => $post['data2']['account2'][2],
//			'race'    => $post['data2']['account2'][3],
//			'bonus'   => isset( $post['data']['bonus2'] ) ? $post['data']['bonus2'] : null,
//			'army'    => isset( $post['data']['army2'] ) ? wt_decode26($post['data']['army2']) : null,
//			'survive' => isset( $post['data']['survive2'] ) ? wt_decode26($post['data']['survive2']) : null,
//			'losses'  => null,
//			'tactic'  => isset( $post['data']['tactics2'] ) ? $post['data']['tactics2'] : null,
//			'wonder'  => isset( $post['data']['wonder2'] ) ? $post['data']['wonder2'] : null,
//		);
//
//		unset( $post['spy'], $post['key'], $post['data']['account1'], $post['data']['account2'],
//		$post['data']['army1'], $post['data']['army2'], $post['data']['bonus1'], $post['data']['bonus2'],
//		$post['data']['defstruct'], $post['data']['fullload'], $post['data']['grabbed'],
//		$post['data']['race1'], $post['data']['race2'], $post['data']['result'],
//		$post['data']['survive1'], $post['data']['survive2'],
//		$post['data']['tactics1'], $post['data']['tactics2'],
//		$post['data']['wonder1'], $post['data']['wonder2'],
//		$post['data']['killed'],
//
//		$post['data']['destroy'], $post['data']['aaddamage'],
//		$post['data']['armyland'], $post['data']['surviveland'],
//		$post['data']['defdamage'], $post['data']['deathair'],
//		$post['data']['wonderstop'],
//
//		$post['data']['data']['armyown'], $post['data']['data']['armythere'],
//		$post['data']['data']['defstruct'], $post['data']['data']['level'],
//		$post['data']['data']['res'], $post['data']['data']['streamlist'],
//
//		$post['data2']['account1'], $post['data2']['account2'], $post['data2']['domain'],
//		$post['data2']['generatetime'], $post['data2']['reporttime'], $post['data2']['owner'],
//		$post['data2']['town1'], $post['data2']['town2'], $post['data2']['townnames']
//		);
//
//		if (empty( $post['data']['data'] )) unset( $post['data']['data'] );
//		if (empty( $post['data'] )) unset( $post['data'] );
//		if (empty( $post['data2'] )) unset( $post['data2'] );
//
////		if (!empty( $post )) {
////			echo $counter;
////			echo '<div style="width:49.5%;float:left;">';
////			dump($log, 0);
////			dump($post, 0);
////			echo '</div>';
////			echo '<div style="width:49.5%;float:right;">';
////			dump($report);
////			echo '</div>';
////		}
//
//		$report['report'] = json_encode($report['report']);
//		$this->db->prepare("INSERT INTO wt_reports_war VALUES (
//				:id, :worldId, :worldSign, :worldTitle, :generateTime, :reportTime,
//				:domain, :ownerId, :spy,
//				:aggressorId, :aggressorTownId, :aggressorCountryId,
//				:defendingId, :defendingTownId, :defendingCountryId,
//				:private, :validKey, :verified, :report)
//				ON DUPLICATE KEY UPDATE
//					report = :report
//			");
//		$this->db->execute($report);
//	}

    public function getValidId()
    {
        $i = 0;
        do {
            $id = substr(strtolower(md5(uniqid())), 0, 12);
            if ($i++ > 100) {
                die('System error: Invalid ID.');
            }
        } while ((int)$this->db->query("SELECT COUNT(*) FROM wt_reports_war WHERE id = '" . $id . "'")->fetchColumn());
        return $id;
    }

    public function getUnits($wid)
    {
        $this->db->query("SELECT unitId, title FROM cd_units WHERE worldId = :id",
            array('id' => (int)$wid));
        $units = $this->db->fetchByField('unitId');
        return $units;
    }

    /**
     * @param $key
     *
     * @return array
     * @deprecated
     */
    public function getReport($key)
    {
        $reportData['generate_time'] = strtotime($reportData['generate_time']);
        $reportData['report_time'] = strtotime($reportData['report_time']);

        $privateOptions = json_decode($reportData['options'], true);
        $privateOptions = array(
            'an' => isset($privateOptions['an']) ? $privateOptions['an'] : 0,
            'as' => isset($privateOptions['as']) ? $privateOptions['as'] : 0,
            'aa' => isset($privateOptions['aa']) ? $privateOptions['aa'] : 0,
            'dn' => isset($privateOptions['dn']) ? $privateOptions['dn'] : 0,
            'ds' => isset($privateOptions['ds']) ? $privateOptions['ds'] : 0,
            'da' => isset($privateOptions['da']) ? $privateOptions['da'] : 0,
        );

        $data = json_decode($reportData['log_data'], true);

        $reportTime = date('j', $reportData['report_time'])
            . ' ' . getMonth(date('n', $reportData['report_time']))
            . ' ' . date('Y', $reportData['report_time']) . ' года'
            . ' в ' . date('H:i', $reportData['report_time']) . '';

        $report = array(
            'domain' => $data['data2']['domain'],
            'reportTime' => date('Y.m.d H:i', (int)$data['data2']['reporttime']),
            'reportTimeString' => $reportTime,
            'generateTime' => date('d.m.Y H:i', (int)$data['data2']['generatetime']),
            'aggressor' => array(
                'id' => (int)$data['data']['account1'],
                'name' => isset($data['data2']['account1'][1]) ? $data['data2']['account1'][1] : '[Нет данных]',
                'townId' => (int)$data['data2']['town1'][0],
                'town' => $data['data2']['town1'][1],
                'bonus' => '',
                'army' => isset($data['data']['army1']) ? wt_decode26($data['data']['army1']) : array(),
                'survive' => array(),
                'losses' => array(),
            ),
            'defending' => array(
                'id' => (int)$data['data']['account2'],
                'name' => isset($data['data2']['account2'][1]) ? $data['data2']['account2'][1] : '[Нет данных]',
                'townId' => (int)$data['data2']['town2'][0],
                'town' => $data['data2']['town2'][1],
                'bonus' => '',
                'army' => isset($data['data']['army2']) ? wt_decode26($data['data']['army2']) : array(),
                'survive' => array(),
                'losses' => array(),
            ),
        );
        if (isset($data['data']['bonus1'])) {
            $report['aggressor']['bonus'] = (int)$data['data']['bonus1'];
        }
        if (isset($data['data']['bonus2'])) {
            $report['defending']['bonus'] = (int)$data['data']['bonus2'];
        }

        if (isset($data['data']['grabbed'])) {
            $report['grabbed'] = wt_decodeAZ($data['data']['grabbed']);
        }

        if (isset($data['data']['fullload'])) {
            $report['fullLoad'] = $data['data']['fullload'] == 1;
        }

        if (isset($data['data']['survive1'])) {
            $report['aggressor']['survive'] = wt_decode26($data['data']['survive1']);
        }
        foreach ($report['aggressor']['army'] as $unitId => $count) {
            if (isset($report['aggressor']['survive'][$unitId])) {
                if ($count != $report['aggressor']['survive'][$unitId]) {
                    $report['aggressor']['losses'][$unitId] = $count - $report['aggressor']['survive'][$unitId];
                }
            } else {
                $report['aggressor']['losses'][$unitId] = $count;
            }
        }

        if (isset($data['data']['survive2'])) {
            $report['defending']['survive'] = wt_decode26($data['data']['survive2']);
        }
        foreach ($report['defending']['army'] as $unitId => $count) {
            if (isset($report['defending']['survive'][$unitId])) {
                if ($count != $report['defending']['survive'][$unitId]) {
                    $report['defending']['losses'][$unitId] = $count - $report['defending']['survive'][$unitId];
                }
            } else {
                $report['defending']['losses'][$unitId] = $count;
            }
        }

        $report['aggressor']['army'] = $this->_loadTactic($report['aggressor']['army']);
        $report['defending']['army'] = $this->_loadTactic($report['defending']['army']);

        if (isset($data['data']['wonder1'])) {
            $report['aggressor']['wonder'] = $data['data']['wonder1'];
        }

        if (isset($data['data']['wonder2'])) {
            $report['defending']['wonder'] = $data['data']['wonder2'];
        }

        if (isset($data['data']['destroy'])) {
            $report['destroy'] = $data['data']['destroy'];
        }

        if (isset($data['data']['killed'])) {
            $report['killed'] = (int)$data['data']['killed'];
        }

        if (isset($data['data']['defstruct'])) {
            $report['fort'] = array(
                'id' => (int)$data['data']['defstruct'][0],
                'level' => (int)$data['data']['defstruct'][1],
            );
        }
        $report['spy'] = isset($data['spy']);
        if (isset($data['spy'])) {

            if (isset($data['data']['data']['streamlist'])) {
                $streams = array('in' => array(), 'out' => array());
                foreach ($data['data']['data']['streamlist'] as $stream) {
                    $key = $stream['in'] ? 'in' : 'out';
                    $streams[$key][] = array(
                        'res' => $stream['res'],
                        'count' => $stream['count'],
                        'townId' => $stream['town'],
                        'town' => isset ($data['data2']['townnames'][$stream['town']])
                            ? $data['data2']['townnames'][$stream['town']]
                            : '[Нет данных]',
                    );
                }

                $report['scout']['streams'] = $streams;
            }

            if (isset($data['data']['data']['res'])) {
                $report['scout']['res'] = wt_decodeAZ($data['data']['data']['res']);
            }

            if (isset($data['data']['data']['armyown'])) {
                $report['scout']['army'] = wt_decode26($data['data']['data']['armyown']);
            }

            if (isset($data['data']['data']['armythere'])) {
                $report['scout']['armyTotal'] = wt_decode26($data['data']['data']['armythere']);
            }

            if (isset($data['data']['data']['defstruct'])) {
                $report['scout']['fort'] = array(
                    'id' => $data['data']['data']['defstruct'][0],
                    'level' => $data['data']['data']['defstruct'][1],
                );
            }
        }

        if (isset($data['data2']['townnames'])) {
            $report['towns'] = $data['data2']['townnames'];
        }

        return array(
            'report' => $report,
            'private' => $privateOptions,
        );
    }

    private function _loadTactic($worldId, $army)
    {
        if (empty($army)) {
            return null;
        }
        $unitsRaw = $this->db->query("
			SELECT unitId, title, unitGroup
			FROM cd_units
			WHERE worldId = $worldId
		")->fetchAll();
        $units = array();
        foreach ($unitsRaw as $unit) {
            $units[$unit['unitId']] = $unit;
        }

        $result = array();
        foreach ($army as $unitId => $unitsCount) {
            $result[$units[$unitId]['unitGroup']][$unitId] = $unitsCount;
        }
        ksort($result);
        return $result;
    }

    private function getTimeString()
    {
        return !is_null($this->reportTime)
            ? date('j', $this->reportTime)
            . ' ' . getMonth(date('n', $this->reportTime))
            . ' ' . date('Y', $this->reportTime) . ' г.'
            . ' в ' . date('H:i', $this->reportTime) . ''
            : '[некорректная дата]';
    }
}

class ReportData
{
    public $aggressor;
    public $defending;
    public $spy;
    public $grab;
    public $full;
    public $result;
    public $kill;
    public $destroy;
    public $fort;
    public $aDamage;
    public $defDamage;
    public $armyLand;
    public $surviveLand;
    public $deathAir;
    public $wonderStop;
    public $options;

    public function __construct($data = null)
    {
        if (!is_null($data)) {
            foreach ($data as $key => $val) {
                $this->$key = $val;
            }
        }
    }
}
