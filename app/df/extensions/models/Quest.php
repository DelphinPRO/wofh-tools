<?php

/**
 * @since       19.04.2016 07:37
 * @package
 * @author      delphinpro delphinpro@yandex.ru
 * @copyright   copyright (C) 2016 delphinpro
 * @license     Licensed under the MIT license
 */

namespace df\extensions\models;

use df\Model;

class Quest extends Model
{
    public function getQuests()
    {
        $quests = $this->db->query("SELECT * FROM wt_quests")->fetchAll();

        foreach ($quests as &$quest) {
            $quest['needs'] = explode(',', $quest['needs']);
            //$level          = $this->getMaxElement($quest['needs']);
            //$result[$level][] = $quest;
        }

        return $quests;
    }

    public function save($data)
    {
        $this->db->beginTransaction();
        $this->db->prepare("UPDATE wt_quests SET x = :x, y = :y WHERE id = :id");
        foreach ($data as $id => $item) {
            $this->db->execute(array(
                'x' => $item['x'],
                'y' => $item['y'],
                'id' => $id,
            ));
        }
        $this->db->commit();
    }

    private function getMaxElement($rg)
    {
        $max = $rg[0];
        foreach ($rg as $item) {
            if ($item > $max) {
                $max = $item;
            }
        }

        return $max;
    }
}
