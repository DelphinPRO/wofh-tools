<?php
/**
 * @since             26.01.14 15:46
 * @package           Wofh Tools
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\models;

use df\exception\DFException;
use df\helper\FS;
use df\helper\JSON;
use df\Model;
use df\Registry;

/**
 * Class GameData
 *
 * @package df\extensions\models
 */
class GameData extends Model
{
    const UNIT_LIST = 'WT_UNIT_LIST';
    const BUILD_LIST = 'WT_BUILD_LIST';
    const WONDER_LIST = 'WT_WONDER_LIST';
    const SCIENCE_LIST = 'WT_SCIENCE_LIST';

    private $_scList;
    private $_nAll;

    private $_constData;
    private $_constCommonData = array();
    private $tokens;

    public function getScience(World $world)
    {
        if (!Registry::exists($world->id . GameData::SCIENCE_LIST)) {
            $this->db->query("
				SELECT *
				FROM cd_science
				WHERE worldId = :worldId
				ORDER BY id
				", array('worldId' => $world->id));
            $data = array();
            while ($row = $this->db->fetch()) {
                $row['bonuses'] = json_decode($row['bonuses'], true);
                $row['cost'] = (int)$row['cost'];
                $data[] = $row;
            }
            Registry::set($world->id . GameData::SCIENCE_LIST, $data);
        }

        return Registry::get($world->id . GameData::SCIENCE_LIST);
    }

    public function getUnits(World $world)
    {
        if (!Registry::exists($world->id . GameData::UNIT_LIST)) {
            $this->db->query("
				SELECT unitId, title
				FROM cd_units
				WHERE worldId = :worldId
				AND health > 0
				ORDER BY title
				", array('worldId' => $world->id));
            $data = $this->db->fetchByField('unitId');
            Registry::set($world->id . GameData::UNIT_LIST, $data);
        }

        return Registry::get($world->id . GameData::UNIT_LIST);
    }

    public function getUnit(World $world, $unitId)
    {
        $this->db->query("
				SELECT *
				FROM cd_units
				WHERE worldId = :worldId
				AND health > 0
				AND unitId = :unitId
				", array('worldId' => $world->id, 'unitId' => $unitId));
        $data = $this->db->fetch();
        $data['cost'] = json_decode($data['cost'], true);
        $data['pay'] = json_decode($data['pay'], true);
        $data['unitId'] = (int)$data['unitId'];
        $data['trainTime'] = (int)$data['trainTime'];
        $data['popCost'] = (int)$data['popCost'];

        return $data;
    }

    public function getBuilds(World $world)
    {
        if (!Registry::exists($world->id . GameData::BUILD_LIST)) {
            $this->db->query("
				SELECT *
				FROM cd_builds
				WHERE worldId = :worldId
				ORDER BY title
				", array('worldId' => $world->id));
            $data = array();
            while ($row = $this->db->fetch()) {
                $row['cost'] = json_decode($row['cost'], true);
                $row['unitsTrain'] = json_decode($row['unitsTrain'], true);
                $data[$row['buildId']] = $row;
            }
            Registry::set($world->id . GameData::BUILD_LIST, $data);
        }

        return Registry::get($world->id . GameData::BUILD_LIST);
    }

    public function getWonders(World $world)
    {
        if (!Registry::exists($world->id . GameData::WONDER_LIST)) {
            $this->db->query("
				SELECT buildId, title
				FROM cd_builds
				WHERE worldId = :worldId
				AND buildType = 16
				ORDER BY title
				", array('worldId' => $world->id));
            $data = $this->db->fetchByField('buildId');
            Registry::set($world->id . GameData::WONDER_LIST, $data);
        }

        return Registry::get($world->id . GameData::WONDER_LIST);
    }

    private function parseTokens()
    {
        $result = array();

        $key = null;
        while (true) {
            $elem = each($this->tokens);
            if (!$elem) {
                break;
            }

            $token = $elem['value'];
            $tokenType = $token[0];
            $tokenVal = $token[1];

            if ($tokenType == '[' || $tokenType == '{') {
                if ($key !== null) {
                    $result[$key] = $this->parseTokens();
                    $key = null;
                } else {
                    $result[] = $this->parseTokens();
                }
            } elseif ($tokenType == J_IDENTIFIER) {
                $key = $tokenVal;
            } elseif ($tokenType == ':') {
                continue;
            } elseif ($tokenType == ',') {
                continue;
            } elseif ($tokenType == '}' || $tokenType == ']') {
                break;
            } elseif ($tokenType == J_STRING_LITERAL) {
                $tokenVal = trim($tokenVal, "'\"");
                if ($key !== null) {
                    $result[$key] = $tokenVal;
                    $key = null;
                } else {
                    $result[] = $tokenVal;
                }
            } else {
                throw new \Exception('Unknown token: ' . $tokenType . ' => ' . htmlspecialchars($tokenVal));
            }
        }

        return $result;
    }

    private function loadConstRaw($remoteFile, $localFile)
    {
        $localFile = FS::path($localFile);
        if (!file_exists($localFile)) {
            //echo 'Download file: ' . $remoteFile . '<br>';
            $constData = FS::readURL($remoteFile);
            FS::saveFile($localFile, $constData, 0776);
        }

        $constRaw = trim(file_get_contents($localFile));
        $start = mb_strpos($constRaw, '{', null, 'UTF-8');
        $end = mb_strrpos($constRaw, '}', null, 'UTF-8');
        $constRaw = mb_substr($constRaw, $start, $end - $start + 1, 'UTF-8');

        $this->_constData = json_decode($constRaw, true);
        $jsonError = json_last_error();
        if ($jsonError != JSON_ERROR_NONE) {
            throw new \Exception(JSON::getErrorMessage($jsonError));
        }
    }

    private function downloadConstFile($remoteFile, $localFile)
    {
        $localFile = FS::path($localFile);
        if (!file_exists($localFile)) {
            //echo 'Download file: ' . $remoteFile . '<br>';
            $constData = FS::readURL($remoteFile);
            $patterns = array('~/\*.+?\*/~s', '~\s+?//.+\r?\n~');
            $constData = preg_replace($patterns, '', $constData);

            FS::saveFile($localFile, $constData, 0776);
        }

        return file_get_contents($localFile);
    }

    private function downloadData(World $world)
    {
        $today = date(STD_DATE);

        FS::mkdir(FS::path("/tmp/const", 0776));

        $fullName = FS::path(sprintf("/tmp/const/const-%s_%s_FULL.json", $world->lowerSign, $today));
        if (file_exists($fullName)) {
            $json = file_get_contents($fullName);
            $this->_constData = json_decode($json, true);
            $jsonError = json_last_error();
            if ($jsonError != JSON_ERROR_NONE) {
                throw new \Exception(JSON::getErrorMessage($jsonError));
            }
        } else {
            $this->loadConstRaw(
                Wofh::getLinkConstData($world->id),
                sprintf("/tmp/const/const-%s_%s.json", $world->lowerSign, $today)
            );

            $dataExtractor = new JsDataExtractor();
            $keys = array('science', 'build', 'unit', 'deposit', 'resource');
            $i18nData = array();
            $template = ($world->id >= 3000 && $world->id < 4000)
                ? "%s/js/src/c.%s.js"
                : "%s/gen/js/ru/_core/c.%s.js";
            foreach ($keys as $what) {
                set_time_limit(30);
                $i18nData[$what] = $this->downloadConstFile(
                    sprintf($template, Wofh::idToDomain($world->id), $what),
                    sprintf("/tmp/const/const-%s_%s_%s.json", $world->lowerSign, $today, $what)
                );
                switch ($what) {
                    case 'science':
                        $list = &$this->_constData['science']['list'];
                        break;
                    case 'build':
                        $list = &$this->_constData['builds'];
                        break;
                    case 'unit':
                        $list = &$this->_constData['units']['list'];
                        break;
                    case 'deposit':
                        $list = &$this->_constData['map']['deposit'];
                        break;
                    case 'resource':
                        $list = &$this->_constData['resource']['data'];
                        break;
                    default:
                        throw new \Exception(__FILE__ . ':' . __LINE__);
                }

                $dataExtractor->importData($i18nData[$what], array(ucfirst($what), '.', 'lib', '='));
                $i18nData[$what] = $dataExtractor->getData();
                foreach ($list as $key => &$value) {
                    $value['name'] = $i18nData[$what][$key]['name'];
                }
            }

            FS::saveFile($fullName, JSON::encode($this->_constData), 0776);
        }
    }

    public function updateConst(World $world)
    {
        $messages = array();

        $this->downloadData($world);

        unset(
            $this->_constData['announce'],
            $this->_constData['chat'],
            $this->_constData['ticket'],
            $this->_constData['tradeally'],
            $this->_constData['trade'],
            $this->_constData['town'],
            $this->_constData['spaceship'],
            $this->_constData['message'],
            $this->_constData['luckbonus'],
            $this->_constData['admin'],
            $this->_constData['country'],
            $this->_constData['mode'],
            $this->_constData['account'],
            $this->_constData['quest']
        );

        //dmp($this->_constData, 0);

        try {
            $this->_updateConstScience($world->id);
        } catch (\Exception $e) {
            $messages[] = 'Ошибка при обновлении данных по наукам';
            $messages[] = $e->getMessage();
        }

        if (!$this->_updateConstUnits($world)) {
            $messages[] = 'Ошибка при обновлении данных по юнитам';
        }

        if (!$this->_updateConstBuilds($world)) {
            $messages[] = 'Ошибка при обновлении данных по строениям';
        }

        if (!$this->_updateConstDeposits($world)) {
            $messages[] = 'Ошибка при обновлении данных по месторождениям';
        }

        if (!$this->_updateConstResources($world->id)) {
            $messages[] .= 'Ошибка при обновлении данных по ресурсам';
        }

        if (empty($message)) {
            $messages[] = 'Обновление констант [' . $world->sign . '] прошло успешно';
            $this->db->simpleQuery("UPDATE wt_worlds SET updateConst = '" . date(STD_DATETIME) . "' WHERE id = '{$world->id}'");
        }

        return join('<br>', $messages);
    }

    private function _updateConstScience($worldId)
    {
        $this->_constCommonData['science_bonuses_k'] = $this->_constData['science']['bonus'];

        try {
            $this->db->beginTransaction();
            $this->db->simpleQuery("DELETE FROM cd_science WHERE worldId = '$worldId'");
            $this->db->prepare("
				INSERT INTO cd_science
				VALUES (:worldId, :id, :title, :cost, :need, :takes, :builds, :units, :bonuses)
			");

            $out = '<pre><table border="1" style="border-spacing: 0;border-collapse: collapse">';
            $out .= '<td>id</td><td>science</td><td>cost</td><td>envs</td><td>bonus</td><td>bonus</td><td>need</td><td>takes</td>';
            foreach ($this->_constData['science']['list'] as $id => &$sc) {
                $builds = array();
                $bonuses = array();
                if (isset($sc['builds'])) {
                    foreach ($sc['builds'] as $build) {
                        $builds[$build[0]] = $build[1];
                    }
                }
                if (isset($sc['bonuses'])) {
                    foreach ($sc['bonuses'] as $bonus) {
                        $bonuses[$bonus[0]] = $bonus[1];
                    }
                }
                $sc['builds'] = $builds;
                $sc['bonuses'] = $bonuses;
                $sc['need'] = json_encode($this->_clearIds($sc['need']));
                $sc['takes'] = json_encode($this->_clearIds($sc['takes']));

                $out .= "<tr>
				<td>$id</td>
				<th><a href='https://w23.wofh.ru/scienceinfo?id=$id' target='_blank'>{$sc['name']}</a></th>
				<td>{$sc['cost']}</td>
				<td>{$sc['envs']}</td>
				<td>";

                $ability = array(
                    1 => "Полная видимость карты",
                    2 => "Новая экономическая система",
                    3 => "Устаревание бартерной торговли",
                    4 => "Возможно производство одежды",
                    5 => "Строительство грунтовых дорог",
                    6 => "Строительство шоссе",
                    7 => "Строительство железной дороги",
                    8 => "Строительство маглев дороги",
                    9 => "Возможна добыча рыбы",
                    10 => "Нет ограничения дальности торговли",
                    11 => "Координация боя",
                );
                if (is_array($sc['bonuses'])) {
                    foreach ($sc['bonuses'] as $bonusId => $bonusVal) {
                        $BONUS = $this->_constData['science']['bonus'][$bonusId];
                        switch ($bonusId) {
                            case 0:
                                $out .= "Рост населения: +" . $bonusVal * 2 . "<br>";
                                break;
                            case 1:
                                $out .= "Культура: +" . round($bonusVal * $BONUS) . "<br>";
                                break;
                            case 2:
                                $out .= "Доход: +" . round($bonusVal * $BONUS * 100) . "%<br>";
                                break;
                            case 3:
                                $out .= "Улучшение НР<br>";
                                break;
                            case 4:
                                $out .= "Война: +" . round($bonusVal * $BONUS) . "%<br>";
                                break;
                            case 5:
                                $out .= "Производство: +" . round($bonusVal * $BONUS * 100) . "%<br>";
                                break;
                            case 6:
                                $out .= '<b>' . $ability[$bonusVal] . "<br></b>";
                                break;
                            case 7:
                                $out .= "Радиус обзора: +" . round($bonusVal * $BONUS) . "<br>";
                                break;
                            case 8:
                                $out .= "Вместимость хранилища: +" . round($bonusVal * $BONUS) . "<br>";
                                break;
                            case 9:
                                $out .= "Торговцы: +" . round($bonusVal * $BONUS) . "<br>";
                                break;
                            case 11:
                                $out .= "Добыча рыбы: +" . round($bonusVal * $BONUS * 100) . "%<br>";
                                break;
                            case 12:
                                $out .= "Добыча гранита: +" . round($bonusVal * $BONUS * 100) . "%<br>";
                                break;
                            case 13:
                                $out .= "Добыча металла: +" . round($bonusVal * $BONUS * 100) . "%<br>";
                                break;
                            default:
                                $out .= "$bonusId => $bonusVal<br>";
                        }
                    }
                }
                $this->db->execute(array(
                    'worldId' => $worldId,
                    'id' => $id,
                    'title' => $sc['name'],
                    'cost' => $sc['cost'],
                    'need' => $sc['need'],
                    'takes' => $sc['takes'],
                    'builds' => json_encode(!is_array($sc['builds']) ? array() : $sc['builds']),
                    'units' => json_encode(!is_array($sc['units']) ? array() : array_values(array_unique($sc['units']))),
                    'bonuses' => json_encode(!is_array($sc['bonuses']) ? array() : $sc['bonuses']),
                ));
                $out .= "</td>";
                $out .= "<td>" . json_encode(!is_array($sc['bonuses']) ? array() : $sc['bonuses']) . "</td>";
                $out .= "<td>{$sc['need']}</td>";
                $out .= "<td>{$sc['takes']}</td>";
                //$out .= "<td>". json_encode(!is_array($sc['builds']) ? array() : $sc['builds'])."</td>";
                $out .= "<td>" . json_encode(!is_array($sc['units']) ? array() : $sc['units']) . "</td>";
                $out .= "</tr>";
            }
            $out .= '</table>';

            $this->db->commit();

        } catch (\Exception $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }

            return false;
        }
        //echo '<div style="width:35%;float:left;margin-right:5%;">';
        //dump($this->_constData, 0);
        //echo '</div>';
        //echo $out;

        return true;
    }

    private function _updateConstResources($worldId)
    {
        try {
            $this->db->beginTransaction();
            $this->db->simpleQuery("DELETE FROM cd_resource WHERE worldId = '$worldId'");
            $this->db->prepare("
				INSERT INTO cd_resource
				VALUES (:worldId, :id, :title, :prodType, :type)
			");
            foreach ($this->_constData['resource']['data'] as $id => $resource) {
                $this->db->execute(array(
                    'worldId' => $worldId,
                    'id' => $id,
                    'title' => $resource['name'],
                    'prodType' => $resource['prodtype'],
                    'type' => $resource['type'],
                ));
            }
            $this->db->commit();
        } catch (\Exception $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }

            return false;
        }

        return true;
    }

    private function _updateConstUnits(World $world)
    {
        try {
            $this->db->beginTransaction();
            $this->db->simpleQuery("DELETE FROM cd_units WHERE worldId = '{$world->id}'");
            $this->db->prepare("
				INSERT INTO cd_units
				SET
					worldId = :worldId,
					unitId = :unitId,
					title = :title,
					ability = :ability,
					airDamage = :airDamage,
					capacity = :capacity,
					damage = :damage,
					unitGroup = :unitGroup,
					health = :health,
					popCost = :popCost,
					race = :race,
					target = :target,
					trainTime = :trainTime,
					unitType = :unitType,
					speed = :speed,
					cost = :cost,
					pay = :pay
			");

//			dmp($this->_constData['units']);
            foreach ($this->_constData['units']['list'] as $unitId => $unit) {

                $cost2 = array();
                foreach ($unit['cost'] as $costId => $cost) {
                    if ($cost > 0) {
                        $cost2[$costId] = $cost;
                    }
                }
                $unit['cost'] = json_encode($cost2);

                $name = isset($unit['name']) ? $unit['name'] : null;

                $this->db->execute(array(
                    'worldId' => $world->id,
                    'unitId' => $unitId,
                    'title' => $name ? $name : 'unit-' . $unitId,
                    'ability' => $unit['ability'],
                    'airDamage' => 0,//todo airDamage
                    'capacity' => (int)round($unit['capacity']),
                    'damage' => (int)round($unit['damage']),
                    'unitGroup' => (int)$unit['group'], //todo unitGroup
                    'health' => (int)round($unit['health']),
                    'popCost' => (int)round($unit['popcost']),
                    'race' => (int)round($unit['racebin']),
                    'target' => 0,//todo target
                    'trainTime' => (int)round($unit['traintime']),
                    'unitType' => (int)round($unit['type']),
                    'speed' => (int)round($unit['speed'] * 3600),
                    'cost' => $unit['cost'],
                    'pay' => json_encode($unit['pay']),
                ));
            }

            $this->db->commit();
        } catch (\PDOException $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }

            return false;
        }

        return true;
    }

    private function _updateConstBuilds(World $world)
    {
        try {
            $this->db->beginTransaction();
            $this->db->simpleQuery("DELETE FROM cd_builds WHERE worldId = '{$world->id}'");
            $this->db->prepare("
				INSERT INTO cd_builds
				VALUES (:worldId, :buildId, :title, :buildType, :buildGroup,
				:bt0, :bt1, :bt2, :bt3,
				:eff0, :eff1, :eff2, :eff3,
				:ug0, :ug1, :ug2, :ug3,
				:pay0, :pay1, :pay2, :pay3,
				:cost, :unitsTrain)");
            foreach ($this->_constData['builds'] as $id => &$build) {
                $build['name'] = empty($build['name']) ? 'build-' . $id : $build['name'];
                $this->db->execute(array(
                    'worldId' => $world->id,
                    'buildId' => $id,
                    'title' => $build['name'],
                    'buildType' => $build['type'],
                    'buildGroup' => $build['group'],
                    'bt0' => $build['buildtime'][0],
                    'bt1' => $build['buildtime'][1],
                    'bt2' => $build['buildtime'][2],
                    'bt3' => $build['buildtime'][3],
                    'eff0' => $build['effect'][0],
                    'eff1' => $build['effect'][1],
                    'eff2' => $build['effect'][2],
                    'eff3' => $build['effect'][3],
                    'ug0' => $build['ungrown'][0],
                    'ug1' => $build['ungrown'][1],
                    'ug2' => $build['ungrown'][2],
                    'ug3' => $build['ungrown'][3],
                    'pay0' => $build['pay'][0],
                    'pay1' => $build['pay'][1],
                    'pay2' => $build['pay'][2],
                    'pay3' => $build['pay'][3],
                    'cost' => json_encode($build['cost']),
                    'unitsTrain' => json_encode($build['unitstrain']),
                ));
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }

            //DFException::handlerException($e);

            return false;
        }

        return true;
    }

    private function _updateConstDeposits(World $world)
    {
        try {
            $this->db->beginTransaction();
            $this->db->simpleQuery("DELETE FROM cd_deposit WHERE worldId = '{$world->id}'");
            $this->db->prepare("
				INSERT INTO cd_deposit
				VALUES (:worldId, :id, :climate, :science, :title)");
            foreach ($this->_constData['map']['deposit'] as $id => $deposit) {
                $this->db->execute(array(
                    'worldId' => $world->id,
                    'id' => $id,
                    'climate' => $deposit['climate'],
                    'science' => $deposit['science'],
                    'title' => $deposit['name'],
                ));
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }

            //DFException::handlerException($e);

            return false;
        }

        return true;
    }

    private function _clearIds($ids)
    {
        $result = array();
        foreach ($ids as $id) {
            if ($id >= 0 && $id != $this->_constData['science']['noscience']) {
                $result[] = $id;
            }
        }

        return $result;
    }

}
