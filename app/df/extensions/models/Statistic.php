<?php
/**
 * @since             19.01.14 00:30
 * @package           Wofh-Tools
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */
namespace df\extensions\models;

use df\exception\DFException;
use df\extensions\models\struct\DataCountry;
use df\helper\JSON;
use df\Model;

/**
 * Class Statistic
 *
 * @package df\component\statistic
 * @property Wofh $wofh
 */
class Statistic extends Model
{

    public function search($q, World $world)
    {
        $name = $this->db->e("%$q%");
        $id = abS((int)$q);
        $this->db->setPrefix('ruXX', $world->lowerSign);
        $this->db->query("
			SELECT accountName, accountId FROM z_ruXX_accounts
			WHERE accountId = $id
			OR accountName LIKE $name
		");
        $accounts = $this->db->fetchAll();
        $this->db->query("
			SELECT townTitle, townId FROM z_ruXX_towns
			WHERE townId = $id
			OR townTitle LIKE $name
		");
        $towns = $this->db->fetchAll();
        $this->db->query("
			SELECT countryTitle, countryId, countryFlag FROM z_ruXX_countries
			WHERE countryId = $id
			OR countryTitle LIKE $name
		");
        $countries = $this->db->fetchAll();
//		pre($this->db->getLastQuery(), 0);
//		dmp($accounts, 0);
//		dmp($towns, 0);
        return array(
            'accounts' => $accounts,
            'towns' => $towns,
            'countries' => $countries,
        );
    }

    public function getCommonData(World $world)
    {
        $sign = $world->lowerSign;
        $this->db->query("
			SELECT * FROM z_{$sign}_common
			ORDER BY stateDate
		");

        $data = array(
            'range' => array(),
            'series' => array(),
        );
        while ($row = $this->db->fetch()) {
            $data['range'][] = $row['stateDate'];

            $data['series'][0]['name'] = 'Количество городов';
            $data['series'][0]['data'][] = (int)$row['countTowns'];

            $data['series'][1]['name'] = 'Новые города';
            $data['series'][1]['data'][] = (int)$row['newTowns'];

            $data['series'][2]['name'] = 'Разрушенные города';
            $data['series'][2]['data'][] = (int)$row['lostTowns'];
        }

        return $data;
//		dmp($data);
    }

    public function getGeneralData(World $world, $stateDate)
    {
        $this->db->query(q("
			SELECT * FROM z_ruXX_common
			WHERE stateDate = :stateDate
		", $world->lowerSign), array('stateDate' => $stateDate));

        return $this->db->fetch();
    }

    public function getWorldStatistic(World $world, $stateDate)
    {
        try {
            $this->db->setPrefix('ruXX', $world->lowerSign);
            $this->db->query("
				SELECT * FROM z_ruXX_common
				WHERE stateDate = :stateDate
			", array('stateDate' => $stateDate));
            $data = $this->db->fetch();

            foreach ($data as &$item) {
                if (is_numeric($item)) {
                    $item = (int)$item;
                }
            }

            $data['events'] = array(
//				array('Игроки', false),
                Wofh::EVENT_ACCOUNT_CREATE => array(
                    'Зарегистрировано %s новых аккаунтов',
                    $data['accountsNew'],
                ),
                Wofh::EVENT_ACCOUNT_RENAME => array('Переименовано %s аккаунтов', $data['accountsRenamed']),
                Wofh::EVENT_ACCOUNT_DELETE => array('Удалено %s аккаунтов', $data['accountsDeleted']),
                Wofh::EVENT_ACCOUNT_COUNTRY_IN => array('%s игроков вступили в страны', $data['accountsCountryIn']),
                Wofh::EVENT_ACCOUNT_COUNTRY_OUT => array('%s игроков покинули страны', $data['accountsCountryOut']),
                Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE => array(
                    '%s игроков сменили гражданство',
                    $data['accountsCountryChange'],
                ),
                Wofh::EVENT_ACCOUNT_ROLE_IN => array('%s игроков получили должности', $data['accountsRoleIn']),
                Wofh::EVENT_ACCOUNT_ROLE_OUT => array(
                    '%s игроков уволены с должностей',
                    $data['accountsRoleOut'],
                ),
//				array('Города', false),
                Wofh::EVENT_TOWN_CREATE => array('Основано %s новых городов', $data['townsNew']),
                Wofh::EVENT_TOWN_RENAME => array('Переименовано %s городов', $data['townsRenamed']),
                Wofh::EVENT_TOWN_LOST => array('Потеряно %s городов', $data['townsLost']),
                Wofh::EVENT_TOWN_DESTROY => array('%s городов полностью разрушено', $data['townsDestroy']),
//				array('Страны', false),
                Wofh::EVENT_COUNTRY_CREATE => array('Создано %s новых стран', $data['countriesNew']),
                Wofh::EVENT_COUNTRY_FLAG => array('%s стран сменили флаги', $data['countriesFlag']),
                Wofh::EVENT_COUNTRY_RENAME => array('%s стран сменили названия', $data['countriesRenamed']),
                Wofh::EVENT_COUNTRY_DESTROY => array('%s стран расформировано', $data['countriesDeleted']),
//				array('Чудеса света', false),
                Wofh::EVENT_WONDER_CREATE => array('Начато строительство %s чудес света', $data['wondersNew']),
                Wofh::EVENT_WONDER_DESTROY => array('Разрушено %s чудес света', $data['wondersDestroy']),
                Wofh::EVENT_WONDER_ACTIVATE => array('Активировано %s чудес света', $data['wondersActivate']),
            );
            $data['dataRaces'] = array(
                array('Индейцы', (int)$data['accountsRace0']),
                array('Европейцы', (int)$data['accountsRace1']),
                array('Азиаты', (int)$data['accountsRace2']),
                array('Африканцы', (int)$data['accountsRace3']),
            );
            $data['dataSex'] = array(
                array('Мужчины', (int)$data['accountsSex1']),
                array('Женщины', (int)$data['accountsSex0']),
            );
            unset(
                $data['accountsRace0'], $data['accountsRace1'],
                $data['accountsRace2'], $data['accountsRace3'],
                $data['accountsSex1'], $data['accountsSex0']
            );

            $avg = $this->db->query("
				SELECT
					avg(accounts) as countriesAvgAccounts,
					max(accounts) as countriesMaxAccounts
				FROM z_ruXX_countries_stat
				WHERE stateDate = '$stateDate'
				AND accounts > 3
			")->fetch();

            $countriesAvgAccounts = (int)round(((float)$avg['countriesAvgAccounts'] + (int)$avg['countriesMaxAccounts']) / 2);
            $data['countriesAvgAccounts'] = $countriesAvgAccounts;

//            $this->db->query(
//                "SELECT
//					a1.stateDate,
//					a1.accountId,
//					a1.countryId,
//					a1.role,
//				--	a2.stateDate,
//				--	a2.countryId,
//					a2.role AS roleOld,
//					co.accounts,
//					co2.countryTitle,
//					co2.countryFlag,
//					co2.active,
//					acc.accountName
//				FROM z_ruXX_accounts_stat a1
//					JOIN z_ruXX_accounts_stat a2
//						ON a2.accountId = a1.accountId
//				--		and a2.stateDate = '2014-04-25' - interval 1 day
//						AND a2.stateDate = (SELECT max(a3.stateDate) FROM z_ruXX_accounts_stat a3 WHERE a3.stateDate < :stateDate)
//						AND a2.role <> a1.role
//					JOIN z_ruXX_countries_stat co
//						ON co.countryId = a1.countryId
//						AND co.stateDate = :stateDate
//						AND co.accounts >= :avg
//					LEFT JOIN z_ruXX_countries co2
//						ON co2.countryId = co.countryId
//					LEFT JOIN z_ruXX_accounts acc
//						ON acc.accountId = a1.accountId
//				WHERE a1.stateDate = :stateDate",
//                array(
//                    'stateDate' => $stateDate,
//                    'avg' => $countriesAvgAccounts,
//                ));
//            $roles = $this->db->fetchByFieldArray('countryId');
////			pre($this->db->getLastQuery(), 0);
////			dmp($roles);
//            $data['roles'] = $roles;

//			dmp($data);
        } catch (\PDOException $e) {
//			pre($e);
            return null;
        }

        return $data;
    }

    public function getCountAccounts(World $world)
    {

        $queryActiveAccounts = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1", $world->lowerSign);

        try {
            $worldStat = array(
                'countAccounts' => (int)$world->countAccounts(),
                'activePlayers' => (int)$this->db->query($queryActiveAccounts)->fetchObject()->total,
            );
        } catch (\PDOException $e) {
            return null;
        }

        return $worldStat;
    }

    public function getWondersStat(World $world, $stateDate)
    {
        $wonders = $this->db->query("
			SELECT buildId, title
			FROM cd_builds
			WHERE worldId = '{$world->id}'
		")->fetchByField('buildId');

        $data = $this->db->query(q("
			SELECT wonderId
			FROM z_ruXX_towns_stat
			WHERE stateDate = '$stateDate'
				AND wonderId > 0", $world->lowerSign)
        )->fetchByFieldArray('wonderId');

        $data2 = $this->db->query(q("
			SELECT wonderId
			FROM z_ruXX_towns_stat
			WHERE stateDate = '$stateDate'
				AND wonderId > 0
				AND wonderLevel = 21", $world->lowerSign)
        )->fetchByFieldArray('wonderId');

        $count = array();
        $total = array();
        $names = array();
        foreach ($data as $wonderId => &$item) {
            $total[$wonderId] = count($item);
            $names[$wonderId] = $wonders[$wonderId]['title'];
        }
        foreach ($data2 as $wonderId => &$item2) {
            $count[$wonderId] = count($item2);
            $names[$wonderId] = $wonders[$wonderId]['title'];
        }

        return array(
            'data' => $data,
            'count' => $count,
            'total' => $total,
            'names' => $names,
        );
    }

    public function getCountriesRating($worldId, $rating, $stateDate, $page)
    {
        try {
            $stData = array(
                'population' => array('Население', 'pop', 'Рейтинг населения стран', ' чел.'),
                'science' => array('Научный потенциал', 'science', 'Научный рейтинг стран', ''),
                'production' => array('Производство', 'production', 'Производственный рейтинг стран', ''),
                'war' => array('Военный рейтинг', 'war', 'Военный рейтинг стран', ''),
            );
            $rating = $rating ?: 'population';
            $field = $stData[$rating][1];
            $perPage = 10;
            $start = $page * $perPage - $perPage;

            $sign = strtolower(Wofh::idToSign($worldId));

            $total = $this->db
                ->query(q("SELECT COUNT(*) AS total FROM z_ruXX_countries WHERE active = 1", $sign))
                ->fetchObject()->total;

            $data = array();
            $this->db->query(q("
			SELECT s.countryId, s.countryTitle, s.countryFlag,
				today.$field AS val, today.accounts, today.towns,
				today.delta" . ucfirst($field) . " AS delta,
				today.deltaAccounts AS deltaPlayersPerDay,
				today.deltaTowns AS deltaTownsPerDay
			FROM z_ruXX_countries s
				LEFT JOIN z_ruXX_countries_stat today
					ON today.countryId = s.countryId
			WHERE today.stateDate = '$stateDate'
			ORDER BY today." . $field . " DESC LIMIT $start, $perPage", $sign));

            $place = 1;
            $max = 0;
            while ($row = $this->db->fetch()) {
                if ($place == 1) {
                    $max = $row['val'];
                    $place = $place + $start;
                }
                $row['percent'] = round(($row['val'] * 100) / $max, 2);
                $row['place'] = $place++;
                $data[$row['countryId']] = $row;
            }

            if (empty($data)) {
                return false;
            }

            $ids = join(',', array_keys($data));
            $chartData = $this->db->query(q("
				SELECT s.countryId, d.stateDate, d." . $field . " AS val
				FROM z_ruXX_countries s
					JOIN z_ruXX_countries_stat d
						ON d.countryId = s.countryId
				WHERE (d.stateDate >= '$stateDate' - INTERVAL 2 WEEK )
					AND (d.stateDate <= '$stateDate')
					AND s.countryId IN ($ids)
				ORDER BY d.stateDate ASC", $sign))->fetchAll();
            $t = null;
            foreach ($chartData as $item) {
                list($d, $m) = explode(' ', date('j n', strtotime($item['stateDate'])));
                $index = $d . ' ' . getMonth($m);
                $data[$item['countryId']]['chart'][$index] = (float)$item['val'] ? (float)$item['val'] : null;
                if ($t === null) {
                    $t = $item['countryId'];
                }
            }
//			dmp($data);
            $range = array_keys($data[$t]['chart']);

            return array(
                'list' => array_values($data),
                'columnTitle' => $stData[$rating][0],
                'chartTitle' => $stData[$rating][2],
                'chartSuffix' => $stData[$rating][3],
                'range' => $range,
                'total' => $total,
                'totalPages' => (int)ceil($total / $perPage),
            );
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function getAccountsRating(World $world, $rating, $stateDate, $page, $ids = array())
    {
        try {
            $stData = array(
                'population' => array('Население', 'pop', 'Рейтинг населения игроков', ' чел.'),
                'science' => array('Научный потенциал', 'science', 'Научный рейтинг игроков', ''),
                'production' => array('Производство', 'production', 'Производственный рейтинг игроков', ''),
                'war' => array('Военный рейтинг', 'war', 'Военный рейтинг игроков', ''),
            );
            $rating = $rating ?: 'population';
            $field = $stData[$rating][1];
            $perPage = 10;
            $start = $page * $perPage - $perPage;

            $total = (int)$this->db
                ->query(q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1", $world->lowerSign))
                ->fetchObject()->total;

            $data = array();

            if (empty($ids)) {
                $this->db->query(q("
				SELECT s.accountId, s.accountName, s.countryId,
					country.countryTitle, country.countryFlag,
					today.$field AS val,
					today.delta" . ucfirst($field) . " AS delta,
					today.towns,
					today.deltaTowns AS deltaTownsPerDay
				FROM z_ruXX_accounts s
					LEFT JOIN z_ruXX_accounts_stat today
						ON today.accountId = s.accountId
					LEFT JOIN z_ruXX_countries country
						ON country.countryId = s.countryId
				WHERE today.stateDate = '$stateDate'
				ORDER BY today.$field DESC LIMIT $start, $perPage", $world->lowerSign));
            } else {
                $this->db->query(q("
				SELECT s.accountId, s.accountName, s.countryId,
					country.countryTitle, country.countryFlag,
					today.$field AS val,
					today.delta" . ucfirst($field) . " AS delta,
					today.towns,
					today.deltaTowns AS deltaTownsPerDay
				FROM z_ruXX_accounts s
					LEFT JOIN z_ruXX_accounts_stat today
						ON today.accountId = s.accountId
					LEFT JOIN z_ruXX_countries country
						ON country.countryId = s.countryId
				WHERE today.stateDate = '$stateDate'
				AND s.accountID IN(" . join(',', $ids) . ")
				ORDER BY today.$field DESC", $world->lowerSign));
            }

            $place = 1;
            $max = 0;
            while ($row = $this->db->fetch()) {
                if ($place == 1) {
                    $max = $row['val'];
                    $place = $place + $start;
                }
                $row['percent'] = round(($row['val'] * 100) / $max, 2);
                $row['place'] = $place++;
                $data[$row['accountId']] = $row;
            }

//			pre($data);

            if (empty($data)) {
                return false;
            }

            $ids = join(',', array_keys($data));
            $chartData = $this->db->query(q("
				SELECT
					z_ruXX_accounts.accountId,
					z_ruXX_accounts_stat.stateDate,
					z_ruXX_accounts_stat.$field AS val
				FROM z_ruXX_accounts
					JOIN z_ruXX_accounts_stat
						ON z_ruXX_accounts_stat.accountId = z_ruXX_accounts.accountId
				WHERE (z_ruXX_accounts_stat.stateDate >= '$stateDate' - INTERVAL 2 WEEK)
					AND (z_ruXX_accounts_stat.stateDate <= '$stateDate')
					AND z_ruXX_accounts.accountId IN ($ids)
				ORDER BY z_ruXX_accounts_stat.stateDate ASC", $world->lowerSign))->fetchAll();

            $t = null;
            foreach ($chartData as $item) {
                list($d, $m) = explode(' ', date('j n', strtotime($item['stateDate'])));
                $index = $d . ' ' . getMonth($m);
                $data[$item['accountId']]['chart'][$index] = (float)$item['val'] ? (float)$item['val'] : 'null';
                if ($t === null) {
                    $t = $item['accountId'];
                }
            }
            $range = array_keys($data[$t]['chart']);

            return array(
                'list' => array_values($data),
                'columnTitle' => $stData[$rating][0],
                'chartTitle' => $stData[$rating][2],
                'chartSuffix' => $stData[$rating][3],
                'range' => $range,
                'total' => $total,
                'totalPages' => (int)ceil($total / $perPage),
            );
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function getTownsRating($worldId, $stateDate, $page)
    {
        try {
            $perPage = 10;
            $start = $page * $perPage - $perPage;

            $sign = strtolower(Wofh::idToSign($worldId));

            $total = (int)$this->db
                ->query(q("
					SELECT COUNT(*) AS total
					FROM z_ruXX_towns
					WHERE lost = 0 AND destroyed = 0", $sign))
                ->fetchObject()->total;

            $data = array();
            $this->db->query(q("
				SELECT s.townId, s.townTitle, s.accountId,
					accounts.accountName, accounts.countryId,
					country.countryTitle, country.countryFlag,
					today.pop,
					today.delta AS delta
				FROM z_ruXX_towns s
					LEFT JOIN z_ruXX_towns_stat today
						ON today.townId = s.townId
					LEFT JOIN z_ruXX_accounts accounts
						ON accounts.accountId = s.accountId
					LEFT JOIN z_ruXX_countries country
						ON country.countryId = accounts.countryId
				WHERE today.stateDate = '$stateDate'
				ORDER BY today.pop DESC LIMIT $start, $perPage", $sign));

            $place = 1;
            $max = 0;
            while ($row = $this->db->fetch()) {
                if ($place == 1) {
                    $max = $row['pop'];
                    $place = $place + $start;
                }
                $row['percent'] = round(($row['pop'] * 100) / $max, 2);
                $row['place'] = $place++;
                $data[$row['townId']] = $row;
            }

            if (empty($data)) {
                return false;
            }

            $ids = join(',', array_keys($data));
            $chartData = $this->db->query(q("
				SELECT s.townId, d.stateDate, d.pop
				FROM z_ruXX_towns s
					JOIN z_ruXX_towns_stat d
						ON d.townId = s.townId
				WHERE (d.stateDate >= '$stateDate' - INTERVAL 2 WEEK)
					AND (d.stateDate <= '$stateDate')
					AND s.townId IN ($ids)
				ORDER BY d.stateDate ASC
			", $sign))->fetchAll();

            $t = null;
            foreach ($chartData as $item) {
                list($d, $m) = explode(' ', date('j n', strtotime($item['stateDate'])));
                $index = $d . ' ' . getMonth($m);
                $data[$item['townId']]['chart'][$index] = (int)$item['pop'] ? (int)$item['pop'] : 'null';
                if ($t === null) {
                    $t = $item['townId'];
                }
            }
            $range = array_keys($data[$t]['chart']);

            return array(
                'list' => array_values($data),
                'columnTitle' => 'Население',
                'chartTitle' => 'Рейтинг населения городов',
                'chartSuffix' => ' чел.',
                'range' => $range,
                'total' => $total,
                'totalPages' => (int)ceil($total / $perPage),
            );
        } catch (\PDOException $e) {
            throw $e;

            return null;
        }
    }

    private function getIconEvent($eventId)
    {
        $data = array(
            20 => 'event-account-create',
            25 => 'event-account-rename',
            24 => 'event-account-delete',
            21 => 'event-account-country-in',
            22 => 'event-account-country-out',
            23 => 'event-account-country-change',
            1 => 'event-town-create',
            2 => 'event-town-rename',
            3 => 'event-town-lost',
            4 => 'event-town-destroy',
            40 => 'event-country-create',
            41 => 'event-country-flag',
            42 => 'event-country-rename',
            43 => 'event-country-destroy',
            61 => 'event-wonder-create',
            60 => 'event-wonder-destroy',
            62 => 'event-wonder-activate',
        );
        if (isset($data[$eventId])) {
            return $data[$eventId];
        } else {
            return '';
        }
    }

    public function getTown($townId, World $world, $stateDate)
    {
        /** @var World $world */

        if (!$townId) {
            return false;
        }

        $td = array();
        $range = array();

        $query = q("
			SELECT s.*, d.* -- , co.countryTitle, co.countryFlag
			FROM z_ruXX_towns s
				LEFT JOIN z_ruXX_towns_stat d
					ON d.townId = s.townId
			--	LEFT JOIN z_ruXX_countries co
			--		ON co.countryId = s.countryId
			WHERE s.townId = '$townId'
				AND d.stateDate = '$stateDate'
		", $world->lowerSign);

        $townData = $this->db->query($query)->fetch();

        if (empty($townData)) {
            $townData = $this->db->query(q("
				SELECT * FROM z_ruXX_towns
				WHERE townId = '$townId'
			", $world->lowerSign))->fetch();
        }

        if (empty($townData)) {
            return false;
        }

        $townData['extra'] = json_decode($townData['extra'], true);
//		pre($townData);
        if ($townData['extra'][2]) {
            $stDate = date(STD_DATE, $townData['extra'][2]);
        } else {
            $stDate = $stateDate;
        }

        $townDynData = $this->db->query(q("
			SELECT
				d.stateDate, d.pop, d.delta
			FROM z_ruXX_towns s
				JOIN z_ruXX_towns_stat d
					ON d.townId = s.townId
			WHERE s.townId = '$townId'
				AND (d.stateDate >= '$stDate' - INTERVAL 3 WEEK)
				AND (d.stateDate <= '$stDate')
			ORDER BY d.stateDate
		", $world->lowerSign))->fetchAll();
        foreach ($townDynData as $item) {
//			list( $d, $m ) = explode(' ', date('j n', strtotime($item['stateDate'])));
//			$key           = $d . ' ' . getMonth($m);

            list($d, $m, $y) = explode(' ', date('d m Y', strtotime($item['stateDate'])));
            $key = $y . '-' . $m . '-' . $d;
            $td['pop'][] = (int)$item['pop'];
            $td['delta'][] = (int)$item['delta'];
            $range[] = $key;
        }

        $townData['charts'] = $td;

        $events = array();
//		$events = $this->db->query("
//			SELECT e.*, p.playerName, p.playerSex
//			FROM st_events e
//				LEFT JOIN st_players p
//					ON p.worldId = e.worldId
//					AND p.playerId = e.playerId
//			WHERE e.worldId = '{$world->id}'
//--				AND stateDate > '$stateDate' - INTERVAL 7 DAY
//				AND e.playerId = '$playerId'
//			ORDER BY e.stateDate DESC, e.eventId DESC
//--			LIMIT 14
//		")->fetchAll();

        return array(
            'events' => $events,
            'town' => $townData,
            'range' => $range,
        );
    }

    public function getTemplate($eventId, $world)
    {
        $template = __DIR__ . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'event_' . $eventId . '.phtml';
        if (!file_exists($template)) {
            return '<div class="alert alert-danger">Template not found: event_' . $eventId . '</div>';
        }
        ob_start();
        include $template;
        $html = ob_get_clean();

        return trim($html);
    }

    public function getEventsChart($eventId, World $world)
    {
        $this->db->query(q("
		SELECT *
		FROM z_ruXX_common
		ORDER BY stateDate
		", $world->lowerSign));
        $data = $this->db->fetchByField('stateDate');

        return $data;
    }

    public function getEventsList($eventId, World $world, $stateDate)
    {
        $this->db->query(q("
		SELECT *
		FROM z_ruXX_events
		WHERE stateDate = '$stateDate'
		AND eventId = '$eventId'
		ORDER BY eventId
		", $world->lowerSign));
        $data = array();
        while ($row = $this->db->fetch()) {
            $row['extra'] = json_decode($row['extra'], true);
            $data[] = $row;
        }

        return $data;
    }

    public function getEventsListAll(World $world, $stateDate)
    {
        $this->db->query(q("
		SELECT *
		FROM z_ruXX_events
		WHERE stateDate = '$stateDate'
		ORDER BY countryId, accountId, townId, eventId
		", $world->lowerSign));
        $data = array();
        while ($row = $this->db->fetch()) {
            $row['extra'] = json_decode($row['extra'], true);
            $data[] = $row;
        }

        return $data;
    }

    public function getAccountsToCompare(World $world, $stateDate, $ids)
    {
        $q = q("
				SELECT s.accountId, s.accountName, s.countryId,
					country.countryTitle, country.countryFlag
				FROM z_ruXX_accounts s
					LEFT JOIN z_ruXX_accounts_stat today ON today.accountId = s.accountId
					LEFT JOIN z_ruXX_countries country ON country.countryId = s.countryId
				WHERE today.stateDate = '$stateDate'
				AND s.accountId IN (" . join(',', $ids) . ")
				", $world->lowerSign);
        $this->db->query($q);

        $data = array();
        while ($row = $this->db->fetch()) {
            $row['countryFlag'] = $world->getFlag($row['countryFlag']);
            $data[$row['accountId']] = $row;
        }

        return $data;
    }
}
