<?php
/**
 * @since           04.07.2015 09:33
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright        Copyright (C) 2015 DelphinPRO
 * @license         Licensed under the MIT license
 */

namespace df\extensions\models;

use df\Model;

class Polling extends Model
{
    public function getPoll($id)
    {
        $this->db->query("
            SELECT *
            FROM polling
            WHERE id = :id",
            array(
                'id' => $id
            ));
        $poll = $this->db->fetch();

        $this->db->query("
            SELECT *
            FROM polling_answers
            WHERE polling_id = :id",
            array(
                'id' => $id
            ));
        $poll['answers'] = $this->db->fetchByField('id');
        return $poll;
    }

    public function checkVote($pollId, $ip, $ua)
    {
        $this->db->query("
            SELECT *
            FROM polling_votes
            WHERE polling_id = :id
            AND ip = INET_ATON(:ip)
            AND user_agent = :ua
            ",
            array(
                'id' => $pollId,
                'ip' => $ip,
                'ua' => $ua
            ));
        $res = $this->db->fetch();
        return (bool)$res;
    }

    public function addVote($pollId, $answerId, $ip, $ua)
    {
        $this->db->query("
            INSERT INTO polling_votes
            (`polling_id`, `answer_id`, `ip`, `user_agent`)
            VALUES (:polling_id, :answer_id, INET_ATON(:ip), :ua)
            ",
            array(
                'polling_id' => $pollId,
                'answer_id' => $answerId,
                'ip' => $ip,
                'ua' => $ua
            ));
    }

    public function getResult($pollId)
    {
        $this->db->query("
            SELECT v.answer_id
            FROM polling_votes v
            WHERE v.polling_id = :id
            ORDER BY v.answer_id
            ",
            array(
                'id' => $pollId,
            ));
        $res = $this->db->fetchByFieldArray('answer_id');
        $this->db->query("
            SELECT id, title
            FROM polling_answers
            WHERE polling_id = :id
            ORDER BY id
            ",
            array(
                'id' => $pollId,
            ));
        $answers = $this->db->fetchByField('id');
        foreach ($answers as &$answer) {
            $answer = $answer['title'];
        }

        $max = 0;
        foreach ($res as $aId => &$item) {
            $item = count($item);
            if ($item > $max) {
                $max = $item;
            }
        }

        $return = array(
            'answers' => $answers,
            'votes' => $res,
            'max' => $max,
        );

        //pre($return);

        return $return;
    }
}
