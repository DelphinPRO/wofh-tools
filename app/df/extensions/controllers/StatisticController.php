<?php
/**
 * @since              01.12.13 04:20
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\helpers\Chart;
use df\extensions\models\stat\Account;
use df\extensions\models\stat\Country;
use df\extensions\models\stat\Event;
use df\extensions\models\Statistic;
use df\extensions\models\Wofh;
use df\extensions\models\World;
use df\Pagination;
use df\Registry;

/**
 * Class StatisticController
 *
 * @package df\component\statistic
 */
class StatisticController extends WTController
{

    /** @var  World */
    protected $_world;

    /** @var  string */
    protected $_sign;

    /** @var  string */
    protected $_stateDate;

    /** @var  string */
    protected $_stateTime;

    /** @var  int */
    protected $_stateTimestamp;

    protected function _before()
    {
        parent::_before();

        $this->_sign = $this->request->get->offsetGet('sign', '');
        $this->_world = Wofh::getWorldBySign($this->_sign);
        $date = $this->request->get->offsetGet('date', '');

        if ($this->_world && !$this->_world->statistic) {
            $this->redirect($this->router->link('statistic'),
                'По миру ' . $this->_world->sign . ' &ndash; ' .
                '&laquo;' . $this->_world->title . '&raquo; нет статистических данных.');
        }

        if ($this->router->route->name == 'statistic') {
            unset($_SESSION['stat-date']);
        }
        if ($date) {
            $this->session->set('stat-date', $date);
        } else {
//			if ($date = $this->session->get('stat-date')) {
//
//				$url = $this->router->getUri();
//				$this->redirect($url . '?date=' . $date);
//			}
        }

        $timestamp = time();
        if ($this->_world instanceof World) {
            $timestamp = !is_null($this->_world->updateStat) ? $this->_world->updateStat : time();
        }
        if ($date) {
            $timestampNew = strtotime($date);
            if ($timestampNew) {
                $timestamp = $timestampNew;
            }
        }
//		pre(date(STD_DATE, $timestamp));

        if ($this->_world) {
            if ($this->_world->isUpdate) {
                $this->redirect('/stat/',
                    'Обновляются статистические данные мира «<strong>' . $this->_world->sign . ' ' . $this->_world->title . '</strong>».</br>' .
                    'Статистика вновь будет доступна после завершения процедуры обновления... ');
            }

            Registry::set('world', $this->_world);
            $realLastUpdate = $this->_world->getRealLastUpdate();
            if ($timestamp < $this->_world->startedTimestamp) {
                $timestamp = $this->_world->startedTimestamp;
            } elseif ($timestamp > $realLastUpdate) {
                $timestamp = $realLastUpdate;
            }
            $this->set('realLastUpdate', $realLastUpdate);
            $this->set('world', $this->_world);
        }
        $this->_stateTimestamp = $timestamp;
        $this->_stateDate = date('Y-m-d', $timestamp);
        $this->_stateTime = date('d.m.Y H:i', $timestamp);
        $this->set('lastUpdateDate', $timestamp);
        $this->set('started', $this->_world->startedTimestamp);
        $this->set('stateDate', $this->_stateDate);
        $this->set('stateTime', $this->_stateTime);

        $this->app->addWidget('right', 'statistic');
        if ($this->router->route->name == 'statAccounts'
            or $this->router->route->name == 'statAccount'
        ) {
            $this->app->addWidget('right', 'statcompare');
        }

    }

    public function display()
    {
        $this->setPageTitle('Статистика игровых миров');
        $this->set('worlds', Wofh::getWorlds(array('WORKING', 'STATISTIC')));
        $this->set('archiveWorlds', Wofh::getWorlds(array('!WORKING', 'STATISTIC')));
    }

    public function world()
    {
        $this->setPageTitle("Статистика {$this->_world->sign} — {$this->_world->title}");

        $statistic = new Statistic();
        $worldStat = $statistic->getWorldStatistic($this->_world, $this->_stateDate);
//		pre(0, 0);
//		pre($worldStat, 0);
        if ($worldStat === null) {
            $this->setLayout('error404');
        }

        $this->set('worldsStat', $worldStat);

//		$wonders = $statistic->getWondersStat($world, $stateDate);
//		pre($wonders);

//		$wtData                     = $this->view->wtData;
        $subtitle = $this->_world->sign . ' «' . $this->_world->title . '», ' . $this->_stateTime;

        $wtData['stat']['charts'][] = Chart::pie(
            '#race-chart',
            'Расовое распределение населения',
            $subtitle,
            'игроков',
            $worldStat['dataRaces']
        );
        $wtData['stat']['charts'][] = Chart::pie(
            '#sex-chart',
            'Гендерное распределение населения',
            $subtitle,
            'игроков',
            $worldStat['dataSex']
        );
//		if (count($wonders['count'])) {
//			$wtData['stat']['charts'][] = array(
//				'cId'      => '#wonders-chart',
//				'type'     => 'bar',
//				'title'    => 'Чудеса света',
//				'subtitle' => $world->sign . ' «' . $world->title . '», ' . $stateTime,
//				'names'    => array_values($wonders['names']),
//				'series'   => array(
//					array(
//						'name' => 'Всего',
//						'data' => array_values($wonders['total']),
//					),
//					array(
//						'name' => 'Активировано',
//						'data' => array_values($wonders['count']),
//					),
//				)
//			);
//			$this->set('wonders', true);
//		} else {
        $this->set('wonders', false);
//		}

//		$this->set('general', $statistic->getGeneralData($this->_world, $this->_stateDate));
//		pre($statistic->getGeneralData($this->_world, $this->_stateDate), 0);

//		$commonData = $statistic->getCommonData($this->_world);
//		dmp($commonData);
//		$this->set('commonData', $commonData);
//		$wtData['stat']['charts'][] = array(
//			'cId'        => '#common-data-1',
//			'type'       => 'lines',
//			'title'      => 'title',//$commonData['chartTitle'],
//			'subtitle'   => $this->_world->sign . ' «' . $this->_world->title . '», ' . $this->_stateTime,
//			'label'      => '',//$commonData['columnTitle'],
//			'suffix'     => '',//$commonData['chartSuffix'],
//			'categories' => $commonData['range'],
//			'series'     => $commonData['series'],
//		);

        $this->set('wtData', $wtData);
    }

    public function countries()
    {
        $rating = $this->request->get->offsetGet('rating', '');
        $page = $this->request->get->getPage();

        $this->setPageTitle("Рейтинг стран <small>{$this->_world->sign} — {$this->_world->title}</small>");

        if ($page <= 0) {
            $this->setLayout('error404');
            return;
        }

        $statistic = new Statistic();
        $countriesData = $statistic->getCountriesRating($this->_world->id, $rating, $this->_stateDate, $page);

//		pre($countriesData);

        if (!$countriesData) {
            $this->setLayout('statistic/no-countries');
        }

        $this->set('countriesData', $countriesData);

        $this->set('linkByPopulation', $this->router->link('statCountries', array('sign' => $this->_world->lowerSign)));
        $this->set('linkByScience',
            $this->router->link('statCountries', array('sign' => $this->_world->lowerSign, 'rating' => 'science')));
        $this->set('linkByProduction',
            $this->router->link('statCountries', array('sign' => $this->_world->lowerSign, 'rating' => 'production')));
        $this->set('linkByWar',
            $this->router->link('statCountries', array('sign' => $this->_world->lowerSign, 'rating' => 'war')));
        $this->set('ratingBy', $rating);
        $this->set('page', $page);

        $wtData = $this->document->get('wtData');

        $s = array();
        foreach ($countriesData['list'] as $tid => $t) {
            $values = array_values($t['chart']);
            if (($r = count($countriesData['range'])) > ($v = count($values))) {
                for ($i = 0; $i < ($r - $v); $i++) {
                    array_unshift($values, null);
                }
            }
            $s[] = array(
                'name' => '<img src="' . $this->_world->getFlag($t['countryFlag']) . '" style="width:20px;height:12px;loat:right"> ' . $t['countryTitle'],
                'type' => 'spline',
                'marker' => array('enabled' => true),
                'data' => $values,
            );
        }

        $wtData['stat']['charts'][] = array(
            'cId' => '#rating-charts',
            'type' => 'lines',
            'title' => $countriesData['chartTitle'],
            'subtitle' => $this->_world->sign . ' «' . $this->_world->title . '», ' . $this->_stateTime,
            'label' => $countriesData['columnTitle'],
            'suffix' => $countriesData['chartSuffix'],
            'categories' => $countriesData['range'],
            'series' => $s,
        );
        $this->set('wtData', $wtData);

//		dmp($countriesData);

        $routeParam = array(
            'sign' => $this->_world->lowerSign,
            'rating' => $rating,
            'page' => 'page%p'
        );
        if (!$rating) {
            unset($routeParam['rating']);
        }

        $this->set('pagination', Pagination::Create()
            ->setTotal((int)$countriesData['total'])
            ->setPage($page)
            ->setPerPage(10)
            ->setLink($this->router->link('statCountries', $routeParam))
        );
    }

    public function accounts()
    {
        $this->setPageTitle("Рейтинг игроков <small>{$this->_world->sign} - {$this->_world->title}</small>");

        $rating = $this->request->get->offsetGet('rating', '');
        $page = $this->request->get->getPage();

        if ($page <= 0) {
            $this->setLayout('error404/display');
        }

        $statistic = new Statistic();
        $compare = isset($_GET['ids']) && is_array($_GET['ids']);
        $queryString = '';
        if ($compare) {
            $ids = $_GET['ids'];
            if (count($ids) > 10) {
                $ids = array_slice($ids, 0, 9);
            }
            $ids = array_map(function ($item) {
                return abs((int)$item);
            }, $ids);
            $ids = array_unique($ids);
            $queryString = '?ids[]=' . join('&ids[]=', $ids);
            $data = $statistic->getAccountsRating($this->_world, $rating, $this->_stateDate, $page, $ids);
        } else {
            $data = $statistic->getAccountsRating($this->_world, $rating, $this->_stateDate, $page);
        }

//		dump($data);

        if (!$data || $page == 0) {
            $this->setLayout('error404');
        }

        $routeParam = array(
            'sign' => $this->_world->lowerSign,
            'rating' => $rating,
            'page' => 'page%p'
        );
        if (!$rating) {
            unset($routeParam['rating']);
        }

        if (!$compare) {
            $this->set('pagination', Pagination::Create()
                ->setTotal($data['total'])
                ->setPage($page)
                ->setPerPage(10)
                ->setLink($this->router->link('statAccounts', $routeParam))
            );
        }

        $this->set('data', $data);
        $this->set('linkByPopulation',
            $this->router->link('statAccounts', array('sign' => $this->_world->lowerSign)) . $queryString);
        $this->set('linkByScience', $this->router->link('statAccounts',
                array('sign' => $this->_world->lowerSign, 'rating' => 'science')) . $queryString);
        $this->set('linkByProduction', $this->router->link('statAccounts',
                array('sign' => $this->_world->lowerSign, 'rating' => 'production')) . $queryString);
        $this->set('linkByWar', $this->router->link('statAccounts',
                array('sign' => $this->_world->lowerSign, 'rating' => 'war')) . $queryString);
        $this->set('ratingBy', $rating);
        $this->set('page', $page);

        $wtData = $this->document->get('wtData');

        $s = array();
        foreach ($data['list'] as $tid => $t) {
            $values = array_values($t['chart']);
            if (($r = count($data['range'])) > ($v = count($values))) {
                for ($i = 0; $i < ($r - $v); $i++) {
                    array_unshift($values, null);
                }
            }
            $flag = $t['countryFlag'] ? '<img src="' . $this->_world->getFlag($t['countryFlag']) . '" style="width:20px;height:12px;"> ' : '';
            $s[] = array(
                'name' => $flag . $t['accountName'],
                'type' => 'spline',
                'marker' => array('enabled' => true),
                'data' => $values,
            );
        }

        $wtData['stat']['charts'][] = array(
            'cId' => '#rating-charts',
            'type' => 'lines',
            'title' => $data['chartTitle'],
            'subtitle' => $this->_world->sign . ' «' . $this->_world->title . '», ' . $this->_stateTime,
            'label' => $data['columnTitle'],
            'suffix' => $data['chartSuffix'],
            'categories' => $data['range'],
            'series' => $s,
        );
        $this->set('wtData', $wtData);

    }

    public function towns()
    {
        $this->setPageTitle("Рейтинг городов <small>{$this->_world->sign} «{$this->_world->title}»</small>");

        $page = $this->request->get->getPage();

        if ($page <= 0) {
            $this->setLayout('error404');
        }

        $statistic = new Statistic();
        $data = $statistic->getTownsRating($this->_world->id, $this->_stateDate, $page);

        if (!$data) {
            $this->setLayout('error404');
        }

//		dmp($data);

        $this->set('pagination', Pagination::Create()
            ->setTotal($data['total'])
            ->setPage($page)
            ->setPerPage(10)
            ->setLink($this->router->link('statTowns',
                array(
                    'sign' => $this->_world->lowerSign,
                    'page' => 'page%p'
                ))
            ));
        $this->set('data', $data);
        $this->set('page', $page);

        $wtData = $this->document->get('wtData');

        $s = array();
        foreach ($data['list'] as $tid => $t) {
            $values = array_values($t['chart']);
            if (($r = count($data['range'])) > ($v = count($values))) {
                for ($i = 0; $i < ($r - $v); $i++) {
                    array_unshift($values, null);
                }
            }
            $flag = ''; //$t['countryFlag'] ? '<img src="' . getFlag($world->sign, $t['countryFlag']) . '" style="width:20px;height:12px;"> ' : '';
            $s[] = array(
                'name' => $flag . $t['townTitle'],
                'type' => 'spline',
                'marker' => array('enabled' => true),
                'data' => $values,
            );
        }

        $wtData['stat']['charts'][] = array(
            'cId' => '#rating-charts',
            'type' => 'lines',
            'title' => $data['chartTitle'],
            'subtitle' => $this->_world->sign . ' «' . $this->_world->title . '», ' . $this->_stateTime,
            'label' => $data['columnTitle'],
            'suffix' => $data['chartSuffix'],
            'categories' => $data['range'],
            'series' => $s,
        );
        $this->set('wtData', $wtData);

    }

    public function getCrumbs()
    {
        $params = Registry::getRouter()->route->params;

        $data['stat'] = array('statistic', 'Статистика');
        if ($this->_world instanceof World) {
            $data['sign'] = array('statWorld', $this->_world->sign . ' &ndash; ' . $this->_world->title);
        }
        $data['countries'] = array('statCountries', 'Страны');
        if ($this->_world instanceof World) {
            if (isset($params['countryId'])) {
                $data['countries'] = array('statCountries', 'Страны');
                $data['countryId'] = array('statCountry', Registry::get('country')->name);
            }
        }
        $data['towns'] = array('statTowns', 'Города');
        if ($this->_world instanceof World) {
            if (isset($params['townId'])) {
                $statistic = new Statistic();
                $town = $statistic->getTown($params['townId'], $this->_world, $this->_stateDate);
                $data['townId'] = array('statTown', $town['town']['townTitle']);
            }
        }

        $data['accounts'] = array('statAccounts', 'Игроки');
        if ($this->_world instanceof World) {
            if (isset($params['accountId'])) {
                $data['accountId'] = array('statAccount', Registry::get('account')->name);
            }
        }

        $result = array(
            array(
                'routeName' => 'home',
                'title' => 'Главная',
                'link' => '/',
            )
        );

        $routeSignature = explode('/', trim($this->router->getRouteSignature($this->router->route->name), '/'));
        foreach ($routeSignature as $key => $section) {
            if (preg_match('~\:([a-z]+)\]~Usi', $section, $m)) {
                $routeSignature[$key] = $m[1];
            }
        }

        foreach ($routeSignature as $section) {
            if (isset($data[$section])) {
                $name = $data[$section][0];
                $link = $this->router->link($name, $this->router->route->params);
                $title = $data[$section][1];
                if (is_callable($data[$section][1])) {
                    $title = $data[$section][1]();
                }
                $result[] = array(
                    'routeName' => $data[$section][0],
                    'title' => $title,
                    'link' => $link,
                );
            } else {
            }
        }
        return $result;
    }

    public function country()
    {
        $countryId = $this->request->get->getInt('countryId', 0);
        $country = new Country($countryId, $this->_stateTimestamp);

        if (!$country->id) {
            $this->setLayout('error404');
            return;
        }

        $country->loadExtendedData();
        Registry::set('country', $country);

        $this->setPageTitle(
            '<span>Страна</span> ' .
            htmlspecialchars($country->name) .
            ' <small>' . $this->_world->sign . ' «' . $this->_world->title . '»</small>');

        $accounts = $country->getAccountsList();
        $events = $country->getEvents();
        $diplomacy = $country->getDiplomacy();

        $this->set('country', $country);
        $this->set('accounts', $accounts);
        $this->set('events', $events);
        $this->set('diplomacy', $diplomacy);

        $wtData = $this->document->get('wtData');

        $wtData['bookmarks']['title'] = $this->_world->sign . " Страна «" . $country->name . "»";
        $wtData['stat']['charts'][] = $country->getChartPop();
        $wtData['stat']['charts'][] = $country->getChartScience();
        $wtData['stat']['charts'][] = $country->getChartProduction();
        $wtData['stat']['charts'][] = $country->getChartWar();
        $this->set('wtData', $wtData);

        $this->set('bookmark', array(
            'id' => md5($this->router->getUri()),
            'uri' => $this->router->getUri(),
            'title' => $this->_world->sign . ' ' . $country->name,
            'section' => 'country',
        ));
        $this->set('externalLink', $this->_world->domain . '/?tid=0#/countryInfo/' . $country->id);
        $this->set('externalLinkText', $country->name);

        Event::$CONTEXT = Event::CONTEXT_VIEW_COUNTRY;
    }

    public function account()
    {
        $accountId = abs((int)($_GET['accountId']));
        $account = new Account($accountId, $this->_stateTimestamp);

        if (!$account->id) {
            $this->setLayout('error404');
        }

        Registry::set('account', $account);
//		dmp($account);

        $this->setPageTitle(
            '<span>Игрок</span> ' .
            htmlspecialchars($account->name) .
            " <small>{$this->_world->sign} «{$this->_world->title}»</small>");

        $this->set('account', $account);
        $this->set('towns', $account->getTowns());
        $this->set('events', $account->getEvents());

        $wtData = $this->document->get('wtData');

        $wtData['bookmarks']['title'] = $this->_world->sign . " Страна «" . $account->name . "»";
        $wtData['stat']['charts'][] = $account->getChartPop();
        $wtData['stat']['charts'][] = $account->getChartScience();
        $wtData['stat']['charts'][] = $account->getChartProduction();
        $wtData['stat']['charts'][] = $account->getChartWar();
        $this->set('wtData', $wtData);

        $this->set('bookmark', array(
            'id' => md5($this->router->getUri()),
            'uri' => $this->router->getUri(),
            'title' => $this->_world->sign . ' ' . $account->name,
            'section' => 'account',
        ));
        $this->set('externalLink', $this->_world->domain . '/#/account/' . $account->id);
        $this->set('externalLinkText', $account->name);

        Event::$CONTEXT = Event::CONTEXT_VIEW_ACCOUNT;
    }

    public function town()
    {
        $townId = abs((int)($_GET['townId']));

        $statistic = new Statistic();
        $townData = $statistic->getTown($townId, $this->_world, $this->_stateDate);
        $account = new Account((int)$townData['town']['accountId'], $this->_stateTimestamp);

        $this->setPageTitle(
            '<span>Город</span> ' .
            htmlspecialchars($townData['town']['townTitle']) .
            " <small>{$this->_world->sign} «{$this->_world->title}»</small>");

        if (!$townData) {
            $this->setLayout('error404');
        }

        $this->set('town', $townData['town']);
        $this->set('range', $townData['range']);
        $this->set('account', $account);

        $wtData = $this->document->get('wtData');
        $wtData['bookmarks']['title'] = $this->_world->sign . " " . $townData['town']['townTitle'] . "";

        $wtData['stat']['charts'][] = array(
            'cId' => '#rating1',
            'type' => 'splineAndArea',
            'lazy' => true,
            'lazyIndex' => '#tab1',
            'title' => "Рейтинг населения города «" . $townData['town']['townTitle'] . "»",
            'subtitle' => $this->_world->sign . ' «' . $this->_world->title . '», ' . $this->_stateTime,
            'primaryColor' => '#4572A7',
            'secondColor' => '#B1D5FB',
            'text1' => 'Население, чел.',
            'text2' => 'Рост/спад, чел.',
            'sfx' => ' чел.',
            'categories' => array_values($townData['range']),
            'series' => array(
                array('text' => 'Население', 'data' => array_values($townData['town']['charts']['pop'])),
                array('text' => 'Рост / спад', 'data' => array_values($townData['town']['charts']['delta'])),
            )
        );
        $this->set('wtData', $wtData);

        $this->set('bookmark', array(
            'id' => md5($this->router->getUri()),
            'uri' => $this->router->getUri(),
            'title' => $this->_world->sign . ' ' . $townData['town']['townTitle'],
            'section' => 'town',
        ));
        $this->set('externalLink', $this->_world->domain . '/towninfo?id=' . $townData['town']['townId']);
        $this->set('externalLinkText', $townData['town']['townTitle']);

    }

    public function events()
    {
        $sign = $this->request->get->offsetGet('sign', '');
        $world = Wofh::getWorldBySign($sign);
        $eventId = $this->request->get->getInt('eventId', 0);

//		if ($eventId < 1 || $eventId > 16) return;
        $statistic = new Statistic();
        $data = $statistic->getEventsChart($eventId, $world);
        $list = $statistic->getEventsList($eventId, $world, $this->_stateDate);

        $this->setPageTitle("События <small>{$world->sign} «{$world->title}»</small>");

        $this->set('data', $data);
        $this->set('list', $list);
        $this->set('world', $world);
//		$this->set('template', $this->statistic->getTemplate($eventId, $this->world));
    }

    public function search()
    {
      $q = $_GET['q'];
        if (!$this->_world) {
            $this->redirect('/stat/', 'Invalid parameter');
        }

        $this->setPageTitle("Результаты поиска <span>«".htmlspecialchars($q)."»</span>");

        if (mb_strlen($q, 'UTF-8') < 3 && !is_numeric($q)) {
            $this->redirect('/stat/' . $this->_world->lowerSign . '/',
                'Слишком короткий запрос для поиска');
        }

        $s = new Statistic();
        $results = $s->search($q, $this->_world);
        $this->set('results', $results);
        $this->set('query', $q);
    }

    public function addToCompare()
    {
        $sign = $this->request->get->offsetGet('sign');
        $id = abs((int)$this->request->get->offsetGet('id'));

        $_SESSION['wt.stat.compare'][$sign]['accounts'][$id] = $id;

        $ids = $_SESSION['wt.stat.compare'][$sign]['accounts'];

        $statistic = new Statistic();
        $data = $statistic->getAccountsToCompare($this->_world, $this->_stateDate, $ids);

        echo json_encode(array_values($data));

        exit;
    }

    public function clearCompare()
    {
        $sign = $this->request->get->offsetGet('sign');
        $_SESSION['wt.stat.compare'][$sign]['accounts'] = null;
        unset($_SESSION['wt.stat.compare'][$sign]['accounts']);
        echo json_encode($_SESSION);
        exit;
    }

    public function compare()
    {
    }
}
