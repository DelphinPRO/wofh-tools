<?php
/**
 * @since              09.09.13 19:33
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\models\GameData;
use df\extensions\models\Technology;
use df\extensions\models\Wofh;
use df\helper\FS;

/**
 * Class TechnologyController
 *
 * @package df\component\tools
 */
class TechnologyController extends WTController
{

    public function technology()
    {
        $this->setPageTitle('Науки');

        if (isset($_SESSION['WT']['currentWorld'])) {
            $worldId = $_SESSION['WT']['currentWorld'];
        } else {
            $worldId = 1027;
        }

        $world = Wofh::getWorld($worldId);

        $tech = new Technology();
        $game = new GameData();

        $this->set('scienceData', $tech->getScienceTree($world));
        $this->set('units', $game->getUnits($world));
        $this->set('builds', $game->getBuilds($world));
        $this->set('world', $world);

        $this->setLayout('technology/technology_new');
    }

    public function clearCache()
    {
        if (!$this->user->isAdmin()) {
            $this->setLayout('error404');
        }

        if (isset($_SESSION['WT']['currentWorld'])) {
            $worldId = $_SESSION['WT']['currentWorld'];
        } else {
            $worldId = 1027;
        }

        $world = Wofh::getWorld($worldId);
        $cacheDir = FS::path('/tmp/cache/' . $world->lowerSign . '/');
        $cacheFiles = array(
            'science.list.json',
            'science.tree.json'
        );
        foreach ($cacheFiles as $cacheFile) {
            if (file_exists($cacheDir . $cacheFile)) {
                unlink($cacheDir . $cacheFile);
            }
        }
        $this->redirect($this->router->link('technology'));
    }

    public function getJsonList()
    {
        $referOk = 'http://' . $_SERVER['HTTP_HOST'];
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
//		if (strpos($refer, $referOk) !== 0) die( 'Access denied.' );

        if (isset($_SESSION['WT']['currentWorld'])) {
            $worldId = $_SESSION['WT']['currentWorld'];
        } else {
            $worldId = 1027;
        }

        $world = Wofh::getWorld($worldId);

        $tech = new Technology($world);

        echo $tech->getJSONList($world);
        die;
//		die(__METHOD__);
    }

    public function technology2()
    {
        $this->redirect('/technology/');

        $this->setPageTitle('Науки');
        $xmlString = file_get_contents(_dir('/data/tree.xgml'));

        /** @var $xml \SimpleXMLElement */
        $xml = simplexml_load_string($xmlString);

        $nodes = array();
        $minX = 100000;
        $minY = 100000;
        $diffX = 0;
        $diffY = 0;

        /** @var $node \SimpleXMLElement */
        foreach ($xml->section->section as $node) {
            $rgNode = array();
            $attributes = xml_getAttributes($node);
            if ($attributes['name'] == 'node') {
                /** @var $param \SimpleXMLElement */
                foreach ($node->children() as $param) {
                    $name = $param->getName();
                    $attributes2 = xml_getAttributes($param);
                    if ($name == 'attribute') {
                        if ($attributes2['key'] == 'id') {
                            $rgNode['id'] = (int)(string)$param;
                        }
                        if ($attributes2['key'] == 'label') {
                            $rgNode['label'] = (string)$param;
                        }
                    }
                    if ($name == 'section') {
                        if ($attributes2['name'] == 'graphics') {
                            /** @var $option \SimpleXMLElement */
                            foreach ($param->children() as $option) {
                                $attr3 = xml_getAttributes($option);
                                if ($attr3['key'] == 'x') {
                                    $rgNode['x'] = intval($option);
                                }
                                if ($attr3['key'] == 'y') {
                                    $rgNode['y'] = intval($option);
                                }
                            }
                            if ($rgNode['x'] < $minX) {
                                $minX = $rgNode['x'];
                            }
                            if ($rgNode['y'] < $minY) {
                                $minY = $rgNode['y'];
                            }
                        }
                    }
                }
                $nodes[] = $rgNode;
            }
        }

        $minX -= 30;
        $minY -= 30;
        foreach ($nodes as &$n) {
            $n['x'] -= $minX;
            $n['y'] -= $minY;
        }

        $this->set('nodes', $nodes);
    }

    private function _getLevel($science, $level)
    {
        foreach ($science['takes'] as $takeId) {
            if ($takeId > -1) {
                if (!isset($this->_scList[$takeId]['place']) || $this->_scList[$takeId]['place'] < $level) {
                    $this->_scList[$takeId]['place'] = $level;
                }
                $this->_getLevel($this->_scList[$takeId], $level + 1);
            }
        }
    }

}
