<?php

/**
 * @since              18.10.13 03:20
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\models\GameData;
use df\extensions\models\Polling;

class PollingController extends WTController
{
    public function vote()
    {
        //pre($_SERVER, 0);
        $_POST['ua'] = md5($_SERVER['HTTP_USER_AGENT']);
        $_POST['ip'] = $_SERVER['REMOTE_ADDR'];
        $_POST['poll_id'] = 1;//abs((int)$_POST['poll_id']);
        $_POST['poll_vote'] = abs((int)$_POST['poll_vote']);
        //pre($_POST, 0);
        $polling = new Polling();
        if ($_POST['poll_vote'] > 0) {
            $isVote = $polling->checkVote($_POST['poll_id'], $_POST['ip'], $_POST['ua']);
            if (!$isVote) {
                $polling->addVote($_POST['poll_id'], $_POST['poll_vote'], $_POST['ip'], $_POST['ua']);
            }
        }
        $res = $polling->getResult($_POST['poll_id']);
        $response = json_encode($res);
        //pre($res, 0);
        //pre($response, 0);
        echo $response;
        exit;
    }
}
