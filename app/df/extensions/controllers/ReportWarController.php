<?php
/**
 * @since              24.09.13 01:37
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\App;
use df\extensions\models\WarReport;
use df\extensions\models\WarReports;
use df\extensions\models\Wofh;
use df\Pagination;

/**
 * Class ReportWarController
 */
class ReportWarController extends WTController
{

    public function display()
    {
        if ($this->user->isAdmin()) {
            $worldId = $this->request->get->getInt('w', 0);
            $page = $this->request->get->getInt('p', 1);

            $reports = new WarReports();
            $worlds = $reports->getWorldsAvailable($worldId);
            $total = $reports->getTotal($worldId);
            $link = ($worldId > 0)
                ? '/log/?w=' . $worldId . '&p=%p#log-list'
                : '/log/?p=%p#log-list';

            $pagination = new Pagination();
            $pagination
                ->setTotal($total)
                ->setPage($page)
                ->setLink($link);

            $this->set('worlds', $worlds);
            $this->set('pagination', $pagination);
            $this->set('logs', $reports->getList($worldId, $page));
            $this->set('logsPerDay', $reports->getCountPerDay());
        }
        $this->setLayout('reports/war_display');
    }

    public function transfer()
    {
        $reports = new WarReports();
        $this->redirect('/log/', $reports->transfer());
    }

    public function report()
    {
        $key = $this->request->get['key'];
        $reports = new WarReports();
        $report = $reports->getReport($key);

        if (!$report) {
            $this->setLayout('reports/war_display');
            $this->display();
            $this->addMessage('Лог не найден [ ' . $key . ' ]', App::ERROR);
            return;
//			$this->redirect('/log/', 'Лог не найден', App::ERROR);
        }
//		dmp((array)$report);
        $text = $report->report->aggressor['title']
            . ' <small>' . ($report->report->spy ? 'разведал' : 'напал на') . '</small> '
            . $report->report->defending['title'];

//		$this->breadcrumbs->change('war_report', $text);
        $this->setPageTitle($text);

//		if ($report['spy']) {
//			$this->setPageTitle('Логовница <small>[ военный отчет :: разведка ]</small>');
//		}

//		$this->loadWidget(new WarReportsWidget(
//			$report['report']['options'],
//			$_GET['key'],
//			getSession('ArmyLogId') != $_GET['key']
//		), 'right');

        $this->set('opts', $report->report->options);
//		$this->set('data', $data);

//		$this->set('data1', urlencode(json_encode_utf8($data['data'])));
//		$this->set('data2', urlencode(json_encode_utf8($data['data2'])));

        $this->set('report', $report);
//		$this->set('myReports', $report->getMyReports());
        $this->set('world', Wofh::getWorld($report->worldId));

        $resources = json_decode('["Знания","Деньги","Пища","Древесина","Металл","Топливо","Гранит","Вьючные животные","Сера","Алюминий","Уран","Фрукты","Кукуруза","Пшеница","Рис","Рыба","Мясо","Вино","Драгоценности","Одежда","Музыка","Фильмы","Книги"]',
            true);
        $builds = json_decode('["Алтарь","Пещера","Землянка","Яма","Колодец","Лесоповал","Ферма","Казарма",
		"Обелиск","Терракотовая армия","Конюшня","Жертвенник","Гранитный карьер","Посольство","Суд","Стрельбище",
		"Верфь","Склад","Филармония","Частокол","Пристань","Шахта","Винодельня","Пастбище","Промысловый порт",
		"Резиденция","Рынок","Студия звукозаписи","Фруктовая плантация","Кукурузная плантация","Пшеничное поле","Рисовая плантация",
		"Мастерская","Философская школа","Фонтан","Библиотека","Дом ткача","Больница","Стена","Замок","Банк","Военный завод",
		"Типография","Мать-Земля","Порт","Склад (Азиаты)","Топливный завод","Киностудия","Дом","Мануфактура","Алюминиевый завод",
		"Монетный двор","Ткацкая фабрика","Ратуша","Завод","Мэрия","Геоглиф","Укрепрайон","Колизей","Космический завод",
		"Великая библиотека","Университет","Музей","Собор","Водяное колесо","Космический корабль","Обогатительный завод",
		"Театр","Статуя","Маяк","Академия","Водопровод","Лаборатория","Пирамида","Военная часть","Колосс","Земляная плотина",
		"Космодром","Сухой док","Храм","Обсерватория","Госпиталь","Монастырь","Капище","Ловушка для рыб","Хижина","Тайник",
		"Площадь","Петроглиф","Сероплавильный завод","Кузница","Гелиоконцентратор","Висячие сады","Ангар","Стоунхендж","Царь-пушка",
		"Дом собирателя","Оракул","Орден Тамплиеров","Академия СунЦзы","Мачу-Пикчу","Зенитная башня","ГЭС","Радиолокационная станция",
		"Торговая база","Муляж","Очистная станция","Лас-Вегас","Бранденбургские ворота","Система ПВО","Очистная станция (Индейцы)",
		"Посольство (Африканцы)","Сфинкс","Резерв","Резерв","Резерв","Резерв","Резерв","Резерв","Резерв"]', true);

        $this->set(array(
            'key' => $key,
            'res' => $resources,
            'builds' => $builds,
//			'units'    => Wofh::Inst()->getUnits($report->worldId),
            'deposits' => $this->getDeposits(),
            'climate' => array("Неизвестно", "Вода", "Луга", "Степь", "Пустыня", "Снега"),
        ));

        $this->setLayout('reports/war_report');
    }

    public function saveSecureOptions()
    {
        $reportKey = isset($_POST['reportKey']) ? $_POST['reportKey'] : null;
        $sessionKey = getSession('ArmyLogId');
        if ($reportKey && $sessionKey && ($reportKey === $sessionKey)) {
            $secureData = json_encode(array(
                'an' => (int)$_POST['aggressorName'],
                'as' => (int)$_POST['aggressorStrength'],
                'aa' => (int)$_POST['aggressorArmy'],
                'dn' => (int)$_POST['defendingName'],
                'ds' => (int)$_POST['defendingStrength'],
                'da' => (int)$_POST['defendingArmy'],
            ));

            $this->db->prepare("UPDATE wt_armylog SET options = :opt WHERE id = :id")
                ->execute(array('opt' => $secureData, 'id' => $reportKey));
            if ($this->db->rowCount()) {
                die('{"error":0, "message":"Настройки приватности сохранены"}');
            } else {
                die('{"error":1, "message":"Применить настройки не удалось"}');
            }
        } else {
            die('{"error":1, "message":"Вы не можете изменять настройки приватности для этого отчета"}');
        }
    }

//	public function saveLog()
//	{
//		$id  = $_GET['key'];
//		$log = unserialize(file_get_contents(_dir('/data/army/' . $id . '.army.log')));
//		$this->_saveLog($log, $id);
//		unlink(_dir('/data/army/' . $id . '.army.log'));
//		$this->redirect('/log/');
//	}

    public function saveReport()
    {
//		$this->redirect('/log/');

        if (!isset($_POST['sData'])) {
            $this->redirect('/log/', 'Нет данных');
        }

        $data = urldecode($_POST['sData']);
        $data = json_decode($data, true);


        $WarReport = new WarReport();
        $worldId = Wofh::domainToId($data['domain']);
        $sign = Wofh::idToSign($worldId);
        $id = $WarReport->getValidId();

//		pre($domain, 0);

//		dmp($id, 0);
//		dmp($worldId, 0);
//		dmp($sign, 0);
//		dmp((array)$WarReport, 0);
//		dmp($POST);

        $WarReport->create($id, $sign, $data);
        $WarReport->save();

//		pre($verified, 0);
//		pre($POST);

        $this->redirect('/log/' . $id . '/');
    }

    public function deleteLog()
    {
        if ($this->user->id != 1) {
            $this->redirect('/log/');
            return;
        }

        $key = $_GET['key'];
        $this->db->prepare('DELETE FROM wt_armylog WHERE id = :id');
        $this->db->execute(array('id' => $key));
        $msg = $this->db->rowCount() ? 'Лог удален' : 'Без изменений';
        $this->redirect('/log/', $msg);
    }

    private function _decodeArmy($data, $delimiter = '-')
    {
        $array = explode($delimiter, trim($data, $delimiter));
        $result = array();
        foreach ($array as $item) {
            if (preg_match('/^([a-z][a-z])(\d+)$/i', $item, $m)) {
                $unitId = wt_convert26($m[1]);
                $unitsCount = (int)$m[2];
                if (isset($result[$unitId])) {
                    $result[$unitId] += $unitsCount;
                } else {
                    $result[$unitId] = $unitsCount;
                }
            }
        }
        ksort($result);
        return $result;
    }

    private function getDeposits()
    {
        return array(
            array("climate" => 2, "name" => "Неизвестно"),
            array("climate" => 2, "name" => "Лес"),
            array("climate" => 3, "name" => "Лес"),
            array("climate" => 5, "name" => "Лес"),
            array("climate" => 4, "name" => "Оазис"),
            array("climate" => 4, "name" => "Бананы"),
            array("climate" => 2, "name" => "Яблоки"),
            array("climate" => 3, "name" => "Абрикосы"),
            array("climate" => 2, "name" => "Виноград"),
            array("climate" => 3, "name" => "Виноград"),
            array("climate" => 2, "name" => "Кукуруза"),
            array("climate" => 2, "name" => "Пшеница"),
            array("climate" => 2, "name" => "Рис"),
            array("climate" => 1, "name" => "Рыба"),
            array("climate" => 1, "name" => "Киты"),
            array("climate" => 1, "name" => "Крабы"),
            array("climate" => 1, "name" => "Устрицы"),
            array("climate" => 3, "name" => "Свиньи"),
            array("climate" => 2, "name" => "Коровы"),
            array("climate" => 5, "name" => "Олени"),
            array("climate" => 3, "name" => "Овцы"),
            array("climate" => 2, "name" => "Овцы"),
            array("climate" => 3, "name" => "Хлопок"),
            array("climate" => 2, "name" => "Лен"),
            array("climate" => 5, "name" => "Золото"),
            array("climate" => 2, "name" => "Серебро"),
            array("climate" => 4, "name" => "Алмазы"),
            array("climate" => 4, "name" => "Изумруды"),
            array("climate" => 3, "name" => "Рубины"),
            array("climate" => 1, "name" => "Жемчуг"),
            array("climate" => 5, "name" => "Железная руда"),
            array("climate" => 2, "name" => "Железная руда"),
            array("climate" => 3, "name" => "Железная руда"),
            array("climate" => 5, "name" => "Гранит"),
            array("climate" => 2, "name" => "Гранит"),
            array("climate" => 3, "name" => "Гранит"),
            array("climate" => 2, "name" => "Лошади"),
            array("climate" => 4, "name" => "Верблюды"),
            array("climate" => 3, "name" => "Слоны"),
            array("climate" => 2, "name" => "Серная руда"),
            array("climate" => 3, "name" => "Серная руда"),
            array("climate" => 4, "name" => "Серная руда"),
            array("climate" => 5, "name" => "Алюминиевая руда"),
            array("climate" => 2, "name" => "Алюминиевая руда"),
            array("climate" => 3, "name" => "Алюминиевая руда"),
            array("climate" => 5, "name" => "Природный газ"),
            array("climate" => 3, "name" => "Природный газ"),
            array("climate" => 4, "name" => "Нефть"),
            array("climate" => 2, "name" => "Нефть"),
            array("climate" => 1, "name" => "Нефть"),
            array("climate" => 2, "name" => "Уголь"),
            array("climate" => 3, "name" => "Уголь"),
            array("climate" => 5, "name" => "Уран"),
            array("climate" => 3, "name" => "Уран"),
            array("climate" => 4, "name" => "Уран"),
            array("climate" => 5, "name" => "Источник мудрости"),
        );
    }

}
