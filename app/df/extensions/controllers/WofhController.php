<?php
/**
 * @since              15.09.13 00:18
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\App;
use df\extensions\models\GameData;
use df\extensions\models\Wofh;
use df\helper\FS;
use df\helper\JSON;

/**
 * Class WofhController
 *
 * @package df\component\wofh
 */
class WofhController extends WTController
{
    public static $UNIT_TYPES = array('Пехота', 'Конница', 'Техника', 'Корабль', 'Авиация', 'Компонент КК');
    public static $UNIT_GROUPS = array('Атака', 'Резерв', 'Стрелки', 'Обход', 'Фланг');

    public function display()
    {
//		$this->setPageTitle('API Wofh');

        if (!$this->user->isAdmin()) {
            $this->setLayout('not-access');
            return;
        }

        $this->set('worlds', Wofh::getWorlds());

        if (isset($_SESSION['WT']['currentWorld'])) {
            $worldId = $_SESSION['WT']['currentWorld'];
        } else {
            $worldId = 1023;
        }

        //$world = Wofh::getWorld($worldId);
        //$today = date(STD_DATE);

        //$localFile = FS::path("/tmp/const/const-data-{$world->lowerSign}-{$today}.json");
        //$constRaw = trim(FS::readFile($localFile));
        //$start    = mb_strpos($constRaw, '{', null, 'UTF-8');
        //$end      = mb_strrpos($constRaw, '}', null, 'UTF-8');
        //$constRaw = mb_substr($constRaw, $start, $end - $start + 1, 'UTF-8');
        //$constData = JSON::decode($constRaw);
        //$this->set('constData', $constData);

//		$units = $this->db->query('SELECT * FROM cd_units ORDER BY title')->fetchAll();

//		$this->view->addScript('bootstrap-datepicker');

        return;

//		$world = getCurrentWorld();
//
//		$science  = json_decode(file_get_contents(_dir("/data/{$world['country']}{$world['id']}/science.json")), true);
//		$resource = json_decode(file_get_contents(_dir("/data/{$world['country']}{$world['id']}/resource.json")), true);
//		$builds   = json_decode(file_get_contents(_dir("/data/{$world['country']}{$world['id']}/builds.json")), true);
//		$units    = json_decode(file_get_contents(_dir("/data/{$world['country']}{$world['id']}/units.json")), true);
//		$deposit  = json_decode(file_get_contents(_dir("/data/{$world['country']}{$world['id']}/deposit.json")), true);
//
//		if (file_exists(_dir('/data/updates.json'))) {
//			$this->set('updates', getJsonFile('/data/updates.json'));
//		}
//		$this->set('worldsList', getWorlds());
//
//		$this->set('science', $science);
//		$this->set('resource', $resource);
//		$this->set('builds', $builds);
//		$this->set('units', $units);
//		$this->set('deposit', $deposit);
    }

    public function scanServers()
    {
        if (!$this->user->isAdmin()) {
            $this->setLayout('not-access');
            return;
        }

        Wofh::check();
        $this->redirect($this->router->link('wofh'));
    }

    public function optionSwitch()
    {
//		sleep(1);

        $o = array('message' => 'Ooops! Error');
        $error = true;

        if (!$this->user->isAdmin()) {
            $o['message'] = 'Access denied';
            $o['error'] = $error;
            echo json_encode((object)$o);
            exit;
        }

        $option = $_POST['option'];
        $worldId = abs((int)$_POST['worldId']);
        $world = Wofh::getWorld($worldId);

        if ($world) {
            $world->{$option} = !$world->{$option};
            $error = !$world->save();
            if ($error) {
                $o['message'] = $world->getLastError();
            }
        }

        $o['error'] = $error;
        $o['world'] = $world;
        $o['$_POST'] = $_POST;

        echo json_encode((object)$o);
        exit;
    }

    public function updateConst()
    {
        if (!$this->user->isAdmin()) {
            return;
        }

        $world = Wofh::getWorld(abs((int)$_POST['worldId']));

        $gameData = new GameData();
        try {
            $message = $gameData->updateConst($world);
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $this->redirect($this->router->link('wofh'), $message);
    }

    private function _formatCost($number)
    {
        $formatCost = $number;
        if ($number > 1000000 && !($number % 1000000)) {
            $formatCost = (string)($number / 1000000) . ' M';
        } elseif (($number >= 10000 && !($number % 1000))) {
            $formatCost = (string)($number / 1000) . ' K';
        }
        return $formatCost;
    }

    private function _getAllNeeds($scienceId)
    {
        foreach ($this->_scList[$scienceId]['need'] as $needId) {
            if ($needId == -1) {
                continue;
            }

            if (!in_array($needId, $this->_nAll)) {
                $this->_nAll[] = $needId;
                $this->_getAllNeeds($needId);
            }
        }

        return $this->_nAll;
    }

}
