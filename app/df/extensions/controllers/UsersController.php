<?php
/**
 * @since              15.08.13 01:32
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\App;
use df\Registry;

class UsersController extends WTController
{
    public function registration()
    {
        if ($this->user->isAuth()) {
            $this->redirect('/profile/');
            return;
        }

        $this->document->setPageTitle('Регистрация');
        $this->set('invite', '');

        if (isset($_GET['invite'])) {
            $this->set('invite', $_GET['invite']);
            $this->_checkInvite($_GET['invite']);
            if ($this->errorCount) {
                $this->redirect('/registration/');
                return;
            }
        }
    }

    public function login()
    {

        if ($this->user->isAuth()) {
            $this->redirect('/profile/');
        }

        $this->setPageTitle('Вход для пользователей');
    }

    public function doLogin()
    {
        if ($this->user->isAuth()) {
            $this->redirect('/profile/');
        }

        $email = $this->request->post['email'];
        $hash = md5($this->request->post['passw']);

        if ($this->user->auth($email, $hash)) {
            $this->redirect('/profile/' . $this->user->id . '/');
        } else {
            $this->redirect('/login/', 'Неверный логин или пароль', App::ERROR);
        }
    }

    public function quit()
    {
        $this->user->logout();
        $this->redirect('/login/');
    }

    public function save()
    {
        $this->setPageTitle('Профиль пользователя');

        if (!$this->user->isAuth()) {
            $this->view->setLayout('not-access');
            return;
        }

        $username = request_post('username', '');
        $sex = abs((int)request_post('sex', 0));
        $sex = $sex > 2 ? 0 : $sex;

        $this->db->prepare('UPDATE users SET username = :username, sex = :sex WHERE id = :userId');
        $this->db->execute(array('username' => $username, 'sex' => $sex, 'userId' => $this->user->id));
        if (!$this->db->rowCount()) {
            $error = $this->db->getError(false);
            if ((int)$error[0]) {
                $this->redirect('/profile/' . $this->user->id . '/edit/', 'Ошибка. Код ' . $error[0], App::ERROR);
            }
        }

        $this->redirect('/profile/' . $this->user->id . '/edit/', 'Сохранено.', App::SUCCESS);
        $_SESSION['user']['username'] = $username;
        $_SESSION['user']['sex'] = $sex;
    }

    public function profile()
    {
        $this->setPageTitle('Профиль пользователя');
        $profileID = $this->request->get->offsetGet('id', (int)$this->user->id);

        if ($profileID <= 0) {
            $this->setLayout('user-only');
            return;
        }

        if ($this->user->isAuth() && ($profileID == $this->user->id)) {
            $this->set('profile', $this->user);
        } else {
            $db = Registry::getDb();
            $db->select("SELECT *, UNIX_TIMESTAMP(register) AS register FROM users WHERE id = :id LIMIT 1",
                array('id' => $profileID));
            $user = $db->fetchObject();
            $user->profile = $this->_getProfile($user->id);
            $this->set('profile', $user);
        }
    }

    public function edit()
    {
        $this->setPageTitle('Профиль пользователя <small>[ редактирование ]</small>');

        $profileID = isset($_GET['id']) ? (int)$_GET['id'] : (int)$this->user->id;

        if ($profileID <= 0) {
            $this->view->setLayout('user-only');
            return;
        }

        if (!$this->user->isAuth() || ($profileID != $this->user->id)) {
            $this->view->setLayout('not-access');
            return;
        }
    }

    /**
     * @param $invite
     *
     * @deprecated
     */
    private function _checkInvite($invite)
    {
        $this->db->select("SELECT * FROM invites WHERE invite = :invite", array('invite' => $invite));
        $invite = $this->db->fetchObject();
        if (!$invite) {
            $this->addMessage('Этот пригласительный код недействителен', App::ERROR);
        } elseif ($invite->activated == 1) {
            $this->addMessage('Этот пригласительный код уже активирован', App::ERROR);
        }
    }

    /**
     * @deprecated
     */
    private function _doRegister()
    {
        $invite = request_post('invite', '');
        $username = request_post('username', '');
        $email = request_post('email', '');
        $passw = request_post('passw', '');
        $passw2 = request_post('passw2', '');

        if (empty($invite)) {
            $this->addMessage('Не заполнено поле "Приглашение"', App::ERROR);
        } else {
            $this->_checkInvite($invite);
        }

        if (empty($username)) {
            $this->addMessage('Не заполнено поле "Имя"', App::ERROR);
        }
        if (empty($email)) {
            $this->addMessage('Не заполнено поле "E-Mail"', App::ERROR);
        }
        if (empty($passw) AND empty($passw2)) {
            $this->addMessage('Не заполнено поле "Пароль"', App::ERROR);
        } elseif ($passw !== $passw2) {
            $this->addMessage('Введенные пароли не совпадают', App::ERROR);
        }
        if ($this->errorCount) {
            $this->redirect(empty($invite) ? '/registration/' : '/invite/' . $invite . '/');
            return;
        }

        $this->db->select("SELECT * FROM users WHERE email = :email", array('email' => $email));
        if ($temp = $this->db->fetchObject()) {
            $this->addMessage('Пользователь с адресом электронной почты «' . htmlspecialchars($email) . '» уже зарегистрирован',
                App::ERROR);
        }

        if ($this->errorCount) {
            $this->redirect(empty($invite) ? '/registration/' : '/invite/' . $invite . '/');
            return;
        }

        try {
            $this->db->beginTransaction();
            $this->db->prepare("INSERT INTO users SET password = :password, email = :email, username = :username");
            $this->db->execute(array('username' => $username, 'password' => _hash($passw), 'email' => $email));
            $userID = $this->db->insertId();
            $this->db->prepare("UPDATE invites SET activated = 1, to_user = :user_id
				WHERE invite = :invite AND activated = 0");
            $this->db->execute(array('user_id' => $userID, 'invite' => $invite));
            if ($this->db->rowCount()) {
                $this->db->commit();
                $this->redirect('/login/', 'Можете войти.', App::SUCCESS);
            } else {
                $this->db->rollBack();
                $this->addMessage('Этот пригласительный код уже активирован', App::ERROR);
                $this->redirect(empty($invite) ? '/registration/' : '/invite/' . $invite . '/');
            }
        } catch (\PDOException $e) {
            $this->db->rollBack();
            _errorLog($e->getMessage());
            $this->addMessage('Ошибка PDO [' . $e->getCode() . ']', App::ERROR);
            $this->redirect(empty($invite) ? '/registration/' : '/invite/' . $invite . '/');
        }
    }

    /**
     * @param $email
     * @param $password_hash
     *
     * @return mixed
     * @deprecated
     */
    private function _checkUser($email, $password_hash)
    {
        $this->db->select("
			SELECT *, UNIX_TIMESTAMP(register) AS register
			FROM users
			WHERE email = :email
			AND password = :pass
			LIMIT 1",
            array('email' => $email, 'pass' => $password_hash));
        return $this->db->fetchObject();
    }

    /**
     * @param $userId
     *
     * @return mixed
     * @deprecated
     */
    private function _getProfile($userId)
    {
        $this->db->select("
			SELECT * FROM profiles
			LEFT JOIN players
				ON profiles.player_id = players.player_id
			WHERE profiles.user_id = :id
			LIMIT 1",
            array('id' => $userId));
        return $this->db->fetchObject();
    }
}
