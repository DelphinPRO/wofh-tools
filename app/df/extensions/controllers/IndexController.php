<?php

/**
 * @since              07.01.13 18:16
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\models\Polling;
use df\extensions\models\ScienceReport;
use df\extensions\models\WarReports;
use df\extensions\models\Wofh;

/**
 * @property Wofh wofh
 */
class IndexController extends WTController
{
    public function display()
    {
        $this->setPageTitle('');
        $worlds = Wofh::getWorlds(array('WORKING'));
        ksort($worlds);
        $this->set('worlds', $worlds);
        $this->set('sciLogsPerDay', ScienceReport::getLogPerDay());
        $this->set('sciLogsToday', ScienceReport::getLogToday());

        $reports = new WarReports();
        $this->set('warLogsPerDay', $reports->getCountPerDay());
        $this->set('warLogsToday', $reports->getCountToday());

        $polling = new Polling();
        $poll = $polling->getPoll(1);
        //pre($poll);
        $ua = md5($_SERVER['HTTP_USER_AGENT']);
        $ip = $_SERVER['REMOTE_ADDR'];
        $isVote = $polling->checkVote(1, $ip, $ua);
        if ($isVote) {
            $result = $polling->getResult(1);
            $this->set('pollResult', $result);
        }

        $this->set('poll', $poll);
    }

    public function contacts()
    {
        $this->document->setPageTitle('Обратная связь');
    }

    public function changeLog()
    {
        $this->document->setPageTitle('Change log');
    }

    public function selectWorld()
    {
        $sign = $this->request->get->offsetGet('sign', '');
        $back = $this->request->get->offsetGet('back', '');
        $world = Wofh::getWorldBySign($sign);
        if ($world) {
            $_SESSION['WT']['currentWorld'] = $world->id;
        }
        if ($back) {
            $this->redirect($back);
        }
        $this->redirect('/');
    }

    public function setIP()
    {
        $this->config->set('app.allowed_ip', $_SERVER['REMOTE_ADDR']);
        $this->config->save();
        $this->redirect('/' . $this->config->get('app.secret') . '/');
    }

    public function unsetIP()
    {
        $this->config->set('app.allowed_ip', '');
        $this->config->save();
        $this->redirect('/' . $this->config->get('app.secret') . '/');
    }

    public function site()
    {
        $this->config->set('app.site_offline', !$this->config->get('app.site_offline'));
        $this->config->save();
        $this->redirect('/' . $this->config->get('app.secret') . '/');
    }

    public function debug()
    {
        $this->config->set('app.debug', !$this->config->get('app.debug'));
        $this->config->save();
        $this->redirect('/' . $this->config->get('app.secret') . '/');
    }

    public function secret()
    {
        $this->setPageTitle($this->config->get('app.site_name') . ' > Secret area :)');
    }
}
