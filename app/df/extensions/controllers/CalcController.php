<?php
/**
 * @since              21.10.13 04:06
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\models\GameData;

class CalcController extends WTController
{
    public function trainSettlers()
    {
        $world = $this->getCurrentWorld();
        $this->setPageTitle("Расчет времени тренировки поселенцев <small>{$world->fullTitle}</small>");
    }

    public function consumptionOfResources()
    {
        $world = $this->getCurrentWorld();
        $this->setPageTitle("Потребление ресурсов <small>{$world->fullTitle}</small>");
    }

    public function costOfBuilds()
    {
        $world = $this->getCurrentWorld();
        $game = new GameData();
        $builds = $game->getBuilds($world);

        $this->set('builds', $builds);
        $this->setPageTitle("Строения <small>{$world->fullTitle}</small>");
    }

    public function costOfBuild()
    {
        $world = $this->getCurrentWorld();
        $game = new GameData();
        $builds = $game->getBuilds($world);
        $id = $this->request->get->getInt('id', -1);

        $this->app->addWidget('right', 'builds');

        $this->set('builds', $builds);
        $this->set('build', $builds[$id]);

        $units = array();
        foreach ($builds[$id]['unitsTrain'] as $unitId) {
            $units[$unitId] = $game->getUnit($world, $unitId);
        }
        $this->set('unitsTrain', $units);
        $this->setPageTitle("{$builds[$id]['title']} <small>{$world->fullTitle}</small>");
    }

}
