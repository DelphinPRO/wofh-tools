<?php
/**
 * @since             17.10.2014 00:07
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\Controller;
use df\extensions\models\Wofh;

abstract class WTController extends Controller
{

    protected $backUrl;

    protected function _before()
    {
        $this->set('wtData', array(
            'route' => $this->router->route->name,
            'stat' => array(
                'world' => array(
                    'sign' => null,
                ),
                'charts' => array(),
            ),
        ));

//		pre($_SESSION, 0);
//		pre($_GET, 0);
//		pre($this);
//		if (isset( $_GET['back'] ) && isset( $_SESSION['WT']['backURL'][$_GET['back']] )) {
//			$this->backUrl = $_SESSION['WT']['backURL'][$_GET['back']];
//		}

//		$backURL                             = uniqid();
//		$_SESSION['WT']['backURL'][$backURL] = $this->router->getUri();

        if (!isset($_SESSION['WT']['currentWorld'])) {
            $_SESSION['WT']['currentWorld'] = 1030;
        }

        $currentWorld = Wofh::getWorld($_SESSION['WT']['currentWorld']);
        $worldsMenu = Wofh::getWorlds(array('WORKING'));

        $this->set('currentWorld', $currentWorld);
        $this->set('worldsMenu', $worldsMenu);
//		$this->set('backURL', $backURL);
    }

    protected function _after()
    {

    }

    /**
     * @return \df\extensions\models\World|null
     */
    public function getCurrentWorld()
    {
        if (isset($_SESSION['WT']['currentWorld'])) {
            $worldId = (int)$_SESSION['WT']['currentWorld'];
        } else {
            $worldId = 1023;
        }

        return Wofh::getWorld($worldId);
    }
}
