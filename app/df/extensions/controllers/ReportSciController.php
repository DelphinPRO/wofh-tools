<?php
/**
 * @since              13.09.13 15:56
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\App;
use df\exception\SciReportException;
use df\extensions\models\ScienceReport;
use df\extensions\models\Wofh;

/**
 * Class ScienceController
 *
 * @package df\extensions\controllers
 */
class ReportSciController extends WTController
{
    public function display()
    {
        if ($this->user->isAdmin()) {
            $this->set('logsPerDay', ScienceReport::getLogPerDay());
            $this->set('logsToday', ScienceReport::getLogToday());
            $this->set('logs', ScienceReport::getLogList());
        }
        $this->setLayout('reports/sci_display');
    }

    public function receiveReport()
    {
        try {
            $report = ScienceReport::Create($_POST['sData']);
            $report->save();

            $_SESSION['wt.science.account.id'] = $report->accountId;
            $_SESSION['wt.science.client.ip'] = $report->clientIP;
            $_SESSION['wt.science.world.id'] = $report->worldId;

            $this->redirect('/sci/' . $report->id . '/');
        } catch (SciReportException $e) {
            $this->redirect('/sci/', $e->getMessage(), App::ERROR);
        }
    }

    public function displayReport()
    {
        try {
            $key = $this->request->get->offsetGet('key', '');
            if (!preg_match('/^[a-f\d]+$/', $key)) {
                $this->redirect('/sci/', 'Некорректный идентификатор лога', App::ERROR);
            }

            $report = ScienceReport::Load($key);
            $world = Wofh::getWorld($report->worldId);

            $relativeReports = ScienceReport::getRelativeReports();

            $this->set('relativeReports', $relativeReports);
            $this->set('report', $report);
            $this->set('world', $world);
            $this->setPageTitle('Наука игрока ' . $report->accountName . ' <small>' . date('d.m.Y H:i',
                    $report->generateTime) . '</small>');
            $this->app->addWidget('right', 'html', 'sci_report');
            $this->setLayout('reports/sci_report');
        } catch (SciReportException $e) {
            if ($e->getCode() == 1) {
                $this->setPageTitle('Научный отчет');
                $this->setLayout('reports/not_found');
            }
            $this->redirect('/sci/', $e->getMessage(), App::ERROR);
        }
    }

    public function compare()
    {
        if (!isset($_POST['key1']) || !isset($_POST['key2'])) {
            $this->redirect('/sci/', 'Логи не найдены', App::ERROR);
        }

        if (!preg_match('/^[a-f\d]+$/', $_POST['key1'])
            || !preg_match('/^[a-f\d]+$/', $_POST['key2'])
        ) {
            $this->redirect('/sci/', 'Некорректный идентификатор лога', App::ERROR);
        }

        $this->redirect('/sci/' . $_POST['key1'] . '/' . $_POST['key2'] . '/');
    }

    public function compareView()
    {
        try {
            $key1 = $this->request->get->offsetGet('key1', '');
            $key2 = $this->request->get->offsetGet('key2', '');

            if (!$key1 || !$key2) {
                $this->redirect('/sci/', 'Логи не найдены', App::ERROR);
            }

            if (!preg_match('/^[a-f\d]+$/', $key1)
                || !preg_match('/^[a-f\d]+$/', $key2)
            ) {
                $this->redirect('/sci/', 'Некорректный идентификатор лога', App::ERROR);
            }

            $report1 = ScienceReport::Load($key1);
            $report2 = ScienceReport::Load($key2);

            if ($report1->worldId != $report2->worldId) {
                $this->redirect('/sci/' . $key1 . '/', 'Не имеет смысла сравнивать отчеты из разных миров ;)');
            }

            if ($report1->worldId < 1023 && $report1->worldId > 1999) {
                $this->redirect('/sci/' . $key1 . '/', 'Сравнение отчетов только для версии ПИ 1.4.x');
            }

            $countAccounts = 0;
            if (isset($_GET['a'])) {
                $countAccounts = abs((int)$_GET['a']);
            }
            if (!$countAccounts && $report1->countryId > 0) {
                $countAccounts = ScienceReport::getCountryAccounts($report1->countryId, $report1->worldId);
            }
            if (!$countAccounts) {
                $countAccounts = 1;
            }

            $this->set('world', Wofh::getWorld($report1->worldId));
            $this->set('report1', $report1);
            $this->set('report2', $report2);
            $this->set('countAccounts', $countAccounts);
            $this->setPageTitle('Сравнение наук <small>' . $report1->accountName . '</small> и <small>' . $report2->accountName . '</small>');

            $this->app->addWidget('right', 'html', 'sci_compare');
            $this->setLayout('reports/sci_compare');
        } catch (SciReportException $e) {
            $this->redirect('/sci/', $e->getMessage(), App::ERROR);
        }
    }

}
