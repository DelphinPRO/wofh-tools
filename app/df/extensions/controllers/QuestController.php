<?php

/**
 * @since       19.04.2016 07:22
 * @package
 * @author      delphinpro delphinpro@yandex.ru
 * @copyright   copyright (C) 2016 delphinpro
 * @license     Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\models\Quest;

class QuestController extends WTController
{
    public function display()
    {
        $questModel = new Quest();
        $quests = $questModel->getQuests();

        $this->set('quests', $quests);
        $this->setPageTitle("Квесты <small>v1.4</small>");
    }

    public function edit()
    {
        if (!$this->user->isAdmin()) {
            $this->setLayout('not-access');

            return;
        }

        $questModel = new Quest();
        $quests = $questModel->getQuests();

        $this->set('quests', $quests);
        $this->setPageTitle("Квесты [редактирование] <small>v1.4</small>");
    }

    public function save()
    {
        if (!$this->user->isAdmin()) {
            $this->redirect($this->router->link('quest'), 'Not access');

            return;
        }

        $questModel = new Quest();
        $questModel->save($_POST['quest']);
        $this->redirect($this->router->link('quest_edit'), 'Saved');
    }
}
