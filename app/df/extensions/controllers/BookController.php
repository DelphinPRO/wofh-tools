<?php

/**
 * @since              18.10.13 03:20
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\extensions\controllers;

use df\extensions\models\GameData;

class BookController extends WTController
{
    public function scienceDistribution()
    {
        $game = new GameData();
        $world = $this->getCurrentWorld();
        $science = $game->getScience($world);
        $this->set('science', $science);
        $this->setPageTitle("Таблица научного распределения <small>{$world->fullTitle}</small>");
        $this->setLayout('book/science-distribution');
    }

    public function corruption()
    {
        $world = $this->getCurrentWorld();
        $this->setPageTitle("Таблица коррупции <small>{$world->fullTitle}</small>");
    }

    public function diplomacyPoints()
    {
        $world = $this->getCurrentWorld();
        $this->setPageTitle("Очки дипломатии <small>{$world->fullTitle}</small>");
        $this->setLayout('book/diplomacy-points');
    }
}
