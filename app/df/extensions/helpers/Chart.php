<?php

/**
 * @since             17.10.2014 00:57
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\extensions\helpers;

class Chart
{
    public static function pie($cId, $title, $subtitle, $tooltip, $data)
    {
        return array(
            'cId' => $cId,
            'type' => 'pie',
            'title' => $title,
            'subtitle' => $subtitle,
            'series' => array(
                array(
                    'type' => 'pie',
                    'name' => $tooltip,
                    'data' => $data,
                )
            )
        );
    }
}
