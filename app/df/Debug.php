<?php
/**
 * @since             22.01.14 21:49
 * @package           DFramework
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df {

    use WofhTools\Core\Conf\Config;

    class Dumper
    {

        const EXPAND = true;

        private static $log = false;
        private static $id = 1;
        private static $level = 0;

        public static function pre($var, $title = '', $exit = 1)
        {
            if (!Registry::getConfig()->showDebug()) {
                return;
            }

            static $first = true;

            if ($first) {
                self::printStylesheet();
                $first = false;
            }

            if (is_int($title)) {
                $exit = $title;
                $title = '';
            }

            echo '<div class="php_debug_message_box">' . PHP_EOL;
            if ($title != '') {
                echo '<div class="php_debug_message_box_title">' . $title . '</div>';
            }
            echo '<div class="php_debug_message_box_inner">';
            if (empty($var) || is_bool($var)) {
                echo '<pre>';
                var_dump($var);
                echo '</pre>';
            } else {
                echo '<pre>', htmlspecialchars(print_r($var, true)), '</pre>';
            }
            echo '</div>';

            echo self::printDebugBacktrace(debug_backtrace(false));

            if ($exit === 1) {
                echo '<div class="php_debug_message_box_footer"><a href="' . $_SERVER['REQUEST_URI'] . '">STOP</a></div>';
            }
            echo '</div>';
            if ($exit === 1) {
                die();
            }
        }

        public static function cliPre($var, $title = '', $exit = 1)
        {
            if (is_int($title)) {
                $exit = $title;
                $title = '';
            }

            if ($title != '') {
                echo $title . PHP_EOL;
            }
            if (empty($var) || is_bool($var)) {
                var_dump($var);
            } else {
                echo print_r($var, true);
            }

            echo 'Stack trace:' . PHP_EOL;
            $stack = debug_backtrace(false);
            array_shift($stack);
            foreach ($stack as $k => $i) {
                if (isset($i['file'])) {
                    $file = str_replace(DIR_ROOT, '', $i['file']);
                    $file = str_replace('\\', '/', $file);
                } else {
                    $file = $i['file'];
                }
                echo '#' . $k . ': ' . $file . ':' . $i['line'];
                if (isset($i['class'], $i['type'])) {
                    echo ' ' . $i['class'] . $i['type'];
                } else {
                    echo ' ';
                }
                echo $i['function'] . '()';
                echo PHP_EOL;
            }

            echo '-----------------------------------------------' . PHP_EOL . PHP_EOL;
            if ($exit === 1) {
                die();
            }
        }

        public static function printDebugBacktrace($trace)
        {
            if (!Registry::getConfig()->showDebug()) {
                return '';
            }

            static $index = 0;

            $i = array_shift($trace);
            echo '<code style="color: #aaa;font-size: 0.85em;padding-left: 5px;">' . $trace[0]['file'] . ':' .
                $trace[0]['line'] .
                '</code>';
            $html = '';
            $html .= '<i class="backtrace_link" onclick="backtraceToggle(' . ++$index . ')">';
            $html .= '<u>debug_backtrace output</u></i>';
            $html .= '<div id="debug_backtrace' . $index . '" class="backtrace_table" style="display:none;">';
            $html .= '<table>';
            foreach ($trace as $item) {
                if (isset($item['function']) && $item['function'] == __FUNCTION__) {
                    continue;
                }

                $html .= '<tr><td>';
                if (isset($item['class'])) {
                    $html .= '<b>' . $item['class'] . '</b>' . $item['type'];
                }
                if (isset($item['function'])) {
                    if (isset($item['class'])) {
                        $html .= '<span style="color:#f0f">';
                    }
                    $html .= $item['function'];
                    if (isset($item['class'])) {
                        $html .= '</span>';
                    }
                    $html .= '();';
                }
                $html .= '</td><td>';
                $html .= (isset($item['file'])) ? str_replace(DIRECTORY_SEPARATOR, '/', str_replace(DIR_ROOT, '', $item['file'])) : '';
                $html .= (isset($item['line'])) ? ' (<b>' . $item['line'] . '</b>)' : '';
                $html .= '</td></tr>';
            }
            $html .= '</table>';
            $html .= '</div>';

            return $html;
        }

        public static function dump($source, $title = '', $exit = 1)
        {
            if (!Config::Inst()->showDebug()) {
                return;
            }

            $exit = (bool)$exit;
            if (is_string($title)) {
                $title = '<div class="df-dump-title">' . htmlspecialchars($title) . '</div>';
            }
            if (is_int($title)) {
                $exit = (bool)$title;
                $title = '';
            }

            self::$level = 0;

            $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            self::_dumpStyles();
            echo '<pre class="df-dump">';
            echo $title;
            self::_dump($source);

            $index = __FILE__ === $trace[0]['file'] ? 1 : 0;
            echo '<div><small>' . str_replace(DIR_ROOT, '',
                    $trace[$index]['file']) . ':' . $trace[$index]['line'] . '</small></div>';
            echo '</pre>';
            if ($exit) {
                exit;
            }
        }

        public static function log($message, $n = true)
        {
            if ($n) {
                $message = "\n" . date('d.m.Y H:i:s') . "\t\t" . $message;
            } else {
                $message = "\t" . $message;
            }
            if (!self::$log) {
                file_put_contents(DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'app.log', $message);
                self::$log = true;
            } else {
                file_put_contents(DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'app.log', $message, FILE_APPEND);
            }
        }

        public static function printTransaction(array $transactions)
        {

            if (!Registry::getConfig()->showDebug()) {
                return;
            }

            $html = '<style>.asd:hover td {background:#000;}</style>';
            $html .= '<table style="width:100%;font-family:Consolas, monospace;">';
            $html .= '<caption style="text-align:left;color: #000;">Количество транзакций: ' . count($transactions) . '</caption>';
            foreach ($transactions as $t) {
                $id = uniqid();
                $html .= '<tr class="asd" style="background:#555;color:#eee;cursor:pointer" onclick="document.getElementById(\'a' . $id . '\').style.display=\'table-row-group\'"><td colspan="3">' . $t['query'] . '</td></tr>';
                $html .= '<tbody style="display:none;" id="a' . $id . '">';
                foreach ($t['trace'] as $k => $trace) {
                    $file = str_replace(DIR_ROOT, '', array_key_exists('file', $trace) ? $trace['file'] : '[no file]');
                    $html .= '<tr>';
                    $html .= '<td style="padding:0 5px;text-align:right;">' . $k . '</td>';
                    $html .= '<td>';
                    if (isset($trace['class'])) {
                        $html .= $trace['class'] . $trace['type'];
                    }
                    $html .= $trace['function'] . '()</td>';
                    $html .= '<td>' . $file . ':<b>' . (array_key_exists('line', $trace) ? $trace['line'] : '?') . '</b></td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody>';
            }
            $html .= '</table>';
            echo $html;
        }

        private static function _dump($source)
        {
            $maxTextLength = 90;
            if (is_array($source)) {
                self::$level++;
                self::_dumpArray($source);
                self::$level--;
            } elseif (is_object($source)) {
                self::$level++;
                self::_dumpObject($source);
                self::$level--;
            } elseif (is_string($source)) {
                if (mb_strlen($source, 'UTF-8') > $maxTextLength) {
                    $short = mb_strcut($source, 0, $maxTextLength, 'UTF-8');
                    echo "string[" . mb_strlen($source, 'UTF-8') . "] <span style='color:blue' title='$source'>'$short...'</span>\n";
                } else {
                    echo "string[" . mb_strlen($source, 'UTF-8') . "] <span style='color:blue'>'$source'</span>\n";
                }
            } elseif (is_int($source)) {
                echo "(int)    <span style='color:red'>$source</span>\n";
            } elseif (is_float($source)) {
                echo "(float)  <span style='color:#ff00ff'>$source</span>\n";
            } elseif (is_null($source)) {
                echo "(null)   <b>NULL</b>\n";
            } elseif (is_bool($source)) {
                echo $source ? "(bool)   <b>TRUE</b>\n" : "(bool)   <b>FALSE</b>\n";
            } else {
                echo "(?)      <i>$source</i>\n";
            }
        }

        private static function _dumpObject($source)
        {
            $reflection = new \ReflectionObject($source);
            $properties = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC);
            $count = count($properties);
            $onClick = $count ? "onclick=\"dfDumpToggle('df-" . self::$id . "')\"" : '';
            echo "<span class='df-switch' $onClick>" .
                "Object[$count] <span style='color:#000'>" . get_class($source) . "</span></span> {";
            if ($count) {
                echo "<div class='df-cont" . (!self::EXPAND || self::$level > 1 ? "-closed" : '') . "' id='df-" . (self::$id++) . "'>";
                $maxKeyLength = self::_dumpGetMaxKeyLength($properties);
                foreach ($properties as $property) {
                    $name = $property->name;
                    $value = $source->$name;

                    $switch = is_array($value) || is_object($value);

                    $key = str_pad($name, $maxKeyLength, ' ');
                    echo $switch ? "+" : ' ';
                    echo "<i class='df-key" . ($switch ? "-closed" : '') . "'";
                    echo $switch ? " onclick=\"dfDumpToggle('df-" . self::$id . "')\"" : '';
                    echo ">$key</i> => ";
                    self::_dump($value);
                }
                echo "</div>";
            } else {
                self::$id++;
            }
            echo "}\n";
        }

        private static function _dumpArray($array)
        {
            $count = count($array);
            $onClick = $count ? "onclick=\"dfDumpToggle('df-" . self::$id . "')\"" : '';
            echo "<span class='df-switch' $onClick>Array[$count]</span> (";
            if ($count) {
                echo "<div class='df-cont" . (!self::EXPAND || self::$level > 1 ? "-closed" : '') . "' id='df-" . (self::$id++) . "'>";
                $maxKeyLength = self::_dumpGetMaxKeyLength(array_keys($array));
                foreach ($array as $key => $value) {
                    $switch = is_array($value) || is_object($value);

                    $key = str_pad($key, $maxKeyLength, ' ');
                    echo is_array($value) ? "+" : ' ';
                    echo "<i class='df-key" . ($switch ? "-closed" : '') . "'";
                    echo $switch ? " onclick=\"dfDumpToggle('df-" . self::$id . "')\"" : '';
                    echo ">$key</i> => ";
                    self::_dump($value);
                }
                echo "</div>";
                echo ")\n";
            } else {
                self::$id++;
                echo "<b>EMPTY</b>)\n";
            }
        }

        private static function _dumpGetMaxKeyLength($keys)
        {
            $max = 0;
            foreach ($keys as $key) {
                if (is_object($key)) {
                    $len = strlen((string)$key->name);
                } else {
                    $len = strlen((string)$key);
                }
                $max = ($len > $max) ? $len : $max;
            }

            return $max;
        }

        private static function _dumpStyles()
        {
            static $css;
            if (!$css) {
                $css = <<<STYLES
	<style>
		.df-dump {
			color: #ccc;
			font-size: 16px;
			background: #fefefe;
			padding: 10px 15px;
			border: 1px solid #ccc;
			border-radius: 5px;
			box-shadow: 0 0 5px -2px #777;
			white-space: pre;
			overflow: auto;
		}
		.df-dump::-webkit-scrollbar{width:10px;height:10px;}
		.df-dump::-webkit-scrollbar-track{background:rgba(0,0,0,0.1);}
		.df-dump::-webkit-scrollbar-thumb{background:rgba(0,0,0,0.2);}
		.df-dump-title {
			background: #eee;
			color: #000;
			margin: -10px -15px 5px;
			padding: 3px 15px;
			font-weight: bold;
		}
		.df-dump b { color: black; }
		.df-cont { /*background: rgba(255,0,0,0.1);*/ margin-left: 2em;}
		.df-cont-closed { display: none; /*background: rgba(255,0,0,0.1);*/ margin-left: 2em; }
		.df-switch { border-bottom: 1px dotted #777; cursor:pointer; /*background: rgba(0,255,0,0.1);*/}
		.df-key { color: #000; font-style: normal; }
		.df-key-closed { color: #000; font-style: normal; border-bottom: 1px dotted #777; cursor: pointer; }
	</style>
	<script>
		function dfDumpToggle(id){
			if (window.jQuery != undefined) {
				jQuery('#'+id).toggle();
			} else {
				var E = document.getElementById(id);
				E.style.display = (E.style.display == "block") ? "none" : "block";
			}
		}
	</script>
STYLES;
                echo $css;
            }
        }

        private static function printStylesheet()
        {
            include DIR_APP . '/admin/templates/debug_stylesheet.phtml';
        }
    }
}

// aliases

namespace {

    function vardump($var, $exit = false)
    {
        echo '<pre style="font-size: 16px">';
        var_dump($var);
        echo '</pre>';
        if ($exit) {
            exit();
        }
    }

    function pre($var, $title = '', $exit = 1)
    {
        if (defined('CLI_MODE')) {
            df\Dumper::cliPre($var, $title, $exit);
        } else {
            df\Dumper::pre($var, $title, $exit);
        }
    }

    function dump($source, $title = '', $exit = 1)
    {
        df\Dumper::dump($source, $title, $exit);
    }

    function dmp($source, $title = '', $exit = 1)
    {
        df\Dumper::dump($source, $title, $exit);
    }

}
