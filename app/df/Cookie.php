<?php

/**
 * @since           02.07.14 21:03
 * @package         DFramework
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df;

class Cookie
{
    /**
     * @param string $cookieName
     * @param string $data
     * @param null $period
     * @param string $path
     */
    public function set($cookieName, $data, $period = null, $path = '/')
    {
        $period = $period ? $period : 3600 * 24 * 7; // default - 7 days
        setcookie($cookieName, $data, time() + $period, $path, $_SERVER['SERVER_NAME'], false, true);
    }

    /**
     * @param $cookieName
     *
     * @return null|mixed
     */
    function get($cookieName)
    {
        return isset($_COOKIE[$cookieName]) ? $_COOKIE[$cookieName] : null;
    }

    /**
     * @param string $cookieName
     * @param string $path
     */
    function del($cookieName, $path = '/')
    {
        setcookie($cookieName, '', time() - 3600 * 25, $path, $_SERVER['SERVER_NAME'], false, true);
    }
}
