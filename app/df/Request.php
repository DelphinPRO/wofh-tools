<?php
/**
 * @since           16.07.14 18:33
 * @package         DFramework
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df;

use df\exception\ErrorApp;

/**
 * Class Request
 *
 * @property Input get
 * @property Input post
 * @package df
 */
class Request
{
    private $rgData;

    public function __construct()
    {

    }

    /**
     * @param $name
     *
     * @return Input
     * @throws exception\ErrorApp
     */
    public function __get($name)
    {
        if ($name == 'get') {
            if (!isset($this->rgData['GET'])) {
                $this->rgData['GET'] = new Input($_GET);
            }
            return $this->rgData['GET'];
        }
        if ($name == 'post') {
            if (!isset($this->rgData['POST'])) {
                $this->rgData['POST'] = new Input($_POST);
            }
            return $this->rgData['POST'];
        }
        throw new ErrorApp('Unknown input array: $_' . strtoupper($name));
    }
}
