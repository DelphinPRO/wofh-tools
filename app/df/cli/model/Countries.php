<?php
/**
 * @since              22.12.13 01:17
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\cli\model;

use df\extensions\models\Wofh;

class Countries extends UpdaterCommon
{
    const TITLE = 0;
    const FLAG = 1;
    const DIPLOMACY = 2;

    const POP = 100;
    const ACCOUNTS = 101;
    const TOWNS = 102;

    const SCIENCE = 103;
    const PRODUCTION = 104;
    const WAR = 105;

    const EXTRA_CREATED = 'create';
    const EXTRA_DELETED = 'delete';
    const EXTRA_NAMES = 'names';
    const EXTRA_FLAGS = 'flags';

    //=======================================================
    //   Public methods
    //=======================================================

    public function getDeletedCountries()
    {
        return $this->db
            ->query(q("SELECT countryId FROM z_ruXX_countries WHERE active = 0", $this->world->lowerSign))
            ->fetchByField('countryId');
    }

    /**
     * @param DataCountry $previous
     * @param DataCountry $current
     */
    public function _checkCountry($previous, $current)
    {
        if ($this->events->isCountryCreate($previous, $current)) {
            $current->extra[Countries::EXTRA_CREATED] = $this->currentTimestamp;
            $current->extra[Countries::EXTRA_NAMES][$this->currentTimestamp] = $current->title;
            $current->extra[Countries::EXTRA_FLAGS][$this->currentTimestamp] = $current->flag;
            $this->add($current);
            $this->addDiplomacy($this->currentDate, $current);
            $this->addData($this->currentDate, $current,
                $current->population, $current->accounts, $current->towns,
                $current->science, $current->production, $current->war);
            $this->events->add($this->data->createEvent(Wofh::EVENT_COUNTRY_CREATE, $previous, $current));
        } elseif ($this->events->isCountryDestroy($previous, $current)) {
            $deltaPop = 0 - $previous->population;
            $deltaAccounts = 0 - $previous->accounts;
            $deltaTowns = 0 - $previous->towns;
            $deltaScience = 0 - $previous->science;
            $deltaProduction = 0 - $previous->production;
            $deltaWar = 0 - $previous->war;

            $previous->active = 0;
            $previous->population = 0;
            $previous->accounts = 0;
            $previous->towns = 0;
            $previous->science = 0;
            $previous->production = 0;
            $previous->war = 0;

            $previous->extra[Countries::EXTRA_DELETED] = $this->currentTimestamp;
            $this->add($previous);
            $this->addData($this->currentDate, $previous, $deltaPop, $deltaAccounts,
                $deltaTowns, $deltaScience, $deltaProduction, $deltaWar);
            $this->events->add($this->data->createEvent(Wofh::EVENT_COUNTRY_DESTROY, $previous, $current));
        } else {
            $updateRequired = false;
            $current->extra = $previous->extra;
            $deltaPop = $current->population - $previous->population;
            $deltaAccounts = $current->accounts - $previous->accounts;
            $deltaTowns = $current->towns - $previous->towns;
            $deltaScience = $current->science - $previous->science;
            $deltaProduction = $current->production - $previous->production;
            $deltaWar = $current->war - $previous->war;

            if ($this->events->isCountryFlag($previous, $current)) {
                $updateRequired = true;
                $current->extra[Countries::EXTRA_FLAGS][$this->currentTimestamp] = $current->flag;
                $this->events->add($this->data->createEvent(Wofh::EVENT_COUNTRY_FLAG, $previous, $current));
            }

            if ($this->events->isCountryRename($previous, $current)) {
                $updateRequired = true;
                $current->extra[Countries::EXTRA_NAMES][$this->currentTimestamp] = $current->title;
                $this->events->add($this->data->createEvent(Wofh::EVENT_COUNTRY_RENAME, $previous, $current));
            }

            if ($updateRequired) {
                $this->add($current);
            }
            $this->addDiplomacy($this->currentDate, $current);
            $this->addData($this->currentDate, $current, $deltaPop, $deltaAccounts,
                $deltaTowns, $deltaScience, $deltaProduction, $deltaWar);
        }
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function updateItems()
    {
        if (count($this->items)) {
//			$this->db->beginTransaction();
            $query = q("REPLACE INTO z_ruXX_countries"
                . " (`countryId`, `countryTitle`, `countryFlag`,"
                . " `active`, `extra`"
                . " ) VALUES ", $this->world->lowerSign);
            $query .= join(",", $this->items) . ';';
            $this->db->query($query);
//			$this->db->commit();
        }

    }

    protected function insertItemsData()
    {
        if (count($this->dataItems)) {
            $query = q("REPLACE INTO z_ruXX_countries_stat"
                . " (`stateDate`, `countryId`,"
                . " `pop`, `accounts`, `towns`, `science`, `production`, `war`,"
                . " `deltaPop`,"
                . " `deltaAccounts`,"
                . " `deltaTowns`,"
                . " `deltaScience`,"
                . " `deltaProduction`,"
                . " `deltaWar`"
                . ") VALUES ", $this->world->lowerSign);
            $query .= join(",", $this->dataItems) . ';';
            $this->db->query($query);
        }
    }

    protected function insertDiplomacyData()
    {
        if (count($this->diplomacy)) {
            $chunks = array_chunk($this->diplomacy, $this->chunkSize);

            foreach ($chunks as $chunk) {
                $query = q("INSERT IGNORE INTO z_ruXX_countries_diplomacy"
                    . " (`stateDate`, `id1`, `id2`, `status`"
                    . ") VALUES ", $this->world->lowerSign);
                $query .= join(",", $chunk) . ';';
                $this->db->query($query);
            }
        }
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function add(DataCountry $country)
    {
        $this->items[] = "("
            . "  " . $country->id
            . ", " . $this->db->e($country->title) . ""
            . ", " . $this->db->e($country->flag) . ""
            . ",'" . $country->active . "'"
            . ", " . $this->db->e(json_encode($country->extra)) . ""
            . ")";

    }

    private function addDiplomacy($date, DataCountry $country)
    {
        foreach ($country->diplomacy as $id => $status) {
            $this->diplomacy[] = "("
                . " '" . $date . "'"
                . ", " . $country->id
                . ", " . abs((int)$id) . ""
                . ", " . abs((int)$status) . ""
                . ")";
        }
//		var_dump($country);
//		exit;
    }

    private function addData(
        $date,
        DataCountry $country,
        $deltaPop,
        $deltaAccounts,
        $deltaTowns,
        $deltaScience,
        $deltaProduction,
        $deltaWar
    ) {
        $this->dataItems[] = "("
            . " '" . $date . "'"
            . ", " . $country->id
            . ",'" . $country->population . "'"
            . ",'" . $country->accounts . "'"
            . ",'" . $country->towns . "'"
            . ",'" . $country->science . "'"
            . ",'" . $country->production . "'"
            . ",'" . $country->war . "'"
            . ",'" . $deltaPop . "'"
            . ",'" . $deltaAccounts . "'"
            . ",'" . $deltaTowns . "'"
            . ",'" . $deltaScience . "'"
            . ",'" . $deltaProduction . "'"
            . ",'" . $deltaWar . "'"
            . ")";
    }

}
