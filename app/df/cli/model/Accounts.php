<?php
/**
 * @since              22.12.13 02:07
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */
namespace df\cli\model;

use df\extensions\models\Wofh;

class Accounts extends UpdaterCommon
{
    const TITLE = 0;
    const RACE = 1;
    const SEX = 2;
    const COUNTRY_ID = 3;
    const RATING_ATTACK = 4;
    const RATING_DEFENCE = 5;
    const RATING_SCIENCE_V1_4 = 6;
    const RATING_PRODUCTION_V1_4 = 7;
    const ROLE_V1_4 = 8;

    // old
    const RATING_SCIENCE = 5;
    const RATING_PRODUCTION = 6;
    const ROLE = 7;


    const POP = 100;
    const TOWNS = 101;

    const EXTRA_CREATED = 'create';
    const EXTRA_DELETED = 'delete';
    const EXTRA_NAMES = 'names';

    //=======================================================
    //   Public methods
    //=======================================================

    public function getDeletedAccounts()
    {
        return $this->db
            ->query(q("SELECT accountId FROM z_ruXX_accounts WHERE active = 0", $this->world->lowerSign))
            ->fetchByField('accountId');
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     */
    public function _checkAccount($previous, $current)
    {
        if ($this->events->isAccountCreate($previous, $current)) {
            $current->extra[Accounts::EXTRA_CREATED] = $this->currentTimestamp;
            $current->extra[Accounts::EXTRA_NAMES][$this->currentTimestamp] = $current->name;
            $this->add($current);
            $this->addData($this->currentDate, $current,
                $current->population, $current->towns,
                $current->science, $current->production, $current->war);

            $this->events->add($this->data->createEvent(Wofh::EVENT_ACCOUNT_CREATE, $previous, $current));
        } elseif ($this->events->isAccountDelete($previous, $current)) {
            $deltaPop = 0 - $previous->population;
            $deltaTowns = 0 - $previous->towns;
            $deltaScience = 0 - $previous->science;
            $deltaProduction = 0 - $previous->production;
            $deltaWar = 0 - $previous->war;

            $previous->active = 0;
            $previous->role = 0;
            $previous->countryId = 0;
            $previous->population = 0;
            $previous->towns = 0;
            $previous->science = 0;
            $previous->production = 0;
            $previous->war = 0;

            $previous->extra[Accounts::EXTRA_DELETED] = $this->currentTimestamp;
            $this->add($previous);
            $this->addData($this->currentDate, $previous, $deltaPop, $deltaTowns,
                $deltaScience, $deltaProduction, $deltaWar);

            $this->events->add($this->data->createEvent(Wofh::EVENT_ACCOUNT_DELETE, $previous, $current));
        } else {
            $updateRequired = false;
            $current->extra = $previous->extra;
            $deltaPop = $current->population - $previous->population;
            $deltaTowns = $current->towns - $previous->towns;
            $deltaScience = $current->science - $previous->science;
            $deltaProduction = $current->production - $previous->production;
            $deltaWar = $current->war - $previous->war;

//            if ($this->events->isAccountChangeRole($previous, $current)) {
//                $updateRequired = true;
//                $roles = array(1, 2, 4, 8, 16, 32, 64, 128);
//                foreach ($roles as $role) {
//                    $cRole = $current->role & $role;
//                    $pRole = $previous->role & $role;
//                    if (!$pRole && $cRole) {
//                        $e = $this->data->createEvent(Wofh::EVENT_ACCOUNT_ROLE_IN, $previous, $current);
//                        $e['role'] = $cRole;
//                        $this->events->add($e);
//                    }
//                    if ($pRole && !$cRole) {
//                        $e = $this->data->createEvent(Wofh::EVENT_ACCOUNT_ROLE_OUT, $previous, $current);
//                        $e['role'] = $pRole;
//                        $this->events->add($e);
//                    }
//                }
//            }

            if ($this->events->isAccountChangeSex($previous, $current)) {
                $updateRequired = true;
            }

            if ($this->events->isAccountRename($previous, $current)) {
                $current->extra[Accounts::EXTRA_NAMES][$this->currentTimestamp] = $current->name;
                $updateRequired = true;
                $this->events->add($this->data->createEvent(Wofh::EVENT_ACCOUNT_RENAME, $previous, $current));
            }

            if ($this->events->isAccountCountryIn($previous, $current)) {
                $updateRequired = true;
                $this->events->add($this->data->createEvent(Wofh::EVENT_ACCOUNT_COUNTRY_IN, $previous, $current));
            }

            if ($this->events->isAccountCountryOut($previous, $current)) {
                $updateRequired = true;
                $this->events->add($this->data->createEvent(Wofh::EVENT_ACCOUNT_COUNTRY_OUT, $previous, $current));
            }

            if ($this->events->isAccountCountryChange($previous, $current)) {
                $updateRequired = true;
                $this->events->add($this->data->createEvent(Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE, $previous, $current));
            }

            if ($updateRequired) {
                $this->add($current);
            }
            $this->addData($this->currentDate, $current, $deltaPop, $deltaTowns,
                $deltaScience, $deltaProduction, $deltaWar);
        }
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function updateItems()
    {
        if (count($this->items)) {
            $chunks = array_chunk($this->items, $this->chunkSize);

            foreach ($chunks as $chunk) {
                $query = q("REPLACE INTO z_ruXX_accounts"
                    . " (`accountId`, `accountName`, `accountRace`, `accountSex`,"
                    . " `countryId`, `role`, `active`, `extra`)"
                    . " VALUES ", $this->world->lowerSign);
                $query .= join(",", $chunk) . ';';
                $this->db->query($query);
            }
        }
    }

    protected function insertItemsData()
    {
        if (count($this->dataItems)) {
            $chunks = array_chunk($this->dataItems, $this->chunkSize);

            foreach ($chunks as $chunk) {
                $query = q("INSERT INTO z_ruXX_accounts_stat"
                    . " (`stateDate`, `accountId`, `countryId`, `role`,"
                    . " `pop`, `towns`, `science`, `production`, `war`,"
                    . " `deltaPop`,"
                    . " `deltaTowns`,"
                    . " `deltaScience`,"
                    . " `deltaProduction`,"
                    . " `deltaWar`"
                    . ") VALUES ", $this->world->lowerSign);
                $query .= join(",", $chunk) . ';';
                $this->db->query($query);
            }
        }
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function add(DataAccount $account)
    {
        $this->items[] = "("
            . "  " . $account->id
            . ", " . $this->db->e($account->name) . ""
            . ",'" . $account->race . "'"
            . ",'" . $account->sex . "'"
            . ",'" . $account->countryId . "'"
            . ",'" . $account->role . "'"
            . ",'" . $account->active . "'"
            . ", " . $this->db->e(json_encode($account->extra)) . ""
            . ")";
    }

    private function addData(
        $date,
        DataAccount $account,
        $deltaPop,
        $deltaTowns,
        $deltaScience,
        $deltaProduction,
        $deltaWar
    ) {
        $this->dataItems[] = "("
            . " '" . $date . "'"
            . ", " . $account->id
            . ",'" . $account->countryId . "'"
            . ",'" . $account->role . "'"
            . ",'" . $account->population . "'"
            . ",'" . $account->towns . "'"
            . ",'" . $account->science . "'"
            . ",'" . $account->production . "'"
            . ",'" . $account->war . "'"
            . ",'" . $deltaPop . "'"
            . ",'" . $deltaTowns . "'"
            . ",'" . $deltaScience . "'"
            . ",'" . $deltaProduction . "'"
            . ",'" . $deltaWar . "'"
            . ")";
    }

    protected function insertDiplomacyData()
    {
        // TODO: Implement insertDiplomacyData() method.
    }
}
