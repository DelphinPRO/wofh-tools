<?php
/**
 * @since             27.02.14 19:45
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\cli\model;

use df\Console;
use df\exception\CliException;
use df\extensions\models\World;
use df\Model;

abstract class UpdaterCommon extends Model
{
    /** @var  World */
    protected $world;

    /** @var  DataStatistic */
    protected $data;

    /** @var  Events */
    protected $events;

    protected $items;
    protected $dataItems;
    protected $diplomacy;

    protected $currentDate;
    protected $currentTimestamp;

    protected $chunkSize = 1000;

    protected $tableName;

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct(World $world, DataStatistic $data, Events $events, $key)
    {
        $this->world = $world;
        $this->data = $data;
        $this->events = $events;
        $this->currentTimestamp = $data->getTime();
        $this->currentDate = $data->getTime(STD_DATE);
        $this->tableName = $key;

        $this->items = array();
        $this->dataItems = array();
        $this->diplomacy = array();
    }

    final public function update()
    {
        $start = microtime(true);

        Console::log("Updating {$this->tableName} info ...", false);

        $this->readPreviousIndex();
        $this->calculate();
        $this->updateItems();
        $this->insertItemsData();
        $this->insertDiplomacyData();

        Console::log(' : ' . round(microtime(true) - $start, 1) . " s");
//		Console::log('                    time : ' . round(microtime(true) - $start, 1) . " s");
    }

    final protected function readPreviousIndex()
    {
        $ids = array(
            'towns' => 'townId',
            'accounts' => 'accountId',
            'countries' => 'countryId',
        );

//		$startTime = microtime(true);

        $lastDate = $this->getRealLastUpdate($this->tableName);

        if ($lastDate == $this->currentDate) {
            throw new CliException(
                'Duplicate date: ' . $this->currentDate . ' ' . $this->tableName,
                CliException::ERROR_CODE_DUPLICATE_DATE);
        }

        if ($lastDate === null) {
//			Console::log("  read previous data ..... no data");
            return;
        }

        $sql = loadSql("select_previous_{$this->tableName}_data", $this->world->sign);
        $this->db->query($sql, array('lastDate' => $lastDate));
        $this->data->setPreviousData($this->tableName, $this->db->fetchByField($ids[$this->tableName]));

//		$endTime = round(microtime(true) - $startTime, 1);
//		Console::log("  read previous data ..... " . $endTime);
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    final protected function calculate()
    {
        $functions = array(
            'towns' => '_checkTown',
            'accounts' => '_checkAccount',
            'countries' => '_checkCountry',
        );

//		$startTime = microtime(true);

        $this->data->each($this->tableName, array($this, $functions[$this->tableName]));

//		$endTime = round(microtime(true) - $startTime, 1);
//		Console::log("  calculate data ......... " . $endTime);
    }

    abstract protected function updateItems();

    abstract protected function insertItemsData();

    abstract protected function insertDiplomacyData();

    protected function getRealLastUpdate($table)
    {
        $sql = "SELECT " . "MAX(stateDate) AS lastDate FROM z_ruXX_" . $table . "_stat";
        return $this->db->query(q($sql, $this->world->lowerSign))->fetchObject()->lastDate;
    }

    //=======================================================
    //   Private methods
    //=======================================================

}
