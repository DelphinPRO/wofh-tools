<?php
/**
 * @since             12.11.2014 00:29
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\cli\model;

use df\Console;
use df\exception\CliException;
use df\extensions\models\Wofh;
use df\extensions\models\World;
use df\Registry;

class DataStatistic
{
    public $sign;

    private $_raw;

    private $_curr;
    private $_prev;

    /** @var World */
    private $_world;

    //=======================================================
    //   Public methods
    //=======================================================

    public function __construct($jsonString, World $world)
    {
        $this->_world = $world;

        if (!is_string($jsonString)) {
            throw new CliException('Invalid parameter');
        }

        $this->_raw = json_decode($jsonString, true);
        if ((json_last_error() != JSON_ERROR_NONE) || is_null($this->_raw)) {
            Console::log('Error. Can\'t decode read data');
            throw new CliException('Error. Can\'t decode read data');
        }

        $this->_curr = array(
            'towns' => array(),
            'accounts' => array(),
            'countries' => array(),
        );

        $this->_prev = array(
            'towns' => array(),
            'accounts' => array(),
            'countries' => array(),
        );
    }

    public function __destruct()
    {
        unset($this->_raw);
    }

    public function checkStruct()
    {
        Console::log("Check structure .......... OK");
    }

    public function getTime($format = null)
    {
        if (is_string($format)) {
            return date($format, $this->_raw['time']);
        }
        return $this->_raw['time'];
    }

    public function getTownName($id)
    {
        if (array_key_exists($id, $this->_raw['towns'])) {
            return $this->_raw['towns'][$id][Towns::TITLE];
        }
        if (array_key_exists($id, $this->_prev['towns'])) {
            return $this->_prev['towns'][$id]['townTitle'];
        }
        return null;
    }

    public function getAccount($id)
    {
        if (array_key_exists($id, $this->_raw['accounts'])) {
            return array(
                'name' => $this->_raw['accounts'][$id][Accounts::TITLE],
                'sex' => $this->_raw['accounts'][$id][Accounts::SEX],
            );
        }
        return array('name' => '', 'sex' => '');
    }

    public function getCountry($accountId)
    {
        if (array_key_exists($accountId, $this->_raw['accounts'])) {
            $countryId = $this->_raw['accounts'][$accountId][Accounts::COUNTRY_ID];
            if (array_key_exists($countryId, $this->_raw['countries'])) {
                return array(
                    'id' => $countryId,
                    'name' => $this->_raw['countries'][$countryId][Countries::TITLE],
                    'flag' => $this->_raw['countries'][$countryId][Countries::FLAG],
                );
            }
        }
        return array('id' => 0, 'name' => '', 'flag' => '');
    }

    public function getCount($key)
    {
        return count($this->_curr[$key]);
    }

    public function getCountRaw($key)
    {
        return count($this->_raw[$key]);
    }

    public static function getWonderName($id, $worldId)
    {
        static $names;
        if (!$names) {
            $names = Registry::getDb()->query("SELECT buildId, title FROM cd_builds WHERE worldId = 1016")->fetchByField('buildId');
        }
        return isset($names[$id]) ? $names[$id]['title'] : $id;
    }

    /**
     * @param int $eventId
     * @param DataTown|DataAccount|DataCountry $previous
     * @param DataTown|DataAccount|DataCountry $current
     *
     * @return array|null
     */
    public function createEvent($eventId, $previous, $current)
    {
        $e = array(
            'eventId' => $eventId,
            'townId' => 0,
            'accountId' => 0,
            'countryId' => 0,
            'countryIdFrom' => 0,
            'role' => 0,
            'extra' => array(
                'townName' => '',
                'accountName' => '',
                'accountSex' => 0,
                'countryName' => '',
                'countryFlag' => '',
                'wonderId' => 0,
                'wonderLevel' => 0,
                'wonderName' => '',
            ),
        );
        switch ($eventId) {
            case Wofh::EVENT_TOWN_CREATE:
                $e['townId'] = $current->id;
                $e['accountId'] = $current->accountId;

                $e['extra']['townName'] = $current->title;

                $e = $this->getExtraData($e, $current->accountId);
                break;

            case Wofh::EVENT_TOWN_DESTROY:
                $e['townId'] = $previous->id;
                $e['accountId'] = $previous->accountId;

                $e['extra']['townName'] = $previous->title;

                $e = $this->getExtraData($e, $previous->accountId);
                break;

            case Wofh::EVENT_TOWN_LOST:
                $e['townId'] = $current->id;
                $e['accountId'] = $current->accountId;

                $e['extra']['townName'] = $current->title;

                $e = $this->getExtraData($e, $current->accountId);
                break;

            case Wofh::EVENT_TOWN_RENAME:
                $e['townId'] = $current->id;
                $e['accountId'] = $current->accountId;

                $e['extra']['townName'] = $current->title;
                $e['extra']['townNameFrom'] = $previous->title;

                $e = $this->getExtraData($e, $current->accountId);
                break;

            case Wofh::EVENT_WONDER_DESTROY:
                $e['townId'] = $current->id;
                $e['accountId'] = $current->accountId;

                $e['extra']['townName'] = $current->title;
                $e['extra']['wonderId'] = $previous->wonderId;
                $e['extra']['wonderLevel'] = $previous->wonderLevel;
                $e['extra']['wonderName'] = self::getWonderName($previous->wonderId, 1016);

                $e = $this->getExtraData($e, $current->accountId);
                break;

            case Wofh::EVENT_WONDER_CREATE:
                $e['townId'] = $current->id;
                $e['accountId'] = $current->accountId;

                $e['extra']['townName'] = $current->title;
                $e['extra']['wonderId'] = $current->wonderId;
                $e['extra']['wonderLevel'] = $current->wonderLevel;
                $e['extra']['wonderName'] = self::getWonderName($current->wonderId, 1016);

                $e = $this->getExtraData($e, $current->accountId);
                break;

            case Wofh::EVENT_WONDER_ACTIVATE:
                $e['townId'] = $current->id;
                $e['accountId'] = $current->accountId;

                $e['extra']['townName'] = $current->title;
                $e['extra']['wonderId'] = $current->wonderId;
                $e['extra']['wonderLevel'] = $current->wonderLevel;
                $e['extra']['wonderName'] = self::getWonderName($current->wonderId, 1016);

                $e = $this->getExtraData($e, $current->accountId);
                break;

            case Wofh::EVENT_ACCOUNT_CREATE:
                $e['accountId'] = $current->id;

                $e = $this->getExtraData($e, $current->id);

                $e['extra']['accountName'] = $current->name;
                $e['extra']['accountSex'] = $current->sex;
                break;

            case Wofh::EVENT_ACCOUNT_DELETE:
                $e['accountId'] = $previous->id;

                $e = $this->getExtraData($e, $previous->id);

                $e['extra']['accountName'] = $previous->name;
                $e['extra']['accountSex'] = $previous->sex;
                break;

            case Wofh::EVENT_ACCOUNT_RENAME:
                $e['accountId'] = $current->id;

                $e = $this->getExtraData($e, $current->id);

                $e['extra']['accountName'] = $current->name;
                $e['extra']['accountFrom'] = $previous->name;
                $e['extra']['accountSex'] = $current->sex;
                break;

            case Wofh::EVENT_ACCOUNT_COUNTRY_IN:
                $e['accountId'] = $current->id;

                $e = $this->getExtraData($e, $current->id);

                $e['extra']['accountName'] = $current->name;
                $e['extra']['accountSex'] = $current->sex;
                break;

            case Wofh::EVENT_ACCOUNT_COUNTRY_OUT:
                $e['accountId'] = $current->id;
                $e['countryId'] = $previous->countryId;

                $e['extra']['accountName'] = $current->name;
                $e['extra']['accountSex'] = $current->sex;

                if (isset($this->_raw['countries'][$previous->countryId]) && $this->_raw['countries'][$previous->countryId] > 0) {
                    $e['extra']['countryName'] = $this->_raw['countries'][$previous->countryId][Countries::TITLE];
                    $e['extra']['countryFlag'] = $this->_raw['countries'][$previous->countryId][Countries::FLAG];
                } elseif (isset($this->_prev['countries'][$previous->countryId]) && $this->_prev['countries'][$previous->countryId] > 0) {
                    $e['extra']['countryName'] = $this->_prev['countries'][$previous->countryId]['countryTitle'];
                    $e['extra']['countryFlag'] = $this->_prev['countries'][$previous->countryId]['countryFlag'];
                } else {
                    $db = Registry::getDb();
                    $db->query(q("SELECT * FROM z_ruXX_countries WHERE countryId = :id", $this->sign), array(
                        'id' => $previous->countryId,
                    ));
                    $d = $db->fetch();
                    $e['extra']['countryName'] = $d['countryTitle'];
                    $e['extra']['countryFlag'] = $d['countryFlag'];
                }
                break;

            case Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE:
                $e['accountId'] = $current->id;
                $e['countryId'] = $current->countryId;
                $e['countryIdFrom'] = $previous->countryId;

                $e['extra']['accountName'] = $current->name;
                $e['extra']['accountSex'] = $current->sex;

                if (isset($this->_raw['countries'][$current->countryId]) && $this->_raw['countries'][$current->countryId] > 0) {
                    $e['extra']['countryName'] = $this->_raw['countries'][$current->countryId][Countries::TITLE];
                    $e['extra']['countryFlag'] = $this->_raw['countries'][$current->countryId][Countries::FLAG];
                } elseif (isset($this->_prev['countries'][$current->countryId]) && $this->_prev['countries'][$current->countryId] > 0) {
                    $e['extra']['countryName'] = $this->_prev['countries'][$current->countryId]['countryTitle'];
                    $e['extra']['countryFlag'] = $this->_prev['countries'][$current->countryId]['countryFlag'];
                } else {
                    $db = Registry::getDb();
                    $db->query(q("SELECT * FROM z_ruXX_countries WHERE countryId = :id", $this->sign), array(
                        'id' => $previous->countryId,
                    ));
                    $d = $db->fetch();
                    $e['extra']['countryName'] = $d['countryTitle'];
                    $e['extra']['countryFlag'] = $d['countryFlag'];
                }

                if (isset($this->_raw['countries'][$previous->countryId]) && $this->_raw['countries'][$previous->countryId] > 0) {
                    $e['extra']['countryNameFrom'] = $this->_raw['countries'][$previous->countryId][Countries::TITLE];
                    $e['extra']['countryFlagFrom'] = $this->_raw['countries'][$previous->countryId][Countries::FLAG];
                } elseif (isset($this->_prev['countries'][$previous->countryId]) && $this->_prev['countries'][$previous->countryId] > 0) {
                    $e['extra']['countryNameFrom'] = $this->_prev['countries'][$previous->countryId]['countryTitle'];
                    $e['extra']['countryFlagFrom'] = $this->_prev['countries'][$previous->countryId]['countryFlag'];
                } else {
                    $db = Registry::getDb();
                    $db->query(q("SELECT * FROM z_ruXX_countries WHERE countryId = :id", $this->sign), array(
                        'id' => $previous->countryId,
                    ));
                    $d = $db->fetch();
                    $e['extra']['countryNameFrom'] = $d['countryTitle'];
                    $e['extra']['countryFlagFrom'] = $d['countryFlag'];
                }
                break;

            case Wofh::EVENT_ACCOUNT_ROLE_IN:
            case Wofh::EVENT_ACCOUNT_ROLE_OUT:
                $e['accountId'] = $current->id;

                $e = $this->getExtraData($e, $current->id);

                $e['extra']['accountName'] = $current->name;
                $e['extra']['accountSex'] = $current->sex;

                break;

            case Wofh::EVENT_COUNTRY_CREATE:
                $e['countryId'] = $current->id;

                $e['extra']['countryName'] = $current->title;
                $e['extra']['countryFlag'] = $current->flag;
                break;

            case Wofh::EVENT_COUNTRY_DESTROY:
                $e['countryId'] = $previous->id;

                $e['extra']['countryName'] = $previous->title;
                $e['extra']['countryFlag'] = $previous->flag;
                break;

            case Wofh::EVENT_COUNTRY_FLAG:
                $e['countryId'] = $current->id;

                $e['extra']['countryName'] = $current->title;
                $e['extra']['countryFlag'] = $current->flag;
                $e['extra']['countryFlagFrom'] = $previous->flag;
                break;

            case Wofh::EVENT_COUNTRY_RENAME:
                $e['countryId'] = $current->id;

                $e['extra']['countryName'] = $current->title;
                $e['extra']['countryNameFrom'] = $previous->title;
                $e['extra']['countryFlag'] = $current->flag;
                break;

            default:
                Console::log('Unknown event: ' . getNameEvent($eventId));
                return null;
        }
        return $e;
    }

    private function getExtraData($e, $accountId)
    {
        if (isset($this->_raw['accounts'][$accountId])) {
            $e['extra']['accountName'] = $this->_raw['accounts'][$accountId][Accounts::TITLE];
            $e['extra']['accountSex'] = $this->_raw['accounts'][$accountId][Accounts::SEX];

            $countryId = $this->_raw['accounts'][$accountId][Accounts::COUNTRY_ID];
            if (isset($this->_raw['countries'][$countryId]) && $this->_raw['countries'][$countryId] > 0) {
                $e['countryId'] = $countryId;
                $e['extra']['countryName'] = $this->_raw['countries'][$countryId][Countries::TITLE];
                $e['extra']['countryFlag'] = $this->_raw['countries'][$countryId][Countries::FLAG];
            } elseif (isset($this->_prev['countries'][$countryId]) && $this->_prev['countries'][$countryId] > 0) {
                $e['countryId'] = $countryId;
                $e['extra']['countryName'] = $this->_prev['countries'][$countryId]['countryTitle'];
                $e['extra']['countryFlag'] = $this->_prev['countries'][$countryId]['countryFlag'];
            } else {
                if (isset($this->_prev['accounts'][$accountId])) {
                    $countryId = $this->_prev['accounts'][$accountId]['countryId'];
                    if (isset($this->_prev['countries'][$countryId]) && $this->_prev['countries'][$countryId] > 0) {
                        $e['countryId'] = $countryId;
                        $e['extra']['countryName'] = $this->_prev['countries'][$countryId]['countryTitle'];
                        $e['extra']['countryFlag'] = $this->_prev['countries'][$countryId]['countryFlag'];
                    }
                }
            }

        }
        return $e;
    }

    public function normalize()
    {
        Console::log("Normalize source data ...... ");

        $this->_normalizeTowns();
        $this->_normalizeAccounts();
        $this->_normalizeCountries();

        $this->_calcTowns();
        $this->_calcAccounts();

        $countCountriesBefore = count($this->_curr['countries']);
        Console::log("  Countries .............. " . $countCountriesBefore . '/' . count($this->_curr['countries']));
    }

    public function setPreviousData($key, array $previousData)
    {
        $this->_prev[$key] = $previousData;
    }

    public function each($key, $callback)
    {
        $classes = array(
            'towns' => 'DataTown',
            'accounts' => 'DataAccount',
            'countries' => 'DataCountry',
        );
        $Classname = __NAMESPACE__ . '\\' . $classes[$key];

        ksort($this->_prev[$key]);
        ksort($this->_curr[$key]);

        end($this->_prev[$key]);
        end($this->_curr[$key]);
        $max = max(key($this->_prev[$key]), key($this->_curr[$key])) + 1;

        for ($i = 0; $i < $max; $i++) {
            $previousExists = array_key_exists($i, $this->_prev[$key]);
            $currentExists = array_key_exists($i, $this->_curr[$key]);
            if ($previousExists || $currentExists) {
                $previous = $previousExists ? new $Classname($i, $this->_prev[$key][$i], true) : null;
                $current = $currentExists ? new $Classname($i, $this->_curr[$key][$i]) : null;

                call_user_func($callback, $previous, $current);
                unset($previous, $current);
            }
        }
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function _calcTowns()
    {
        $countTownsBefore = count($this->_curr['towns']);
        $this->_curr['towns'] = array_filter($this->_curr['towns'], function ($item) {
            return $item[Towns::POP] > 0;
        });

        foreach ($this->_curr['towns'] as &$town) {
            $accountId = $town[Towns::ACCOUNT_ID];

            if (intval($accountId) > 0) {
                $this->_curr['accounts'][$accountId][Accounts::POP] += $town[Towns::POP];
                $this->_curr['accounts'][$accountId][Accounts::TOWNS] += 1;
            }
        }

        Console::log("  Towns .................. " . $countTownsBefore . '/' . count($this->_curr['towns']));
    }

    private function _calcAccounts()
    {
        $countAccountsBefore = count($this->_curr['accounts']);
        $this->_curr['accounts'] = array_filter($this->_curr['accounts'], function ($item) {
            return $item[Accounts::POP] > 0;
        });

        foreach ($this->_curr['accounts'] as &$account) {
            $countryId = $account[Accounts::COUNTRY_ID];

            if (intval($countryId) > 0) {
                $this->_curr['countries'][$countryId][Countries::POP] += $account[Accounts::POP];
                $this->_curr['countries'][$countryId][Countries::TOWNS] += $account[Accounts::TOWNS];
                $this->_curr['countries'][$countryId][Countries::ACCOUNTS] += 1;

                $this->_curr['countries'][$countryId][Countries::PRODUCTION] += $account[Accounts::RATING_PRODUCTION_V1_4];
                $this->_curr['countries'][$countryId][Countries::SCIENCE] += $account[Accounts::RATING_SCIENCE_V1_4];
                $this->_curr['countries'][$countryId][Countries::WAR] += $account[Accounts::RATING_ATTACK];
            }
        }

        Console::log("  Accounts ............... " . $countAccountsBefore . '/' . count($this->_curr['accounts']));
    }

    private function _normalizeTowns()
    {
        foreach ($this->_raw['towns'] as $id => $town) {
            $wonder = array_key_exists(Towns::WONDER, $town) ? $town[Towns::WONDER] : 0;

            $this->_curr['towns'][$id] = array(
                Towns::TITLE => $town[Towns::TITLE],
                Towns::ACCOUNT_ID => $town[Towns::ACCOUNT_ID],
                Towns::POP => $town[Towns::POP],
                Towns::WONDER => $wonder,
                Towns::WONDER_ID => $wonder % 1000,
                Towns::WONDER_LEVEL => (int)floor($wonder / 1000),
            );
        }
    }

    private function _normalizeAccounts()
    {
        foreach ($this->_raw['accounts'] as $id => $account) {
            // TODO add field to World class - "version"
            $ratingAttack = array_key_exists(Accounts::RATING_ATTACK, $account) ? $account[Accounts::RATING_ATTACK] : 0;
            if ($this->_world->id >= 1023 && $this->_world->id < 2000) {
                $ratingDefence = array_key_exists(Accounts::RATING_DEFENCE,
                    $account) ? $account[Accounts::RATING_DEFENCE] : 0;
                $ratingScience = array_key_exists(Accounts::RATING_SCIENCE_V1_4,
                    $account) ? $account[Accounts::RATING_SCIENCE_V1_4] : 0;
                $ratingProduction = array_key_exists(Accounts::RATING_PRODUCTION_V1_4,
                    $account) ? $account[Accounts::RATING_PRODUCTION_V1_4] : 0;
                $role = array_key_exists(Accounts::ROLE_V1_4, $account) ? $account[Accounts::ROLE_V1_4] : 0;
            } else {
                $ratingDefence = 0;
                $ratingScience = array_key_exists(Accounts::RATING_SCIENCE,
                    $account) ? $account[Accounts::RATING_SCIENCE] : 0;
                $ratingProduction = array_key_exists(Accounts::RATING_PRODUCTION,
                    $account) ? $account[Accounts::RATING_PRODUCTION] : 0;
                $role = array_key_exists(Accounts::ROLE, $account) ? $account[Accounts::ROLE] : 0;
            }

            $this->_curr['accounts'][$id] = array(
                Accounts::TITLE => $account[Accounts::TITLE],
                Accounts::RACE => $account[Accounts::RACE],
                Accounts::SEX => $account[Accounts::SEX],
                Accounts::COUNTRY_ID => $account[Accounts::COUNTRY_ID],
                Accounts::RATING_ATTACK => $ratingAttack,
                Accounts::RATING_DEFENCE => $ratingDefence,
                Accounts::RATING_SCIENCE_V1_4 => $ratingScience,
                Accounts::RATING_PRODUCTION_V1_4 => $ratingProduction,
                Accounts::ROLE_V1_4 => $role,
                Accounts::POP => 0,
                Accounts::TOWNS => 0,
            );
        }
    }

    private function _normalizeCountries()
    {
        if (empty($this->_raw['countries'])) {
            $this->_raw['countries'] = array();
            return;
        }

        foreach ($this->_raw['countries'] as $id => $country) {
            $diplomacy = array_key_exists(Countries::DIPLOMACY, $country) ? $country[Countries::DIPLOMACY] : array();

            $this->_curr['countries'][$id] = array(
                Countries::TITLE => $country[Countries::TITLE],
                Countries::FLAG => $country[Countries::FLAG],
                Countries::DIPLOMACY => $diplomacy,

                Countries::POP => 0,
                Countries::ACCOUNTS => 0,
                Countries::TOWNS => 0,
                Countries::SCIENCE => 0,
                Countries::PRODUCTION => 0,
                Countries::WAR => 0,
            );
        }
    }

}

class DataTown
{
    public $id;
    public $title;
    public $accountId;
    public $realAccountId;
    public $population;
    public $wonderId;
    public $wonderLevel;
    public $lost;
    public $destroyed;
    public $extra;

    public function __construct($id, array $town, $assoc = false)
    {
        $extraDefault = array(
            Towns::EXTRA_CREATED => null,
            Towns::EXTRA_LOSTED => null,
            Towns::EXTRA_DESTROYED => null,
            Towns::EXTRA_NAMES => array(),
        );
        if (!$assoc) {
            $this->id = $id;
            $this->title = $town[Towns::TITLE];
            $this->accountId = $town[Towns::ACCOUNT_ID];
            $this->realAccountId = $town[Towns::ACCOUNT_ID];
            $this->population = $town[Towns::POP];

            $this->wonderId = $town[Towns::WONDER_ID];
            $this->wonderLevel = $town[Towns::WONDER_LEVEL];

            $this->lost = 0;
            $this->destroyed = 0;

            $this->extra = $extraDefault;
        } else {
            $this->id = (int)$town['townId'];
            $this->title = $town['townTitle'];
            $this->accountId = (int)$town['accountId'];
            $this->realAccountId = (int)$town['realAccountId'];
            $this->population = (int)$town['pop'];

            $this->wonderId = (int)$town['wonderId'];
            $this->wonderLevel = (int)$town['wonderLevel'];

            $this->lost = (int)$town['lost'];
            $this->destroyed = (int)$town['destroyed'];

            $this->extra = isset($town['extra'])
                ? json_decode($town['extra'], true)
                : $extraDefault;
        }
    }
}

class DataAccount
{
    public $id;
    public $name;
    public $race;
    public $sex;
    public $countryId;
    public $role;
    public $active;

    public $population;
    public $towns;
    public $science;
    public $production;
    public $war;

    public $extra;

    /**
     * @param       $id
     * @param array $account
     * @param bool $assoc TRUE if from db, FALSE if from json.file
     */
    public function __construct($id, array $account, $assoc = false)
    {
        $extraDefault = array(
            Accounts::EXTRA_CREATED => null,
            Accounts::EXTRA_DELETED => null,
            Accounts::EXTRA_NAMES => array(),
        );
        if (!$assoc) {
            $this->id = $id;
            $this->name = $account[Accounts::TITLE];
            $this->race = $account[Accounts::RACE];
            $this->sex = $account[Accounts::SEX];
            $this->countryId = $account[Accounts::COUNTRY_ID];
            $this->role = $account[Accounts::ROLE_V1_4];
            $this->active = 1;

            $this->population = $account[Accounts::POP];
            $this->towns = $account[Accounts::TOWNS];
            $this->science = $account[Accounts::RATING_SCIENCE_V1_4];
            $this->production = $account[Accounts::RATING_PRODUCTION_V1_4];
            $this->war = $account[Accounts::RATING_ATTACK];

            $this->extra = $extraDefault;
        } else {
            $this->id = $id;
            $this->name = $account['accountName'];
            $this->race = 0;
            $this->sex = (int)$account['accountSex'];
            $this->countryId = (int)$account['countryId'];
            $this->role = (int)$account['role'];
            $this->active = 1;

            $this->population = (int)$account['pop'];
            $this->towns = (int)$account['towns'];
            $this->science = (float)$account['science'];
            $this->production = (float)$account['production'];
            $this->war = (float)$account['war'];

            $this->extra = isset($account['extra'])
                ? json_decode($account['extra'], true)
                : $extraDefault;
        }
    }
}

class DataCountry
{
    public $id;
    public $title;
    public $flag;
    public $active;

    public $population;
    public $towns;
    public $accounts;
    public $science;
    public $production;
    public $war;

    public $diplomacy;

    public $extra;

    public function __construct($id, array $country, $assoc = false)
    {
        $extraDefault = array(
            Countries::EXTRA_CREATED => null,
            Countries::EXTRA_DELETED => null,
            Countries::EXTRA_NAMES => array(),
            Countries::EXTRA_FLAGS => array(),
        );
        if (!$assoc) {
            $this->id = $id;
            $this->title = $country[Countries::TITLE];
            $this->flag = $country[Countries::FLAG];
            $this->active = 1;

            $this->population = $country[Countries::POP];
            $this->towns = $country[Countries::TOWNS];
            $this->accounts = $country[Countries::ACCOUNTS];
            $this->science = $country[Countries::SCIENCE];
            $this->production = $country[Countries::PRODUCTION];
            $this->war = $country[Countries::WAR];

            $this->diplomacy = $country[Countries::DIPLOMACY] ?: array();

            $this->extra = $extraDefault;
        } else {
            $this->id = $id;
            $this->title = $country['countryTitle'];
            $this->flag = $country['countryFlag'];
            $this->active = 1;

            $this->population = (int)$country['pop'];
            $this->towns = (int)$country['towns'];
            $this->accounts = (int)$country['accounts'];
            $this->science = (float)$country['science'];
            $this->production = (float)$country['production'];
            $this->war = (float)$country['war'];

            $this->extra = isset($country['extra'])
                ? json_decode($country['extra'], true)
                : $extraDefault;
        }
    }
}
