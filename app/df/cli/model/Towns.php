<?php
/**
 * @since              22.12.13 01:51
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */
namespace df\cli\model;

use df\extensions\models\Wofh;

class Towns extends UpdaterCommon
{

    const TITLE = 0;
    const ACCOUNT_ID = 1;
    const POP = 2;
    const WONDER = 3;

    const WONDER_ID = 100;
    const WONDER_LEVEL = 101;

    const EXTRA_CREATED = 'create';
    const EXTRA_LOSTED = 'lost';
    const EXTRA_DESTROYED = 'delete';
    const EXTRA_NAMES = 'names';

    //=======================================================
    //   Public methods
    //=======================================================

    /**
     * @param DataTown $previous
     * @param DataTown $current
     */
    public function _checkTown($previous, $current)
    {
        if ($this->events->isTownCreate($previous, $current)) {
            $current->extra[Towns::EXTRA_CREATED] = $this->currentTimestamp;
            $current->extra[Towns::EXTRA_NAMES][$this->currentTimestamp] = $current->title;
            $this->add($current);
            $this->addData($this->currentDate, $current, $current->population);

            $this->events->add($this->data->createEvent(Wofh::EVENT_TOWN_CREATE, $previous, $current));
        } elseif ($this->events->isTownDestroy($previous, $current)) {
            $delta = 0 - $previous->population;

            $previous->lost = 1;
            $previous->destroyed = 1;
            $previous->population = 0;
            $previous->wonderId = 0;
            $previous->wonderLevel = 0;

            $previous->extra[Towns::EXTRA_DESTROYED] = $this->currentTimestamp;
            if (is_null($previous->extra[Towns::EXTRA_LOSTED])) {
                $previous->extra[Towns::EXTRA_LOSTED] = $this->currentTimestamp;
            }

            $this->add($previous);
            $this->addData($this->currentDate, $previous, $delta);
            $this->events->add($this->data->createEvent(Wofh::EVENT_TOWN_DESTROY, $previous, $current));
        } else {
            $updateRequired = false;
            $current->extra = $previous->extra;

            if ($this->events->isTownLost($previous, $current)) {
                $updateRequired = true;

                $current->accountId = $previous->accountId;
                $current->lost = 1;
                $current->extra[Towns::EXTRA_LOSTED] = $this->currentTimestamp;

                $this->events->add($this->data->createEvent(Wofh::EVENT_TOWN_LOST, $previous, $current));
            }

            if ($this->events->isTownRename($previous, $current)) {
                $updateRequired = true;
                $current->extra[Towns::EXTRA_NAMES][$this->currentTimestamp] = $current->title;

                $this->events->add($this->data->createEvent(Wofh::EVENT_TOWN_RENAME, $previous, $current));
            }

            if ($this->events->isWonderDestroy($previous, $current)) {
                $this->events->add($this->data->createEvent(Wofh::EVENT_WONDER_DESTROY, $previous, $current));
            }

            if ($this->events->isWonderCreate($previous, $current)) {
                $this->events->add($this->data->createEvent(Wofh::EVENT_WONDER_CREATE, $previous, $current));
            }

            if ($this->events->isWonderActivate($previous, $current)) {
                $this->events->add($this->data->createEvent(Wofh::EVENT_WONDER_ACTIVATE, $previous, $current));
            }

            $delta = $current->population - $previous->population;

            if ($updateRequired) {
                $this->add($current);
            }
            $this->addData($this->currentDate, $current, $delta);
        }
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    protected function updateItems()
    {
        $chunks = array_chunk($this->items, $this->chunkSize);
//		$this->db->beginTransaction();
        foreach ($chunks as $chunk) {
            $query = q("REPLACE INTO z_ruXX_towns"
                . " (`townId`, `townTitle`, `accountId`, `lost`, `destroyed`, `extra`)"
                . " VALUES ", $this->world->lowerSign);
            $query .= join(",", $chunk) . ';';
            $this->db->query($query);
        }
//		$this->db->commit();
    }

    protected function insertItemsData()
    {
        $chunks = array_chunk($this->dataItems, $this->chunkSize);
//		$this->db->beginTransaction();
        foreach ($chunks as $chunk) {
            $query = q("INSERT IGNORE INTO z_ruXX_towns_stat"
                . " (`stateDate`, `townId`, `accountId`, `pop`, `wonderId`, `wonderLevel`, `delta`)"
                . " VALUES ", $this->world->lowerSign);
            $query .= join(",", $chunk) . ';';
            $this->db->query($query);
        }
//		$this->db->commit();
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function add(DataTown $town)
    {
        $this->items[] = "("
            . "  " . $town->id
            . ",'" . $town->title . "'"
            . ",'" . $town->accountId . "'"
            . ",'" . $town->lost . "'"
            . ",'" . $town->destroyed . "'"
            . "," . $this->db->e(json_encode($town->extra)) . ""
            . ")";
    }

    private function addData($date, DataTown $town, $delta)
    {
        $this->dataItems[] = "("
            . " '" . $date . "'"
            . ", " . $town->id
            . ",'" . $town->realAccountId . "'"
            . ",'" . $town->population . "'"
            . ",'" . $town->wonderId . "'"
            . ",'" . $town->wonderLevel . "'"
            . ",'" . $delta . "'"
            . ")";
    }

    protected function insertDiplomacyData()
    {
        // TODO: Implement insertDiplomacyData() method.
    }
}
