<?php
/**
 * @since              22.12.13 02:28
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */
namespace df\cli\model;

use df\Console;
use df\extensions\models\Wofh;
use df\helper\JSON;
use df\Model;

class Events extends Model
{

    private $events;
    private $world;
    /** @var  DataStatistic */
    private $data;

    public function __construct($world, $data)
    {
        $this->events = array();
        $this->world = $world;
        $this->data = $data;
    }

    public function add($event)
    {
        if (!$event) {
            return;
        }

        $type = $event['eventId'];
        if (!isset($this->events[$type])) {
            $this->events[$type] = array();
        }

        $this->events[$type][] = $event;
    }

    public function update()
    {
        $i = 0;
        $start = microtime(true);
        Console::log("Updating events .......... ", false);

        if (count($this->events)) {
            $this->db->prepare(q("
				INSERT INTO z_ruXX_events (
					stateDate,
					eventId,
					townId,
					accountId,
					countryId,
					countryIdFrom,
					role,
					extra
				)
				VALUES (
					:stateDate,
					:eventId,
					:townId,
					:accountId,
					:countryId,
					:countryIdFrom,
					:role,
					:extra
				)
			", $this->world->lowerSign));

            foreach ($this->events as $eventId => &$events) {
                if (empty($events)) {
                    continue;
                }

                foreach ($events as $event) {
                    $eventId = (int)$eventId;
                    if (($eventId == Wofh::EVENT_ACCOUNT_ROLE_IN OR $eventId == Wofh::EVENT_ACCOUNT_ROLE_OUT)
                        AND $this->world->lowerSign == 'ru23'
                    ) {
                        continue;
                    }

                    $this->db->execute(array(
                        'stateDate' => $this->data->getTime(STD_DATE),
                        'eventId' => (int)$eventId,
                        'townId' => (int)$event['townId'],
                        'accountId' => (int)$event['accountId'],
                        'countryId' => (int)$event['countryId'],
                        'countryIdFrom' => (int)$event['countryIdFrom'],
                        'role' => (int)$event['role'],
                        'extra' => JSON::encode($event['extra']),
                    ));
                    $i++;
                }
            }

        }

        Console::log(round(microtime(true) - $start, 1) . " s");
    }

    public function getCount($eventType)
    {
        return array_key_exists($eventType, $this->events)
            ? count($this->events[$eventType])
            : 0;
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isTownCreate($previousTown, $currentTown)
    {
        return is_null($previousTown) && $currentTown;
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isTownDestroy($previousTown, $currentTown)
    {
        return $previousTown && is_null($currentTown);
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isTownLost(DataTown $previousTown, DataTown $currentTown)
    {
        return ($previousTown->realAccountId > 0)
        && ($currentTown->accountId == 0);
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isTownRename(DataTown $previousTown, DataTown $currentTown)
    {
        return $previousTown->title != $currentTown->title;
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isWonderCreate(DataTown $previousTown, DataTown $currentTown)
    {
        if ($previousTown->wonderId > 0
            && $currentTown->wonderId > 0
            && $previousTown->wonderId != $currentTown->wonderId
        ) { // Разрушил старое, начал строить новое
            return true;
        }

        if ($previousTown->wonderId == 0 && $currentTown->wonderId > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isWonderDestroy(DataTown $previousTown, DataTown $currentTown)
    {
        if ($previousTown->wonderId > 0
            && $currentTown->wonderId > 0
            && $previousTown->wonderId != $currentTown->wonderId
        ) { // Разрушил старое, начал строить новое
            return true;
        }

        if ($previousTown->wonderId > 0 && $currentTown->wonderId == 0) {
            return true;
        }

        return false;
    }

    /**
     * @param DataTown $previousTown
     * @param DataTown $currentTown
     *
     * @return bool
     */
    public function isWonderActivate(DataTown $previousTown, DataTown $currentTown)
    {
        if ($previousTown->wonderId > 0
            && $currentTown->wonderId > 0
            && $previousTown->wonderLevel < 21
            && $currentTown->wonderLevel == 21
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountCreate($previous, $current)
    {
        return is_null($previous) && $current;
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountDelete($previous, $current)
    {
        return $previous && is_null($current);
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountRename(DataAccount $previous, DataAccount $current)
    {
        return ($previous->name != $current->name);
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountChangeRole(DataAccount $previous, DataAccount $current)
    {
        return ($previous->role != $current->role);
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountChangeSex(DataAccount $previous, DataAccount $current)
    {
        return ($previous->sex != $current->sex);
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountCountryIn(DataAccount $previous, DataAccount $current)
    {
        return ($previous->countryId == 0)
        && ($current->countryId > 0);
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountCountryOut(DataAccount $previous, DataAccount $current)
    {
        return ($previous->countryId > 0)
        && ($current->countryId == 0);
    }

    /**
     * @param DataAccount $previous
     * @param DataAccount $current
     *
     * @return bool
     */
    public function isAccountCountryChange(DataAccount $previous, DataAccount $current)
    {
        return ($previous->countryId != $current->countryId)
        && ($previous->countryId > 0)
        && ($current->countryId > 0);
    }

    /**
     * @param DataCountry $previous
     * @param DataCountry $current
     *
     * @return bool
     */
    public function isCountryCreate($previous, $current)
    {
        return is_null($previous) && $current;
    }

    /**
     * @param DataCountry $previous
     * @param DataCountry $current
     *
     * @return bool
     */
    public function isCountryDestroy($previous, $current)
    {
        return $previous && is_null($current);
    }

    /**
     * @param DataCountry $previous
     * @param DataCountry $current
     *
     * @return bool
     */
    public function isCountryRename(DataCountry $previous, DataCountry $current)
    {
        return ($previous->title != $current->title);
    }

    /**
     * @param DataCountry $previous
     * @param DataCountry $current
     *
     * @return bool
     */
    public function isCountryFlag(DataCountry $previous, DataCountry $current)
    {
        return ($previous->flag != $current->flag);
    }
}
