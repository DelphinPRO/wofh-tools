<?php
/**
 * @since              09.12.13 23:30
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\cli\model;

use df\CliApp;
use df\Console;
use df\exception\CliException;
use df\extensions\models\Wofh;
use df\extensions\models\World;
use df\helper\FS;
use df\Model;

function save($var)
{
    file_put_contents(DIR_ROOT . DIRECTORY_SEPARATOR . 'temp.json', json_encode($var));
}

/**
 * Class StatUpdater
 *
 * @package df\cli\model
 */
class StatUpdater extends Model
{
    //=======================================================
    //   Public methods
    //=======================================================

    public function __destruct()
    {
        unset($this->events);
    }

    public function update(World $world, $realDataPath, $dataFile)
    {
        try {
            $world->beginUpdate();

            $data = new DataStatistic(FS::readFile($realDataPath . $dataFile), $world);
            $data->sign = $world->lowerSign;

            Console::log("------------- " . $world->sign . ' ' . $data->getTime("d-m-Y") . " ------------------");
            Console::log("Processing for ........... "
                . $data->getTime("d-m-Y") . " " . $data->getTime("H:i:s"));

            $this->db->beginTransaction();

            $data->checkStruct();
            $data->normalize();

            $events = new Events($world, $data);
            $updaterTowns = new Towns($world, $data, $events, 'towns');
            $updaterAccounts = new Accounts($world, $data, $events, 'accounts');
            $updaterCountries = new Countries($world, $data, $events, 'countries');

            $updaterTowns->update();
            $updaterAccounts->update();
            $updaterCountries->update();
            $events->update();

            $this->updateCommonData($world, $data, $events);

            $this->db->commit();

            $world->endUpdate($data->getTime());
            CliApp::$sendEmail = true;

            unset($updaterCountries, $updaterAccounts, $updaterTowns, $updaterEvents);
            unset($deletedAccounts, $deletedCountries);
        } catch (CliException $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }
            $world->endUpdate();
            unset($updaterCountries, $updaterAccounts, $updaterTowns, $events);
            if ($e->getCode() == CliException::ERROR_CODE_DUPLICATE_DATE) {
                Console::line();
                Console::log('ERROR: ' . $e->getMessage());
                Console::line();
            }
            return false;
        }
        return true;
    }

    private function updateCommonData(World $world, DataStatistic $data, Events $events)
    {
        $queryActiveAccounts = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1", $world->lowerSign);
        $queryRace0 = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1 AND accountRace = 0",
            $world->lowerSign);
        $queryRace1 = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1 AND accountRace = 1",
            $world->lowerSign);
        $queryRace2 = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1 AND accountRace = 2",
            $world->lowerSign);
        $queryRace3 = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1 AND accountRace = 3",
            $world->lowerSign);
        $querySex0 = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1 AND accountSex = 0",
            $world->lowerSign);
        $querySex1 = q("SELECT COUNT(*) AS total FROM z_ruXX_accounts WHERE active = 1 AND accountSex = 1",
            $world->lowerSign);

        $this->db->query(q("
			INSERT INTO z_ruXX_common
			SET
				stateDate = :stateDate,

				townsTotal = :townsTotal,
				townsNew = :townsNew,
				townsRenamed = :townsRenamed,
				townsLost = :townsLost,
				townsDestroy = :townsDestroy,

				wondersNew = :wondersNew,
				wondersDestroy = :wondersDestroy,
				wondersActivate = :wondersActivate,

				accountsTotal = :accountsTotal,
				accountsActive = :accountsActive,
				accountsRace0 = :accountsRace0,
				accountsRace1 = :accountsRace1,
				accountsRace2 = :accountsRace2,
				accountsRace3 = :accountsRace3,
				accountsSex0 = :accountsSex0,
				accountsSex1 = :accountsSex1,

				accountsNew = :accountsNew,
				accountsCountryChange = :accountsCountryChange,
				accountsCountryIn = :accountsCountryIn,
				accountsCountryOut = :accountsCountryOut,
				accountsDeleted = :accountsDeleted,
				accountsRenamed = :accountsRenamed,
				accountsRoleIn = :accountsRoleIn,
				accountsRoleOut = :accountsRoleOut,

				countriesTotal = :countriesTotal,
				countriesNew = :countriesNew,
				countriesRenamed = :countriesRenamed,
				countriesFlag = :countriesFlag,
				countriesDeleted = :countriesDeleted;
		", $world->lowerSign), array(
            'stateDate' => $data->getTime(STD_DATE),

            'townsTotal' => $data->getCount('towns'),
            'townsNew' => $events->getCount(Wofh::EVENT_TOWN_CREATE),
            'townsRenamed' => $events->getCount(Wofh::EVENT_TOWN_RENAME),
            'townsLost' => $events->getCount(Wofh::EVENT_TOWN_LOST),
            'townsDestroy' => $events->getCount(Wofh::EVENT_TOWN_DESTROY),

            'wondersNew' => $events->getCount(Wofh::EVENT_WONDER_CREATE),
            'wondersDestroy' => $events->getCount(Wofh::EVENT_WONDER_DESTROY),
            'wondersActivate' => $events->getCount(Wofh::EVENT_WONDER_ACTIVATE),

            'accountsTotal' => $data->getCountRaw('accounts'),
            'accountsActive' => (int)$this->db->query($queryActiveAccounts)->fetchObject()->total,
            'accountsRace0' => (int)$this->db->query($queryRace0)->fetchObject()->total,
            'accountsRace1' => (int)$this->db->query($queryRace1)->fetchObject()->total,
            'accountsRace2' => (int)$this->db->query($queryRace2)->fetchObject()->total,
            'accountsRace3' => (int)$this->db->query($queryRace3)->fetchObject()->total,
            'accountsSex0' => (int)$this->db->query($querySex0)->fetchObject()->total,
            'accountsSex1' => (int)$this->db->query($querySex1)->fetchObject()->total,

            'accountsNew' => $events->getCount(Wofh::EVENT_ACCOUNT_CREATE),
            'accountsCountryChange' => $events->getCount(Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE),
            'accountsCountryIn' => $events->getCount(Wofh::EVENT_ACCOUNT_COUNTRY_IN),
            'accountsCountryOut' => $events->getCount(Wofh::EVENT_ACCOUNT_COUNTRY_OUT),
            'accountsDeleted' => $events->getCount(Wofh::EVENT_ACCOUNT_DELETE),
            'accountsRenamed' => $events->getCount(Wofh::EVENT_ACCOUNT_RENAME),
            'accountsRoleIn' => $events->getCount(Wofh::EVENT_ACCOUNT_ROLE_IN),
            'accountsRoleOut' => $events->getCount(Wofh::EVENT_ACCOUNT_ROLE_OUT),

            'countriesTotal' => $data->getCount('countries'),
            'countriesNew' => $events->getCount(Wofh::EVENT_COUNTRY_CREATE),
            'countriesRenamed' => $events->getCount(Wofh::EVENT_COUNTRY_RENAME),
            'countriesFlag' => $events->getCount(Wofh::EVENT_COUNTRY_FLAG),
            'countriesDeleted' => $events->getCount(Wofh::EVENT_COUNTRY_DESTROY),
        ));
    }

    public function _checkTables($sign)
    {
        $queries = array(
            'towns' => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_towns` (
					`townId` INT(10) UNSIGNED NOT NULL,
					`townTitle` VARCHAR(50) NOT NULL,
					`accountId` MEDIUMINT(9) UNSIGNED NOT NULL,
					`lost` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
					`destroyed` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
					`extra` TEXT NOT NULL,
					PRIMARY KEY (`townId`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            'towns_stat' => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_towns_stat` (
					`stateDate` DATE NOT NULL,
					`townId` MEDIUMINT(6) UNSIGNED NOT NULL,
					`accountId` SMALLINT(6) UNSIGNED NOT NULL,
					`pop` MEDIUMINT(10) NULL DEFAULT NULL,
					`wonderId` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
					`wonderLevel` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
					`delta` SMALLINT(6) NULL DEFAULT NULL,
					PRIMARY KEY (`stateDate`, `townId`),
					INDEX `stateDate` (`stateDate`),
					INDEX `townId` (`townId`),
					INDEX `townPop` (`pop`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            "accounts" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_accounts` (
					`accountId` MEDIUMINT(11) UNSIGNED NOT NULL,
					`accountName` VARCHAR(30) NOT NULL,
					`accountRace` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
					`accountSex` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
					`countryId` SMALLINT(11) UNSIGNED NOT NULL DEFAULT '0',
					`role` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0',
					`active` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
					`extra` TEXT NOT NULL,
					PRIMARY KEY (`accountId`),
					INDEX `countryId` (`countryId`),
					INDEX `active` (`active`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            "accounts_stat" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_accounts_stat` (
					`stateDate` DATE NOT NULL,
					`accountId` MEDIUMINT(10) UNSIGNED NOT NULL,
					`countryId` SMALLINT(10) UNSIGNED NOT NULL DEFAULT '0',
					`role` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0',
					`pop` MEDIUMINT(10) NOT NULL DEFAULT '0',
					`towns` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
					`science` FLOAT NOT NULL DEFAULT '0',
					`production` FLOAT NOT NULL DEFAULT '0',
					`war` FLOAT NOT NULL DEFAULT '0',
					`deltaPop` MEDIUMINT(8) NOT NULL DEFAULT '0',
					`deltaTowns` TINYINT(4) NOT NULL DEFAULT '0',
					`deltaScience` FLOAT NOT NULL DEFAULT '0',
					`deltaProduction` FLOAT NOT NULL DEFAULT '0',
					`deltaWar` FLOAT NOT NULL DEFAULT '0',
					PRIMARY KEY (`stateDate`, `accountId`),
					INDEX `accountId` (`accountId`),
					INDEX `stateDate` (`stateDate`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            "countries" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_countries` (
					`countryId` SMALLINT(5) UNSIGNED NOT NULL,
					`countryTitle` VARCHAR(30) NULL DEFAULT NULL,
					`countryFlag` VARCHAR(30) NULL DEFAULT NULL,
					`active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
					`extra` TEXT NOT NULL,
					PRIMARY KEY (`countryId`),
					INDEX `active` (`active`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            "countries_stat" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_countries_stat` (
					`stateDate` DATE NOT NULL,
					`countryId` SMALLINT(5) UNSIGNED NOT NULL,
					`pop` INT(10) UNSIGNED NOT NULL DEFAULT '0',
					`accounts` TINYINT(10) UNSIGNED NOT NULL DEFAULT '0',
					`towns` SMALLINT(10) UNSIGNED NOT NULL DEFAULT '0',
					`science` FLOAT NULL DEFAULT NULL,
					`production` FLOAT NULL DEFAULT NULL,
					`war` FLOAT NULL DEFAULT NULL,
					`deltaPop` MEDIUMINT(8) NULL DEFAULT NULL,
					`deltaAccounts` TINYINT(4) NULL DEFAULT NULL,
					`deltaTowns` SMALLINT(6) NULL DEFAULT NULL,
					`deltaScience` FLOAT NULL DEFAULT NULL,
					`deltaProduction` FLOAT NULL DEFAULT NULL,
					`deltaWar` FLOAT NULL DEFAULT NULL,
					PRIMARY KEY (`stateDate`, `countryId`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            "countries_diplomacy" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_countries_diplomacy` (
					`stateDate` DATE NOT NULL,
					`id1` SMALLINT(5) UNSIGNED NOT NULL,
					`id2` SMALLINT(5) UNSIGNED NOT NULL,
					`status` TINYINT(3) UNSIGNED NOT NULL,
					PRIMARY KEY (`stateDate`, `id1`, `id2`),
					INDEX `status` (`status`),
					INDEX `stateDate_id1` (`stateDate`, `id1`)
				) ENGINE=InnoDB;",
            "events" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_events` (
					`stateDate` DATE NOT NULL,
					`eventId` INT(11) NOT NULL,
					`townId` INT(11) NOT NULL,
					`accountId` INT(11) NOT NULL,
					`countryId` INT(11) NOT NULL,
					`countryIdFrom` INT(11) NOT NULL,
					`role` INT(11) NOT NULL,
					`extra` TEXT NOT NULL,
					INDEX `eventId` (`eventId`),
					INDEX `stateDate` (`stateDate`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
            "common" => "
				CREATE TABLE IF NOT EXISTS `z_ruXX_common` (
					`stateDate`             DATE NOT NULL,

					`townsTotal`            SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`townsNew`              SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`townsRenamed`          SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`townsLost`             SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`townsDestroy`          SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',

					`wondersNew`            SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`wondersDestroy`        SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`wondersActivate`       SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',

					`accountsTotal`         SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsActive`        SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRace0`         SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRace1`         SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRace2`         SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRace3`         SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsSex0`          SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsSex1`          SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',

					`accountsNew`           SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsCountryChange` SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsCountryIn`     SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsCountryOut`    SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsDeleted`       SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRenamed`       SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRoleIn`        SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`accountsRoleOut`       SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',

					`countriesTotal`        SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`countriesNew`          SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`countriesRenamed`      SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`countriesFlag`         SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',
					`countriesDeleted`      SMALLINT(6) UNSIGNED NOT NULL DEFAULT '0',

					PRIMARY KEY (`stateDate`)
				) COLLATE='utf8_general_ci' ENGINE=InnoDB;",
        );

        foreach ($queries as $query) {
            if ($this->config->get('app.debug')) {
                $this->db->query($query);
            }
            $this->db->query(str_replace('ruXX', $sign, $query));
        }
    }

    //=======================================================
    //   Private methods
    //=======================================================

}
