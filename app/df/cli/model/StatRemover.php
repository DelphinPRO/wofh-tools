<?php
/**
 * @since             19.11.2014 10:18
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\cli\model;

use df\extensions\models\World;
use df\Model;

class StatRemover extends Model
{
    //=======================================================
    //   Public methods
    //=======================================================

    public function remove(World $world)
    {
        $queries = array(
            'DROP TABLE IF EXISTS z_ruXX_accounts;',
            'DROP TABLE IF EXISTS z_ruXX_accounts_stat;',
            'DROP TABLE IF EXISTS z_ruXX_countries;',
            'DROP TABLE IF EXISTS z_ruXX_countries_stat;',
            'DROP TABLE IF EXISTS z_ruXX_countries_diplomacy;',
            'DROP TABLE IF EXISTS z_ruXX_towns;',
            'DROP TABLE IF EXISTS z_ruXX_towns_stat;',
            'DROP TABLE IF EXISTS z_ruXX_events;',
            'DROP TABLE IF EXISTS z_ruXX_common;',
            'UPDATE wt_worlds SET updateStat = NULL WHERE id = ' . $world->id . ';',
        );

        $this->db->setPrefix('ruXX', $world->lowerSign);
        foreach ($queries as $query) {
            $this->db->query($query);
        }

    }

    //=======================================================
    //   Protected methods
    //=======================================================

    //=======================================================
    //   Private methods
    //=======================================================

}
