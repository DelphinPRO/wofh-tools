<?php

/**
 * @since             08.11.2014 11:05
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */
namespace df\cli\model;

use df\Model;

class Installer extends Model
{
    public function checkTables()
    {
        $query =
            "CREATE TABLE IF NOT EXISTS `wt_worlds` (
	`id` INT(10) UNSIGNED NOT NULL,
	`title` VARCHAR(50) NOT NULL DEFAULT '',
	`sign` VARCHAR(5) NOT NULL,
	`started` DATETIME NULL DEFAULT NULL,
	`canReg` TINYINT(1) NOT NULL DEFAULT '0',
	`working` TINYINT(1) NOT NULL DEFAULT '0',
	`needCode` TINYINT(1) NOT NULL DEFAULT '0',
	`statistic` TINYINT(1) NOT NULL DEFAULT '0',
	`loadStat` DATETIME NULL DEFAULT NULL,
	`updateStat` DATETIME NULL DEFAULT NULL,
	`updateConst` DATETIME NULL DEFAULT NULL,
	`desc` TEXT NULL,
	`isUpdate` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
";
    }
}
