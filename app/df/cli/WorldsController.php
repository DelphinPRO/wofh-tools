<?php
/**
 * @since             18.08.14 16:14
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\cli;

use df\CliController;
use df\helper\FS;
use df\model\Wofh;

class WorldsController extends CliController
{
    public function check()
    {
        $this->_echo('Check worlds status...');
        $gameCountries = Wofh::getGameCountries();

        foreach ($gameCountries as $sign => $gameCountry) {
            $this->_echo('Check ' . strtoupper($sign) . ' worlds...', false);
//			if ($gameCountry != 3) continue;
            $statusLink = Wofh::getLinkStatusWorlds($gameCountry);
            $data = FS::readURL($statusLink);
            if (FS::getCurlErrorNo() != 0) {
                $this->_echo(' FAIL'); // TODO send email
                $this->_echo('    Status not available: ' . Wofh::getGameUrl($gameCountry));
                $this->_echo('    Curl error message  : ' . FS::getCurlError());
                continue;
            }

            $json = json_decode($data, true);
            if (json_last_error() != JSON_ERROR_NONE) {
                $this->_echo(' FAIL');
                $this->_echo('    Status not available: ' . Wofh::getGameUrl($gameCountry));
                continue;
            }
//			var_export($json);

            foreach ($json['worlds'] as $domain => $status) {
                $id = Wofh::domainToId($domain);
//				$this->_echo($id);
                $world = Wofh::getWorld($id);
                $world->canReg = (bool)(int)$status['canreg'];
                $world->working = (bool)(int)$status['working'];
                $world->needCode = (bool)(int)$status['needcode'];
                $world->save();
                unset($world);
//				var_export($world);
            }

            $this->_echo(' OK');
        }
    }
}
