<?php
/**
 * @since             31.01.14 10:08
 * @package           Wofh Tools
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\cli;

use df\cli\model\StatRemover;
use df\cli\model\StatUpdater;
use df\CliController;
use df\Console;
use df\extensions\models\Wofh;
use df\extensions\models\World;
use df\helper\FS;

/**
 * Class StatController
 *
 * @package df\cli
 */
class StatController extends CliController
{

    const DOWNLOAD_STAT_INTERVAL_HOURS = 6;

    //=======================================================
    //   Public methods
    //=======================================================

    public function load()
    {
        Wofh::check();

        $worlds = Wofh::getWorlds(array('WORKING'));
        $first = true;

        foreach ($worlds as $world) {

            $linkStatistic = Wofh::getLinkStatistic($world->id);
            $realDataPath = FS::path(\df\statisticPath() . $world->lowerSign . '/');

            if (isset($this->args['dev'])) {
                $dataPath = FS::path('/../../wofh-tools.ru/public_html' . \df\statisticPath() . $world->lowerSign . '/');
                $realDataPath = realpath($dataPath) . '/';
            }

            if (!$first) {
                Console::line();
            }
            $first = false;
            Console::log('Load statistic for ' . $world->sign, false);
            Console::log(' ( ' . $linkStatistic . ' )');

            if (!is_dir($realDataPath)) {
                if (!FS::mkdir($realDataPath, 0777)) {
                    Console::log('Error. Can not create dir: ' . \df\statisticPath() . $world->lowerSign . '/');
                    continue;
                }
            }

            if (!$this->_requiredDownload($realDataPath, $world)) {
                Console::log('No download required');
                continue;
            }

            $start = microtime(true);
            Console::log('Start downloading ........ ', false);

            $stat = FS::readURL($linkStatistic);
//			$stat = FS::readFile($savePath . 'stat-' . $world->lowerSign . '-' . date(STD_DATE) . '.json');
//			sleep(2);

            if (FS::getCurlErrorNo() != 0) {
                Console::log(' FAIL');
                Console::log('Error. Can\'t read URL');
                continue;
            }

            $data = json_decode($stat, true);

            if ((json_last_error() != JSON_ERROR_NONE) || is_null($data)) {
                Console::log(' FAIL');
                Console::log('Error. Can\'t decode read data');
                continue;
            }

            Console::log("success (" . round(microtime(true) - $start, 2) . "c)");
            $date = date(STD_DATE, $data['time']);
            $filename = $realDataPath . 'stat-' . $world->lowerSign . '-' . $date . '.json';

            if (!FS::saveFile($filename, $stat, 0777)) {
                Console::log('Error saving file. ' . 'stat-' . $world->lowerSign . '-' . $date . '.json');
                continue;
            }

            $world->loadStat = $data['time'];
            $world->save();

            Console::log('New last time download ... ' . date(STD_DATETIME, $data['time']));
//			break;
        }
    }

    public function update()
    {
        if (isset($this->args['world'])) {

            $world = Wofh::getWorldBySign($this->args['world']);

            if (!$world) {
                Console::log('Invalid parameter --world');
                Console::log('EXIT');
                return;
            }

            $this->_updateWorld($world);

        } else {

            Console::log('Multiple worlds update');

            $first = true;
            $worlds = Wofh::getWorlds(array('WORKING'));

            foreach ($worlds as $world) {
                if (!$world->statistic) {
                    continue;
                }

                if (!$first) {
                    Console::line();
                }
                $first = false;

                $this->_updateWorld($world);
            }
        }

    }

    public function delete()
    {
        if (isset($this->args['world'])) {

            $world = Wofh::getWorldBySign($this->args['world']);

            if (!$world) {
                Console::log('Invalid parameter --world');
                Console::log('EXIT');
                return;
            }

            $remover = new StatRemover();
            $remover->remove($world);
            Console::log('Statistic removed for ' . $world->sign);
        } else {
            Console::log('Required parameter --world');
            Console::log('EXIT');
        }
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function _getLastDownloadTime($realDataPath, $sign)
    {
        $files = glob($realDataPath . 'stat-' . $sign . '-*.json');
        if (empty($files)) {
            return false;
        }

        sort($files);
        $lastFile = array_pop($files);
        $data = FS::loadJSON($lastFile);
        return $data['time'];
    }

    private function _requiredDownload($realDataPath, World $world)
    {
        Console::log('Check last download ...... ', false);

        $lastLoadStat = $this->_getLastDownloadTime($realDataPath, $world->lowerSign);

        if (!$lastLoadStat) {
            Console::log('never');
            return true;
        } else {
            Console::log(date(STD_DATETIME, $lastLoadStat));
            Console::log('Current time ............. ' . date(STD_DATETIME));

            $dt = new \DateTime('@' . time());
            $di = $dt->diff(new \DateTime('@' . $lastLoadStat));
            Console::log('Diff time ................ ', false);
            Console::log($di->format('%dd, %hh, %im, %ss'));
            if ($di->d > 0 || $di->h >= self::DOWNLOAD_STAT_INTERVAL_HOURS) {
                return true;
            }
        }

        return false;
    }

    private function _updateWorld(World $world)
    {
        Console::log('Update statistic for ' . $world->sign);

        $realDataPath = FS::path(\df\statisticPath() . $world->lowerSign . '/');

        if (isset($this->args['dev'])) {
            $dataPath = FS::path('/../../wofh-tools.ru/public_html' . \df\statisticPath() . $world->lowerSign . '/');
            $realDataPath = realpath($dataPath) . '/';
        }

        if (!is_dir($realDataPath)) {
            Console::log('Error. Data not found: ' . $realDataPath);
            return;
        }

        $lastUpdateTime = $world->getRealLastUpdate();
        $lastUpdateText = ($lastUpdateTime ? date(STD_DATETIME, $lastUpdateTime) : 'never');

        $lastDownloadTime = $this->_getLastDownloadTime($realDataPath, $world->lowerSign);
        $lastDownloadText = ($lastDownloadTime ? date(STD_DATETIME, $lastDownloadTime) : 'never');

        Console::log('Last download ............ ' . $lastDownloadText);
        Console::log('Last update .............. ' . $lastUpdateText);

        $dataFiles = scandir($realDataPath, 0);
        if (!is_array($dataFiles)) {
            Console::log('Error. Can\'t read data files');
            return;
        }

        $dataFiles = array_filter($dataFiles, function ($item) {
            return pathinfo($item, PATHINFO_EXTENSION) == 'json';
        });

        if (empty($dataFiles)) {
            Console::log('Error. Data files not found: ' . \df\statisticPath() . $world->lowerSign . '/*.json');
            return;
        }

        sort($dataFiles);

        $process = false;
        $lastDataFile = '';

        if (!$lastUpdateTime) {
            $process = true;
        } else {
            $lastDataFile = 'stat-' . $world->lowerSign . '-' . date(STD_DATE, $lastUpdateTime) . '.json';
        }

        $limit = isset($this->args['limit']) ? (int)$this->args['limit'] : false;
        $counter = 0;

        foreach ($dataFiles as $dataFile) {
            $startTime = microtime(true);
            if (!is_file($realDataPath . $dataFile)) {
                if (!is_dir($realDataPath . $dataFile)) {
                    Console::log('Error. Data file not readable: ' . $realDataPath . $dataFile);
                }
                continue;
            }

            if ($dataFile == $lastDataFile) {
                $process = true;
                continue;
            }

            if (!$process) {
                continue;
            }

            $counter++;

            $updater = new StatUpdater();
            $updater->_checkTables($world->lowerSign);
            Console::log("Check tables ............. OK");

            $updater->update($world, $realDataPath, $dataFile);
            unset($updater);

            $endTime = round(microtime(true) - $startTime, 1);
            Console::log("              total time : " . $endTime . " c.");
            Console::log("            memory usage : "
                . preg_replace('~(\d(?=(?:\d{3})+(?!\d)))~s', "\\1 ", memory_get_usage(true))
                . " of " . ini_get('memory_limit'));

            if (isset($this->args['one'])) {
                break;
            }

            if ($limit) {
                Console::log("              limit : " . $counter . " of " . $limit);
            }

            if ($limit && $counter >= $limit) {
                Console::line();
                Console::log("              LIMIT : " . $limit . ". STOP update.");
                break;
            }
        }
    }

}
