<?php
/**
 * @since             08.11.2014 10:59
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\cli;

use df\cli\model\Installer;
use df\CliController;

class InstallController extends CliController
{
    public function install()
    {
        $installer = new Installer();
        $installer->checkTables();
    }
}
