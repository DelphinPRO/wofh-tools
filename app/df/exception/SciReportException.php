<?php
/**
 * @since           26.11.2014 05:43
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright        Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df\exception;

class SciReportException extends DFException
{
    //=======================================================
    //   Public methods
    //=======================================================

    //=======================================================
    //   Protected methods
    //=======================================================

    //=======================================================
    //   Private methods
    //=======================================================

}
