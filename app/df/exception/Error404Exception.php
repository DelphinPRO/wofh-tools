<?php

/**
 * @since              08.02.13 14:38
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\exception;

class Error404Exception extends DFException
{

    public function __construct($message, $code = 404, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->code = 404;
        header('http/1.0 404 Not Found');
    }
}
