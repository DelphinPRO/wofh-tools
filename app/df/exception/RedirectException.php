<?php
/**
 * @since             25.01.14 13:23
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\exception;

class RedirectException extends DFException
{
    private $targetUrl;
    private $userMessage;
    private $userMessageStyle;

    /**
     * @param string $targetURL
     * @param string $message
     * @param int $messageStyle
     */
    public function __construct($targetURL, $message = "", $messageStyle = 0)
    {
        $this->targetUrl = $targetURL;
        $this->userMessage = $message;
        $this->userMessageStyle = $messageStyle;
    }

    /**
     * @return string
     */
    public function getTargetUrl()
    {
        return $this->targetUrl;
    }

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return $this->userMessage;
    }

    /**
     * @return int
     */
    public function getUserMessageStyle()
    {
        return $this->userMessageStyle;
    }

}
