<?php

/**
 * @since              08.02.13 16:11
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\exception;

class ErrorApp extends DFException
{

    public function __construct($message, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        if (ob_get_level()) {
            ob_end_clean();
        }
    }
}
