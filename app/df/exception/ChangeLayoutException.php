<?php
/**
 * @since             22.11.2014 01:06
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\exception;

class ChangeLayoutException extends DFException
{

    //=======================================================
    //   Public methods
    //=======================================================

    //=======================================================
    //   Protected methods
    //=======================================================

    //=======================================================
    //   Private methods
    //=======================================================

}
