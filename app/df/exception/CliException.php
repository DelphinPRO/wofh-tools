<?php
/**
 * @since             31.01.14 09:35
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df\exception;

class CliException extends DFException
{

    const ERROR_CODE_DUPLICATE_DATE = 2000;

//	public function __toString()
//	{
//		return $this->getMessage() . PHP_EOL
//		. $this->getFile() . " - " . $this->getLine() . PHP_EOL
//		. $this->getTraceAsString();
//	}
}
