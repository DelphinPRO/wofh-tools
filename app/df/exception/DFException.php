<?php
/**
 * @since              26.04.13 18:40
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\exception;

use WofhTools\Core\Conf\Config;

class DFException extends \Exception
{
    public static function handlerException(\Exception $e)
    {
        if (Config::Inst()->get('app.debug')) {
            include DIR_APP . '/df/system/templates/_df_exception.phtml';
        } else {
            switch ($e->getCode()) {
                case '42S02':
                    echo 'Error: Base table or view not found.<br>Code: 42S02.';
                    break;
                default:
                    echo 'Error: Unknown.<br>Code: ' . $e->getCode() . '.';
            }
            echo '<h2><a href="/">Index</a>
                    / <a href="javascript:history.back()" style="font-weight:400">Back</a></h2>';
        }
    }

    public static function handlerError($errNo, $errStr, $errFile, $errLine, $errContext)
    {
        global $_ERROR_BUFFERING, $_ERROR_STACK;

        if (!(error_reporting() & $errNo)) {
            return false; // Этот код ошибки не включен в error_reporting
        }

        $errFile = str_replace('\\', '/', str_replace(DIR_ROOT, '', $errFile));

        $errorMessage = '';

        switch ($errNo) {
            case E_USER_ERROR:
                echo "<b>ERROR</b> [$errNo] $errStr<br />\n";
                echo "  Фатальная ошибка в строке $errLine файла $errFile";
                echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                echo "Завершение работы...<br />\n";
                exit(1);
                break;

            case E_WARNING:
            case E_USER_WARNING:
                $errorMessage .= "<div><code><span class='label label-danger'>WARNING</span> <i>$errStr</i>";
                $errorMessage .= " in <b>$errFile</b> on line <b>$errLine</b></code></div>\n";
                break;

            case E_NOTICE:
            case E_USER_NOTICE:
                $errorMessage .= "<div><code><span class='label label-warning'>NOTICE</span> <i>$errStr</i>";
                $errorMessage .= " in <b>$errFile</b> on line <b>$errLine</b></code></div>\n";
                break;

            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $errorMessage .= "<b>DEPRECATED:</b> $errStr<br />\n";
                $errorMessage .= "in <b>$errFile</b> on line <b>$errLine</b><br />\n";
                break;

            case E_STRICT:
                $errorMessage .= "<b>STRICT:</b> [$errNo] $errStr<br />\n";
                $errorMessage .= "in <b>$errFile</b> on line <b>$errLine</b><br />\n";
                break;

            default:
                $errorMessage .= "<b>Неизвестная ошибка:</b> [$errNo] $errStr<br />\n";
                $errorMessage .= "in <b>$errFile</b> on line <b>$errLine</b><br />\n";
                break;
        }
        if (!$_ERROR_BUFFERING) {
            echo $errorMessage;
        }
        $_ERROR_STACK[] = $errorMessage;
        return true;
//		return false;
    }

    public function __toString()
    {
        if (defined('CLI_MODE')) {
            return
                "exception '" . get_class($this) . "' with message:" . PHP_EOL .
                $this->getMessage() . PHP_EOL .
                'in ' . $this->getFile() . ":" . $this->getLine() . PHP_EOL .
                'Stack trace:' . PHP_EOL .
                $this->getTraceAsString();
        }

        ob_start();
        self::handlerException($this);
        return ob_get_clean();
    }
}
