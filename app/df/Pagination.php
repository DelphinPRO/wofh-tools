<?php
/**
 * @since             29.07.14 18:48
 * @package           DFramework
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df;

class Pagination
{
    private $count;
    private $total;
    private $perPage;
    private $page;
    private $link;

    /**
     * @return Pagination
     */
    public static function Create()
    {
        return new Pagination();
    }

    public function __construct()
    {
        $this->count = 10;
        $this->total = 0;
        $this->perPage = 20;
        $this->page = 1;
        $this->link = '?p=%p';
    }

    /**
     * @param $total
     *
     * @return Pagination $this
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @param $page
     *
     * @return Pagination $this
     */
    public function setPage($page)
    {
        $this->page = ($page < 1) ? 1 : $page;
        return $this;
    }

    /**
     * @param $perPage
     *
     * @return Pagination $this
     */
    public function setPerPage($perPage)
    {
        $this->perPage = ($perPage < 1) ? 1 : $perPage;
        return $this;
    }

    /**
     * @param $link
     *
     * @return Pagination $this
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    public function getCurrentPage()
    {
        return $this->page;
    }

    public function getPerPage()
    {
        return $this->perPage;
    }

    public function getStartNumeation()
    {
        return $this->page * $this->perPage - $this->perPage + 1;
    }

    public function render()
    {
        $center = floor($this->count / 2) - 1;
        $pages = ceil($this->total / $this->perPage);

        if ($pages < 2) {
            return '';
        }

        $start = $this->page - $center;
        if ($this->page <= $center) {
            $start = 1;
        }
        if ($pages <= $this->count) {
            $start = 1;
        }

        $end = $start + $this->count - 1;
        if ($end > $pages) {
            $end = $pages;
            $start = $end - $this->count + 1;
            if ($start < 1) {
                $start = 1;
            }
        }

        $prev = $this->page - 1;
        if ($prev < 1) {
            $prev = 1;
        }
        $next = $this->page + 1;
        if ($next > $end) {
            $next = $end;
        }

        $html = '';
        $html .= '<ul class="pagination pagination-sm">';
        $html .= '<li>';
        $html .= '<a href="' . $this->link($prev) . '">&laquo;</a>';
        $html .= '</li>';
        for ($i = $start; $i <= $this->total && $i <= $end; $i++) {
            $html .= '<li' . (($i == $this->page) ? ' class="active"' : '') . '>';
            $html .= '<a href="' . $this->link($i) . '">' . $i . '</a>';
            $html .= '</li>';
        }
        $html .= '<li>';
        $html .= '<a href="' . $this->link($next) . '">&raquo;</a>';
        $html .= '</li>';
        $html .= '</ul>';
        if (Registry::getUser()->isAdmin()) {
            $html .= '<div class="admin-area">Total: ' . $this->total . '; Pages: ' . $pages . '</div>';
        }
//		$html .= '<br>Center: ' . $center;
//		$html .= '<br>Start: ' . $start;
//		$html .= '<br>Max: ' . $end;
        return $html;
    }

    public function __toString()
    {
        return $this->render();
    }

    private function link($page)
    {
        return str_replace('%p', $page, $this->link);
    }

}
