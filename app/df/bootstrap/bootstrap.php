<?php
/**
 * Bootstrap file
 *
 * @since              25.01.13 18:08
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use WofhTools\Core\Conf\Config;

defined('DIR_ROOT') or die('Restricted access: ' . basename(__FILE__));

if (is_file(__DIR__ . '/../../version.php')) {
    include __DIR__ . '/../../version.php';
}

define('STD_DATETIME', 'Y-m-d H:i:s');
define('STD_DATE', 'Y-m-d');
define('STD_TIME', 'H:i:s');

require DIR_APP . '/df/Debug.php';


Config::load(DIR_PRIVATE . DIRECTORY_SEPARATOR . 'config');

if (PHP_SAPI != 'cli') {
    set_error_handler(array('\df\exception\DFException', 'handlerError'));
}
set_exception_handler(array('\df\exception\DFException', 'handlerException'));

/** @deprecated */
function statisticPath()
{
    $sp = Config::Inst()->get('app.statisticPath');
    return $sp ? $sp : '/tmp/';
}

Router::$cacheFile = DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'routes.json';
Router::$adminFolder = Config::Inst()->get('app.admin_folder');

if (Config::Inst()->get('app.debug')) {

    ini_set('display_errors', 'On');
    ini_set('html_errors', 'On');
    error_reporting(E_ALL | E_STRICT);

} else {

    ini_set('display_errors', 'Off');
    ini_set('html_errors', 'Off');
    error_reporting(0);

}

mb_internal_encoding('UTF-8');
date_default_timezone_set(Config::Inst()->get('app.default_timezone_name'));

if (PHP_SAPI != 'cli') {
    if (Config::Inst()->get('app.site_offline')) {
        if (!(Config::Inst()->get('app.allowed_ip')
            && $_SERVER['REMOTE_ADDR'] == Config::Inst()->get('app.allowed_ip'))
        ) {
            $secret = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
            if (array_shift($secret) != Config::Inst()->get('app.secret')) {
                include DIR_APP . '/admin/templates/offline.phtml';
                exit;
            }
        }
    }
}

require DIR_APP . '/functions.php';

if (PHP_SAPI == 'cli') {
    ini_set('html_errors', 'Off');
    ignore_user_abort(true);
    CliApp::Inst()->run();
} else {
    App::Inst()->run();
}

exit;
