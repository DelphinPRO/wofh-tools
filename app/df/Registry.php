<?php
/**
 * @since              17.01.14 22:07
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use df\view\Document;
use WofhTools\Core\Conf\Config;

final class Registry
{
    private static $_registry = array();

    /**
     * @param string|int $key
     * @param mixed $data
     */
    public static function set($key, $data)
    {
        self::$_registry[$key] = $data;
    }

    /**
     * @param string|int $key
     *
     * @return mixed|null
     */
    public static function get($key)
    {
        return isset(self::$_registry[$key]) ? self::$_registry[$key] : null;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public static function exists($key)
    {
        return isset(self::$_registry[$key]);
    }

    /**
     * @param $class
     *
     * @return mixed
     */
    public static function getInstance($class)
    {
        if (!isset(self::$_registry[$class])) {
            self::$_registry[$class] = new $class();
        }
        return self::$_registry[$class];
    }

    /**
     * @return Router
     */
    public static function getRouter()
    {
        return self::getInstance('df\Router');
    }

    /**
     * @deprecated
     * @return Config
     */
    public static function getConfig()
    {
        return Config::Inst();
    }

    /**
     * @return \df\Db
     */
    public static function getDb()
    {
        return self::getInstance('df\Db');
    }

    /**
     * @return Document
     */
    public static function getDocument()
    {
        return self::getInstance('df\view\Document');
    }

    /**
     * @return User
     */
    public static function getUser()
    {
        return self::getInstance('df\User');
    }

    /**
     * @return Request
     */
    public static function getRequest()
    {
        return self::getInstance('df\Request');
    }

    /**
     * @return Session
     */
    public static function getSession()
    {
        return self::getInstance('df\Session');
    }

    /**
     * @return App
     */
    public static function getApp()
    {
        return App::Inst();
    }

}
