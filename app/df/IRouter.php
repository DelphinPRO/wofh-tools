<?php
/**
 * @since             29.08.14 02:11
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df;

interface IRouter
{

    public function isPost();

    public function getRoutingMap();

    public function getRequestMethod();

    public function getUri();

    public function getRouteSignature($routeName);

    public function link($routeName, array $params = array());

    public function match();

}
