<?php

/**
 * Front controller.
 *
 * @since              07.01.13 16:24
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use df\exception\DFException;
use df\exception\Error404Exception;
use df\exception\RedirectException;
use df\system\controllers\Error404Controller;

final class App extends Object
{

    const
        INFO = 'info',
        SUCCESS = 'success',
        WARNING = 'warning',
        ERROR = 'danger';

    /** @var App */
    private static $_application;

    /** @var bool */
    private static $_isRunning = false;

    /** @var Controller */
    private $controller;

    /** @var string */
    private $redirectUrl = '';

    private $widgets;

    //=======================================================
    //   Public methods
    //=======================================================

    /** @return App */
    public static function Inst()
    {
        if (!is_object(self::$_application)) {
            self::$_application = new App();
        }

        return self::$_application;
    }

    public function run()
    {
        if (self::$_isRunning) {
            return;
        }
        self::$_isRunning = true;

        if (PHP_SAPI == 'cli') {
            return;
        }

        try {

            // routing
            $this->_loadRoutesMap();

//			throw new Error404Exception( 'TESTING 404' );

            if (!$route = $this->router->match()) {
                throw new Error404Exception(strip_tags(
                    'Invalid URL: [' . $this->router->getRequestMethod() . ']'
                    . $this->router->getUri()));
            }

            // processing
            if (!$controller = $this->loadController($route, $controllerClass)) {
                throw new Error404Exception('Invalid Controller: ' . $controllerClass);
            }

            if (!method_exists($controller, $this->router->route->action)) {
                throw new Error404Exception('Action not found: '
                    . get_class($controller) . '::' . $this->router->route->action);
            }

            $controller->process($this->router->route->action);

//			pre($this->router);
//			pre($this->router->route);
//			pre($this->document);
//			$this->document->setController($route->controller);
//			$this->document->setLayout($route->action);
        } catch (RedirectException $e) {
            $message = $e->getUserMessage();
            if (!empty($message)) {
                $this->addSystemMessage($e->getUserMessage(), $e->getUserMessageStyle());
            }
            header('Location: ' . $e->getTargetUrl());
            die();
        }
//		catch (CliException $e) {
//			die('CLI');
//			echo $e;
//		}
//		catch (\PDOException $e) {
//			if (defined('CLI_MODE')) die( $e );
//			$this->document->set('e', $e);
//			$this->document->setLayout('errorApp');
//			echo $this->document->render('index');
//		}
//		catch (ErrorApp $e) {
//			$this->document->set('e', $e);
//			$this->document->setLayout('errorApp');
//			echo $this->document->render('index');
//		}
//		catch (DFException $e) {
//			$this->document->set('e', $e);
//			$this->document->setLayout('errorApp');
//			echo $this->document->render('index');
//		}
        catch (Error404Exception $e) {
            $controller = new Error404Controller();
//			$controller->setLayout('error404_display');
            $controller->process('display');
//			dmp((array)$controller, 'controller', 0);
//			dmp((array)$this->document, 'document', 0);
            $this->document->set('e', $e);
//			$this->document->setController('Error404Controller');
//			$this->document->setLayout('error404');
        }

        // render
        echo $this->document->render($controller->getTemplate(), $controller->getLayout());
//		foreach ($_ERROR_STACK as $error) {
//			echo $error;
//		}
    }

    public function addWidget($position, $name, $layout = 'default')
    {
        $widgetClass = '\\df\\extensions\\widgets\\' . $name . '\\' . $name . 'Widget';
        if (class_exists($widgetClass)) {
            $widgetInstance = new $widgetClass($name);
            if (!$widgetInstance instanceof Widget) {
                throw new DFException('Invalid instance of widget: ' . $widgetClass);
            }
            $widgetInstance->setLayout($layout);
            $this->widgets[$position][] = $widgetInstance;
        } else {
            throw new DFException('Unknown widget: ' . $widgetClass);
        }
    }

    public function countWidgets($position)
    {
        if (array_key_exists($position, $this->widgets)) {
            return count($this->widgets[$position]);
        }
        return 0;
    }

    public function getWidgets($position = null)
    {
        if (is_null($position)) {
            return $this->widgets;
        }
        if (array_key_exists($position, $this->widgets)) {
            return $this->widgets[$position];
        }
        return array();
    }

    public function sysMessage()
    {
        if (!isset($_SESSION['__DF']['systemMessages'])) {
            return '';
        }

        $s = '';
        foreach ($_SESSION['__DF']['systemMessages'] as $status => $messages) {
            if (empty($messages)) {
                continue;
            }

            $s .= '<div class="alert alert-' . $status . '">';
            foreach ($messages as $msg) {
                $s .= "<p>$msg</p>";
            }
            $s .= "</div>";
        }

        unset ($_SESSION['__DF']['systemMessages']);
        return $s;
    }

    public function sysMessageExists()
    {
        return isset($_SESSION['__DF']['systemMessages']);
    }

    public function setRedirect($targetUrl, $message = '', $status = App::INFO)
    {
        $this->redirectUrl = '/' . ltrim($targetUrl, '/');
        if (!empty($message)) {
            $this->addSystemMessage($message, $status);
        }
    }

    public function addSystemMessage($message, $status = 'info')
    {
        if (in_array($status, array(self::INFO, self::SUCCESS, self::WARNING, self::ERROR))) {
            $_SESSION['__DF']['systemMessages'][$status][] = $message;
        } else {
            die('Unknown message status.');
        }
    }

    public function __destruct()
    {
//		Dumper::log("STOP APPLICATION");
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    //=======================================================
    //   Private methods
    //=======================================================

    private function __construct()
    {
        if (PHP_SAPI != 'cli') {
            session_start();
        }
        $this->widgets = array();
    }

    private function _loadRoutesMap()
    {
        $routing = include DIR_APP . '/config/routing.php';

        /*if (file_exists(DIR_APP . '/df/component/admin/routing.php')) {
            $adminRouting = include DIR_APP . '/df/component/admin/routing.php';
            foreach ($adminRouting['routes'] as &$admRoute) {
                $admRoute[1] = '/' . $this->config->app->admin_folder . $admRoute[1];
                $admRoute[2] = $this->config->app->admin_folder . '/' . $admRoute[2];
            }
            $routing['matchTypes'] = array_merge($routing['matchTypes'], $adminRouting['matchTypes']);
            $routing['routes']     = array_merge($routing['routes'], $adminRouting['routes']);
        }*/

//		pre($adminRouting, 0);
//		pre($routing,0);

        $this->router->setBasePath(isset($routing['basePath']) ? $routing['basePath'] : '');
        if (isset($routing['matchTypes']) && is_array($routing['matchTypes'])) {
            foreach ($routing['matchTypes'] as $matchType => $matchTypeRegex) {
                $this->router->addMatchTypes(array($matchType => $matchTypeRegex));
            }
        }

        $this->router->map('GET', '/', array(
//			'component'  => $this->config->app->default_component,
            'controller' => $this->config->get('app.default_controller'),
            'action' => $this->config->get('app.default_action'),
        ), 'home');

        foreach ($routing['routes'] as $nameRule => $rule) {
            $method = $rule[0];
            $url = $rule[1];
            $target = explode('/', $rule[2]);
//			array_shift($target);
//			$target = join('#', $target);
            $pi = pathinfo($url);
            $format = 'html';
            if (isset($pi['extension'])
                && in_array($pi['extension'], array('json', 'html', 'xml'))
            ) {
                $format = $pi['extension'];
            }
            $this->router->map(
                $method,
                $url,
//				$target,
                array(
//					'component'  => $target[0],
                    'controller' => $target[0],
                    'action' => $target[1],
                    'format' => $format,
                ),
                is_int($nameRule) ? null : $nameRule
            );
        }

        if ($this->config->get('app.debug')) {
//			$compiledRoutes = $this->router->getRoutingMap();
//			pre($compiledRoutes);
        }
    }

    /**
     * @param Route $route
     * @param string $class
     *
     * @return Controller
     */
    private function loadController(Route $route, &$class)
    {
        $class = "\\df\\extensions\\controllers\\" . $route->controller . "Controller";

        if (class_exists($class)) {
            return new $class();
        }

        return false;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
