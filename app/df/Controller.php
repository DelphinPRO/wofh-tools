<?php

/**
 * Базовый контроллер
 *
 * @since              07.01.13 18:38
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use df\exception\ChangeLayoutException;
use df\exception\RedirectException;

abstract class Controller extends Object
{

    protected $errorCount = 0;
    protected $template;
    protected $layout;
    protected $handles;

    //=======================================================
    //   Public methods
    //=======================================================

    /**
     * Вызывает запрошенный метод контроллера.
     *
     * @param $action
     */
    public function process($action)
    {

        if (strpos($action, '_') === 0) {
            die('Protected method');
        }

        $t = explode('\\', str_replace('Controller', '', get_class($this)) . '/' . $action);

        $accessGranted = true;
        $this->setTemplate('index');
        $this->_setLayout(strtolower(array_pop($t)));

        $routing = include DIR_APP . '/config/routing.php';
        $route = $this->router->route;
        $routeSignature = $this->router->getRouteSignature();

//		$d = rawurldecode($_POST['sdata']);
//		$d = json_decode($d, true);
//
//		pre($d);

        if (array_key_exists('disabled', $routing)) {
            foreach ($routing['disabled'] as $disabledSignature => $data) {
                if (strpos($routeSignature, $disabledSignature) === 0) {
                    $this->_setLayout('error_disabled');
                    $this->set('message', $data);
                    $accessGranted = false;
                    break;
                }
            }
        }

        if ($accessGranted) {
            $this->_before();

            try {
                $this->$action();
            } catch (ChangeLayoutException $e) {
            }

            $this->_after();
        }

        if ($this->router->isPost()) {
            return;
        }

        $this->document->setBreadcrumbs($this->getCrumbs());
        $this->processWidgets();

    }

    protected function getCrumbs()
    {
        return array();
    }

    abstract protected function _before();

    abstract protected function _after();

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param $layout
     *
     * @throws ChangeLayoutException
     */
    public function setLayout($layout)
    {
        $this->_setLayout($layout);
        throw new ChangeLayoutException();
    }

    //=======================================================
    //   Protected methods
    //=======================================================

    final protected function redirect($targetUrl, $message = '', $status = App::INFO)
    {
        throw new RedirectException($targetUrl, $message, $status);
    }

    final protected function addMessage($message, $status = App::INFO)
    {
        App::Inst()->addSystemMessage($message, $status);
        if ($status == App::ERROR) {
            $this->errorCount++;
        }
    }

    final protected function set($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $n => $v) {
                $this->document->set($n, $v);
            }
        } else {
            $this->document->set($name, $value);
        }
    }

    final protected function setPageTitle($title)
    {
        $this->document->setPageTitle($title);
    }

    final private function processWidgets()
    {
        $widgets = $this->app->getWidgets();
        foreach ($widgets as $position => $widgetInstances) {
            foreach ($widgetInstances as $widgetInstance) {
                /** @var Widget $widgetInstance */
                $widgetInstance->callWidget($this);
            }
        }
    }

    final public function get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        } else {
            return false;
        }
    }

    //=======================================================
    //   Private methods
    //=======================================================

    private function _setLayout($layout)
    {
        $this->layout = $layout;
    }

}
