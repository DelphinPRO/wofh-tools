<?php
/**
 * @since           12.07.14 15:48
 * @package
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df\view;

use df\helper\FS;
use df\View;
use df\Widget;

class WidgetView extends View
{

    private $_widget;

    public function __construct(Widget $widget, array $data = array())
    {
        $this->_dfVars = $data;
        $this->_widget = $widget;
    }

    public function render()
    {
        $this->_dfVars = array_merge($this->_dfVars, $this->_widget->getData());

        $template = FS::path('/app/df/extensions/widgets/'
            . $this->_widget->getName() . '/'
            . 'widget_' . $this->_widget->getName() . '_' . $this->_widget->getLayout());
//		pre($template);
//		pre($this);
        return $this->_render($template);
    }
}