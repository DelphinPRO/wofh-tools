<?php
/**
 * @since           11.07.14 18:33
 * @package
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df\view;

use df\exception\ErrorApp;
use df\helper\FS;
use df\View;
use df\Widget;

class Document extends View
{

    private $_layout;
    private $_template;
    private $_pageTitle;
    private $_breadcrumbs;

    public function __construct()
    {
        parent::__construct();
        $this->_pageTitle = '';
        $this->_breadcrumbs = array();
    }

    public function render($templateName, $layout)
    {
//		if ($this->router->route->format == 'json') {
//			$data = $this->_dfVars[$this->__stack];
//			unset( $data['title'], $data['wtData'] );
//			return json_encode_utf8($data);
//		}
        $this->_template = $templateName;
        $this->_layout = $layout;

        $template = FS::path('/app/df/extensions/templates/'
            . $this->config->get('app.template') . '/'
            . $this->_template
        );
//		$template = DIR_APP
//			. DIRECTORY_SEPARATOR . 'templates'
//			. DIRECTORY_SEPARATOR . $this->config->app->template
//			. DIRECTORY_SEPARATOR . $this->_template;

        if (file_exists($template . '.phtml')) {
            return $this->_render($template);
        }

        $template = FS::path('/app/df/system/templates/' . $this->_template);
//		pre($template, 0);
//		pre($this);

        return $this->_render($template);
    }

    public function renderWidgets($position)
    {
        /** @var Widget $widgetInstance */

        $widgets = $this->app->getWidgets($position);

        $html = $position;
        if ($widgets) {
            $html = '';
            foreach ($widgets as $widgetInstance) {
                $render = new WidgetView($widgetInstance, $this->_dfVars);
                $html .= $render->render();
            }
        }
        return $html;
    }

    public function component()
    {
//		pre($this->_dfLayout);
//		$templateFilename = $this->_dfController . "_" . $this->_dfLayout;
        $temp = '';

//		$template = '/app/df/component/' . $this->router->route->component . '/tmpl/' . $templateFilename;
//		$temp .= $template;
//
//		if (!file_exists(FS::path($template . '.phtml'))) {

        $template = '/app/df/extensions/templates/'
            . $this->config->get('app.template') . '/'
            . $this->_layout;
        $temp .= "<br>\n" . $template;
//		}

        if (!file_exists(FS::path($template . '.phtml'))) {
            $template = '/app/df/system/templates/' . $this->_layout;
            $temp .= "<br>\n" . $template;
        }

//		pre($temp);
//		pre($this);
        if (!file_exists(FS::path($template . '.phtml'))) {
//			return 'Component not found: ' . $template . '<br>' .
//			__FILE__ . ':' . __LINE__ . '<br>' .
//			Dumper::printDebugBacktrace(debug_backtrace(false));
            throw new ErrorApp("Template not found: <br>\n" . $temp);
        }

        return $this->_render(FS::path($template));
    }

    public function get($name)
    {
        return $this->_dfVars[$name];
    }

    public function getTitle()
    {
        if ($this->_pageTitle == '') {
            return $this->config->get('app.site_name');
        }

        if ($this->_pageTitle != $this->config->get('app.site_name')) {
            return $this->_pageTitle . ' :: ' . $this->config->get('app.site_name');
        }

        return $this->config->get('app.site_name');
    }

    public function getPageTitle()
    {
        return empty($this->_pageTitle) ? '' : $this->_pageTitle;
    }

    public function setPageTitle($title)
    {
        $this->_pageTitle = $title;
    }

    public function setBreadcrumbs($bc)
    {
        $this->_breadcrumbs = $bc;
    }

    public function breadcrumbs()
    {
//		if ($this->breadcrumbs->isHidden()) return '';
        if (empty($this->_breadcrumbs)) {
            return '';
        }

//		pre($this->_breadcrumbs, 0);

        $html = '<ol class="breadcrumb">';
        foreach ($this->_breadcrumbs as $item) {
            if ($item['routeName'] == $this->router->route->name) {
                $html .= '<li class="active">' . $item['title'] . '</li> ';
            } else {
                $html .= '<li><a href="' . $item['link'] . '">' . $item['title'] . '</a></li> ';
            }
        }
        $html .= '</ol>';
        return $html;
    }

}