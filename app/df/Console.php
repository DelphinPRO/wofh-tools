<?php
/**
 * @since             08.11.2014 10:38
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df;

class Console
{

    public static $buffer = array();

    public static function log($text, $ln = true)
    {
        $text = ($ln) ? $text . PHP_EOL : $text;
        self::$buffer[] = $text;
        echo $text;
    }

    public static function line()
    {
        self::log("-----------------------------------------------");
    }

    public static function doubleLine()
    {
        self::log("===============================================");
    }
}
