<?php
/**
 * @since              08.11.13 01:18
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

class Html
{
    public static function checkbox(array $attr = array())
    {
        $attributes = array(
            'id' => isset($attr['id']) ? ' id="' . $attr['id'] . '"' : '',
            'class' => isset($attr['class']) ? ' class="' . $attr['class'] . '"' : '',
            'value' => isset($attr['value']) ? ' value="' . $attr['value'] . '"' : '',
            'disabled' => isset($attr['disabled']) && $attr['disabled'] ? ' disabled' : '',
            'checked' => isset($attr['checked']) && $attr['checked'] ? ' checked' : '',
        );
        $html = '';
        $html .= '<input type="checkbox"'
            . $attributes['id']
            . $attributes['class']
            . $attributes['value']
            . $attributes['disabled']
            . $attributes['checked']
            . '>';
        return $html;
    }

    public static function checkboxSw(array $attr = array(), $mod = '', $data = array(), $ext = array())
    {
        $id = isset($attr['id']) ? $attr['id'] : '';
        $onText = isset($ext['on']) ? $ext['on'] : 'Вкл';
        $offText = isset($ext['off']) ? $ext['off'] : 'Выкл';
        $attributes = array(
            'id' => isset($attr['id']) ? ' id="' . $attr['id'] . '"' : '',
            'class' => isset($attr['class']) ? ' class="' . $attr['class'] . '"' : '',
            'value' => isset($attr['value']) ? ' value="' . $attr['value'] . '"' : '',
            'disabled' => isset($attr['disabled']) && $attr['disabled'] ? ' disabled' : '',
            'checked' => isset($attr['checked']) && $attr['checked'] ? ' checked' : '',
        );

        $html = '<div class="checkbox-switcher ' . $mod . '">';
        $html .= '<input type="checkbox"'
            . $attributes['id']
            . $attributes['class']
            . $attributes['value']
            . $attributes['disabled']
            . $attributes['checked'];
        foreach ($data as $key => $item) {
            $html .= ' data-' . $key . '="' . $item . '"';
        }
        $html .= ' />';
        $html .= '<label for="' . $id . '">';
        $html .= '<span class="on">' . $onText . '</span>';
        $html .= '<span class="off">' . $offText . '</span>';
        $html .= '<span class="sw"></span>';
        $html .= '</label>';
        $html .= '</div>';
        return $html;
    }
}
