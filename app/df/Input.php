<?php
/**
 * @since           18.07.14 13:50
 * @package         DFramework
 * @author          DelphinPRO delphinpro@yandex.ru
 * @copyright       Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license         Licensed under the MIT license
 */

namespace df;

use df\exception\ErrorApp;

class Input implements \ArrayAccess, \Iterator, \Countable
{

    private $_rgData;

    public function __construct($inputArray)
    {
        $this->_rgData = $inputArray;
    }

    public function getInt($name, $default = 0)
    {
        if (array_key_exists($name, $this->_rgData)) {
            return (int)$this->_rgData[$name];
        } else {
            if (!is_int($default)) {
                throw new ErrorApp('Invalid argument type. Integer required.');
            }
            return $default;
        }
    }

    public function getPage()
    {
        $name = 'page';
        if (array_key_exists($name, $this->_rgData)) {
            return (int)str_replace('page', '', $this->_rgData[$name]);
        } else {
            return 1;
        }
    }

    /**
     * Return the current element
     *
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->_rgData[$this->key()];
    }

    /**
     * Move forward to next element
     *
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        next($this->_rgData);
    }

    /**
     * Return the key of the current element
     *
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return key($this->_rgData);
    }

    /**
     * Checks if current position is valid
     *
     * @return boolean The return value will be casted to boolean and then evaluated.
     *       Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->_rgData[$this->key()]);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        reset($this->_rgData);
    }

    /**
     * Whether a offset exists
     *
     * @param mixed $offset An offset to check for.
     *
     * @return boolean true on success or false on failure.
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this->_rgData[$offset]);
    }

    /**
     * Offset to retrieve
     *
     * @param mixed $offset The offset to retrieve.
     * @param null $default
     *
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset, $default = null)
    {
        if (isset($this->_rgData[$offset])) {
            if (is_array($this->_rgData[$offset])) {
                $this->_rgData[$offset] = new self($this->_rgData[$offset]);
            }
            return $this->_rgData[$offset];
        } else {
            return $default;
        }
    }

    /**
     * Offset to set
     *
     * @param mixed $offset The offset to assign the value to.
     * @param mixed $value The value to set.
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->_rgData[] = $value;
        } else {
            $this->_rgData[$offset] = $value;
        }
    }

    /**
     * Offset to unset
     *
     * @param mixed $offset The offset to unset.
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->_rgData[$offset]);
    }

    /**
     * Count elements of an object
     *
     * @return int The custom count as an integer.
     *       The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->_rgData);
    }
}
