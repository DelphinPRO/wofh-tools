<?php
/**
 * @since              25.05.13 03:00
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use df\exception\CliException;
use df\exception\ErrorApp;
use df\helper\FS;
use df\view\Document;
use WofhTools\Core\Conf\Config;

/**
 * @property Router router
 * @property Config config
 * @property User user
 * @property Document document
 * @property Request request
 * @property App app
 * @property Session session
 */
abstract class Object
{

    public function __get($name)
    {
//		$class     = get_class($this);
//		$namespace = substr($class, 0, strrpos($class, '\\') + 1);
        $name = ucfirst($name);

        if (method_exists(__NAMESPACE__ . '\\Registry', 'get' . $name)) {
            return call_user_func(array(__NAMESPACE__ . '\\Registry', 'get' . $name));
        }

        /*if ($this->user->isAdmin()) {
            echo '<div class="admin-area">';
            pre(array(
                'class'     => $class,
                'namespace' => $namespace,
                'name'      => $name,
            ), __METHOD__, 0);
//			pre(debug_backtrace(false));
            echo '</div>';
            exit;
        }*/

        return null;
    }

    /**
     * @param      $className
     * @param null $property
     *
     * @throws ErrorApp
     * @deprecated
     */
    protected function loadModel($className, $property = null)
    {
        $className = trim($className, '\\');
        if (!is_string($property)) {
            $names = explode('\\', $className);
            $property = array_pop($names);
        }

        if (PHP_SAPI == 'cli') {
            $this->_loadCliModel(strtolower($property), $className);
        } else {
//			$this->_loadModel(strtolower($property), $className);
        }
        throw new ErrorApp('DEPRECATED METHOD ' . __METHOD__);
    }

    final private function _loadCliModel($property)
    {
        $className = ucfirst($property);
        if (file_exists(FS::path('/app/cli/' . $className . '.php'))) {
            require_once FS::path('/app/cli/' . $className . '.php');
        }
        if (class_exists("\\df\\cli\\" . $className)) {
            return Registry::getInstance("\\df\\cli\\" . $className);
        } elseif (class_exists("\\df\\model\\" . $className)) {
            return Registry::getInstance("\\df\\model\\" . $className);
        }
        throw new CliException("Model not exists: " . $className);
    }

    final private function _loadModel($property, $className)
    {
        if (class_exists('\\df\\component\\' . $className)) {
            $this->$property = Registry::getInstance('\\df\\component\\' . $className);
        } elseif (class_exists('\\df\\model\\' . $className)) {
            $this->$property = Registry::getInstance('\\df\\model\\' . $className);
        } else {
            throw new ErrorApp("Model not exists: " . $className);
        }
        throw new ErrorApp('DEPRECATED METHOD ' . __METHOD__);
    }

}
