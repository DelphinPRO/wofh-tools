<?php
/**
 * @since             25.07.14 15:57
 * @package
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df;

class BreadcrumbsOff extends Model
{
    private $_breadcrumbs;

//	private $_hidden = false;

    public function set($crumbs)
    {
        pre($this->router->getRouteSignature($this->router->route->name), 0);
        dmp(array(
            (array)$this->router,
            (array)$this->router->route,
            $crumbs,
        ));
        $this->_breadcrumbs = $crumbs;
    }

//	public function create($routeName)
//	{
//		$result = array();
//		$res    =
//			$this->db
//				->query(
//					"SELECT * FROM df_pages WHERE route = :route",
//					array('route' => $routeName)
//				)
//				->fetchObject();
//		if ($res) {
//			$pageId = (int)$res->id;
//			$this->db->query("CALL get_breadcrumbs($pageId, 0)");
//			while ($row = $this->db->fetch()) {
//				$row['link']           = $this->router->link($row['route']);
//				$result[$row['route']] = $row;
//			}
//		}
//		$this->_breadcrumbs = $result;
//
//		pre($this->router);
//		pre($routeName, 0);
//		pre($result);
//	}

//	public function change($route, $title, $link = null)
//	{
//		if ($title) {
//			$this->_breadcrumbs[$route]['title'] = $title;
//		}
//		if ($link) {
//			$this->_breadcrumbs[$route]['link'] = $this->router->link($route, $link);
//		}
//	}

    public function getArray()
    {
        return is_array($this->_breadcrumbs) ? $this->_breadcrumbs : array();
    }

//	public function getNode($name) {
//		$nodes = $this->getArray();
//		return (isset($nodes[$name])) ? $nodes[$name] : null;
//	}

//	public function hide()
//	{
//		$this->_hidden = true;
//	}

//	public function isHidden()
//	{
//		return $this->_hidden;
//	}
}