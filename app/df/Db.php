<?php
/**
 * @since              24.05.13 20:18
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df;

use WofhTools\Core\Conf\Config;

class Db
{
    public static $QUERIES_LOG = false;
    /** @var \PDO */
    private $pdo;

    /** @var \PDOStatement */
    private $statement;

    /** @var bool */
    private $status;

    /** @var array */
    private $_transactions;

    private $_prefixes;

    private $_lastQuery;

    public function e($value, $paramType = \PDO::PARAM_STR)
    {
        return $this->pdo->quote($value, $paramType);
    }

    public function n($value, $paramType = \PDO::PARAM_STR)
    {
        return is_null($value) ? 'NULL' : $this->pdo->quote($value, $paramType);
    }

    public function simpleQuery($query)
    {
        $query = $this->parsePrefixes($query);
        $this->_lastQuery = $query;
        $this->_addTransaction($query, debug_backtrace(0));
        return $this->pdo->query($query);
    }

    public function prepare($query)
    {
        $query = $this->parsePrefixes($query);
        $this->_lastQuery = $query;
        $this->_addTransaction($query, debug_backtrace(0));
        $this->statement = $this->pdo->prepare($query);
        return $this;
    }

    private function parsePrefixes($query)
    {
        foreach ($this->_prefixes as $prefix => $value) {
            $query = str_replace($prefix, $value, $query);
        }
        return $query;
    }

    public function getLastQuery()
    {
        return $this->_lastQuery;
    }

    public function execute($data)
    {
        $this->statement->execute($data);
        return $this;
    }

    public function status()
    {
        return $this->status;
    }

    public function insertId()
    {
        return $this->pdo->lastInsertId();
    }

    public function rowCount()
    {
        return $this->statement->rowCount();
    }

    public function query($query, array $data = array())
    {
        $this->prepare($query);
        $this->statement->execute($data);
        return $this;
    }

    public function fetchColumn($columnNumber = 0)
    {
        return $this->statement->fetchColumn($columnNumber);
    }

    public function fetchAll($mode = \PDO::FETCH_ASSOC)
    {
        return $this->statement->fetchAll($mode);
    }

    public function fetchObject($className = 'stdClass')
    {
        return $this->statement->fetchObject($className);
    }

    public function fetchObjects($className = 'stdClass')
    {
        return $this->statement->fetchAll(\PDO::FETCH_CLASS, $className);
    }

    public function fetchByField($field, $mode = \PDO::FETCH_ASSOC)
    {
        $tmp = array();
        while ($row = $this->statement->fetch($mode)) {
            $tmp[$row[$field]] = $row;
        }
        return $tmp;
    }

    public function fetchByFieldArray($field, $mode = \PDO::FETCH_ASSOC)
    {
        $tmp = array();
        while ($row = $this->statement->fetch($mode)) {
            $tmp[$row[$field]][] = $row;
        }
        return $tmp;
    }

    public function fetch($mode = \PDO::FETCH_ASSOC)
    {
        return $this->statement->fetch($mode);
    }

    public function beginTransaction()
    {
        $this->pdo->beginTransaction();
    }

    public function inTransaction()
    {
        return $this->pdo->inTransaction();
    }

    public function commit()
    {
        $this->pdo->commit();
    }

    public function rollBack()
    {
        $this->pdo->rollBack();
    }

    public function getError($db = true)
    {
        return $db ? $this->pdo->errorInfo() : $this->statement->errorInfo();
    }

    public function setPrefix($prefixSign, $prefixData)
    {
        $this->_prefixes[$prefixSign] = $prefixData;
    }

    public function __construct()
    {
        if (PHP_SAPI != 'cli') {
            self::$QUERIES_LOG = true;
        }

        $this->_prefixes = array();

        $config = Config::Inst();

        $host = $config->get('db.host');
        $user = $config->get('db.user');
        $pass = $config->get('db.pass');
        $name = $config->get('db.name');

        $this->_transactions = array();
        $this->pdo = new \PDO("mysql:dbname=$name;host=$host", $user, $pass);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo->query("SET NAMES 'UTF8'");

    }

    public function getCountTransactions()
    {
        return count($this->_transactions);
    }

    public function getTransactions()
    {
        return $this->_transactions;
    }

    private function _addTransaction($query, $stackTrace = null)
    {
        if (self::$QUERIES_LOG) {
            $query = str_replace(array("\t", "\n"), array('', ' '), trim($query));
            $this->_transactions[] = array(
                'query' => $query,
                'trace' => $stackTrace,
            );
        } else {
            $this->_transactions[] = array(
                'query' => 0,
                'trace' => 0,
            );
        }
    }

    private function __wakeup()
    {
    }

    private function __clone()
    {
    }
}
