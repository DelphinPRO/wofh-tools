<?php
/**
 * @since             29.08.14 02:08
 * @package           DFramework
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */

namespace df;

use \AltoRouter;

/**
 * Class Router
 *
 * @property Route route
 * @package df
 */
class Router extends AltoRouter implements IRouter
{
    public static $cacheFile;
    public static $adminFolder;

    protected $_requestMethod;
    protected $_requestUri;
    protected $_route;

    public static function buildQuery()
    {
        $get = $_GET;
        unset($get['archive'], $get['sign']);
        $query = http_build_query($get);
        return $query ? '?' . $query : '';
    }

    public function __construct()
    {
        $this->_requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        $this->_requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
    }

    public function __get($name)
    {
        if ($name == 'route') {
            return $this->_route;
        }
        return null;
    }

    public function getRequestMethod()
    {
        return $this->_requestMethod;
    }

    public function getRoutingMap()
    {
        return array('routes' => $this->routes, 'names' => $this->namedRoutes);
    }

    public function getUri()
    {
        return $this->_requestUri;
    }

    public function isPost()
    {
        return $this->_requestMethod == 'POST';
    }

    public function isGet()
    {
        return $this->_requestMethod == 'GET';
    }

    public function link($routeName, array $params = array())
    {
//		$params = array_filter($params);
        return $this->generate($routeName, $params);
    }

    public function match($requestUrl = null, $requestMethod = null)
    {
        $this->_route = parent::match($this->_requestUri, $this->_requestMethod);
        if ($this->_route) {
            $this->_route = new Route($this->_route);
            if ($this->isGet()) {
                foreach ($this->_route->params as $key => $value) {
                    $_GET[$key] = $value;
                }
            }
        }
        return $this->_route;
    }

    public function getRouteSignature($routeName = null)
    {
        if (is_null($routeName)) {
            $routeName = $this->route->name;
        }

        if (array_key_exists($routeName, $this->namedRoutes)) {
            return $this->namedRoutes[$routeName];
        }
        return null;
    }

    public function compare($route)
    {
        $result = false;
        if (is_array($route)) {
            return $this->multiCompare($route);
        }
        $multi = strpos($route, '*') !== false;
        $route = str_replace('*', '', $route);
        $regex = isset($this->namedRoutes[$this->route->name]) ? $this->namedRoutes[$this->route->name] : '';

        if ($multi && strpos($regex, $route) === 0) {
            $result = true;
        } elseif ($regex === $route) {
            $result = true;
        }

//		pre(strpos($regex, $route), 0);
//		pre($multi, 0);
//		pre($route, 0);
//		pre($regex, 0);
//		dump($result, 0);
//		pre($this);
        return $result;
    }

    private function multiCompare($routes)
    {
        foreach ($routes as $route) {
            if ($this->compare($route)) {
                return true;
            }
        }
        return false;
    }

}

class Route
{
//	public $component;
    public $controller;
    public $action;
    public $format;
    public $params;
    public $name;

    public function __construct($route)
    {
//		$this->component  = isset( $route['target']['component'] ) ? $route['target']['component'] : '';
        $this->controller = isset($route['target']['controller']) ? $route['target']['controller'] : '';
        $this->action = isset($route['target']['action']) ? $route['target']['action'] : '';
        $this->format = isset($route['format']) ? $route['format'] : 'html';
        $this->params = isset($route['params']) ? $route['params'] : array();
        $this->name = isset($route['name']) ? $route['name'] : null;
    }
}
