<?php
/**
 * @since              04.10.13 21:56
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

use df\extensions\models\Wofh;
use df\extensions\models\World;
use df\Registry;

/**
 * @param        $data
 * @param string $delimiter
 *
 * @return array
 * @deprecated
 */
function wt_decodeAZ($data, $delimiter = '^')
{
	$array  = explode($delimiter, trim($data, $delimiter));
	$result = array();
	foreach ($array as $res) {
		if (preg_match('/^([a-z])(\d+)$/i', $res, $m)) {
			$result[ord($m[1]) - 97] = (int)$m[2];
		}
	}
	return $result;
}

function UC2UID($unitCode)
{
	static $code;
	if (is_null($code)) {
		$code = array();
		for ($i = 'aa'; $i != 'aaa'; $code[] = $i, $i++) ;
		$code = array_flip($code);
	}
	return $code[$unitCode];
}

function UID2UC($unitId)
{
	static $code;
	if (is_null($code)) {
		$code = array();
		for ($i = 'aa'; $i != 'aaa'; $code[] = $i, $i++) ;
	}
	return $code[$unitId];
}

/**
 * @param $az
 *
 * @return mixed
 * @deprecated
 */
function wt_convert26($az)
{
	static $n26 = array(
		'aa' => 0, 'ab' => 1, 'ac' => 2, 'ad' => 3, 'ae' => 4, 'af' => 5, 'ag' => 6, 'ah' => 7, 'ai' => 8,
		'aj' => 9, 'ak' => 10, 'al' => 11, 'am' => 12, 'an' => 13, 'ao' => 14, 'ap' => 15, 'aq' => 16, 'ar' => 17,
		'as' => 18, 'at' => 19, 'au' => 20, 'av' => 21, 'aw' => 22, 'ax' => 23, 'ay' => 24, 'az' => 25, 'ba' => 26,
		'bb' => 27, 'bc' => 28, 'bd' => 29, 'be' => 30, 'bf' => 31, 'bg' => 32, 'bh' => 33, 'bi' => 34, 'bj' => 35,
		'bk' => 36, 'bl' => 37, 'bm' => 38, 'bn' => 39, 'bo' => 40, 'bp' => 41, 'bq' => 42, 'br' => 43, 'bs' => 44,
		'bt' => 45, 'bu' => 46, 'bv' => 47, 'bw' => 48, 'bx' => 49, 'by' => 50, 'bz' => 51, 'ca' => 52, 'cb' => 53,
		'cc' => 54, 'cd' => 55, 'ce' => 56, 'cf' => 57, 'cg' => 58, 'ch' => 59, 'ci' => 60, 'cj' => 61, 'ck' => 62,
		'cl' => 63, 'cm' => 64, 'cn' => 65, 'co' => 66, 'cp' => 67, 'cq' => 68, 'cr' => 69, 'cs' => 70, 'ct' => 71,
		'cu' => 72, 'cv' => 73, 'cw' => 74, 'cx' => 75, 'cy' => 76, 'cz' => 77, 'da' => 78, 'db' => 79, 'dc' => 80,
		'dd' => 81, 'de' => 82, 'df' => 83, 'dg' => 84, 'dh' => 85, 'di' => 86, 'dj' => 87, 'dk' => 88, 'dl' => 89,
		'dm' => 90, 'dn' => 91, 'do' => 92, 'dp' => 93, 'dq' => 94, 'dr' => 95, 'ds' => 96, 'dt' => 97, 'du' => 98,
		'dv' => 99, 'dw' => 100, 'dx' => 101, 'dy' => 102, 'dz' => 103, 'ea' => 104, 'eb' => 105, 'ec' => 106,
		'ed' => 107, 'ee' => 108, 'ef' => 109, 'eg' => 110, 'eh' => 111, 'ei' => 112, 'ej' => 113, 'ek' => 114,
		'el' => 115, 'em' => 116, 'en' => 117, 'eo' => 118, 'ep' => 119, 'eq' => 120, 'er' => 121, 'es' => 122,
		'et' => 123, 'eu' => 124, 'ev' => 125, 'ew' => 126, 'ex' => 127, 'ey' => 128, 'ez' => 129, 'fa' => 130,
		'fb' => 131, 'fc' => 132, 'fd' => 133, 'fe' => 134, 'ff' => 135, 'fg' => 136, 'fh' => 137, 'fi' => 138,
		'fj' => 139, 'fk' => 140, 'fl' => 141, 'fm' => 142, 'fn' => 143, 'fo' => 144, 'fp' => 145, 'fq' => 146,
		'fr' => 147, 'fs' => 148, 'ft' => 149, 'fu' => 150);
	return $n26[$az];
}

/**
 * @param        $data
 * @param string $delimiter
 * @param bool   $sort
 *
 * @return array
 * @deprecated
 */
function wt_decode26($data, $delimiter = '-', $sort = false)
{
	$array  = explode($delimiter, trim($data, $delimiter));
	$result = array();
	foreach ($array as $item) {
		if (preg_match('/^([a-z][a-z])(\d+)$/i', $item, $m)) {
			$id          = wt_convert26($m[1]);
			$result[$id] = (int)$m[2];
		}
	}
	ksort($result);
	return $result;
}

function getSignedNumber($number, $zero = null)
{
	if (is_null($number)) {
		return is_null($zero) ? '' : $zero;
	}

	$number = (int)$number;
	if ($number == 0) return is_null($zero) ? $number : $zero;
	$present = preg_replace('~(\d(?=(?:\d{3})+(?!\d)))~s', "\\1 ", intval(abs($number)));
	return ( $number > 0 ) ? ( '+ ' . $present ) : ( '- ' . $present );
}

function getStatusClass($value)
{
	$value = intval($value);
	if ($value > 0) return 'text-success';
	if ($value < 0) return 'text-danger';
	return '';
}

/**
 * @param $name
 * @param $id
 * @param $sign
 *
 * @return string
 * @deprecated
 */
function playerLink($name, $id, $sign)
{
	return '<a href="/stat/' . strtolower($sign) . '/account/' . $id . '/">' . $name . '</a>';
}

/**
 * @param $name
 * @param $id
 * @param $sign
 *
 * @return string
 * @deprecated
 */
function countryLink($name, $id, $sign)
{
	return '<a href="/stat/' . strtolower($sign) . '/countries/' . $id . '/">' . $name . '</a>';
}

/**
 * @param $event
 * @param $sign
 *
 * @return string
 * @deprecated
 */
function getTextEvent($event, $sign)
{
	$text  = '';
	$world = Wofh::getWorldBySign(strtolower($sign));

	switch ($event['eventId']) {
		case Wofh::EVENT_TOWN_DESTROY:
			if (stripos($event['townTitle'], 'город') === false) $text .= "город ";
			$text .= '<span class="text-danger">';
			$text .= " <a href=\"/stat/" . strtolower($sign) . "/town/{$event['townId']}/\">{$event['townTitle']}</a>";
			$text .= " разрушен";
			$text .= '</span>';
			break;
		case Wofh::EVENT_TOWN_CREATE:
			$text .= '<span class="text-success">' . playerLink($event['accountName'], $event['accountId'], $sign);
			$text .= " основал" . ( $event['accountSex'] ? '' : 'a' );
			if (stripos($event['townTitle'], 'город') === false) $text .= " город";
			$text .= " <a href=\"/stat/" . strtolower($sign) . "/town/{$event['townId']}/\">{$event['townTitle']}</a>";
			$text .= '</span>';
			break;
		case Wofh::EVENT_TOWN_LOST:
			$text .= '<span class="text-danger">';
			$text .= playerLink($event['accountName'], $event['accountId'], $sign)
				. " потерял" . ( $event['accountSex'] ? '' : 'a' ) . " город ";
			$text .= "<a href=\"/stat/" . strtolower($sign) . "/town/{$event['townId']}/\">{$event['townTitle']}</a>";
			$text .= '</span>';
			break;
		case Wofh::EVENT_TOWN_RENAME:
			$text .= playerLink($event['accountName'], $event['accountId'], $sign);
			$text .= " переименовал" . ( $event['accountSex'] ? '' : 'a' );
			if (stripos($event['townTitleOld'], 'город') === false) $text .= " город";
			$text .= " <span class=\"text-muted\">{$event['townTitleOld']}</span>";
			$text .= " в <a href=\"/stat/" . strtolower($sign) . "/town/{$event['townId']}/\">{$event['townTitle']}</a>";
			break;
		case Wofh::EVENT_WONDER_CREATE:
			$text .= playerLink($event['accountName'], $event['accountId'], $sign);
			$text .= " начал" . ( $event['accountSex'] ? '' : 'a' );
			$text .= " строить <a href=\"#\">{$event['wonderTitle']}</a>";
			break;
		case Wofh::EVENT_ACCOUNT_CREATE:
			$text = "В мире появился новый игрок: " . playerLink($event['accountName'], $event['accountId'], $sign);
			break;
		case Wofh::EVENT_ACCOUNT_DELETE:
			$text = playerLink($event['accountName'], $event['accountId'], $sign)
				. " удалил" . ( $event['accountSex'] ? '' : 'a' ) . " аккаунт";
			break;
		case Wofh::EVENT_ACCOUNT_COUNTRY_IN:
			$text = '<span class="text-success">' . playerLink($event['accountName'], $event['accountId'], $sign)
				. " присоединил" . ( $event['accountSex'] ? 'ся' : 'ась' ) . " к стране "
				. countryLink($event['countryTitle'], $event['countryId'], $sign)
				. "</span>";
			break;

		case Wofh::EVENT_ACCOUNT_COUNTRY_OUT:
			$text = '<span class="text-danger">' . playerLink($event['accountName'], $event['accountId'], $sign)
				. " покинул" . ( $event['accountSex'] ? '' : 'а' ) . " страну "
				. countryLink($event['countryTitle'], $event['countryId'], $sign)
				. "</span>";
			break;
		case Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE:
			$countryId = isset( $_GET['countryId'] ) ? abs((int)$_GET['countryId']) : 0;
			if ($countryId) {
				if ($countryId == $event['countryIdFrom']) {
					$text .= '<span class="text-danger">';
					$text .= playerLink($event['accountName'], $event['accountId'], $sign);
					$text .= " у" . ( (int)$event['accountSex'] ? 'шел' : 'шла' );
					$text .= " в страну";
					$text .= " <a href=\"/stat/" . strtolower($sign) . "/countries/{$event['countryId']}/\">{$event['countryTitle']}</a>";
					$text .= " <img src=\"" . $world->getFlag($event['countryFlag']) . "\" alt=\"\">";
					$text .= '</span>';
				}
				if ($countryId == $event['countryId']) {
					$text .= '<span class="text-success">';
					$text .= playerLink($event['accountName'], $event['accountId'], $sign);
					$text .= " при" . ( (int)$event['accountSex'] ? 'шел' : 'шла' );
					$text .= " из страны";
					$text .= " <a href=\"/stat/" . strtolower($sign) . "/countries/{$event['countryIdFrom']}/\">{$event['countryTitleFrom']}</a>";
					$text .= " <img src=\"" . $world->getFlag($event['countryFlagFrom']) . "\" alt=\"\">";
					$text .= '</span>';
				}
			} else {
				$text .= playerLink($event['accountName'], $event['accountId'], $sign);
				$text .= " переш" . ( (int)$event['accountSex'] ? 'ел' : 'ла' );
				$text .= " из страны <img src=\"" . $world->getFlag($event['countryFlagFrom']) . "\" alt=\"\"><a href=\"/stat/" . strtolower($sign) . "/country/{$event['countryIdFrom']}/\">{$event['countryTitleFrom']}</a>";
				$text .= " в <img src=\"" . $world->getFlag($event['countryFlag']) . "\" alt=\"\"><a href=\"/stat/" . strtolower($sign) . "/country/{$event['countryId']}/\">{$event['countryTitle']}</a>";
			}
			break;
		case Wofh::EVENT_COUNTRY_CREATE:
			$text .= "Основана страна ";
			$text .= countryLink($event['countryTitle'], $event['countryId'], $sign);
			break;
		case Wofh::EVENT_COUNTRY_DESTROY:
			$text = "Страна <a href=\"#\">{$event['countryTitle']}</a> прекратила свое существование";
			break;
		case Wofh::EVENT_COUNTRY_RENAME:
			$text = "Страна <span class=\"text-muted\">{$event['countryTitleOld']}</span> переименована в "
				. countryLink($event['countryTitle'], $event['countryId'], $sign);
			break;
		case Wofh::EVENT_COUNTRY_FLAG:
			$text .= "Страна поменяла флаг ";
			$text .= "с <img class=\"flag-mini\" src='" . $world->getFlag($event['countryFlagOld']) . "'> ";
			$text .= "на <img class=\"flag-mini\" src='" . $world->getFlag($event['countryFlag']) . "'>";
			break;
		case Wofh::EVENT_ACCOUNT_RENAME:
			$text .= "Игрок ";
			$text .= playerLink($event['accountName'], $event['accountId'], $sign);
			$text .= " переименовал аккаунт";
			$text .= " (<s>" . $event['accountNameOld'] . "</s>)";
			break;
		default:
			$text = getNameEvent($event['eventId']);
	}
	return $text;
}

/** @deprecated */
function getIconEvent($eventId)
{
	$data = array(
		20 => 'event-account-create',
		25 => 'event-account-rename',
		24 => 'event-account-delete',
		21 => 'event-account-country-in',
		22 => 'event-account-country-out',
		23 => 'event-account-country-change',
		1  => 'event-town-create',
		2  => 'event-town-rename',
		3  => 'event-town-lost',
		4  => 'event-town-destroy',
		40 => 'event-country-create',
		41 => 'event-country-flag',
		42 => 'event-country-rename',
		43 => 'event-country-destroy',
		61 => 'event-wonder-create',
		60 => 'event-wonder-destroy',
		62 => 'event-wonder-activate',
	);
	if (isset( $data[$eventId] )) {
		return '/img/icons/' . $data[$eventId] . '.png';
	} else {
		return '';
	}
}

function parseTmpl($tmpl, $params)
{
	foreach ($params as $key => $value) {
		$tmpl = str_replace('{%' . $key . '%}', $value, $tmpl);
	}
	return $tmpl;
}

function createCountryLink(array $event, World $world)
{
	$router = Registry::getRouter();
	$link   = $router->link('statCountry', array(
		'sign'      => $world->lowerSign,
		'countryId' => $event['countryId'],
	));
	return '<a href="' . $link . '">' . $event['extra']['countryName'] . '</a>';
}

function createAccountLink(array $event, World $world)
{
	$router = Registry::getRouter();
	$link   = $router->link('statAccount', array(
		'sign'      => $world->lowerSign,
		'accountId' => $event['accountId'],
	));
	return '<a href="' . $link . '">' . $event['extra']['accountName'] . '</a>';
}

function createTownLink(array $event, World $world)
{
	$router = Registry::getRouter();
	$link   = $router->link('statTown', array(
		'sign'   => $world->lowerSign,
		'townId' => $event['townId'],
	));
	return '<a href="' . $link . '">' . $event['extra']['townName'] . '</a>';
}

function createFlagImage($flag, World $world)
{
	return '<img src="' . $world->getFlag($flag) . '">';
}

function createRoleText($role)
{
	$names = array(1 => 'Глава', 2 => 'Мудрец', 4 => 'Казначей', 8 => 'Воевода', 16 => 'Помощник воеводы', 32 => 'Второй помощник воеводы', 64 => '64', 128 => '128');
	return '<span class="role role-' . $role . '"> ' . $names[$role] . '</span>';
}

/** @deprecated */
function getTextOfEvent(array $event, World $world)
{
	$tmpl = array(
		Wofh::EVENT_TOWN_CREATE            => '<span class="text-success text-event">{%accountName%} основал{%sex%} {%town%} {%townName%}</span>',
		Wofh::EVENT_TOWN_DESTROY           => '<span class="text-danger text-event">Разрушен {%town%} {%townName%} (бывш{%sex1%} владел{%sex2%} {%accountName%})</span>',
		Wofh::EVENT_TOWN_RENAME            => '<span class="text-event">{%accountName%} переименовал{%sex%} {%town%} {%townNameFrom%} в {%townName%}</span>',
		Wofh::EVENT_TOWN_LOST              => '<span class="text-danger text-event">{%accountName%} потерял{%sex%} {%town%} {%townName%}</span>',
		Wofh::EVENT_WONDER_DESTROY         => '<span class="text-danger text-event">Чудо <span style="font-size:1.2em">{%wonder%}</span> разрушено в {%townName%}, владелец {%accountName%}</span>',
		Wofh::EVENT_WONDER_CREATE          => '<span class="text-success text-event">{%accountName%} начал{%sex%} строить <span style="font-size:1.2em">{%wonder%}</span> в {%townName%}</span>',
		Wofh::EVENT_WONDER_ACTIVATE        => '<span class="text-success text-event">Чудо <span style="font-size:1.2em">{%wonder%}</span> активировал{%sex%} {%accountName%} в городе {%townName%}</span>',
		Wofh::EVENT_ACCOUNT_CREATE         => '<span class="text-success text-event">Новый игрок приходит в мир: {%accountName%}</span>',
		Wofh::EVENT_ACCOUNT_DELETE         => '<span class="text-danger text-event">{%accountName%} удалил аккаунт</span>',
		Wofh::EVENT_ACCOUNT_COUNTRY_IN     => '<span class="text-success text-event">{%accountName%} вступил в страну {%flag%} {%country%}</span>',
		Wofh::EVENT_ACCOUNT_COUNTRY_OUT    => '<span class="text-danger text-event">{%accountName%} покинул страну {%flag%} {%country%}</span>',
		Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE => '<span class="text-event">{%accountName%} перешел из страны {%flagFrom%} {%countryFrom%} в {%flag%} {%country%}</span>',
		Wofh::EVENT_ACCOUNT_ROLE_IN        => '<span class="text-event">{%accountName%} получил должность {%role%} в стране {%flag%} {%country%}</span>',
		Wofh::EVENT_ACCOUNT_ROLE_OUT       => '<span class="text-event">{%accountName%} уволен с должности {%role%} в стране {%flag%} {%country%}</span>',
		Wofh::EVENT_COUNTRY_CREATE         => '<span class="text-success text-event">Основана страна {%flag%} {%country%}</span>',
		Wofh::EVENT_COUNTRY_FLAG           => '<span class="text-event">Страна {%country%} сменила государственный флаг {%flagFrom%} -> {%flag%}</span>',
		Wofh::EVENT_COUNTRY_RENAME         => '<span class="text-event">Страна {%countryFrom%} теперь называется {%flag%} {%country%}</span>',
		Wofh::EVENT_COUNTRY_DESTROY        => '<span class="text-danger text-event">Страна {%flag%} {%country%} больше не существует</span>',
	);

	$html = '';
	switch ($event['eventId']) {
		case Wofh::EVENT_TOWN_CREATE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'town'        => ( stripos($event['extra']['townName'], 'город') === false ? "город" : '' ),
				'townName'    => createTownLink($event, $world),
				'sex'         => ( $event['extra']['accountSex'] ? '' : 'a' ),
			));
			break;

		case Wofh::EVENT_TOWN_DESTROY:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'town'        => ( stripos($event['extra']['townName'], 'город') === false ? "город" : '' ),
				'townName'    => createTownLink($event, $world),
				'sex1'        => ( $event['extra']['accountSex'] ? 'ий' : 'ая' ),
				'sex2'        => ( $event['extra']['accountSex'] ? 'ец' : 'ица' ),
			));
			break;

		case Wofh::EVENT_TOWN_LOST:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'town'        => ( stripos($event['extra']['townName'], 'город') === false ? "город" : '' ),
				'townName'    => createTownLink($event, $world),
				'sex'         => ( $event['extra']['accountSex'] ? '' : 'a' ),
			));
			break;

		case Wofh::EVENT_TOWN_RENAME:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName'  => createAccountLink($event, $world),
				'town'         => ( stripos($event['extra']['townName'], 'город') === false ? "город" : '' ),
				'townName'     => createTownLink($event, $world),
				'townNameFrom' => $event['extra']['townNameFrom'],
				'sex'          => ( $event['extra']['accountSex'] ? '' : 'a' ),
			));
			break;

		case Wofh::EVENT_WONDER_DESTROY:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'wonder'      => $event['extra']['wonderName'],
				'townName'    => createTownLink($event, $world),
				'sex'         => ( $event['extra']['accountSex'] ? '' : 'a' ),
			));
			break;

		case Wofh::EVENT_WONDER_CREATE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'wonder'      => $event['extra']['wonderName'],
				'townName'    => createTownLink($event, $world),
				'sex'         => ( $event['extra']['accountSex'] ? '' : 'a' ),
			));
			break;

		case Wofh::EVENT_WONDER_ACTIVATE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'wonder'      => $event['extra']['wonderName'],
				'townName'    => createTownLink($event, $world),
				'sex'         => ( $event['extra']['accountSex'] ? '' : 'a' ),
			));
			break;

		case Wofh::EVENT_ACCOUNT_CREATE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
			));
			break;

		case Wofh::EVENT_ACCOUNT_DELETE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
			));
			break;

		case Wofh::EVENT_ACCOUNT_COUNTRY_IN:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'flag'        => createFlagImage($event['extra']['countryFlag'], $world),
				'country'     => createCountryLink($event, $world),
			));
			break;

		case Wofh::EVENT_ACCOUNT_COUNTRY_OUT:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'flag'        => createFlagImage($event['extra']['countryFlag'], $world),
				'country'     => createCountryLink($event, $world),
			));
			break;

		case Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'flag'        => createFlagImage($event['extra']['countryFlag'], $world),
				'country'     => createCountryLink($event, $world),
				'flagFrom'    => createFlagImage($event['extra']['countryFlagFrom'], $world),
				'countryFrom' => '<a href="' . Registry::getRouter()->link('statCountry', array(
						'sign'   => $world->lowerSign,
						'townId' => $event['countryIdFrom'],
					)) . '">' . $event['extra']['countryNameFrom'] . '</a>',
			));
			break;

		case Wofh::EVENT_ACCOUNT_ROLE_IN:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'flag'        => createFlagImage($event['extra']['countryFlag'], $world),
				'country'     => createCountryLink($event, $world),
				'role'        => createRoleText($event['role']),
			));
			break;

		case Wofh::EVENT_ACCOUNT_ROLE_OUT:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'accountName' => createAccountLink($event, $world),
				'flag'        => createFlagImage($event['extra']['countryFlag'], $world),
				'country'     => createCountryLink($event, $world),
				'role'        => createRoleText($event['role']),
			));
			break;

		case Wofh::EVENT_COUNTRY_CREATE:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'flag'    => createFlagImage($event['extra']['countryFlag'], $world),
				'country' => createCountryLink($event, $world),
			));
			break;

		case Wofh::EVENT_COUNTRY_RENAME:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'flag'        => createFlagImage($event['extra']['countryFlag'], $world),
				'country'     => createCountryLink($event, $world),
				'countryFrom' => $event['extra']['countryNameFrom'],
			));
			break;

		case Wofh::EVENT_COUNTRY_FLAG:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'flag'     => createFlagImage($event['extra']['countryFlag'], $world),
				'flagFrom' => createFlagImage($event['extra']['countryFlagFrom'], $world),
				'country'  => createCountryLink($event, $world),
			));
			break;

		case Wofh::EVENT_COUNTRY_DESTROY:
			$html .= parseTmpl($tmpl[$event['eventId']], array(
				'flag'    => createFlagImage($event['extra']['countryFlag'], $world),
				'country' => createCountryLink($event, $world),
			));
			break;

		default:
			$html = '<span>' . getNameEvent($event['eventId']) . '</span>';
	}

	if ($event['countryId'] > 0
		&& $event['eventId'] != Wofh::EVENT_ACCOUNT_COUNTRY_IN
		&& $event['eventId'] != Wofh::EVENT_ACCOUNT_COUNTRY_OUT
		&& $event['eventId'] != Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE
		&& $event['eventId'] != Wofh::EVENT_ACCOUNT_ROLE_IN
		&& $event['eventId'] != Wofh::EVENT_ACCOUNT_ROLE_OUT
		&& $event['eventId'] != Wofh::EVENT_COUNTRY_CREATE
		&& $event['eventId'] != Wofh::EVENT_COUNTRY_DESTROY
		&& $event['eventId'] != Wofh::EVENT_COUNTRY_RENAME
		&& $event['eventId'] != Wofh::EVENT_COUNTRY_FLAG
	) {
		$html .= ' (<img src="' . $world->getFlag($event['extra']['countryFlag']) . '"> '
			. $event['extra']['countryName'] . ')';
	}
	return $html;
}

function getNameEvent($eventId)
{
	$data = array(
		Wofh::EVENT_TOWN_CREATE            => 'EVENT_TOWN_CREATE',
		Wofh::EVENT_TOWN_RENAME            => 'EVENT_TOWN_RENAME',
		Wofh::EVENT_TOWN_LOST              => 'EVENT_TOWN_LOST',
		Wofh::EVENT_TOWN_DESTROY           => 'EVENT_TOWN_DESTROY',

		Wofh::EVENT_ACCOUNT_CREATE         => 'EVENT_ACCOUNT_CREATE',
		Wofh::EVENT_ACCOUNT_COUNTRY_IN     => 'EVENT_ACCOUNT_COUNTRY_IN',
		Wofh::EVENT_ACCOUNT_COUNTRY_OUT    => 'EVENT_ACCOUNT_COUNTRY_OUT',
		Wofh::EVENT_ACCOUNT_COUNTRY_CHANGE => 'EVENT_ACCOUNT_COUNTRY_CHANGE',
		Wofh::EVENT_ACCOUNT_DELETE         => 'EVENT_ACCOUNT_DELETE',
		Wofh::EVENT_ACCOUNT_RENAME         => 'EVENT_ACCOUNT_RENAME',

		Wofh::EVENT_COUNTRY_CREATE         => 'EVENT_COUNTRY_CREATE',
		Wofh::EVENT_COUNTRY_FLAG           => 'EVENT_COUNTRY_FLAG',
		Wofh::EVENT_COUNTRY_RENAME         => 'EVENT_COUNTRY_RENAME',
		Wofh::EVENT_COUNTRY_DESTROY        => 'EVENT_COUNTRY_DESTROY',

		Wofh::EVENT_WONDER_DESTROY         => 'EVENT_WONDER_DESTROY',
		Wofh::EVENT_WONDER_CREATE          => 'EVENT_WONDER_CREATE',
		Wofh::EVENT_WONDER_ACTIVATE        => 'EVENT_WONDER_ACTIVATE',
	);
	return isset( $data[$eventId] ) ? $data[$eventId] . ' (' . $eventId . ')' : 'EventId - ' . $eventId;
}