<?php
/**
 * @since              09.12.13 23:19
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */
namespace df\cli;

class StatLoader
{
//	private $timestamp;
	private $realLoad;
	private $stateDate;
	private $stateDateTime;
	private $worldNum;
	private $worldId;
	private $sign;
	private $sourceStatUri;
	private $country;

	private function _start($sign, $date = null)
	{
		$this->realLoad      = is_null($date);
		$this->stateDate     = $this->realLoad ? date('Y-m-d') : $date;
		$this->stateDateTime = $this->realLoad ? date('Y-m-d H:i:s') : $date . ' 00:00:00';

		if (preg_match('/ru(\d+)/', $sign, $m)) {
			$this->worldNum = $m[1];
			$this->worldId  = getWorldId(1, $this->worldNum);
			$this->sign     = $sign;
			$this->country  = 'ru';

			$this->sourceStatUri = $this->realLoad
				? "https://w{$this->worldNum}.wofh.ru/aj_statistics"
				: DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . strtolower($sign) . DIRECTORY_SEPARATOR . "stat-" . strtolower($sign) . "-" . $this->stateDate . ".json";
		}

		if (preg_match('/en(\d+)/', $sign, $m)) {
			$this->worldNum = $m[1];
			$this->worldId  = getWorldId(2, $this->worldNum);
			$this->sign     = $sign;
			$this->country  = 'en';

			$this->sourceStatUri = $this->realLoad
				? "https://en{$this->worldNum}.waysofhistory.com/aj_statistics"
				: DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . strtolower($sign) . DIRECTORY_SEPARATOR . "stat-" . strtolower($sign) . "-" . $this->stateDate . ".json";
		}

	}

	private function _checkLastDownload()
	{
		writeLog("Check last download ...", false);

		$lastDownload = db()->query("SELECT loadStat FROM wt_worlds WHERE id = {$this->worldId}")->fetchColumn();
		$lastDownload = strtotime($lastDownload);

		if (!$lastDownload) {
			writeLog("... never");
		} else {
			writeLog("... " . date('Y-m-d H:i:s', $lastDownload));

			$oldDate = new DateTime( date('Y-m-d', $lastDownload) );
			$newDate = new DateTime( $this->stateDate );
			$hour    = (int)date('G');

			writeLog("Old date .................  " . date('Y-m-d', $lastDownload));
			writeLog("New date .................  " . $this->stateDate);

			if ($newDate > $oldDate) {
				if ($this->realLoad && $hour >= X) return true;
				if (!$this->realLoad) return true;
			}

			writeLog("No download required ..... ", false);
			$dt = new DateTime( '@' . time() );
			$di = $dt->diff(new DateTime( '@' . $lastDownload ));
			writeLog($di->format('%d days, %h hours, %i min, %s seconds'));
			return false;
		}
		return true;
	}

	private function _writeLastDownload($timestamp)
	{
		$date = date('Y-m-d H:i:s', $timestamp);
		db()->query("UPDATE wt_worlds SET loadStat = '$date' WHERE id = {$this->worldId}");
	}

	public function get($sign, $date = null)
	{
		$start = microtime(true);
		$this->_start($sign, $date);

		writeLog("Processing for ........... " . $this->stateDateTime);
		writeLog("Load statistic for ....... {$this->sign} (" . str_replace(DIR_ROOT, '', $this->sourceStatUri) . ")");

		if (!$this->_checkLastDownload()) return false;

		writeLog("Start downloading ...", false);
		if (!$sData = get_remote_file($this->sourceStatUri)) {
			throw new Exception( "Error load statistic\nSource: {$this->sourceStatUri}" );
		}
		writeLog("..... success (" . round(microtime(true) - $start, 2) . "c)");

		$this->_checkStatisticObject($sData);

		writeLog("Saving data ...", false);
		$rgData = json_decode($sData, true);
		$start  = microtime(true);
		if (!is_dir(DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $this->country . $this->worldNum)) {
			if (!$res = @mkdir(DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $this->country . $this->worldNum, 0777, true)) {
				throw new Exception( "Error make dir: " . "/tmp/" . $this->country . $this->worldNum );
			}
		}

		$date = isset( $rgData['time'] ) ? date('Y-m-d', $rgData['time']) : $this->stateDate;
		if ($this->realLoad) {
			if (file_put_contents(DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $this->country . $this->worldNum . DIRECTORY_SEPARATOR . "stat-{$this->country}{$this->worldNum}-{$date}.json", $sData)) {
				$this->_writeLastDownLoad(isset( $rgData['time'] ) ? $rgData['time'] : time());
				writeLog("........... success (" . round(microtime(true) - $start, 2) . "c)");
			} else {
				writeLog("........... FAIL (" . round(microtime(true) - $start, 2) . "c)");
			}
		}

		CLI::$sendEmail = true;
		return true;
	}

	/**
	 * @param $sData
	 *
	 * @throws Exception
	 */
	private function _checkStatisticObject(&$sData)
	{
		$errorCheck = false;
		$s          = '';

		writeLog("Check object structure ...", false);
		$rgData = json_decode($sData, true);
		if (( $errorCode = json_last_error() ) != JSON_ERROR_NONE) {
			throw new Exception( getJsonErrorMessage($errorCode) );
		}

		if (is_null($rgData['countries'])) $rgData['countries'] = array();
		if (!isset( $rgData['accounts'], $rgData['countries'], $rgData['time'], $rgData['towns'] ) && count($rgData) == 4) {
			$errorCheck = true;
			$s          = 'Top Level object';
		} else {
			$check = array_pop($rgData['accounts']);
			if (!( count($check) == 4 && is_string($check[0]) && is_int($check[1]) && is_int($check[2]) && is_int($check[3]) )) {
				$errorCheck = true;
				$s          = 'Accounts object';
			}

			if (!empty( $rgData['countries'] )) {
				$check = array_pop($rgData['countries']);
				if (!( count($check) == 2 && is_string($check[0]) && is_string($check[1]) )) {
					$errorCheck = true;
					$s          = 'Countries object';
				}
			}

			$check = array_pop($rgData['towns']);
			if (!( count($check) == 3 && is_string($check[0]) && is_int($check[1]) && is_int($check[2]) )) {
				$errorCheck = true;
				$s          = 'Towns object';
			}

			if (!is_int($rgData['time'])) {
				$errorCheck = true;
				$s          = 'Time object';
			}
		}

		$date = isset( $rgData['time'] ) ? date('Y-m-d', $rgData['time']) : $this->stateDate;

		if ($errorCheck) {
			writeLog(" Fail");
			file_put_contents(DIR_ROOT . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . "fail-stat-{$this->country}{$this->worldNum}-{$date}.json", $sData);
			throw new Exception( "Invalid stat object structure\n$s" );
		}

		writeLog(" OK"); // check structure

		unset( $rgData, $check );
	}
}
