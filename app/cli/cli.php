<?php
/**
 * @since              04.12.13 21:39
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */
exit;
ini_set('display_errors', 'Off');
error_reporting(0);

defined('__DIR__') or define( '__DIR__', dirname(__FILE__) );
define ( 'CLI_MODE', true );
define ( 'DIR_ROOT', realpath(__DIR__ . '/../../') );

//=========================================================

$startTime = microtime(true);
$LOG       = array();

$mailTo      = 'delphinpro@yandex.ru';
$mailSubject = 'wofh-tools.ru: cron/';
$headers     = 'From: cron@wofh-tools.ru' . "\r\n"
	. 'X-Mailer: Wofh-Tools.ru' . "\r\n"
	. 'Content-type: text/plain; charset=utf-8';

set_time_limit(0);
date_default_timezone_set("Europe/Moscow");

//=========================================================

include DIR_ROOT . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'wt.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'CliParser.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'StatLoader.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'StatUpdater.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'UpdaterCountries.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'UpdaterPlayers.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'UpdaterTowns.php';
include __DIR__ . DIRECTORY_SEPARATOR . 'UpdaterEvents.php';

//=========================================================

$CLI = new CLI();
$CLI->run();

//=========================================================

/**
 * Command line interface
 * Class CLI
 *
 * @deprecated
 */
class CLI2
{
	public static $outputStd;
	public static $outputCompact;
	public static $sendEmail;

	/** @var \CLI_Parser */
	private $rParser;

	/** @var Array */
	private $rgArgs;

	private $stateDate;
	private $stateDateTime;

	//=======================================================
	//   Public methods
	//=======================================================

	/**
	 * @deprecated
	 */
	public function run()
	{
		try {
			$action = $this->_getAction();
			$sign   = isset( $this->rgArgs['world'] ) && preg_match('/(ru|en)[\d]+/', $this->rgArgs['world']) ? $this->rgArgs['world'] : null;
			if ($action == 'getStat' || $action == 'updateStat') {
				if ($sign) $this->$action($sign);
			} else {
				writeLog("--load=const      updates const data");
				writeLog("--load=stat       load statistic data from wofh servers");
				writeLog("--update=stat     update statistic data");
				writeLog("--world=ru17      identify wofh server");
				writeLog("--date=07122013   update statistic use point date");
				writeLog("--output          direct output");
				writeLog("--output=compact  compact direct output");
			}
		}
		catch (Exception $e) {
			CLI::$sendEmail = true;
			if (db()->inTransaction()) {
				db()->rollBack();
			}
			writeLog("\n-----------------------------------------------");
			writeLog($e->getMessage());
			writeLog($e->getFile() . " - ", false);
			writeLog($e->getLine());
			writeLog($e->getTraceAsString());
		}

		$this->_halt();
		exit;
	}

	/**
	 * @param string $sign Identify world (example: ru17)
	 *
	 * @deprecated
	 */
	public function getStat($sign)
	{
		global $mailSubject;

		$mailSubject .= 'getStat ' . $sign;
		$date = null;
		if (isset( $this->rgArgs['date'] ) && preg_match('/^(\d{2})(\d{2})(\d{4})$/', $this->rgArgs['date'], $m)) {
			$date = "{$m[3]}-{$m[2]}-{$m[1]}";
		}

		$loader = new StatLoader();
		$loader->get($sign, $date);
	}

	/**
	 * @param string $sign Identify world (example: ru17)
	 *
	 * @deprecated
	 */
	public function updateStat($sign)
	{
		global $startTime, $mailSubject;

		$mailSubject .= 'updateStat ' . $sign;
		$date = date('Y-m-d', $startTime);
		if (isset( $this->rgArgs['date'] ) && preg_match('/^(\d{2})(\d{2})(\d{4})$/', $this->rgArgs['date'], $m)) {
			$date = "{$m[3]}-{$m[2]}-{$m[1]}";
		}

		$updater = new StatUpdater();
		$updater->update($sign, $date);
	}

	//=======================================================
	//   Private methods
	//=======================================================

	private function _halt()
	{
		exit;
	}

}
