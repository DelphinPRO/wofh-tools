-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.35-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.2.0.4690
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table delphinpro_wt.cd_builds
CREATE TABLE IF NOT EXISTS `cd_builds` (
  `worldId` int(10) unsigned NOT NULL,
  `buildId` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `buildType` tinyint(3) unsigned NOT NULL,
  `bt0` float unsigned NOT NULL,
  `bt1` float unsigned NOT NULL,
  `bt2` float unsigned NOT NULL,
  `bt3` float unsigned NOT NULL,
  `eff0` float unsigned NOT NULL,
  `eff1` float unsigned NOT NULL,
  `eff2` float unsigned NOT NULL,
  `eff3` float unsigned NOT NULL,
  `ug0` float unsigned NOT NULL,
  `ug1` float unsigned NOT NULL,
  `ug2` float unsigned NOT NULL,
  `ug3` float unsigned NOT NULL,
  `cost` varchar(255) NOT NULL,
  PRIMARY KEY (`worldId`,`buildId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.cd_resource
CREATE TABLE IF NOT EXISTS `cd_resource` (
  `worldId` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `prodType` tinyint(3) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`worldId`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.cd_science
CREATE TABLE IF NOT EXISTS `cd_science` (
  `worldId` int(3) unsigned NOT NULL,
  `id` int(3) unsigned NOT NULL,
  `title` varchar(25) NOT NULL,
  `cost` int(10) unsigned NOT NULL,
  `need` varchar(50) NOT NULL,
  `takes` varchar(50) NOT NULL,
  `builds` varchar(255) NOT NULL,
  `units` varchar(50) NOT NULL,
  `bonuses` varchar(50) NOT NULL,
  PRIMARY KEY (`id`,`worldId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.cd_units
CREATE TABLE IF NOT EXISTS `cd_units` (
  `worldId` int(10) unsigned NOT NULL,
  `unitId` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `ability` int(10) unsigned NOT NULL,
  `airDamage` int(10) unsigned NOT NULL,
  `capacity` int(10) unsigned NOT NULL,
  `damage` int(10) unsigned NOT NULL,
  `unitGroup` tinyint(3) unsigned NOT NULL,
  `health` tinyint(3) unsigned NOT NULL,
  `popCost` tinyint(3) unsigned NOT NULL,
  `race` tinyint(3) unsigned NOT NULL,
  `target` tinyint(3) NOT NULL,
  `trainTime` int(10) unsigned NOT NULL,
  `unitType` tinyint(3) NOT NULL,
  `speed` tinyint(3) unsigned NOT NULL,
  `cost` varchar(255) NOT NULL DEFAULT '[]',
  `pay` varchar(255) NOT NULL DEFAULT '[]',
  PRIMARY KEY (`unitId`,`worldId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.invites
CREATE TABLE IF NOT EXISTS `invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invite` varchar(32) NOT NULL,
  `activated` int(11) NOT NULL DEFAULT '0',
  `from_user` int(11) DEFAULT NULL,
  `to_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `INVITE_UNIQUE` (`invite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `target` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(10) unsigned NOT NULL,
  `player_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `player_id` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_countries
CREATE TABLE IF NOT EXISTS `st_countries` (
  `worldId` smallint(5) unsigned NOT NULL,
  `countryId` smallint(5) unsigned NOT NULL,
  `countryTitle` varchar(25) DEFAULT NULL,
  `countryFlag` varchar(25) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `existsCountry` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`countryId`,`worldId`),
  KEY `existsCountry` (`existsCountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_countries_dyn
CREATE TABLE IF NOT EXISTS `st_countries_dyn` (
  `worldId` smallint(5) unsigned NOT NULL,
  `countryId` smallint(5) unsigned NOT NULL,
  `stateDate` date NOT NULL,
  `countryPop` int(10) unsigned NOT NULL DEFAULT '0',
  `countryPlayers` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `countryTowns` smallint(10) unsigned NOT NULL DEFAULT '0',
  `deltaPopPerDay` int(10) DEFAULT NULL,
  `deltaPopPerWeek` int(10) DEFAULT NULL,
  `deltaPopPerMonth` int(10) DEFAULT NULL,
  `deltaPlayersPerDay` tinyint(10) DEFAULT NULL,
  `deltaPlayersPerWeek` tinyint(10) DEFAULT NULL,
  `deltaPlayersPerMonth` tinyint(10) DEFAULT NULL,
  `deltaTownsPerDay` smallint(10) DEFAULT NULL,
  `deltaTownsPerWeek` smallint(10) DEFAULT NULL,
  `deltaTownsPerMonth` smallint(10) DEFAULT NULL,
  PRIMARY KEY (`worldId`,`countryId`,`stateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_events
CREATE TABLE IF NOT EXISTS `st_events` (
  `worldId` smallint(10) NOT NULL,
  `stateDate` date NOT NULL,
  `eventId` tinyint(10) NOT NULL,
  `playerId` int(10) DEFAULT NULL,
  `townId` int(10) DEFAULT NULL,
  `wonderId` int(10) DEFAULT NULL,
  `countryId` int(10) DEFAULT NULL,
  `countryIdOld` int(10) DEFAULT NULL,
  `playerName` varchar(25) DEFAULT NULL,
  `townTitle` varchar(25) DEFAULT NULL,
  `townTitleOld` varchar(25) DEFAULT NULL,
  `countryTitle` varchar(25) DEFAULT NULL,
  `countryTitleOld` varchar(25) DEFAULT NULL,
  `countryFlag` varchar(25) DEFAULT NULL,
  `countryFlagOld` varchar(25) DEFAULT NULL,
  KEY `stateDate` (`stateDate`),
  KEY `worldId` (`worldId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_players
CREATE TABLE IF NOT EXISTS `st_players` (
  `worldId` smallint(10) unsigned NOT NULL,
  `playerId` mediumint(11) unsigned NOT NULL,
  `playerName` varchar(25) NOT NULL,
  `playerRace` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `playerSex` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `countryId` smallint(11) unsigned NOT NULL DEFAULT '0',
  `created` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`playerId`,`worldId`),
  KEY `countryId` (`countryId`),
  KEY `existsPlayer` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_players_dyn
CREATE TABLE IF NOT EXISTS `st_players_dyn` (
  `worldId` smallint(10) unsigned NOT NULL,
  `playerId` mediumint(10) unsigned NOT NULL,
  `stateDate` date NOT NULL,
  `countryId` smallint(10) unsigned NOT NULL,
  `playerPop` mediumint(10) DEFAULT NULL,
  `playerTowns` tinyint(3) unsigned DEFAULT NULL,
  `playerRatingWar` int(10) DEFAULT NULL,
  `playerRatingScience` int(10) DEFAULT NULL,
  `playerRatingProd` int(10) DEFAULT NULL,
  `deltaPopPerDay` mediumint(10) DEFAULT NULL,
  `deltaPopPerWeek` mediumint(10) DEFAULT NULL,
  `deltaPopPerMonth` mediumint(10) DEFAULT NULL,
  `deltaTownsPerDay` tinyint(3) DEFAULT NULL,
  `deltaTownsPerWeek` tinyint(3) DEFAULT NULL,
  `deltaTownsPerMonth` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`worldId`,`playerId`,`stateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_towns
CREATE TABLE IF NOT EXISTS `st_towns` (
  `worldId` smallint(5) unsigned NOT NULL,
  `townId` mediumint(6) unsigned NOT NULL,
  `townTitle` varchar(50) DEFAULT NULL,
  `playerId` mediumint(6) unsigned NOT NULL,
  `createDate` date DEFAULT NULL,
  `lostDate` date DEFAULT NULL,
  `destroyDate` date DEFAULT NULL,
  `isLost` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isDestroy` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`townId`,`worldId`),
  KEY `playerId` (`playerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.st_towns_dyn
CREATE TABLE IF NOT EXISTS `st_towns_dyn` (
  `worldId` smallint(5) unsigned NOT NULL,
  `stateDate` date NOT NULL,
  `townId` mediumint(6) unsigned NOT NULL,
  `townPop` mediumint(10) DEFAULT NULL,
  `playerId` mediumint(10) unsigned NOT NULL DEFAULT '0',
  `deltaPerDay` mediumint(10) DEFAULT NULL,
  `deltaPerWeek` mediumint(10) DEFAULT NULL,
  `deltaPerMonth` mediumint(10) DEFAULT NULL,
  `wonderId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `wonderLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`worldId`,`stateDate`,`townId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.supply
CREATE TABLE IF NOT EXISTS `supply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stream` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `player_id` int(10) unsigned NOT NULL,
  `town_id` int(10) unsigned NOT NULL,
  `target` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_open` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `r1` int(3) unsigned NOT NULL DEFAULT '0',
  `r2` int(3) unsigned NOT NULL DEFAULT '0',
  `r3` int(3) unsigned NOT NULL DEFAULT '0',
  `r4` int(3) unsigned NOT NULL DEFAULT '0',
  `r5` int(3) unsigned NOT NULL DEFAULT '0',
  `r6` int(3) unsigned NOT NULL DEFAULT '0',
  `r7` int(3) unsigned NOT NULL DEFAULT '0',
  `r8` int(3) unsigned NOT NULL DEFAULT '0',
  `r9` int(3) unsigned NOT NULL DEFAULT '0',
  `r10` int(3) unsigned NOT NULL DEFAULT '0',
  `r11` int(3) unsigned NOT NULL DEFAULT '0',
  `r12` int(3) unsigned NOT NULL DEFAULT '0',
  `r13` int(3) unsigned NOT NULL DEFAULT '0',
  `r14` int(3) unsigned NOT NULL DEFAULT '0',
  `r15` int(3) unsigned NOT NULL DEFAULT '0',
  `r16` int(3) unsigned NOT NULL DEFAULT '0',
  `r17` int(3) unsigned NOT NULL DEFAULT '0',
  `r18` int(3) unsigned NOT NULL DEFAULT '0',
  `r19` int(3) unsigned NOT NULL DEFAULT '0',
  `r20` int(3) unsigned NOT NULL DEFAULT '0',
  `r21` int(3) unsigned NOT NULL DEFAULT '0',
  `r22` int(3) unsigned NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.tiles
CREATE TABLE IF NOT EXISTS `tiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `climate` int(11) NOT NULL,
  `relief` int(11) NOT NULL,
  `deposit` int(11) NOT NULL,
  `wonder` int(11) NOT NULL,
  `ecology` int(11) NOT NULL,
  `rid` int(11) unsigned NOT NULL,
  `road` int(11) unsigned NOT NULL,
  `town_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `diplomacy` int(11) NOT NULL,
  `town_level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `KEY_RID` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastVisit` timestamp NULL DEFAULT NULL,
  `sex` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `group` int(11) NOT NULL DEFAULT '0',
  `rights` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`,`email`),
  UNIQUE KEY `uid` (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.wt_armylog
CREATE TABLE IF NOT EXISTS `wt_armylog` (
  `id` varchar(32) NOT NULL,
  `world_country` enum('EN','RU') NOT NULL,
  `world_id` int(10) unsigned NOT NULL,
  `generate_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `report_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `options` varchar(50) DEFAULT '[]',
  `verified` int(1) DEFAULT NULL,
  `log_data` text NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `world_country_world_id` (`world_country`,`world_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.wt_reports_science
CREATE TABLE IF NOT EXISTS `wt_reports_science` (
  `id` varchar(50) NOT NULL,
  `worldId` smallint(5) DEFAULT NULL,
  `worldSign` varchar(4) DEFAULT NULL,
  `worldTitle` varchar(25) DEFAULT NULL,
  `generateTime` datetime DEFAULT NULL,
  `domain` varchar(50) DEFAULT NULL,
  `playerId` int(11) DEFAULT NULL,
  `playerTitle` varchar(25) DEFAULT NULL,
  `countryId` int(11) DEFAULT NULL,
  `countryTitle` varchar(25) DEFAULT NULL,
  `countryFlag` varchar(50) DEFAULT NULL,
  `scCurrent` varchar(255) DEFAULT NULL,
  `scKnown` varchar(500) DEFAULT NULL,
  `scStarted` varchar(500) DEFAULT NULL,
  `scNext` int(11) DEFAULT NULL,
  `investedTotal` int(10) DEFAULT NULL,
  `markupTotal` int(10) DEFAULT NULL,
  `markupNext` int(10) DEFAULT NULL,
  `countTotal` int(10) DEFAULT NULL,
  `validKey` varchar(32) DEFAULT NULL,
  `verified` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `playerId` (`playerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.wt_reports_war
CREATE TABLE IF NOT EXISTS `wt_reports_war` (
  `id` varchar(32) NOT NULL,
  `worldId` smallint(5) DEFAULT NULL,
  `worldSign` varchar(4) DEFAULT NULL,
  `worldTitle` varchar(25) DEFAULT NULL,
  `generateTime` datetime DEFAULT NULL,
  `reportTime` datetime DEFAULT NULL,
  `domain` varchar(50) DEFAULT NULL,
  `owner` int(10) unsigned DEFAULT NULL,
  `spy` tinyint(3) unsigned DEFAULT NULL,
  `aggressorId` int(10) unsigned DEFAULT NULL,
  `aggressorTownId` int(10) unsigned DEFAULT NULL,
  `aggressorCountryId` int(10) unsigned DEFAULT NULL,
  `defendingId` int(10) unsigned DEFAULT NULL,
  `defendingTownId` int(10) unsigned DEFAULT NULL,
  `defendingCountryId` int(10) unsigned DEFAULT NULL,
  `private` tinyint(3) unsigned DEFAULT NULL,
  `validKey` varchar(32) DEFAULT NULL,
  `verified` tinyint(3) unsigned DEFAULT '0',
  `report` text,
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`),
  KEY `generateTime` (`generateTime`),
  KEY `worldId` (`worldId`),
  KEY `private` (`private`),
  KEY `verified` (`verified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table delphinpro_wt.wt_worlds
CREATE TABLE IF NOT EXISTS `wt_worlds` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `sign` varchar(5) NOT NULL,
  `started` datetime DEFAULT NULL,
  `canReg` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `statistic` tinyint(1) DEFAULT '0',
  `loadStat` datetime DEFAULT NULL,
  `updateStat` datetime DEFAULT NULL,
  `updateConst` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
