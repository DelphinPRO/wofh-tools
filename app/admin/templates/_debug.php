<?php
/**
 * @since              17.08.13 00:23
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 * @var df\View $this
 */
?>
<? if (DEBUG) { ?>
	<br>
	<div class="panel-gro-up" id="accordion-Debug">
		<? if ($this->is('e')) { ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" data-p-arent="#accordionDebug" href="#collapseThree"> Исключение:
						<code><?= $this->e->getMessage() ?></code> </a>
				</div>
				<div id="collapseThree" class="panel-collapse collapse">
					<div class="panel-body">
						<?= $this->e ?>
					</div>
				</div>
			</div>
		<? } ?>

		<? if ($_ERROR_STACK) { ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" data-p-arent="#accordionDebug" href="#collapseTwo">Errors list</a>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse in">
					<div class="panel-body">
						<? foreach ($_ERROR_STACK as $errorMessage) { ?>
							<?= $errorMessage ?>
						<? } ?>
					</div>
				</div>
			</div>
		<? } ?>

		<div class="panel panel-default">
			<div class="panel-heading">
				<a data-toggle="collapse" data-p-arent="#accordionDebug" href="#collapseOne">Debug info</a>
			</div>
			<div id="collapseOne" class="panel-collapse collapse">
				<div class="panel-body">
					<? pre($_SESSION, '$_SESSION', 0); ?>
					<? pre($this->router, 'Router', 0); ?>
					<? pre($_GET, '$_GET', 0); ?>
					<? pre($_POST, '$_POST', 0); ?>
				</div>
			</div>
		</div>
	</div>
<? } ?>
