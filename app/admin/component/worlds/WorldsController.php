<?php
/**
 * @since              06.09.13 17:54
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\admin\components\worlds;

use df\Controller;

class WorldsController extends Controller
{
	public function display()
	{
		$this->view->set('title', 'Подключенные миры');
		$user = get_user();
		if (!$user || $user->id != 1) {
			$this->view->setLayout('login');
		}

		$worlds = glob(_dir('/config/config_*.ini'));

		$this->view->set('worlds', $worlds);
		$this->view->set('db_host', $this->config->get('db.host'));
		$this->view->set('db_user', $this->config->get('db.user'));
	}

	public function create()
	{
		$user = get_user();
		if (!$user || $user->id != 1) {
			$this->redirect('/admin/');
			return;
		}

		$worldNumber = (int)$_POST['worldNumber'];
		$dbHost      = $_POST['db_host'];
		$dbName      = $_POST['db_name'];
		$dbUser      = $_POST['db_user'];
		$dbPass      = $_POST['db_pass'];

		$ini = "\n"
			. "[db]\n"
			. "host = $dbHost\n"
			. "name = $dbName\n"
			. "user = $dbUser\n"
			. "pass = $dbPass\n"
			. "";

		$db = new \PDO( "mysql:dbname=$dbName;host=$dbHost", $dbUser, $dbPass );
		$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$db->query("SET NAMES 'UTF8'");

		$db->query("
			CREATE TABLE wt_map (
				id         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				x          INT(11) NOT NULL,
				y          INT(11) NOT NULL,
				climate    INT(11) NOT NULL,
				relief     INT(11) NOT NULL,
				deposit    INT(11) NOT NULL,
				wonder     INT(11) NOT NULL,
				ecology    INT(11) NOT NULL,
				rid        INT(11) UNSIGNED NOT NULL,
				road       INT(11) UNSIGNED NOT NULL,
				town_id    INT(11) NOT NULL,
				player_id  INT(11) NOT NULL,
				country_id INT(11) NOT NULL,
				diplomacy  INT(11) NOT NULL,
				town_level INT(11) NOT NULL,
				PRIMARY KEY (id)
			)
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB
			AUTO_INCREMENT=1;
		");
		$db->query("
			CREATE TABLE wt_players (
				player_id         INT(10) UNSIGNED NOT NULL,
				player_country_id INT(10) UNSIGNED NULL DEFAULT NULL,
				player_title      VARCHAR(255) NULL DEFAULT NULL,
				UNIQUE INDEX player_id (player_id),
				INDEX player_country_id (player_country_id)
			)
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;
		");
		$db->query("
			CREATE TABLE wt_towns (
				town_id    INT(10) UNSIGNED NOT NULL,
				player_id  INT(10) UNSIGNED NOT NULL DEFAULT '0',
				country_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
				town_level INT(10) UNSIGNED NOT NULL DEFAULT '0',
				town_title VARCHAR(255) NULL DEFAULT NULL,
				deposit INT(10) NOT NULL DEFAULT '-1',
				UNIQUE INDEX town_id (town_id),
				INDEX player_id (player_id),
				INDEX country_id (country_id)
			)
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;
		");
		$db->query("
			CREATE TABLE wt_counties (
				country_id    INT(10) UNSIGNED NOT NULL,
				country_title VARCHAR(50) NULL DEFAULT NULL,
				UNIQUE INDEX country_id (country_id)
			)
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;
		");

		file_put_contents(_dir("/config/config_$worldNumber.ini"), $ini);

		$this->redirect('/admin/worlds/');
	}
}