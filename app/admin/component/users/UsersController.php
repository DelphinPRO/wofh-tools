<?php
/**
 * @since              19.08.13 21:07
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\admin\components\users;

use df\Controller;

class UsersController extends Controller
{
	public function display()
	{
		if ($this->user->id != 1) {
			$this->redirect('/admin/');
			return;
		}
		$this->set('title', 'Пользователи');
		$this->set('users', $this->_getUsers());
	}

	public function invites()
	{
		if ($this->user->id != 1) {
			$this->redirect('/admin/');
			return;
		}

		$this->set('title', 'Инвайты');
		$this->set('invites', $this->_getInvites());
	}

	public function generate()
	{
		if ($this->user->id != 1) {
			$this->redirect('/admin/');
			return;
		}

		if ($this->router->isPost()) {
			$q     = array();
			$query = 'INSERT INTO invites (invite, from_user, to_user) VALUES';
			for ($i = 0; $i < 10; $i++) {
				$invite = md5(uniqid('', true));
				$q[] .= "\n('$invite', 1, 0)";
			}
			$query .= join(',', $q);
			$this->db->query($query);
		}
		$this->redirect('/admin/users/invites/');
	}

	private function _getInvites()
	{
		return $this->db->select("
			SELECT invites.*
			FROM invites
			WHERE activated = 0
		")->fetchAll();
	}

	private function _getUsers()
	{
		return $this->db->select("SELECT *, UNIX_TIMESTAMP(register) AS register FROM users")->fetchAll();
	}
}
