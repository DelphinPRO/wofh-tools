<?php
/**
 * @since              19.08.13 21:10
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

return array(
	"users"         => array(
		"component"  => "users",
		"controller" => "users",
		"action"     => "display"
	),
	"users/invites" => array(
		"component"  => "users",
		"controller" => "users",
		"action"     => "invites"
	),
);
