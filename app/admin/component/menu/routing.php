<?php
/**
 * @since              16.08.13 06:18
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

return array(
	"menu"                          => array(
		"component"  => "menu",
		"controller" => "menu",
		"action"     => "display"
	),

	"menu/edit/(?'id'\\d+)"         => array(
		"component"  => "menu",
		"controller" => "menu",
		"action"     => "edit"
	),

	"menu/item"                     => array(
		"component"  => "menu",
		"controller" => "items",
		"action"     => "display"
	),

	"menu/item/create"              => array(
		"component"  => "menu",
		"controller" => "items",
		"action"     => "create"
	),

	"menu/item/getComponentLayouts" => array(
		"component"  => "menu",
		"controller" => "items",
		"action"     => "getComponentLayouts"
	),
);
