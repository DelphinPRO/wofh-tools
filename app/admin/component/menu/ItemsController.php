<?php
/**
 * @since              16.08.13 06:20
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

namespace df\admin\components\menu;

use df\Controller;

class ItemsController extends Controller
{
	public function create()
	{
		$user = get_user();
		if (!$user || $user->id != 1) {
			$this->redirect('/admin/');
			return;
		}

		$this->view->set('title', 'Создать пункт меню');
		$this->view->setLayout('edit');

		$item = (object)array(
			'id'    => 0,
			'title' => ''
		);

		$this->view->set('menuItem', $item);
		$this->view->set('component', $this->_getComponents());
	}

	public function save()
	{
		$user = get_user();
		if (!$user || $user->id != 1) {
			$this->redirect('/admin/');
			return;
		}

		$id = isset( $_POST['id'] ) ? (int)$_POST['id'] : 0;
		if ($id == 0) {
			$this->db
				->prepare("
					INSERT INTO menu_items
					SET
						menu_id = :menuId,
						title   = :title,
						target  = :target,
						alias   = :alias
					")
				->execute(array(
						'menuId' => 1,
						'title'  => $_POST['title'],
						'target' => $_POST['target'],
						'alias'  => time(),
					)
				);
			$this->redirect('/admin/menu/edit/1/');
		}
	}

	private function _getComponents()
	{
		$files  = glob(_dir('app/component/*'), GLOB_ONLYDIR);
		$result = array();
		foreach ($files as $file) {
			$name  = basename($file);
			$views = glob($file . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . '*.phtml');
			foreach ($views as &$view) {
				$view = str_replace('com_', '', pathinfo($view, PATHINFO_FILENAME));
			}
			$result[$name] = $views;
		}
		return $result;
	}
}
