<?php
/**
 * @since             22.01.14 20:17
 * @package           DFramework
 * @author            DelphinPRO delphinpro@yandex.ru
 * @copyright         Copyright (C) 2014 DelphinPRO. All rights reserved.
 * @license           Licensed under the MIT license
 */
?>
<!DOCTYPE html>
<html>
<head>
	<title>DFramework / Installation</title>
	<!-- Bootstrap -->
	<link href="/css/bootstrap/bootstrap.css" rel="stylesheet" media="screen">
	<link href="/css/install.css" rel="stylesheet" media="screen">

	<script src="/system/js/jquery-1.10.2.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
</head>
<body>

<div class="wrapper" id="wrapper">
	<div class="container">

		<h1 class="page-header">DFramework / Installation</h1>

		<form action="/" method="post" class="form-horizontal">
			<fieldset>
				<legend>Database</legend>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="db-host">Host:</label>
					<div class="col-sm-4">
						<input id="db-host" class="form-control" type="text" name="db_host">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="db-user">User:</label>
					<div class="col-sm-4">
						<input id="db-user" class="form-control" type="text" name="db_user">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="db-pass">Pass:</label>
					<div class="col-sm-4">
						<input id="db-pass" class="form-control" type="text" name="db_pass">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="db-name">Name:</label>
					<div class="col-sm-4">
						<input id="db-name" class="form-control" type="text" name="db_name">
					</div>
				</div>
			</fieldset>
		</form>

	</div>
</div>

<div class="footer">
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-sm-4">
						<div>© DFramework, v2.0, <a href="http://delphinpro.ru" target="_blank">DelphinPRO</a>,
							2013-<?= date('Y') ?> гг.
						</div>
					</div>
					<div class="col-sm-4">
					</div>
					<div class="col-sm-4 text-right">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>