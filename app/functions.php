<?php

/**
 * Набор вспомогательных функций
 *
 * @since              07.01.13 16:13
 * @package            DFramework
 * @author             DelphinPRO delphinpro@yandex.ru
 * @copyright          Copyright (C) 2013-2014 DelphinPRO. All rights reserved.
 * @license            Licensed under the MIT license
 */

use df\exception\DFException;
use df\helper\FS;

include 'wt.php'; // TODO include 'wt.php'

// Функции

/** @deprecated */
function q($query, $sign)
{
  return str_replace('ruXX', strtolower($sign), $query);
}

function loadSql($file, $sign = null)
{
  $filename = FS::path('/app/df/sql/' . $file . '.sql');
  if (file_exists($filename)) {
    $sql = file_get_contents($filename);
    if ($sign) {
      $sql = q($sql, $sign);
    }
    return $sql;
  }
  throw new DFException('SQL file not found: ' . $file);
}

/**
 * @param $image
 * @return string
 * @deprecated
 */
function svg_image($image)
{
  return file_get_contents(FS::path('/public_html/img/svg/' . $image . '.svg'));
}

function get_execution_time()
{
  global $_PROFILER_TIME_START;

  $time = microtime(true) - $_PROFILER_TIME_START;
  return round($time, 3);
}

function numFormat($number)
{
  return number_format($number, 0, '.', ' '); //&#8201; узкий пробел
}

function pluralForm($n, $forms)
{
  return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
}

function getMonth($number, $lang = 'RU', $form = 0)
{
  $months = array(
      'RU' => array(
          array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря')
      )
  );
  return $months[$lang][$form][(int)$number];
}

function svgIcon($id, $class = '')
{
  return '<svg class="i i-' . $id . ' ' . $class . '"><use xlink:href="/static/svg-sprite.svg#' . $id . '"></use></svg>';
}
